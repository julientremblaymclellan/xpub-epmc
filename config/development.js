const { deferConfig } = require('config/defer')

module.exports = {
  'pubsweet-server': {
    baseUrl: deferConfig(
      cfg => `http://localhost:${cfg['pubsweet-server'].port}`,
    ),
    secret: 'secret-string',
  },
  dbManager: {
    username: 'admin',
    password: 'password',
    email: 'admin@example.com',
    admin: true,
  },
  ftp_directory: 'ftpdata',
  ncbiPdfTransformerApi: {
    url: 'https://api.ncbi.nlm.nih.gov/lit/jats2pdf/api',
    freq: 120000,
  },
  users: [],
  ftp_tagger: {
    username: 'taggers',
    email: '',
  },
  ncbiFileTypes: [
    {
      value: 'supplement',
      numeric: 6,
    },
    {
      value: 'PMC',
      numeric: 8,
    },
    {
      value: 'IMGsmall',
      numeric: 9,
    },
    {
      value: 'IMGview',
      numeric: 10,
    },
    {
      value: 'IMGprint',
      numeric: 11,
    },
    {
      value: 'supplement_tag',
      numeric: 21,
    },
    {
      value: 'pdf4load',
      numeric: 33,
    },
  ],
  ncbiInitialState: 'ncbi-ready',
  ncbiSentState: 'ncbi-sent',
  ncbiFailedState: 'ncbi-failed',
}
