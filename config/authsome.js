const states = require('./states.json')

const authorisationMap = {
  create: {
    User: {
      roles: ['admin'],
    },
    Identity: {
      roles: ['all'],
    },
    Team: {
      roles: ['admin'],
    },
    Manuscript: {
      roles: ['all'],
    },
  },
  read: {
    User: {
      roles: ['admin'],
    },
    Identity: {
      roles: ['admin', 'owner'],
    },
    Team: {
      roles: ['admin'],
    },
    Manuscript: {
      roles: {
        admin: states,
        submitter: states.slice(0, states.length - 1),
        reviewer: states.slice(0, states.length - 1),
        tagger: ['tagging'],
        'external-admin': ['xml-qa'],
      },
      entityAuthorisationAttribute: 'status',
    },
  },
  update: {
    User: {
      roles: ['admin', 'owner'],
    },
    Identity: {
      roles: ['owner'],
    },
    Team: {
      roles: ['admin'],
    },
    Manuscript: {
      roles: {
        admin: states,
        submitter: ['INITIAL', 'READY', 'submission-error'],
        reviewer: ['in-review', 'xml-review'],
        tagger: ['tagging'],
        'external-admin': ['xml-qa'],
      },
      entityAuthorisationAttribute: 'status',
    },
  },
  delete: {
    User: {
      roles: ['admin'],
    },
    Identity: {
      roles: ['admin'],
    },
    Team: {
      roles: ['admin'],
    },
    Manuscript: {
      roles: {
        admin: states,
        submitter: states,
        reviewer: [],
        tagger: [],
        'external-admin': [],
      },
      entityAuthorisationAttribute: 'status',
    },
  },
}
const fetchUserRoles = async (userId, context, object) => {
  const manId = object.type === 'Manuscript' ? object.id : null
  const userRoles = await context.models.EpmcTeam.selectByUserIdOrManuscriptId(
    userId,
    manId,
  )
  return userRoles.map(row => row.roleName)
}
const isOwner = (userId, object, context) => {
  // checks if user owns an object, the only case we have now is identity
  if (object.type === 'Identity' && object.userId === userId) {
    return true
  }
  return false
}
// checks if user is member of some entity, no use cases for this aroused yet
const isMember = (user, team) => false

const atLeastOneCommon = (arr1, arr2) => {
  for (let i = 0; i < arr1.length; i += 1) {
    if (arr2.includes(arr1[i])) {
      return true
    }
  }
  return false
}

const getAuthorisedEntity = async (object, context) => {
  // currently files, annotations and reviews don't have their own rules, they depend on manuscripts.
  let manuscript = {}
  if (object.type === 'File' || object.type === 'Review') {
    if (!object.manuscriptId) {
      const entity = await context.models[object.type].selectById(object.id)
      object.manuscriptId = entity.manuscriptId
    }
    manuscript = await context.models.Manuscript.selectById(object.manuscriptId)
    object = manuscript
    object.type = 'Manuscript'
  }
  if (object.type === 'Annotation') {
    if (!object.reviewId) {
      const entity = await context.models[object.type].selectById(object.id)
      object.reviewId = entity.reviewId
    }
    const review = await context.models.Review.selectById(object.reviewId)
    const manuscript = await context.models.Manuscript.selectById(
      review.manuscriptId,
    )
    object = manuscript
    object.type = 'Manuscript'
  }
  return object
}

// authorisation based on roles
const basicAuthorisation = async (userId, operation, object, context) => {
  object = await getAuthorisedEntity(object, context)
  const objectAuthorisation = authorisationMap[operation][object.type]
  const objectOperationRoles = objectAuthorisation.roles
  const userRoles = await fetchUserRoles(userId, context, object)
  let basicAuthorised = false
  if (Array.isArray(objectOperationRoles)) {
    basicAuthorised = atLeastOneCommon(userRoles, objectOperationRoles)
  } else {
    for (let i = 0; i < userRoles.length; i += 1) {
      if (
        objectOperationRoles[userRoles[i]] &&
        objectOperationRoles[userRoles[i]].includes(
          object[objectAuthorisation.entityAuthorisationAttribute],
        )
      ) {
        basicAuthorised = true
      }
    }
  }
  return { basicAuthorised, userRoles }
}
// authorisation on non-role relation like membership or ownership
const advancedAuthorisation = async (userId, operation, object, context) => {
  let owner = false
  let member = false
  const type = ['File', 'Review', 'Annotation'].includes(object.type)
    ? 'Manuscript'
    : object.type
  const objectOperationRoles = authorisationMap[operation][type].roles
  if (
    Array.isArray(objectOperationRoles) &&
    objectOperationRoles.includes('owner')
  ) {
    owner = isOwner(userId, object, context)
  }
  if (
    Array.isArray(objectOperationRoles) &&
    objectOperationRoles.includes('member')
  ) {
    // check membership
    member = isMember(userId, object, context)
  }
  return owner || member
}

const isAuthorised = async (userId, operation, object, context) => {
  const { basicAuthorised } = await basicAuthorisation(
    userId,
    operation,
    object,
    context,
  )
  if (basicAuthorised) {
    return true
  }
  // check ownership
  return advancedAuthorisation(userId, operation, object, context)
}

// the arguments are expected to be
// userId: user uuid
// operation: create or read or update or delete. Also custom operation type can be added, if it can't fit into the previous generic one
// object: {
//   type: the entity name whether file, manuscript, team, ..etc
//   id: the id of the entity, this can be null in case of select all or create for example,
// ..attrs
// }
// context: this is the injected context in authsome, don't need to worry about
module.exports = {
  // This runs before all other authorisation queries and is used here
  // to allow admin to do everything
  before: async (userId, operation, object, context) => {
    const type = ['File', 'Review', 'Annotation'].includes(object.type)
      ? 'Manuscript'
      : object.type
    const objectOperationRoles = authorisationMap[operation][type].roles
    if (
      Array.isArray(objectOperationRoles) &&
      objectOperationRoles.includes('all')
    ) {
      return true
    }
    const userRoles = await fetchUserRoles(userId, context, object)
    return userRoles.includes('admin')
  },
  create: async (userId, operation, object, context) => {
    const { basicAuthorised } = await basicAuthorisation(
      userId,
      operation,
      object,
      context,
    )
    return basicAuthorised
  },
  read: async (userId, operation, object, context) => {
    const isAuthorized = await isAuthorised(userId, operation, object, context)
    return isAuthorized
  },
  update: async (userId, operation, object, context) => {
    const isAuthorized = await isAuthorised(userId, operation, object, context)
    return isAuthorized
  },
  delete: async (userId, operation, object, context) => {
    const isAuthorized = await isAuthorised(userId, operation, object, context)
    return isAuthorized
  },
}
