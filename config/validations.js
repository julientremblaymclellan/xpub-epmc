const Joi = require('joi')

module.exports = {
  collection: {
    // project
    collectionType: Joi.string(),
    created: Joi.date(),
    title: Joi.string(),
    status: Joi.string(),
    reviewers: Joi.array(),
    emsid: Joi.number(),
  },
  fragment: [
    {
      fragmentType: Joi.valid('version').required(),
      created: Joi.date(),
      version: Joi.number(),
      submitted: Joi.date(),
      source: Joi.string(), // TODO: move to a file
      metadata: Joi.object({
        title: Joi.string(),
        abstract: Joi.string(),
        articleType: Joi.string(),
        articleSection: Joi.array().items(Joi.string()),
        authors: Joi.array(),
        keywords: Joi.array(),
        articleId: Joi.array().items(
          Joi.object({
            type: Joi.string(),
            id: Joi.string(),
          }),
        ),
        volume: Joi.string(),
        issue: Joi.string(),
        fpage: Joi.string(),
        lpage: Joi.string(),
        elocationId: Joi.string(),
        fundingGroup: Joi.array().items(
          Joi.object({
            fundingSource: Joi.string(),
            awardId: Joi.string(),
            title: Joi.string(),
            principalInvestigator: Joi.object({
              surname: Joi.string(),
              givenNames: Joi.string(),
              prefix: Joi.string(),
              email: Joi.string().email({ minDomainAtoms: 2 }),
            }),
          }),
        ),
        publicationDate: Joi.array().items(
          Joi.object({
            type: Joi.string(),
            day: Joi.number(),
            month: Joi.number(),
            year: Joi.number(),
            stringDate: Joi.string(),
          }),
        ),
        customMeta: Joi.object({
          unmatchedJournal: Joi.string(),
          releaseDelay: Joi.number(),
        }),
        journalMeta: Joi.object({
          nlmuniqueid: Joi.string(),
          title: Joi.string(),
          pubmedStatus: Joi.boolean(),
        }),
        notes: Joi.array().items(
          Joi.object({
            notesType: Joi.string(),
            content: Joi.string(),
          }),
        ),
      }),
      declarations: Joi.object().unknown(),
      suggestions: Joi.object({
        reviewers: Joi.object({
          suggested: Joi.array().items(
            Joi.object({
              name: Joi.string(),
              email: Joi.string().email({ minDomainAtoms: 2 }),
              id: Joi.string(),
            }),
          ),
          opposed: Joi.array().items(Joi.string().allow('')),
        }),
        editors: Joi.object({
          suggested: Joi.array().items(Joi.string().allow('')),
          opposed: Joi.array().items(Joi.string().allow('')),
        }),
      }),
      files: Joi.array().items(
        Joi.object({
          type: Joi.string(),
          label: Joi.string(),
          filename: Joi.string().required(),
          url: Joi.string().required(),
          mimeType: Joi.string().required(),
          size: Joi.number(),
        }),
      ),
      reviewers: Joi.array(),
      lock: Joi.object(),
      decision: Joi.object(),
    },
  ],
  user: {
    name: Joi.string(), // TODO: add "name" to the login form
    roles: Joi.object(),
  },
  team: {
    group: Joi.string(),
  },
}
