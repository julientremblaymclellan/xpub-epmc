if (process.env.NODE_ENV !== 'production') {
  require('dotenv').load()
}

const path = require('path')
const components = require('./components.json')
const states = require('./states.json')
const logger = require('winston')
require('winston-daily-rotate-file')
const gql = require('graphql-tag')

logger.add(logger.transports.DailyRotateFile, {
  dirname: './logs',
  filename: 'xpub-epmc.log',
  datePattern: 'YYYY-MM-DD',
  zippedArchive: true,
  maxFiles: '30d',
})

module.exports = {
  schema: {},
  authsome: {
    mode: path.resolve(__dirname, 'authsome.js'),
    teams: {
      admin: {
        name: 'admin',
      },
      submitter: {
        name: 'submitter',
      },
      reviewer: {
        name: 'reviewer',
      },
    },
  },
  validations: path.resolve(__dirname, 'validations.js'),
  pubsweet: {
    components,
  },
  mailer: {
    from: 'helpdesk@europepmc.org',
    path: `${__dirname}/mailer`,
    transport: {
      sendmail: true,
    },
  },
  logger,
  'pubsweet-server': {
    db: {
      port: 5432,
    },
    port: 3000,
    logger,
    uploads: 'uploads',
    graphiql: true,
    enableExperimentalGraphql: true,
    typeDefs: `
      extend type User {
        name: String
      }

      extend type Team {
        group: String
      }

      extend type Collection {
        collectionType: String
        created: String
        title: String
        status: String
        reviewers: [CollectionReviewer]
      }

      type CollectionReviewer {
        id: String!
        user: String!
      }

      extend type Fragment {
        created: String
        version: Int
        submitted: String
        source: String
        metadata: VersionMetadata
        declarations: VersionDeclaration
        suggestions: VersionSuggestionGroup
        files: VersionFileGroup
        notes: VersionNotes
        reviewers: [ReviewerMeta]
        # TODO
        #lock: VersionLock
        #decision: VersionDecision
      }
      type VersionMetadata {
        title: String
        abstract: String
        articleType: String
        articleSection: [String]
        authors: [String]
        keywords: [String]
      }
      type VersionDeclaration {
        #TODO make these boolean?
        openData: String
        previouslySubmitted: String
        openPeerReview: String
        streamlinedReview: String
        researchNexus: String
        preregistered: String
      }
      type VersionSuggestionGroup {
        reviewers: VersionSuggestions
        editors: VersionSuggestions
      }
      type VersionSuggestions {
        suggested: [String]
        opposed: [String]
      }
      type VersionFileGroup {
        manuscript: VersionFile
        supplementary: [VersionFile]
      }
      type VersionFile {
        name: String!
        type: String
        size: Int
        url: String
      }
      type VersionNotes {
        fundingAcknowledgement: String
        specialInstructions: String
      }
      type ReviewerMeta {
        id: String
        reviewer: String
        status: String
        _reviewer: CollectionReviewer
        _user: User
      }
    `,
  },
  'pubsweet-client': {
    API_ENDPOINT: '/api',
    'login-redirect': '/',
    'redux-log': false,
    'password-reset': {
      graphql: {
        mutations: {
          emailPasswordResetLink: gql`
            mutation($email: String!) {
              epmc_emailPasswordResetLink(email: $email)
            }
          `,
          resetPassword: gql`
            mutation($token: String!, $newPassword: String!) {
              epmc_resetPassword(token: $token, newPassword: $newPassword)
            }
          `,
        },
      },
      appName: 'Europe PMC Plus',
    },
  },
  'epmc-email': {
    url: process.env.PUBSWEET_URL || 'http://localhost:3000/',
    sender: process.env.PUBSWEET_SENDER || 'helpdesk@europepmc.org',
    testAddress: process.env.PUBSWEET_TEST_EMAIL || 'plusdev@ebi.ac.uk',
    // remove PUBSWEET_TEST_EMAIL and change 'plusdev@ebi.ac.uk' to blank string for production
    system: process.env.PUBSWEET_SYSTEM_EMAIL || 'plusadmin@ebi.ac.uk',
  },
  'pubsweet-component-ink-backend': {
    inkEndpoint:
      process.env.INK_ENDPOINT || 'http://inkdemo-api.coko.foundation/',
    email: process.env.INK_USERNAME,
    password: process.env.INK_PASSWORD,
    maxRetries: 500,
    recipes: {
      'editoria-typescript': '2',
    },
  },
  'pubsweet-component-aws-s3': {
    secretAccessKey: process.env.AWS_S3_SECRET_KEY,
    accessKeyId: process.env.AWS_S3_ACCESS_KEY,
    region: process.env.AWS_S3_REGION,
    bucket: process.env.AWS_S3_BUCKET,
    validations: path.resolve(__dirname, 'upload-validations.js'),
  },
  'pubsweet-component-minio': {
    accessKeyId: process.env.MINIO_ACCESS_KEY,
    secretAccessKey: process.env.MINIO_SECRET_KEY,
    endPoint: process.env.MINIO_ENDPOINT,
    port: process.env.MINIO_PORT,
    secure: process.env.MINIO_SECURITY === 'true',
    bucket: process.env.MINIO_BUCKET,
    uploads: process.env.MINIO_UPLOADS_FOLDER_NAME,
  },
  publicKeys: [
    'pubsweet-client',
    'authsome',
    'validations',
    'file',
    'pageSize',
    'states',
  ],
  // do we need this elife variable?
  elife: {
    api: {
      url: 'localhost',
    },
  },
  login: {
    enableMock: false,
  },
  // do we need the stuff up to the previous comment?
  organization: {
    europepmc_plus: {
      name: 'Europe PMC Plus',
    },
  },
  manuscript: {
    status: {
      initial: 'INITIAL',
    },
  },
  states,
  user: {
    identity: {
      default: 'local',
      local: 'local',
    },
  },
  file: {
    type: {
      manuscript: 'manuscript',
    },
    url: {
      download: '/download',
    },
  },
  pageSize: process.env.PAGE_SIZE || 50,
}
