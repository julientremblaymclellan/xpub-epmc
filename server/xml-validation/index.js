const path = require('path')
const mime = require('mime-types')
const libxml = require('libxmljs')
const xslt4node = require('xslt4node')
const fs = require('fs')
const uuidv4 = require('uuid/v4')
const tar = require('../utils/unTar.js')
const getUser = require('../utils/user.js')
const files = require('../utils/files.js')
const db = require('../utils/db.js')
const logger = require('@pubsweet/logger')
const Manuscript = require('../xpub-model/entities/manuscript/data-access')
const File = require('../xpub-model/entities/file/data-access')
const config = require('config')

const { url } = config['epmc-email']

xslt4node.addLibrary('./saxon9he.jar')
xslt4node.addOptions('-Xmx1g')

// pushXML(`/download/7742e570-3458-11e9-9868-95e725ed2ade.xml`, 'EMS90000')

module.exports.pushXML = async function pushXML(fileUrl, manuscriptId) {
  try {
    const downloadUrl = url + fileUrl.replace(/^\/+/g, '') // remove leading slash
    const xml = await files.fetchFile(downloadUrl)
    const xsdBlue = await files.readData(
      path.resolve(__dirname, 'xsl/xsd/publishing/journalpublishing3.xsd'),
    )
    const xsdGreen = await files.readData(
      path.resolve(__dirname, 'xsl/xsd/archiving/archivearticle3.xsd'),
    )

    const xmlIsWellformed = libxml.parseXml(xml)

    const baseUrlB = path.resolve(__dirname, 'xsl/xsd/publishing/')
    const xsdBlueDoc = libxml.parseXml(xsdBlue, {
      baseUrl: `${baseUrlB}/`,
    })

    const baseUrlG = path.resolve(__dirname, 'xsl/xsd/archiving/')
    const xsdGreenDoc = libxml.parseXml(xsdGreen, {
      baseUrl: `${baseUrlG}/`,
    })

    const xmlIsValid = xmlIsWellformed.validate(xsdBlueDoc)

    const errors = xmlIsWellformed.validationErrors
    if (!xmlIsValid) {
      let errString = 'Invalid XML: \n'
      errors.forEach((err, i) => {
        errString += `${err}\nLine: ${err.line}.`
        if (err.str1) {
          errString += ` ID: ${err.str1}.`
        }
        if (i !== errors.length - 1) {
          errString += `\n\n`
        }
      })
      await Manuscript.updateErrorMsg(manuscriptId, errString)
      logger.error(errString)
    } else {
      const nxml = await transformXML(
        xml,
        path.resolve(__dirname, 'xsl/pnihms2pmc3.xsl'),
      )

      const nxmlIsWellformed = libxml.parseXml(nxml)
      const nxmlIsValid = nxmlIsWellformed.validate(xsdGreenDoc)
      const nxmlErrors = nxmlIsWellformed.validationErrors

      if (!nxmlIsValid) {
        let errString = 'Invalid NXML: \n\n'
        nxmlErrors.forEach((err, i) => {
          errString += `${err}\nLine: ${err.line}.`
          if (err.str1) {
            errString += ` ID: ${err.str1}.`
          }
          if (i !== nxmlErrors.length - 1) {
            errString += `\n\n`
          }
        })
        await Manuscript.updateErrorMsg(manuscriptId, errString)
        logger.error(errString)
      } else {
        // Check NXML against the stylechecker
        const checked = await transformXML(
          nxml,
          path.resolve(__dirname, 'xsl/stylechecker/nlm-stylechecker.xsl'),
        )

        const result = libxml.parseXml(checked)

        const styleErrors = result.find('//error')

        if (styleErrors.length === 0) {
          const manuscriptFiles = await File.selectByManuscriptId(manuscriptId)
          const filelist = getFilelist(manuscriptFiles)

          const tmpPath = await tar.createTempDir()
          const filesArr = []
          const user = await getUser.getAdminUser()

          // now we will transform the nXML into HTML:
          const html = await transformXML(
            nxml,
            path.resolve(__dirname, 'xsl/nxml2html.xsl'),
            filelist,
          )

          // save converted.html to tmp folder
          fs.writeFileSync(`${tmpPath}/${manuscriptId}.html`, html)

          // save converted.nxml to tmp folder
          fs.writeFileSync(`${tmpPath}/${manuscriptId}.nxml`, nxml)

          filesArr.push(
            getFileInfo(`${tmpPath}/${manuscriptId}.html`, manuscriptId, user),
          )
          filesArr.push(
            getFileInfo(`${tmpPath}/${manuscriptId}.nxml`, manuscriptId, user),
          )
          const uuid = uuidv4()

          // upload to minio
          for (let i = 0; i < filesArr.length; i += 1) {
            files.uploadFileToMinio(
              `${uuid}${filesArr[i].extension}`,
              filesArr[i].filename,
              filesArr[i].url,
              filesArr[i].mimeType,
            )
          }
          const dbFilesArr = filesArr.map(obj => {
            obj.url = `/download/${uuid}${obj.extension}`
            delete obj.extension
            return obj
          })

          await db.upsertFileUrl(dbFilesArr, manuscriptId)
          await Manuscript.updatePdfDepositState(manuscriptId)
          await Manuscript.clearFormState(manuscriptId)
          logger.info('Uploading to Minio and the database has been completed.')
        } else {
          let styleErrString = `Style Errors: <br/><br/>`
          styleErrors.forEach((err, i) => {
            styleErrString += err.text()
            if (i !== styleErrors.length - 1) {
              styleErrString += `<br/><br/>`
            }
          })
          await Manuscript.updateErrorMsg(manuscriptId, styleErrString)
          logger.error(styleErrString)
        }
      }
    }
  } catch (err) {
    await Manuscript.updateErrorMsg(manuscriptId, err.message)
    logger.error(err.message)
  }
}

function getFileInfo(filepath, manuscriptId, user) {
  const filename = filepath.substring(filepath.lastIndexOf('/') + 1)
  const fileExt = path.extname(filepath)
  const fileSize = fs.statSync(filepath).size

  const fileInfo = {
    url: filepath,
    filename,
    type: (function getType() {
      if (fileExt === '.nxml') return 'PMCfinal'
      else if (fileExt === '.html') return 'tempHTML'
      else if (fileExt === '.xml') return 'PMC'
      return ''
    })(),
    label: '',
    size: fileSize,
    extension: fileExt,
    mimeType: `${mime.contentType(fileExt)}`,
    manuscriptId,
    updatedBy: user.id,
  }
  return fileInfo
}

function transformXML(xmlString, xslPath, params) {
  xmlString = xmlString.replace(/<!DOCTYPE[^>[]*(\\[[^]]*\\])?>/, '')
  const config = {
    xsltPath: xslPath,
    source: xmlString,
    result: String,
    props: {
      indent: 'yes',
    },
  }
  if (params) {
    config.params = params
  }
  return new Promise((resolve, reject) => {
    xslt4node.transform(config, (err, result) => {
      if (err) {
        reject(result)
      } else {
        resolve(result)
      }
    })
  })
}

function getFilelist(files) {
  return {
    filelist: files
      .filter(
        file =>
          file.type === 'IMGview' ||
          file.type === 'supplement_tag' ||
          file.type === 'supplement',
      )
      .map(
        file =>
          `${file.filename.substring(
            0,
            file.filename.lastIndexOf('.'),
          )}:/api/files/${file.url.split('/').pop()};`,
      )
      .join(''),
  }
}
