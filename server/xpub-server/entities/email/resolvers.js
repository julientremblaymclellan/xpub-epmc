const config = require('config')
const rfr = require('rfr')
const authentication = require('../user/authentication')

const { sendMail, userMessage } = rfr('server/email')
const { resetPasswordTemplate } = rfr('server/email/templates')
const { UserManager, NoteManager } = rfr('server/xpub-model')

const { url } = config['epmc-email']

const resolvers = {
  Mutation: {
    async epmc_email(_, { manuscriptId, to, subject, message }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const uniqueTo = [...new Set(to)]
      // Get email addresses
      const sendTo = await Promise.all(
        uniqueTo.map(async userId =>
          userId === 'helpdesk'
            ? 'helpdesk@europepmc.org'
            : UserManager.findEmail(userId),
        ),
      )
      const content = { to, subject, message }
      const note = {
        manuscriptId,
        notesType: 'userMessage',
        content: JSON.stringify(content),
      }
      // Send email
      await userMessage(sendTo, `${manuscriptId}: ${subject}`, message)
      // Create a note
      await NoteManager.create(note, user)
      return true
    },
    async epmc_emailPasswordResetLink(_, { email }, ctx) {
      const user = await UserManager.findByEmail(email)
      if (!user) {
        throw new Error('Email not registered')
      }
      const token = authentication.token.create({
        email,
        id: user.id,
      })
      const passwordResetLink = `${url}password-reset/${token}`

      await sendMail(
        email,
        'Europe PMC Plus - Reset Password',
        resetPasswordTemplate(user, passwordResetLink),
      )
      return true
    },
  },
}

module.exports = resolvers
