type Count {
  type: String
  count: Int
}

type Error {
  message: String
}

type ManuscriptSearchResult {
  total: Int!
  manuscripts: [Manuscript]!
  errors: [Error]
}

type ManuscriptResult {
  manuscript: Manuscript
  errors: [Error]
}

extend type Query {
  manuscript(id: ID!): Manuscript!
  manuscripts: [Manuscript]!
  adminManuscripts(external: Boolean): ManuscriptSearchResult!
  countByStatus: [Count]!
  checkDuplicates(id: ID!, articleIds: [String], title: String!): ManuscriptSearchResult!
  searchArticleIds(id: String!): ManuscriptResult!
  findByStatus(query: String!, page: Int, pageSize: Int): ManuscriptSearchResult!
  searchManuscripts(query: String!, page: Int, pageSize: Int): ManuscriptSearchResult!
}

extend type Mutation {
  createManuscript(data: CreateManuscriptInput!): Manuscript!
  deleteManuscript(id: ID!): Boolean!
  replaceManuscript(keepId: ID!, throwId: ID!): Boolean!
  updateManuscript(data: ManuscriptInput!): Manuscript!
  submitManuscript(data: ManuscriptInput!): ManuscriptResult!
  rejectManuscript(data: NewNoteInput!): ManuscriptResult!
  retagManuscript(data: NewNoteInput!): ManuscriptResult!
  reviewManuscript(data: ManuscriptInput!): ManuscriptResult!
  claimManuscript(id: ID!): ManuscriptResult!
  unclaimManuscript(id: ID!): ManuscriptResult!
}

input ManuscriptInput {
  id: ID!
  status: String
  journalId: ID
  meta: ManuscriptMetaInput
}

input CreateManuscriptInput {
  journalId: ID
  meta: ManuscriptMetaInput
}

input ManuscriptMetaInput {
  title: String
  volume: String
  issue: String
  location: LocationInput
  articleIds: [ArticleIdInput]
  publicationDates: [MetaDateInput]
  fundingGroup: [FundingGroupInput]
  releaseDelay: String
  unmatchedJournal: String
}

input LocationInput {
  fpage: String
  lpage: String
  elocationId: String
}

input ArticleIdInput {
  pubIdType: String
  id: String
}

input MetaDateInput {
  type: String
  date: DateTime
  jatsDate: JatsDateInput
}

input JatsDateInput {
  year: String
  season: String
  month: String
  day: String
}

input FundingGroupInput {
  fundingSource: String
  awardId: String
  title: String
  pi: PrincipalInput
}

input PrincipalInput {
  surname: String
  givenNames: String
  title: String
  email: Email
}