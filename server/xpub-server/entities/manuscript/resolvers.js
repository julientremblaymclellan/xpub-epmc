const logger = require('@pubsweet/logger')
const rfr = require('rfr')

const { ManuscriptManager, OrganizationManager } = rfr('server/xpub-model')

const resolvers = {
  Query: {
    async manuscript(_, { id }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }

      return ManuscriptManager.findById(id, user)
    },
    async manuscripts(_, vars, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }

      return ManuscriptManager.all(user)
    },
    async checkDuplicates(_, { id, articleIds, title }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      return ManuscriptManager.checkDuplicates(id, articleIds, title, user)
    },
    async countByStatus(_, vars, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      return ManuscriptManager.countByStatus(user)
    },
    async adminManuscripts(_, { external }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const statuses = external
        ? ['xml-qa']
        : ['submitted', 'xml-qa', 'xml-triage']
      return ManuscriptManager.findByStatus(statuses, -1, 0, user)
    },
    async findByStatus(_, { query, page, pageSize }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      return ManuscriptManager.findByStatus(
        query.split(','),
        page,
        pageSize,
        user,
      )
    },
    async searchManuscripts(_, { query, page, pageSize }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }

      return ManuscriptManager.search(query, page, pageSize, user)
    },
    async searchArticleIds(_, { id }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }

      return ManuscriptManager.findByArticleId(id, user)
    },
  },

  Mutation: {
    async createManuscript(_, { data }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }

      const organizationId = await OrganizationManager.getEuropePMCPlusID()
      const saved = await ManuscriptManager.create(data, user, organizationId)
      logger.debug('saved: ', saved)
      return saved
    },

    // TODO restrict this in production
    async deleteManuscript(_, { id }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }

      return ManuscriptManager.delete(id, user)
    },
    async replaceManuscript(_, { keepId, throwId }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }

      return ManuscriptManager.replace(keepId, throwId, user)
    },
    async updateManuscript(_, { data }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }

      const updatedMan = await ManuscriptManager.update(data, user)
      return updatedMan
    },
    async claimManuscript(_, { id }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }

      const updatedMan = await ManuscriptManager.changeClaim(id, user)
      return updatedMan
    },
    async unclaimManuscript(_, { id }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }

      const updatedMan = await ManuscriptManager.changeClaim(id, user, true)
      return updatedMan
    },

    async submitManuscript(_, { data }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const submittedMan = await ManuscriptManager.submit(data, user)
      return submittedMan
    },

    async rejectManuscript(_, { data }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const rejectedMan = await ManuscriptManager.reject(data, user)
      return rejectedMan
    },

    async reviewManuscript(_, { data }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const submittedMan = await ManuscriptManager.review(data, user)
      return submittedMan
    },

    async retagManuscript(_, { data }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const rejectedMan = await ManuscriptManager.retag(data, user)
      return rejectedMan
    },
  },
}

module.exports = resolvers
