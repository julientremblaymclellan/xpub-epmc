const resolvers = {
  Object: {
    __resolveType: obj => {
      if (obj.status) return 'Manuscript'
      if (obj.filename) return 'File'
      if (obj.journalTitle) return 'Journal'
      if (obj.notesType) return 'Note'
      if (obj.name) return 'Organization'
      return 'Review'
    },
  },
}

module.exports = resolvers
