const logger = require('@pubsweet/logger')
const rfr = require('rfr')
const config = require('config')
const uploadFiles = require('./uploadFiles')

const FileManager = rfr('server/xpub-model/entities/file')
const FileModel = rfr('server/xpub-model/entities/file/data-access')
const { pushXML } = rfr('server/xml-validation')

const resolvers = {
  Mutation: {
    uploadFiles,
    async updateFile(_, { id, type, label }, { user }) {
      if (!id || (type === undefined && label === undefined)) {
        return false
      }

      const props = { id, updatedBy: user }
      type !== undefined && (props.type = type)
      label !== undefined && (props.label = label)

      const file = new FileModel(props)
      const updatedFile = await file.save()
      logger.debug('updated: ', updatedFile)
      return true
    },
    async deleteFile(_, { id }, { user }) {
      const deletedFile = await FileManager.deleteById(id, user)
      logger.info('deletedFile: ', deletedFile)
      return true
    },
    async replaceManuscriptFile(_, { id, fileId, files }, { user }) {
      const deletedFile = await FileManager.deleteById(fileId, user)
      logger.info('deletedFile: ', deletedFile)

      return uploadFiles(
        _,
        { id, files, type: config.file.type.manuscript },
        { user },
      )
    },
    async convertXML(_, { id }, { user }) {
      try {
        const xmlFile = await FileManager.find(id)
        await pushXML(xmlFile.url, xmlFile.manuscriptId)
      } catch (error) {
        logger.error(error)
        return false
      }
      return true
    },
  },
}

module.exports = resolvers
