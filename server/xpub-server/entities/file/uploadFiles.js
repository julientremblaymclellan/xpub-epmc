const uuidv1 = require('uuid/v1')
const logger = require('@pubsweet/logger')
const config = require('config')
const rfr = require('rfr')
const { minioClient } = require('express-middleware-minio')

const FileModel = rfr('server/xpub-model/entities/file/data-access')

const extractFileExtension = filename => {
  if (filename) {
    return filename.split('.').pop()
  }
  return ''
}

const handleFile = async file => {
  const { stream, filename, mimetype } = await file

  let newFilename = uuidv1()
  if (filename) {
    const extension = extractFileExtension(filename)
    if (extension) {
      newFilename += `.${extension}`
    }
  }

  try {
    await minioClient.uploadFileSteam(newFilename, filename, mimetype, stream)
    return { filename, url: newFilename, mimetype }
  } catch (err) {
    return { err }
  }
}

async function uploadFiles(_, { id, files, type = '' }, { user }) {
  const resolvedFiles = await files
  const savedFiles = []
  await Promise.all(
    resolvedFiles.map(async resolvedFile => {
      const { err, filename, url, mimetype } = await handleFile(resolvedFile)
      if (err || !filename) {
        logger.error('Minio error: ', err)
        return
      }
      logger.info('Minio filename: ', filename)
      // Update database
      const file = new FileModel({
        manuscriptId: id,
        filename,
        mimeType: mimetype,
        url: `${config.file.url.download}/${url}`,
        type,
        updatedBy: user,
      })
      const savedFile = await file.save()
      logger.debug('savedFile: ', savedFile)
      savedFiles.push(savedFile)
    }),
  )

  const manuscript = {
    id,
    files: savedFiles,
  }
  logger.debug('manuscript: ', manuscript)
  return savedFiles.length > 0
}

module.exports = uploadFiles
