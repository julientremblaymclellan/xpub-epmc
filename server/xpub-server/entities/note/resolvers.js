const rfr = require('rfr')
const logger = require('@pubsweet/logger')

const { NoteManager } = rfr('server/xpub-model')

const resolvers = {
  Query: {},

  Mutation: {
    async createNote(_, { data }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }
      const saved = await NoteManager.create(data, user)
      logger.debug('saved: ', saved)
      return saved
    },

    // TODO restrict this in production
    async deleteNote(_, { id }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }

      return NoteManager.delete(id, user)
    },

    async updateNote(_, { data }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }

      const updated = await NoteManager.update(data, user)
      return updated
    },
  },
}

module.exports = resolvers
