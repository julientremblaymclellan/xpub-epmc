const logger = require('@pubsweet/logger')
const config = require('config')
const parseString = require('xml2js').parseString // eslint-disable-line prefer-destructuring
const https = require('https')
const Journal = require('../xpub-model/entities/journal/data-access')

const root_uri = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/'
const email = process.env.PUBMED_EUTILS_EMAIL || 'literature@ebi.ac.uk'
let apiKey = null
try {
  apiKey = config.get('eutils-api-key')
} catch (e) {
  // console.log("eutils-api-key is not defined")
}
// const apiKey = '59fdb1e97d6c82c703206a006ad284447208'
const retmode = 'xml'
const version = '2.0'
const database_nlmcatalog = 'nlmcatalog'
const term_ncbijournals = 'ncbijournals'
const usehistory = 'y'

let retmax = 2000
let webEnv = ''
let queryKey = ''
const mainIds = []
let mainIdsBatch = [] // current batch of ids fetched
let main_nlm_ids_mappings = []

async function parseXML() {
  const countUrl = `${root_uri}esearch.fcgi?api_key=${apiKey}&db=${database_nlmcatalog}&email=${email}&retmax=10&retmode=${retmode}&retstart=0&term=${term_ncbijournals}&usehistory=${usehistory}`
  const count = await getXMLData(countUrl, 'getCount')

  const beforeUpdate = Date.now()

  for (let retstart = 0; retstart < count; retstart += retmax) {
    const esearch_url = await generateEsearchUrl(retstart, retmax) // eslint-disable-line no-await-in-loop
    const searchRes = await getXMLData(esearch_url, 'esearch') // eslint-disable-line no-await-in-loop

    /* eslint-disable no-await-in-loop */

    const efetch_url = await generateEfetchUrl(
      searchRes.webEnv,
      searchRes.queryKey,
      retstart,
      retmax,
    )

    const fetchRes = await getXMLData(efetch_url, 'efetch')
    await Journal.upsertMulti(fetchRes)
    await sleep(3000)
  }

  logger.info(`Journal table was updated in: ${timeDiff(beforeUpdate)} minutes`)
}

function timeDiff(beforeUpdate) {
  return Math.floor((Date.now() - beforeUpdate) / 60000)
}

function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms)
  })
}

Journal.findByField('meta,nlmta', 'Nature').then(async natureJournal => {
  let run = true
  if (
    process.argv.length > 2 &&
    process.argv[2] === 'seed' &&
    natureJournal.length > 0
  ) {
    logger.info('Will not run journals seed.')
    run = false
  }

  if (run) {
    logger.info('Running journals seed.')
    await parseXML()
    logger.info('Journals seeding is done.')
  }
  process.exit(0)
})

function getSearchResults(search_results) {
  parseString(search_results, (err, result) => {
    try {
      webEnv = result.eSearchResult.WebEnv.toString()
      queryKey = result.eSearchResult.QueryKey.toString()
      retmax = parseInt(result.eSearchResult.RetMax, 10)
      mainIdsBatch = result.eSearchResult.IdList[0].Id
    } catch (e) {
      throw new Error(e.message)
    }
    if (err) {
      throw new Error(err)
    }
  })
}

function getSummaryResults(results) {
  let mapping = []
  parseString(results, (err, result) => {
    try {
      const arr = result.eSummaryResult.DocumentSummarySet[0].DocumentSummary
      mapping = arr.map(x => {
        const rObj = {}
        rObj[x.NLMUniqueID.toString()] = x.$.uid
        return rObj
      })
    } catch (e) {
      console.error(e.message)
    }
    if (err) {
      throw new Error(err)
    }
  })
  return mapping
}

function getFetchResults(results) {
  let mapping = []
  parseString(results, (err, result) => {
    const arr = result.NLMCatalogRecordSet.NLMCatalogRecord

    if (arr !== undefined || arr.length > 0) {
      mapping = arr.map(x => {
        const nlm_unique_id_var = x.NlmUniqueID.toString()
        const rObj = {}

        rObj.journalTitle = (function getJournalTitle() {
          if (x.TitleAlternate && x.TitleAlternate.length > 0) {
            x.TitleAlternate.forEach(element => {
              if (element !== undefined && element.$.TitleType === 'Uniform')
                return element.Title[0]._
            })
            if (x.TitleMain[0].Title[0]._ !== undefined) {
              return x.TitleMain[0].Title[0]._.toString()
            }
          } else if (x.TitleMain[0].Title[0]._ !== undefined) {
            return x.TitleMain[0].Title[0]._.toString()
          }
          return ''
        })()

        rObj['meta,publisherName'] =
          x.PublicationInfo !== undefined &&
          x.PublicationInfo[0].Imprint !== undefined
            ? x.PublicationInfo.map(x => x.Imprint[0].ImprintFull[0]).slice(
                -1,
              )[0]
            : ''
        rObj['meta,issn'] =
          x.ISSN !== undefined
            ? x.ISSN.map(t => {
                const issnsObj = {}
                issnsObj.id = t._
                issnsObj.type = t.$.IssnType
                return issnsObj
              })
            : []
        rObj.dateRevised = new Date(
          Date.UTC(
            x.DateRevised[0].Year.toString(),
            parseInt(x.DateRevised[0].Month.toString(), 10) - 1,
            x.DateRevised[0].Day,
          ),
        ).toISOString()

        rObj['meta,nlmta'] =
          x.MedlineTA !== undefined ? x.MedlineTA.toString() : ''
        rObj['meta,nlmuniqueid'] = nlm_unique_id_var

        const indexingSrcNameArr =
          x.IndexingSourceList !== undefined
            ? x.IndexingSourceList.map(t => t.IndexingSource)
                .map(t => t)[0]
                .map(t => t)
                .map(t => t.IndexingSourceName[0])
                .map(t => t._)
            : []

        const indexStatusArr =
          x.IndexingSourceList !== undefined
            ? x.IndexingSourceList.map(t => t.IndexingSource)
                .map(t => t)[0]
                .map(t => t)
                .map(t => t.IndexingSourceName[0])
                .map(t => t.$.IndexingStatus)
            : []

        rObj['meta,pmc_status'] = indexingSrcNameArr.includes('PMC')
        rObj['meta,pubmed_status'] =
          indexingSrcNameArr.includes('PubMed') &&
          indexStatusArr.includes('Currently-indexed')
        rObj.updated = new Date()
        rObj['meta,firstYear'] =
          x.PublicationInfo[0].PublicationFirstYear !== undefined
            ? x.PublicationInfo[0].PublicationFirstYear[0]
            : ''
        rObj['meta,endYear'] =
          x.PublicationInfo[0].PublicationEndYear !== undefined
            ? x.PublicationInfo[0].PublicationEndYear[0]
            : ''

        return rObj
      })
    }
    if (err) {
      throw new Error(err)
    }
  })
  return mapping
}

function getXMLData(url, eutilFunc) {
  return new Promise((resolve, reject) => {
    https
      .get(url, res => {
        if (checkUrl(res) === false) return
        res.setEncoding('utf8')
        let rawData = ''
        res.on('data', chunk => {
          rawData += chunk
        })
        res.on('end', () => {
          try {
            if (eutilFunc === 'getCount') {
              parseString(rawData, (err, result) => {
                resolve(parseInt(result.eSearchResult.Count, 10))
                if (err) {
                  throw new Error(err)
                }
              })
            } else if (eutilFunc === 'esearch') {
              getSearchResults(rawData)
              mainIds.push(mainIdsBatch)
              resolve({
                main_ids: mainIdsBatch,
                webEnv,
                queryKey,
              })
            } else if (eutilFunc === 'esummary') {
              main_nlm_ids_mappings = getSummaryResults(rawData)
              resolve(main_nlm_ids_mappings)
            } else if (eutilFunc === 'efetch') {
              const fetchData = getFetchResults(rawData)
              resolve(fetchData)
            }
          } catch (e) {
            console.error(e.message)
          }
        })
      })
      .on('error', e => {
        console.error(`Got error: ${e.message}`)
      })
  })
}

function generateEsearchUrl(retstart, retmax) {
  return new Promise((resolve, reject) => {
    resolve(
      `${root_uri}esearch.fcgi?api_key=${apiKey}&db=${database_nlmcatalog}&email=${email}&retmax=${retmax}&retmode=${retmode}&retstart=${retstart}&term=${term_ncbijournals}&usehistory=${usehistory}`,
    )
  })
}

function generateEfetchUrl(webenv, querykey, retstart, retmax) {
  return new Promise((resolve, reject) => {
    resolve(
      `${root_uri}efetch.fcgi?WebEnv=${webenv}&api_key=${apiKey}&db=${database_nlmcatalog}&email=${email}&query_key=${querykey}&retmax=${retmax}&retmode=${retmode}&retstart=${retstart}&version=${version}`,
    )
  })
}

function checkUrl(res) {
  const { statusCode } = res
  const contentType = res.headers['content-type']
  let error
  if (statusCode !== 200) {
    error = new Error(`Request Failed.\nStatus Code: ${statusCode}`)
  } else if (!/xml/.test(contentType)) {
    error = new Error(
      `Invalid content-type.\nExpected xml but received ${contentType}`,
    )
  }
  if (error) {
    console.error(error.message)
    res.resume() // consume response data to free up memory
    return false
  }
  return true
}
