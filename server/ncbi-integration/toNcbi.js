const logger = require('@pubsweet/logger')
const path = require('path')
const { exec } = require('child_process')
const http = require('http')
const fs = require('fs')
const os = require('os')
const config = require('config')
const md5File = require('md5-file')
const Client = require('ftp')

const pubsweetServer = config.get('pubsweet-server.baseUrl')
// const ftpLocation = '/home/mselim/test'
const ncbiFileTypes = config.get('ncbiFileTypes')
const NCBI_PACKAGE_EXT = '.ld'
const ManuscriptManager = require('../xpub-model/entities/manuscript')

module.exports = toNcbi => {
  ManuscriptManager.findNcbiReady().then(manuscripts => {
    manuscripts.forEach(manuscript => {
      logger.info(`Sending package ${manuscript.id} to ncbi FTP location.`)
      createTempDir(manuscript).then(tmpPath =>
        getFiles(tmpPath, manuscript).then(() =>
          getFunderInfo(manuscript).then(funders =>
            createManifest(tmpPath, manuscript, funders).then(() =>
              compress(tmpPath, manuscript).then(destination => {
                send(destination, manuscript).then(() => {
                  tidyUp(tmpPath)
                  updateManuscriptNcbiStatus(manuscript)
                })
              }),
            ),
          ),
        ),
      )
    })
  })
}

function createTempDir(manuscript) {
  return new Promise((resolve, reject) => {
    // noinspection JSCheckFunctionSignatures
    const directory = `${os.tmpdir()}/${getFilesPrefix(manuscript)}`
    // if temp directory exists, remove its contents:
    fs.access(directory, fs.constants.F_OK, err => {
      if (!err) {
        tidyUp(directory)
      }
      fs.mkdir(directory, err => {
        if (err) {
          reject(err)
        }
        resolve(directory)
      })
    })
  })
}

function getFunderInfo(manuscript) {
  return new Promise((resolve, reject) => {
    http
      .get(
        'http://www.ebi.ac.uk/europepmc/GristAPI/rest/api/get/funders',
        res => {
          let data = ''
          res.on('data', chunk => {
            data += chunk
          })
          res.on('end', () => {
            resolve(JSON.parse(data))
            res = null
          })
        },
      )
      .on('error', err => {
        // Handle errors
        reject(err)
      })
  })
}

function getFiles(tmpPath, manuscript) {
  const promises = [] // array of promises
  // list of files needed by ncbi
  const files = getNcbiFiles(manuscript)
  files.forEach(file => {
    const promise = new Promise((resolve, reject) => {
      const fileOnDisk = fs.createWriteStream(`${tmpPath}/${file.filename}`)
      http
        .get(`${pubsweetServer}${file.url}`, response => {
          let stream = response.pipe(fileOnDisk)
          stream.on('finish', () => {
            resolve(true)
            stream = null
          })
        })
        .on('error', err => {
          // Handle errors
          reject(err)
        })
    })
    promises.push(promise)
  })
  return Promise.all(promises)
}

function createManifest(tmpPath, manuscript, funders) {
  const { funderInfoList } = funders
  const pmcId = manuscript['meta,articleIds'].find(
    id => id.pubIdType === 'pmcid',
  )
    ? manuscript['meta,articleIds'].find(id => id.pubIdType === 'pmcid').id
    : 0
  const pmId = manuscript['meta,articleIds'].find(id => id.pubIdType === 'pmid')
    ? manuscript['meta,articleIds'].find(id => id.pubIdType === 'pmid').id
    : 0
  const createdDate = new Date(manuscript.created).toISOString().split('T')[0]
  const publishDate = new Date(
    manuscript['meta,publicationDates'].find(date => date.type === 'ppub').date,
  )
    .toISOString()
    .split('T')[0]
  const hasPubPdf = manuscript.files.find(file => file.type === 'pdf4load')
    ? '1'
    : '0'
  const journalNlmId = manuscript.journal['meta,nlmuniqueid']
  const grant = funderInfoList.find(
    funder => funder.name === manuscript['meta,fundingGroup'][0].fundingSource,
  )
  const granteName = grant.grantPrefix.includes('_')
    ? `${grant.grantPrefix}${manuscript['meta,fundingGroup'][0].awardId}`
    : grant.grantPrefix
  const files = getNcbiFiles(manuscript)
  let text = `<?xml version="1.0"?>\n <!DOCTYPE pnihms-xdata PUBLIC "-//PNIHMS-EXCHANGE//DTD pNIHMS Exchange DTD//EN" "pnihms_exchange.dtd"> \n
      <pnihms-xdata mid="${getFilesPrefix(
        manuscript,
      )}" pubmed-id="${pmId}" pmc-id="${pmcId}" create-date="${createdDate}" publish-date="${publishDate}" pub-pdf="${hasPubPdf}" nlm-journal-id="${journalNlmId}"> \n
      <grants> \n
      <grant name="${granteName}" authority="WT" /> \n
	    </grants> \n
      <blobs> \n`

  files.forEach(file => {
    const { numeric } = ncbiFileTypes.find(type => type.value === file.type)
    const fileMd5 = getFileMd5(`${tmpPath}/${file.filename}`)
    text += `<blob md5="${fileMd5}" name="${
      file.filename
    }" type="${numeric}" /> \n`
  })
  text += `</blobs> \n
  </pnihms-xdata>`

  return new Promise((resolve, reject) => {
    fs.writeFile(`${tmpPath}/${getFilesPrefix(manuscript)}.mxml`, text, err => {
      if (err) {
        reject(err)
      }
      resolve(tmpPath)
    })
  })
}

function compress(tmpPath, manuscript) {
  return new Promise((resolve, reject) => {
    const currentDate = new Date()
    // const date = currentDate.toISOString().split("T")[0]
    const timestamp = currentDate
      .toISOString()
      .replace(/-/g, '_')
      .replace('T', '-')
      .replace(/:/g, '_')
      .split('.')[0]
    const dest = `${tmpPath}/${getFilesPrefix(
      manuscript,
    )}.${timestamp}${NCBI_PACKAGE_EXT}.zip`
    const cmd = `cd ${tmpPath} && zip -r ${dest} .`
    exec(cmd, err => {
      if (err) {
        // node couldn't execute the command
        throw new Error('cannot compress files')
      }
      global.destination = dest
      resolve(dest)
    })
  })
}

function send(dest, manuscript) {
  return new Promise((resolve, reject) => {
    let c = new Client()
    c.on('ready', () => {
      c.put(
        dest,
        `/${config.get('ncbi-ftp')['receive-folder']}/${path.basename(dest)}`,
        err => {
          if (err) {
            reject(err)
          }
          c.end()
          c = null
          resolve(true)
        },
      )
    })
    const { host, user, password } = config.get('ncbi-ftp')
    c.connect({ host, user, password })
  })
}

function tidyUp(tmpPath) {
  try {
    const cmd = `rm -rf ${tmpPath}*`
    return new Promise((resolve, reject) => {
      exec(cmd, (err, stdout, stderr) => {
        if (err) {
          // node couldn't execute the command
          reject(err)
        }
        // logger.info(`Finished process`)
        resolve(true)
      })
    })
  } catch (e) {
    logger.error('Error while tidying up after another error!')
  }
}

function getFilesPrefix(manuscript) {
  return manuscript.id.toLowerCase()
}

function getNcbiFiles(manuscript) {
  return manuscript.files.filter(file =>
    ncbiFileTypes.find(type => type.value === file.type),
  )
}

function getFileMd5(filePath) {
  return md5File.sync(filePath)
}

function updateManuscriptNcbiStatus(manuscript) {
  ManuscriptManager.update(
    {
      id: manuscript.id,
      status: config.get('ncbiSentState'),
      ncbiState: 'sent',
    },
    manuscript.updatedBy,
  ).then(() =>
    logger.info(`Created package ${manuscript.id} to ncbi FTP location.`),
  )
}
