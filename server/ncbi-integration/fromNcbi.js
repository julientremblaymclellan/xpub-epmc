const Client = require('ftp')
const config = require('config')
const fs = require('fs')
const os = require('os')
const Dom = require('xmldom').DOMParser
const logger = require('@pubsweet/logger')
const ManuscriptManager = require('../xpub-model/entities/manuscript')
const ManuscriptAccess = require('../xpub-model/entities/manuscript/data-access')

const NCBI_RESPONSE_EXT = new RegExp(/\S+.ld.response.xml$/i)

module.exports = fromNcbi => {
  let c = new Client()
  c.on('ready', () => {
    c.list(`/${config.get('ncbi-ftp')['send-folder']}`, false, (err, list) => {
      if (err) {
        c = null
        return
      }
      const responseFiles = list.filter(file =>
        NCBI_RESPONSE_EXT.test(file.name),
      )
      if (!responseFiles.length) {
        c.end()
      }
      responseFiles.forEach((file, index, array) => {
        const remoteFilePath = `/${config.get('ncbi-ftp')['send-folder']}/${
          file.name
        }`
        c.get(remoteFilePath, (err, stream) => {
          if (err) return
          const path = `${os.tmpdir()}/${file.name}`
          stream.pipe(fs.createWriteStream(path))
          processFile(path).then(response => {
            updateManuscriptNcbiStatus(file.name, response).then(() => {
              c.rename(remoteFilePath, `${remoteFilePath}.processed`, err => {
                if (err) {
                  logger.info(err)
                }
                if (index === array.length - 1) {
                  c.end()
                  c = null
                }
              })
            })
          })
        })
      })
    })
  })
  const { host, user, password } = config.get('ncbi-ftp')
  c.connect({ host, user, password })
}

function processFile(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', (err, data) => {
      if (err) {
        reject(err)
      }
      const doc = new Dom().parseFromString(data)
      if (doc) {
        const receipt = doc.getElementsByTagName('receipt')[0]
        resolve({
          status: parseInt(receipt.getAttribute('status'), 10),
          pmcid: receipt.getAttribute('pmcid'),
        })
      } else {
        reject(new Error('xml parse error'))
      }
    })
  })
}

async function updateManuscriptNcbiStatus(fileName, response) {
  // filename example: ems76395.2019_01_01-08_30_06.ld.response.xml
  const manuscriptId = fileName.split('.')[0].toUpperCase()
  const manuscript = await ManuscriptAccess.selectById(manuscriptId)
  const newIds = manuscript['meta,articleIds'].filter(
    id => id.pubIdType !== 'pmcid',
  )
  if (!response.status && response.pmcid) {
    newIds.push({ pubIdType: 'pmcid', id: response.pmcid })
  }
  return ManuscriptManager.update({
    id: manuscriptId,
    status: response.status
      ? config.get('ncbiFailedState')
      : config.get('ncbiSentState'),
    ncbiState: response.status ? 'failure' : 'success',
    'meta,articleIds': response.pmcid ? newIds : manuscript['meta,articleIds'],
  })
}
