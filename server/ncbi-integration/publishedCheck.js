const logger = require('@pubsweet/logger')
const fetch = require('node-fetch')
const Manuscript = require('../xpub-model/entities/manuscript/data-access')
const rfr = require('rfr')

const { manuscriptHasBeenPublishedEmail } = rfr('server/email')

;(async () => {
  const beforeUpdate = Date.now()
  await publishedCheck()
  Manuscript.knex().destroy()
  logger.info(`Published check was finished in ${Date.now() - beforeUpdate} ms`)
})()

// module.exports = async publishedCheck => {
async function publishedCheck() {
  const manuscriptsDb = await Manuscript.findByFieldEager(
    'status',
    'ncbi-sent',
    'users.[roles, identities]',
  )
  const manuscripts = manuscriptsDb.filter(manuscript => !manuscript.deleted)

  /* eslint-disable no-await-in-loop */
  /* eslint-disable no-restricted-syntax */
  for (const manuscript of manuscripts) {
    if (await checkEuropepmc(manuscript)) {
      manuscript.status = 'published'
      const manuscript4Db = new Manuscript(manuscript)
      delete manuscript4Db.users
      await manuscript4Db.save()
      logger.info(
        `Status of manuscript ${manuscript.id} was changed to 'published'`,
      )
      sendEmails(manuscript)
    }
  }
}

function checkEuropepmc(manuscript) {
  return new Promise((resolve, reject) => {
    try {
      const pmcid = manuscript['meta,articleIds'].filter(
        id => id.pubIdType === 'pmcid',
      )[0].id

      fetch(
        `https://www.ebi.ac.uk/europepmc/webservices/rest/search?resulttype=idlist&format=json&query=PMCID:PMC${pmcid}`,
        {
          method: 'GET',
        },
      )
        .then(response => response.json())
        .then(data => resolve(data.hitCount !== '0'))
        .catch(err => {
          logger.error(err)
          resolve(false)
        })
    } catch (ignored) {
      resolve(false)
    }
  })
}

function getUnique(arr, comp) {
  const unique = arr
    .map(e => e[comp])

    // store the keys of the unique objects
    .map((e, i, final) => final.indexOf(e) === i && i)

    // eliminate the dead keys & store unique objects
    .filter(e => arr[e])
    .map(e => arr[e])

  return unique
}

function sendEmails(manuscript) {
  const pmcid = manuscript['meta,articleIds'].filter(
    id => id.pubIdType === 'pmcid',
  )[0].id
  getUnique(manuscript.users, 'id')
    .filter(user => !user.deleted)
    .filter(user =>
      user.roles.find(
        role => role.name === 'reviewer' || role.name === 'submitter',
      ),
    )
    .forEach(user => {
      user.identities
        .filter(identity => !identity.deleted)
        .forEach(identity => {
          logger.info(
            `Emailing ${identity.email} about their published manuscript ${
              manuscript.id
            }.`,
          )
          manuscriptHasBeenPublishedEmail(
            identity.email,
            `PMC${pmcid}: ${manuscript['meta,title']}`,
            `https://europepmc.org/articles/PMC${pmcid}`,
          )
        })
    })
}
