const tar = require('tar')
const tmp = require('tmp')
const fs = require('fs')

module.exports.untar = function untar(source, dest) {
  return new Promise((resolve, reject) => {
    tar.x(
      {
        file: source,
        cwd: dest,
      },
      err => {
        if (err) {
          reject(err)
        }
        fs.readdir(dest, (err, items) => {
          if (err) reject(err)
          resolve(`${dest}/${items[0]}`)
          // or? resolve(`${dest}`)
        })
      },
    )
  })
}

module.exports.createTempDir = function createTempDir() {
  return new Promise((resolve, reject) => {
    tmp.dir({ mode: '0750', prefix: 'xpubTmpDir_' }, (err, tmpPath) => {
      if (err) reject(err)
      resolve(tmpPath)
    })
  })
}
