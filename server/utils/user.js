const rfr = require('rfr')
const config = require('config')

const Identity = rfr('server/xpub-model/entities/identity/data-access')
const User = rfr('server/xpub-model/entities/user/data-access')
const ftpUsers = config.get('users')

module.exports.getFTPUser = function getFTPUser(rootPath, filename) {
  return new Promise((resolve, reject) => {
    const userName = 'taggers'

    Identity.findByFieldEager(
      'meta',
      `{"ftpUserName": "${userName}"}`,
      '[user]',
    )
      .then(identityDb => {
        const user = ftpUsers.find(user =>
          user.identities
            .filter(identity => identity.meta)
            .find(identity => identity.meta.ftpUserName === userName),
        )
        const identity = user.identities
          .filter(identity => identity.meta)
          .find(identity => identity.meta.ftpUserName === userName)
        /* eslint-disable dot-notation */
        identity['userId'] = identityDb[0].userId
        resolve(identity)
      })
      .catch(error => reject(error))
  })
}

module.exports.getAdminUser = function getAdminUser() {
  return new Promise((resolve, reject) => {
    User.findByEmail('helpdesk@europepmc.org')
      .then(user => {
        resolve(user)
      })
      .catch(error => reject(error))
  })
}
