const logger = require('@pubsweet/logger')
const FileModel = require('../xpub-model/entities/file/data-access')

module.exports.createTaggersFiles = async function createTaggersFiles(
  filesArr,
) {
  for (let i = 0; i < filesArr.length; i += 1) {
    /* eslint-disable no-await-in-loop */
    const filedb = await new FileModel(filesArr[i]).save({
      insertMissing: true,
    })
    logger.info(filedb)
  }
}

module.exports.upsertFileUrl = async function upsertFileUrl(
  filesArr,
  manuscriptId,
) {
  const existingFiles = await FileModel.selectByManIdConvertedFiles(
    manuscriptId,
  )
  const fileTypesArr = ['tempHTML', 'PMCfinal']

  for (let i = 0; i < fileTypesArr.length; i += 1) {
    /* eslint-disable no-await-in-loop */
    const newFile = filesArr.find(file => file.type === fileTypesArr[i])
    const existingFile = existingFiles.find(
      file => file.type === fileTypesArr[i],
    )
    if (existingFile) {
      const filedb = await FileModel.updateFileUrl(existingFile.id, newFile.url)
      logger.info(filedb)
    } else {
      const filedb = await new FileModel(newFile).save({
        insertMissing: true,
      })
      logger.info(filedb)
    }
  }
}

module.exports.upsertHtmlPrevFile = async function upsertHtmlPrevFile(
  newFile,
  manuscriptId,
) {
  const existingFiles = await FileModel.selectByManuscriptId(manuscriptId)

  const existingFile = existingFiles.find(file => file.type === 'source')
  if (existingFile) {
    const filedb = await FileModel.updateFileUrl(existingFile.id, newFile.url)
    logger.info(filedb)
  } else {
    const filedb = await new FileModel(newFile).save({
      insertMissing: true,
    })
    logger.info(filedb)
  }
}

module.exports.deleteTaggingFiles = async function deleteTaggingFiles(
  manuscriptId,
  user,
) {
  const manFiles = await FileModel.selectByManuscriptId(manuscriptId)
  const submissionFilesTypes = ['manuscript', 'figure', 'table', 'supplement']
  const taggingFiles = manFiles.filter(
    el => !submissionFilesTypes.includes(el.type),
  )
  // delete tagging files
  for (let i = 0; i < taggingFiles.length; i += 1) {
    await FileModel.deleteById(taggingFiles[i].id, user.userId)
  }
  logger.info(`${taggingFiles.length} files deleted.`)
}
