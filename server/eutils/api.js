const superagent = require('superagent')
const config = require('config')

let eutilsApiKey = null
try {
  eutilsApiKey = config.get('eutils-api-key')
} catch (e) {
  // console.log("eutils-api-key is not defined")
}

// const authBearer = passport.authenticate('bearer', { session: false })

module.exports = app => {
  app.get('/eutils/esearch', (req, res) => {
    res.set({ 'Content-Type': 'application/json' })

    const { term, db, retstart, sort } = req.query
    const encodedTerm = encodeURIComponent(term)
    const url = `https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=${db}&term=${encodedTerm}${
      /^\d+$/.test(encodedTerm) ? '[uid]' : ''
    }+NOT+pubmed+books[filter]&sort=${sort}&retmode=json&retstart=${retstart}&retmax=25${
      eutilsApiKey ? `&api_key=${eutilsApiKey}` : ''
    }`

    // console.log(url)

    superagent.get(url).end((err, response) => {
      if (err) {
        res.send(JSON.stringify({ error: err }))
      } else {
        res.send(JSON.stringify(response.body))
      }
    })
  })
  app.get('/eutils/efetch', (req, res) => {
    res.set({ 'Content-Type': 'application/xml' })

    const { id, db } = req.query

    const url = `https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=${db}&id=${id}&retmode=xml${
      eutilsApiKey ? `&api_key=${eutilsApiKey}` : ''
    }`

    superagent.get(url).end((err, response) => {
      if (err) {
        res.send({ error: err })
      }
      res.send(response.text.toString())
    })
  })
  app.get('/eutils/esummary', (req, res) => {
    res.set({ 'Content-Type': 'application/json' })

    const { id, db } = req.query

    const url = `https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=${db}&id=${id}&version=2.0&retmode=json${
      eutilsApiKey ? `&api_key=${eutilsApiKey}` : ''
    }`

    superagent.get(url).end((err, response) => {
      if (err) {
        res.send(JSON.stringify({ error: err }))
      }
      res.send(JSON.stringify(response.body))
    })
  })
}
