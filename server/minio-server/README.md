<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Minio File Upload Configuration](#minio-file-upload-configuration)
- [`component-minio` API](#component-minio-api)
  - [Upload a file [POST]](#upload-a-file-post)
      - [Request](#request)
      - [Request body](#request-body)
      - [Response](#response)
  - [Retrieve file [GET]](#retrieve-file-get)
      - [Request](#request-1)
      - [Response](#response-1)
  - [List files of a fragment [GET]](#list-files-of-a-fragment-get)
      - [Request](#request-2)
      - [Response](#response-2)
  - [Delete file [DELETE]](#delete-file-delete)
      - [Request](#request-3)
      - [Response](#response-3)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Minio File Upload Configuration

In order to use this component, the following configuration needs to be added to a PubSweet application inside a section named `pubsweet-component-minio`. You can set the actual values in the `.env` file

For example:

```json
  'pubsweet-component-minio': {
    accessKeyId: process.env.MINIO_ACCESS_KEY,
    secretAccessKey: process.env.MINIO_SECRET_KEY,
    endPoint: process.env.MINIO_ENDPOINT,
    port: process.env.MINIO_PORT,
    secure: process.env.MINIO_SECURITY === 'true',
    bucket: process.env.MINIO_BUCKET,
    uploads: process.env.MINIO_UPLOADS_FOLDER_NAME,
  },
```

The `.env` file should look like this:

```shell
MINIO_ACCESS_KEY=exampleKey
MINIO_SECRET_KEY=exampleKey
MINIO_ENDPOINT=example.org
MINIO_PORT=9000
MINIO_SECURITY=true
MINIO_BUCKET=manuscripts
MINIO_UPLOADS_FOLDER_NAME=uploads
```

Then, as soon as possible in your app you should add the `dotenv` package:

```js
require('dotenv').config()
```

# `component-minio` API

A list of endpoints that help you upload, download and delete S3 files.

## Upload a file [POST]

#### Request

`POST /api/files`

#### Request body

```
Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryWfPNVh4wuWBlyEyQ

------WebKitFormBoundaryWfPNVh4wuWBlyEyQ
Content-Disposition: form-data; name="fileType"

supplementary
------WebKitFormBoundaryWfPNVh4wuWBlyEyQ
Content-Disposition: form-data; name="fragmentId"

545
------WebKitFormBoundaryWfPNVh4wuWBlyEyQ
Content-Disposition: form-data; name="file"; filename="attachment.txt"
Content-Type: text/plain

[file content goes there]
------WebKitFormBoundaryWfPNVh4wuWBlyEyQ
```

#### Response

```json
{
  "id": "fragment-id/file-name"
}
```

## Retrieve file [GET]

This endpoint allows you to retrieve a file's signed URL that can be used to download the file.

#### Request

`GET /api/files/{fragmentId}/{fileId}`

| URI Parameter | Requiered | Requirements | Description            |
| ------------- | --------- | ------------ | ---------------------- |
| fragmentId    | Yes       | String       | The ID of the fragment |
| fileId        | Yes       | String       | The ID/Name of the file|

#### Response

Typically, browsers will prompt the user for download.

## List files of a fragment [GET]

This endpoint allows you to retrieve a fragment's files as a zip archive.

#### Request

`GET /api/files/{fragmentId}`

| URI Parameter | Requiered | Requirements | Description            |
| ------------- | --------- | ------------ | ---------------------- |
| fragmentId    | Yes       | String       | The ID of the fragment |

#### Response

```json
HTTP/1.1 200
[
   {
      "name":"admin/uploads/a693aaad-1640-4f2e-b5ec-0538bba34636/IMG_20180521_133845.jpg",
      "lastModified":"2018-06-13T09:52:57.259Z",
      "etag":"1d27dcaca270402af8be556e370b8ed4",
      "size":2826661
   },
   {
      "name":"admin/uploads/a693aaad-1640-4f2e-b5ec-0538bba34636/list-002.pdf",
      "lastModified":"2018-06-13T10:16:58.976Z",
      "etag":"d41d8cd98f00b204e9800998ecf8427e",
      "size":28663
   }
]
```

## Delete file [DELETE]

#### Request

`DELETE /api/files/{fileId}`

| URI Parameter | Requiered | Requirements | Description        |
| ------------- | --------- | ------------ | ------------------ |
| fileId        | Yes       | String       | The ID of the file |

#### Response

```json
HTTP/1.1 200
```

---
