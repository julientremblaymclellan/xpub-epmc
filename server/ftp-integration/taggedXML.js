const chokidar = require('chokidar')
const logger = require('@pubsweet/logger')
const getUser = require('../utils/user.js')
const tar = require('../utils/unTar.js')
const files = require('../utils/files.js')
const db = require('../utils/db.js')
const { execSync } = require('child_process')
const uuidv4 = require('uuid/v4')
const config = require('config')
const manuscriptModel = require('../xpub-model/entities/manuscript/data-access')

const ftpTagger = config.get('ftp_tagger')
const { processedTaggerEmail, bulkUploaderEmail } = require('../email')

const parentRootPath = `${process.env.HOME}/${config.get('ftp_directory')}/${
  ftpTagger.username
}`

const rootPath = `${parentRootPath}/Done`

const ignoreNewFolder = `${parentRootPath}/New`

const cmd = `mkdir -p ${rootPath} ${ignoreNewFolder}`
execSync(cmd)

// chokidar monitors tagged folder
const watcher = chokidar.watch(`${rootPath}**/*.tar.gz`, {
  persistent: true,
  ignored: ignoreNewFolder,
  awaitWriteFinish: {
    stabilityThreshold: 2000,
    pollInterval: 100,
  },
})

watcher
  .on('add', path => logger.debug(`File ${path} has been added.`))
  .on('add', path => processFile(path))
  .on('change', path => logger.debug(`File ${path} has been changed`))
  .on('unlink', path => logger.debug(`File ${path} has been removed`))
  .on('error', error => logger.error(`Watcher error: ${error}`))
  .on('ready', () =>
    logger.info(
      `Initial scan complete. Ready for changes under folder ${rootPath}`,
    ),
  )
  .on('raw', (event, path, details) => {
    logger.debug('Raw event info:', event, path, details)
  })

watcher.on('change', (path, stats) => {
  if (stats) logger.debug(`File ${path} changed size to ${stats.size}`)
})

async function processFile(path) {
  try {
    logger.info(`Processing tagged XML packages: ${path}`)

    const user = await getUser.getFTPUser(rootPath, path)
    const tmpPath = await tar.createTempDir()
    const extractedFilePath = await tar.untar(path, tmpPath)
    const manifestFilename = await files.getManifestFilename(extractedFilePath)
    const [manuscriptId, filesData] = await files.getManifestFileData(
      extractedFilePath,
      manifestFilename,
    )
    const filesArr = await files.checkFiles(filesData, tmpPath, user)

    // delete current existing 'tagging' files
    await db.deleteTaggingFiles(manuscriptId, user)

    const uuid = uuidv4()

    // upload to minio
    for (let i = 0; i < filesArr.length; i += 1) {
      files.uploadFileToMinio(
        `${uuid}${filesArr[i].extension}`,
        filesArr[i].filename,
        filesArr[i].url,
        filesArr[i].mimeType,
      )
    }

    const dbFilesArr = filesArr.map(obj => {
      obj.url = `/download/${uuid}${obj.extension}`
      delete obj.extension
      return obj
    })

    // create rows in 'file' table
    await db.createTaggersFiles(dbFilesArr)
    await files.renameFile(path)

    logger.info('Uploading to Minio and the database has been completed.')

    const manuscriptObj = await manuscriptModel.getManuscriptById(manuscriptId)

    // send email to taggers
    processedTaggerEmail(
      ftpTagger.email,
      manuscriptId,
      manuscriptObj[0]['meta,title'],
      `${path}`,
    )
  } catch (err) {
    logger.error('Error', err.message)

    // send email to taggers
    bulkUploaderEmail(ftpTagger.email, `${path}`)
  }
}
