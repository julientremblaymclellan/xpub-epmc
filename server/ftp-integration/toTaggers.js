const dateFormat = require('dateformat')
const rfr = require('rfr')
const logger = require('@pubsweet/logger')
const { exec } = require('child_process')
const http = require('http')
const fs = require('fs')
const os = require('os')
// const fetch = require('node-fetch')
const config = require('config')
const passport = require('passport')
const Manuscript = require('../xpub-model/entities/manuscript/data-access')

const { taggerEmail } = rfr('server/email')
const pubsweetServer = config.get('pubsweet-server.baseUrl')
const ftpTagger = config.get('ftp_tagger')

const ftpLocation = `${process.env.HOME}/${config.get('ftp_directory')}/${
  ftpTagger.username
}/New`

const authBearer = passport.authenticate('bearer', { session: false })

module.exports = app => {
  app.get('/tagging/send', authBearer, async (req, res) => {
    res.set({ 'Content-Type': 'application/json' })

    const { manid } = req.query

    await createPackageForTaggers(manid)
    res.send(JSON.stringify('result:success'))
  })
}

// createPackageForTaggers('EMS90002')

// noinspection JSAnnotator
async function createPackageForTaggers(manid) {
  const datedFolder = dateFormat(new Date(), 'yyyy-mm-dd')
  logger.info(`Sending package ${manid} to tagger FTP location.`)
  const manuscript = await getManuscript(manid)
  const tmpPath = await createTempDir(manuscript)
  await writeFiles(tmpPath, manuscript)
  await createMetadataXML(tmpPath, manuscript)
  await createManifest(tmpPath, manuscript)
  await compress(tmpPath, manuscript, datedFolder)

  taggerEmail(
    ftpTagger.email,
    manuscript.id,
    manuscript['meta,title'],
    `ftp://${process.env.PUBSWEET_HOST}/New/${datedFolder}/${
      manuscript.id
    }.tar.gz`,
  )

  await tidyUp(tmpPath)
}

function getManuscript(manId) {
  return new Promise((resolve, reject) => {
    Manuscript.findByFieldEager('id', manId, '[journal, files, audits]')
      .then(rows => {
        const manuscript = rows[0]
        manuscript.files = manuscript.files.filter(file => !file.deleted)
        resolve(manuscript)
      })
      .catch(err => reject(err))
  })
}

function createTempDir(manuscript) {
  return new Promise((resolve, reject) => {
    // noinspection JSCheckFunctionSignatures

    const directory = `${os.tmpdir()}/${manuscript.id}`

    // if temp directory exists, remove its contents:
    fs.access(directory, fs.constants.F_OK, err => {
      if (!err) {
        tidyUp(directory)
      }
      fs.mkdir(directory, err => {
        if (err) reject(err)
        resolve(directory)
      })
    })
  })
}

async function createMetadataXML(tmpPath, manuscript) {
  /*
  let pubDate = ''
  try {
    const firstPubDate = manuscript['meta,publication_dates'][0]
    pubDate = `${firstPubDate.year}-${firstPubDate.month}-${firstPubDate.day}`
  } catch (e) {
    logger.debug('Could not construct pubdate for taggers package.')
  }
*/
  // <journal-id journal-id-type="nlm-ta">journal_title_abbrev</journal-id>
  //     <issn pub-type="ppub">the_journals_issn</issn>
  let xml =
    '<?xml version="1.0" encoding="UTF-8"?>\n' +
    '<!-- use DTD http://dtd.nlm.nih.gov/publishing/3.0/journalpublishing3.dtd -->\n' +
    '<?properties manuscript?>\n' +
    '<?origin ukpmcpa?>\n' +
    '<nihms-source-xml xmlns:xlink="http://www.w3.org/1999/xlink">\n' +
    '<journal-meta>\n'
  try {
    if (manuscript.journal) {
      // if manuscript has been matched to a journal in the database
      xml += `<journal-id journal-id-type="nlm-ta">${
        manuscript.journal['meta,nlmta']
      }</journal-id>\n`

      xml += `<journal-title-group>\n<journal-title>${
        manuscript.journal.journalTitle
      }</journal-title>\n</journal-title-group>\n`

      manuscript.journal['meta,issn']
        .filter(issn => issn.type === 'Print')
        .forEach(issn => {
          xml += `<issn pub-type="ppub">${issn.id}</issn>\n`
        })

      manuscript.journal['meta,issn']
        .filter(issn => issn.type === 'Electronic')
        .forEach(issn => {
          xml += `<issn pub-type="epub">${issn.id}</issn>\n`
        })
    } else {
      // else try from the unmatched journal column
      xml += `<journal-title-group>\n<journal-title>${
        manuscript['meta,unmatchedJournal']
      }</journal-title>\n</journal-title-group>\n`
    }
  } catch (ignored) {
    // ignore
  }
  xml +=
    '</journal-meta>\n' +
    `<article-id pub-id-type="manuscript">${manuscript.id}</article-id>\n`
  if (manuscript['meta,publicationDates']) {
    manuscript['meta,publicationDates'].forEach(pubDate => {
      const dateObj = new Date(pubDate.date)
      xml +=
        `<pub-date pub-type="${pubDate.type}">` +
        `<day>${dateObj.getDate()}</day><month>${dateObj.getMonth() +
          1}</month><year>${dateObj.getFullYear()}</year>` +
        '</pub-date>'
    })
  }

  // decide on the nihms-submitted date
  //   audrey [9:32 AM]
  //   I'm not sure if it's the date the submission moves to the 'submitted' state or the date we send it to PMC
  //   We will get it from the audit_log table if it's the first one
  //   mike (20190121): It's the day the manuscript files were packaged for the tagger, i.e. when it moved to the state "tagging_ready".
  //   we will get this from the audit table as a set of JSON objects.
  //   audrey (20190121): it can be sent out for retagging multiple times, and it should be the first date
  try {
    const submittedDate = manuscript.audits
      .filter(audit => audit.changes.status === 'tagging_ready')
      .sort((action1, action2) => action1.created - action2.created)[0].created

    const dateObj = new Date(submittedDate)
    xml +=
      '<pub-date pub-type="nihms-submitted">' +
      `<day>${dateObj.getDate()}</day><month>${dateObj.getMonth() +
        1}</month><year>${dateObj.getFullYear()}</year> ` +
      '</pub-date>\n'
  } catch (ignored) {
    logger.debug('Did not create nihms-submitted date ')
  }

  try {
    const metadata = await fs.readFileSync(`${tmpPath}/metadata.xml`, 'utf8')
    xml += `${metadata}\n`
  } catch (ignored) {
    // ignored
  }

  // supplementary files Issue #290
  const suppFiles = manuscript.files
    .filter(file => file.type === 'supplement')
    .sort((file1, file2) => file1.label - file2.label)

  if (suppFiles && suppFiles.length > 0) {
    xml +=
      '<sec sec-type="supplementary-material" id="SM">\n' +
      '<title>Supplementary Material</title>\n'

    for (let i = 0; i < suppFiles.length; i += 1) {
      const file = suppFiles[i]
      const mimetypes = file.mimeType.split('/')
      xml +=
        `<supplementary-material id="SD${i + 1}" content-type="local-data">\n` +
        `<label>${file.label ? file.label : ''}</label>\n` +
        `<media xlink:href="${file.filename}" mimetype="${
          mimetypes[0]
        }" mime-subtype="${mimetypes[1]}" />\n` +
        '</supplementary-material>\n'
    }
    xml += '</sec>\n'
  }

  xml += '</nihms-source-xml>'

  return new Promise((resolve, reject) => {
    fs.writeFile(`${tmpPath}/${manuscript.id}.xml`, xml, err => {
      if (err) {
        reject(err)
      }
      resolve(`${manuscript.id}.xml`)
    })
  })
}

// file types included for taggers
function filterFiles(files) {
  const allowedTypes = [
    'manuscript',
    'supplement',
    'figure',
    'PMC',
    'IMGview',
    'IMGprint',
  ]
  const result = files.filter(file => allowedTypes.includes(file.type))
  return result
}

function writeFiles(tmpPath, manuscript) {
  const promises = [] // array of promises

  const filteredFiles = filterFiles(manuscript.files)

  // do not export the metadata file
  filteredFiles.forEach(file => {
    const promise = new Promise((resolve, reject) => {
      const fileOnDisk = fs.createWriteStream(`${tmpPath}/${file.filename}`)

      http
        .get(`${pubsweetServer}${file.url}`, response => {
          const stream = response.pipe(fileOnDisk)
          stream.on('finish', () => resolve(true))
        })
        .on('error', err => {
          // Handle errors
          reject(err)
        })
    })
    promises.push(promise)
  })
  return Promise.all(promises)
}

function createManifest(tmpPath, manuscript) {
  const order = []
  order.meta = 0
  order.manuscript = 1
  order.figure = 2
  order.table = 3
  order.supplement = 4
  order.PDF = 5

  manuscript.files.sort((file1, file2) => order[file1.type] - order[file2.type])

  let text = `meta\t\t${manuscript.id}.xml\n`
  manuscript.files
    .filter(file => file.type !== 'metadata')
    .forEach(file => {
      text += `${file.type}\t\t${file.label ? file.label : ''}\t\t${
        file.filename
      }\n`
    })
  return new Promise((resolve, reject) => {
    fs.writeFile(`${tmpPath}/manifest.txt`, text, err => {
      if (err) {
        reject(err)
      }
      resolve(true)
    })
  })
}

function compress(tmpPath, manuscript, datedFolder) {
  return new Promise((resolve, reject) => {
    const dest = `${ftpLocation}/${datedFolder}/${manuscript.id}.tar.gz`
    const cmd =
      `mkdir -p ${ftpLocation}/${datedFolder}; sleep 1 ;` +
      `find ${tmpPath} -printf "%P\n" -type f | tar --exclude='metadata.xml' --no-recursion -czf ${dest} -C ${tmpPath} -T -`
    exec(cmd, err => {
      if (err) {
        // node couldn't execute the command
        logger.error(err)
        reject(err)
      }
      logger.info(`Created package ${dest} to tagger FTP location.`)
      resolve(dest)
    })
  })
}

process.on('unhandledRejection', (reason, promise) => {
  logger.error(`Unhandled Rejection at: ${reason.stack || reason}`)
  tidyUp()
})

function tidyUp(tmpPath) {
  try {
    const cmd = `rm -rf ${tmpPath}`
    return new Promise((resolve, reject) => {
      exec(cmd, (err, stdout, stderr) => {
        if (err) {
          // node couldn't execute the command
          reject(err)
        }
        // logger.info(`Finished process`)
        resolve(true)
      })
    })
  } catch (e) {
    logger.error('Error while tidying up after another error!')
  }
}

module.exports.createPackageForTaggers = createPackageForTaggers
