require('dotenv').config()
const htmlDecode = require('decode-html')
const { ApolloClient } = require('apollo-boost')
const { InMemoryCache } = require('apollo-cache-inmemory')
const { createHttpLink } = require('apollo-link-http')
const gql = require('graphql-tag')
const fetch = require('node-fetch')
const uuidv4 = require('uuid/v4')
const { minioClient } = require('express-middleware-minio')
const Manuscript = require('../xpub-model/entities/manuscript/data-access')
const ManuscriptManager = require('../xpub-model/entities/manuscript/index')
const Journal = require('../xpub-model/entities/journal/data-access')
const Identity = require('../xpub-model/entities/identity/data-access')
const Organization = require('../xpub-model/entities/organization/data-access')
const logger = require('@pubsweet/logger')
// const FormData = require('form-data')
const lodash = require('lodash')
const mime = require('mime-types')
const fs = require('fs')
const tmp = require('tmp')
const chokidar = require('chokidar')
const xpath = require('xpath')
const Dom = require('xmldom').DOMParser
const path = require('path')
const { exec } = require('child_process')
const config = require('config')
const rfr = require('rfr')

const xsweetConverter = rfr('server/xsweet-conversion')
const { bulkUploaderEmail } = rfr('server/email')
// global.globalPath = ''
// global.globalTmpPath = ''
// global.manId = ''
global.token = ''

// Initialize watcher.
const rootPath = `${process.env.HOME}/${config.get('ftp_directory')}/`
const pubsweetServer = config.get('pubsweet-server.baseUrl')
const link = createHttpLink({ uri: `${pubsweetServer}/graphql`, fetch })
const graphqlClient = new ApolloClient({
  cache: new InMemoryCache(),
  link,
})
const ftpUsers = config.get('users')
const ignoreTaggerFolder = `${rootPath}${config.get('ftp_tagger').username}/*`

const watcher = chokidar.watch(`${rootPath}**/*.tar.gz`, {
  persistent: true,
  ignored: ignoreTaggerFolder,
  awaitWriteFinish: {
    stabilityThreshold: 2000,
    pollInterval: 100,
  },
})

async function tidyUp(filePath, tmpPath, manuscript, isError) {
  try {
    const emsid = manuscript ? manuscript.id : 'UNKNOWN'
    let parentDir = filePath

    // Issue 162
    // go up directories until you find the ftp root path. This is where the dated folder should be created.
    do {
      parentDir = path.dirname(parentDir)
    } while (`${path.dirname(parentDir)}/` !== rootPath)

    const fileName = path.basename(filePath)
    const fileNameExt = isError ? 'ERROR.' : `loaded.mid-${emsid}.`
    const cmd = `mkdir -p ${parentDir}/$(date +%Y-%m-%d); mv ${filePath} ${parentDir}/$(date +%Y-%m-%d)/${fileName}.${fileNameExt}$(date +%Y-%m-%d) ; rm -rf ${tmpPath}`
    return new Promise((resolve, reject) => {
      exec(cmd, (err, stdout, stderr) => {
        if (err) {
          // node couldn't execute the command
          reject(err)
        }
        logger.info(
          `Finished Bulk import process for ${filePath} as manuscript ${
            manuscript.id
          }.`,
        )
        resolve(true)
      })
    })
  } catch (e) {
    logger.error('Error while tidying up after another error!')
  }
}

process
  .on('uncaughtException', err => {
    if (err.submitter) {
      bulkUploaderEmail(err.submitter.email, err.message)
    }
    logger.error('whoops! there was an error', err.stack)
    tidyUp(err.filePath, err.tmpPath, err.manId, true)
  })
  .on('unhandledRejection', (reason, promise) => {
    if (reason.submitter) {
      bulkUploaderEmail(reason.submitter.email, reason.message)
    }
    logger.error(`Unhandled Rejection at: ${reason.stack || reason}`)
    tidyUp(reason.filePath, reason.tmpPath, reason.manId, true)
  })

// Something to use when events are received.
// const log = console.log.bind(console)
// Add event listeners.
watcher
  // .on('add', path => logger.info(`File ${path} has been added`))
  .on('add', path => runProcess(path))
  .on('change', path => logger.debug(`File ${path} has been changed`))
  .on('unlink', path => logger.debug(`File ${path} has been removed`))

// More possible events.
watcher
  .on('addDir', path => logger.debug(`Directory ${path} has been added`))
  .on('unlinkDir', path => logger.debug(`Directory ${path} has been removed`))
  .on('error', error => logger.error(`Watcher error: ${error}`))
  .on('ready', () =>
    logger.info(
      `Initial scan complete. Ready for changes under folder ${rootPath}`,
    ),
  )
  .on('raw', (event, path, details) => {
    logger.debug('Raw event info:', event, path, details)
  })

// 'add', 'addDir' and 'change' events also receive stat() results as second
// argument when available: http://nodejs.org/api/fs.html#fs_class_fs_stats
watcher.on('change', (path, stats) => {
  if (stats) logger.debug(`File ${path} changed size to ${stats.size}`)
})

function getUser(rootPath, filename) {
  return new Promise((resolve, reject) => {
    // const userName = path.basename(path.dirname(filename))
    const rootParts = rootPath.split('/')
    const fileNameParts = filename.split('/')
    const userName = fileNameParts[rootParts.length - 1]
    // We will need the userId from the database for further relating the manuscript to this user.
    Identity.findByFieldEager(
      'meta',
      `{"ftpUserName": "${userName}"}`,
      '[user]',
    )
      .then(identityDb => {
        const user = ftpUsers.find(user =>
          user.identities
            .filter(identity => identity.meta)
            .find(identity => identity.meta.ftpUserName === userName),
        )
        const identity = user.identities
          .filter(identity => identity.meta)
          .find(identity => identity.meta.ftpUserName === userName)
        /* eslint-disable dot-notation */
        identity['userId'] = identityDb[0].userId
        resolve(identity)
      })
      .catch(error => reject(error))
  })
}

function runProcess(packagePath) {
  logger.info(`Starting Bulk Import Process for ${packagePath}`)
  // global.globalPath = packagePath
  const packageName = path.basename(packagePath)
  getUser(rootPath, packagePath).then(submitter =>
    authenticate(pubsweetServer, submitter).then(token =>
      createTempDir().then(tmpPath =>
        extractFiles(packagePath, tmpPath).then(tmpPath =>
          parseManifest(tmpPath, packageName, submitter).then(parsedInfo =>
            parseXml(parsedInfo.metaXml).then(
              async xmlObj => {
                // convertFromInk(pubsweetServer, parsedInfo, token).then(
                //   async convertedManuscript => {
                const manuscriptDb = await createManuscript(
                  pubsweetServer,
                  xmlObj,
                  parsedInfo,
                  // convertedManuscript,
                  tmpPath,
                  submitter,
                  packageName,
                  error => {
                    logger.error(error)
                    tidyUp(packagePath, tmpPath, null, true)
                  },
                )
                tidyUp(packagePath, tmpPath, manuscriptDb)
              },
              // ),
            ),
          ),
        ),
      ),
    ),
  )
}

function createTempDir() {
  return new Promise((resolve, reject) => {
    tmp.dir({ mode: '0750', prefix: 'xpubTmpDir_' }, (err, tmpPath) => {
      if (err) reject(err)

      // global.globalTmpPath = tmpPath
      resolve(tmpPath)
    })
  })
}

function extractFiles(source, dest) {
  return new Promise((resolve, reject) => {
    const cmd = `tar vzxf ${source} -C ${dest} --xform='s#^.+/##x'` // the xform removes all directories
    exec(cmd, err => {
      if (err) {
        // node couldn't execute the command
        reject(err)
      }
      resolve(dest)
    })
  })
}

function parseManifest(tempFolder, packageName = path, submitter) {
  const files = []
  let metaFile = ''
  // let manFile = ''

  const lineReader = require('readline').createInterface({
    input: require('fs').createReadStream(`${tempFolder}/manifest.txt`, {
      encoding: 'UTF-8',
    }),
  })

  return new Promise((resolve, reject) => {
    lineReader
      .on('line', line => {
        if (line) {
          const row = line.split(/\t{1,}/).filter(x => x) // return only non empty tokens
          let supplement_info
          let supplement_filename
          let metaxml_filename
          let manuscript_filename

          if (row[0] === 'bulksub_meta_xml') {
            metaxml_filename = row[row.length - 1] // always take the last token
            if (
              !metaxml_filename ||
              !fs.existsSync(`${tempFolder}/${metaxml_filename}`)
            ) {
              const error = new Error(
                `Error in package ${packageName}. File ${metaxml_filename} does not exist.`,
              )
              error.submitter = submitter
              reject(error)
            } else {
              metaFile = `${tempFolder}/${metaxml_filename}`
            }
          } else if (row[0] === 'manuscript') {
            manuscript_filename = row[row.length - 1]
            if (
              !manuscript_filename ||
              !fs.existsSync(`${tempFolder}/${manuscript_filename}`)
            ) {
              const error = new Error(
                `Error in package ${packageName}. File ${manuscript_filename} does not exist.`,
              )
              error.submitter = submitter
              reject(error)
            } else {
              files.push({
                fileURI: `${tempFolder}/${manuscript_filename}`,
                filename: manuscript_filename,
                type: 'manuscript',
              })
            }
          } else if (
            row[0] === 'supplement' ||
            row[0] === 'figure' ||
            row[0] === 'table'
          ) {
            // get last element
            supplement_filename = row.pop()
            if (row.length > 1) {
              // get previous element only if it is not the first
              supplement_info = row.pop()
            }
            if (
              !supplement_filename ||
              !fs.existsSync(`${tempFolder}/${supplement_filename}`)
            ) {
              const error = new Error(
                `Error in package ${packageName}. File ${supplement_filename} does not exist.`,
              )
              error.submitter = submitter
              reject(error)
            } else {
              files.push({
                fileURI: `${tempFolder}/${supplement_filename}`,
                filename: supplement_filename,
                type: row[0],
                info: supplement_info,
              })
            }
          } else {
            const error = new Error(
              `Something is wrong with the manifest.txt file in package ${packageName}`,
            )
            error.submitter = submitter
            reject(error)
          }
        }
      })
      .on('close', () => {
        const result = {
          metaXml: metaFile,
          files,
        }
        resolve(result)
      })
  })
}

function parseXml(xmlFile) {
  return new Promise((resolve, reject) => {
    fs.readFile(xmlFile, 'utf8', (err, data) => {
      if (err) {
        reject(err)
      }
      const doc = new Dom().parseFromString(data)

      // manuscript nodeset
      const manuscript_nodeset = xpath.select1('/nihms-submit/manuscript', doc)

      // let manuscript_pmid_attr = '0'
      // try {
      //   manuscript_pmid_attr = xpath.select1('@pmid', manuscript_nodeset).value
      // } catch (e) {
      //   //do nothing
      // }
      //
      let manuscript_embargo_attr
      try {
        manuscript_embargo_attr = xpath.select1('@embargo', manuscript_nodeset)
          .value
      } catch (ignore) {
        // ignore
      }

      let permissions

      try {
        permissions = xpath.select1('/nihms-submit/permissions', doc).toString()
      } catch (ignore) {
        // ignore
      }

      const journal_meta_nodeset = xpath.select1(
        '/nihms-submit/journal-meta',
        doc,
      )

      const journal_title_val = xpath.select1(
        'journal-title',
        journal_meta_nodeset,
      ).firstChild.data

      const journal_nlmta = xpath.select1(
        'journal-id[@journal-id-type="nlm-ta"]',
        journal_meta_nodeset,
      ).firstChild.data

      /*
      //     <issn pub-type="ppub">2469-7311</issn>
      //     <issn pub-type="epub">2469-7303</issn>
      const issns = []
      xpath.select('issn', journal_meta_nodeset)
        .forEach(issn => {
          issns.push({
            value: issn.firstChild.data,
            'pub-type': xpath.select1('@pub-type', issn).value
          })
        })
*/
      //
      // // pubdate_nodeset
      // const pubdate_nodeset = xpath.select1(
      //   '/nihms-submit/article-meta/pub-date[@pub-type="ppub"]',
      //   doc,
      // )
      // let pub_day = '0'
      // let pub_month = '0'
      // let pub_year = '0'
      //
      // try {
      //   pub_day = 1 //parseInt(xpath.select1('day', pubdate_nodeset).firstChild.data)
      //   pub_month = getMonthFromString(
      //     xpath.select1('month', pubdate_nodeset).firstChild.data,
      //   )
      //   pub_year = xpath.select1('year', pubdate_nodeset).firstChild.data
      // } catch (e) {
      //   //do nothing
      // }
      //
      //
      // let manuscript_doi_attr = ''
      // try {
      //   manuscript_pmid_attr = xpath.select1('@doi', manuscript_nodeset).value
      // } catch (e) {
      //   //do nothing
      // }
      //
      // contacts nodeset
      const reviewers = []
      xpath
        .select('/nihms-submit/contacts/person', doc)
        .forEach(personElement => {
          const email = xpath.select1('@email', personElement).value
          const corrpi = xpath.select1('@corrpi="yes"', personElement)
          if (email && !reviewers.find(reviewer => reviewer.email === email)) {
            reviewers.push({
              givenNames: `${xpath.select1('@fname', personElement).value}`,
              surname: `${xpath.select1('@lname', personElement).value}`,
              email,
              corrpi,
            })
          }
        })

      //
      // const person_email_val = xpath.select1('@email', first_person_element)
      //   .value
      // const person_fname_val = xpath.select1('@fname', first_person_element)
      //   .value
      // const person_lname_val = xpath.select1('@lname', first_person_element)
      //   .value
      // --------------------

      // title nodeset
      const title_value = lodash.escape(
        xpath.select1('/nihms-submit/title', doc).firstChild.data,
      )

      const xmlObj = {
        title: title_value,
        // articleId: [{ type: 'pmid', id: manuscript_pmid_attr }],
        // publicationDate: [{ day: pub_day, month: pub_month, year: pub_year }],
        journal: journal_title_val,
        journal_nlmta,
        embargo: manuscript_embargo_attr,
        reviewers,
        permissions,
      }
      resolve(xmlObj)
    })
  })
}

function authenticate(pubsweetServer, user) {
  const { email, password } = user
  return new Promise((resolve, reject) =>
    graphqlClient
      .mutate({
        mutation: gql`
          mutation {
            epmc_signinUser(
              input: { email: "${email}", password: "${password}" }
            ) {
              token
            }
          }
        `,
      })
      .then(data => resolve(data.data.epmc_signinUser.token))
      .catch(error => reject(error)),
  )
}

function uploadFile(
  filename,
  originalFileName,
  extension,
  manuscript,
  filePath,
  item_size,
  mimeType,
  fileType,
  fileLabel,
  submitter,
) {
  return new Promise((resolve, reject) => {
    minioClient.uploadFile(
      filename,
      originalFileName,
      mimeType,
      filePath,
      (error, etag) => {
        if (error) {
          fs.unlinkSync(filePath)
          logger.error(error)
          error.submitter = submitter
          reject(error)
        } else {
          logger.debug(`${originalFileName} file uploaded to minio`)
          // fs.unlinkSync(filePath)
          const uploadedFile = {
            url: `/download/${filename}`,
            size: item_size,
            filename: originalFileName,
            label: fileLabel,
            type: fileType,
            mimeType,
            updatedBy: submitter.userId,
          }
          manuscript.files.push(uploadedFile)
          resolve(manuscript)
        }
      },
    )
  })
}

async function submitManuscript(manuscriptdb, submitter) {
  const input = {
    id: manuscriptdb.id,
    status: 'submitted',
  }
  const { userId } = submitter
  await ManuscriptManager.submit(input, userId)
}

async function createManuscript(
  pubsweetServer,
  xmlObj,
  parsedInfo,
  // htmlManuscript,
  tmpPath,
  submitter,
  packageName,
) {
  const manuscript = {
    'meta,title': htmlDecode(xmlObj.title),
    'meta,releaseDelay': xmlObj.embargo,
    updatedBy: submitter.userId,
    status: 'in-review',
    notes: [],
    files: [],
  }

  /* eslint-disable dot-notation */
  const organisation = await Organization.all()
  manuscript['organizationId'] = organisation[0].id
  const journal = await Journal.findByField('meta,nlmta', xmlObj.journal_nlmta)
  // audrey (20190122): I think if there is not a single NLM TA match you should have the journal as unmatchedJournal
  // and let the helpdesk figure it out
  if (journal === undefined || journal.length !== 1) {
    manuscript['meta,unmatchedJournal'] = xmlObj.journal
  } else {
    manuscript['journalId'] = journal[0].id
  }

  if (xmlObj.permissions) {
    const fileURI = `${tmpPath}/metadata.xml`
    await fs.writeFileSync(fileURI, xmlObj.permissions)
    parsedInfo.files.push({
      fileURI,
      filename: 'metadata.xml',
      type: 'metadata',
    })
  }

  if (xmlObj.reviewers) {
    let xmlReviewer
    if (xmlObj.reviewers.length === 1) {
      // if just one contact, keep this one
      ;[xmlReviewer] = xmlObj.reviewers
    } else {
      const corrpis = xmlObj.reviewers.filter(reviewer => reviewer.corrpi)
      if (corrpis && corrpis.length > 0) {
        ;[xmlReviewer] = corrpis // keep the first corrpi
      } else {
        // if there are not corrpis
        ;[xmlReviewer] = xmlObj.reviewers // keep the first contact
      }
    }
    const reviewer = {
      email: xmlReviewer.email,
      name: {
        givenNames: xmlReviewer.givenNames,
        surname: xmlReviewer.surname,
      },
    }

    if (!validateEmail(xmlReviewer.email)) {
      const error = new Error(
        `Reviewer's email is not valid: ${
          xmlReviewer.email
        } in package ${packageName}`,
      )
      error.submitter = submitter
      throw error
    }

    const identities = await Identity.findByField('email', xmlReviewer.email)
    if (identities && identities.length > 0) {
      // identity exists
      reviewer.id = identities[0].userId
    }
    const noteData = {
      notesType: 'selectedReviewer',
      content: JSON.stringify(reviewer),
      updatedBy: submitter.userId,
    }
    manuscript.notes.push(noteData)
  }

  const promises = []

  /*
  files.push({
    fileURI: `${tempFolder}/${supplement_filename}`,
    filename: supplement_filename,
    type: 'figure',
    info: supplement_info,
  })
   */

  for (let i = 0; i < parsedInfo.files.length; i += 1) {
    const fileInfo = parsedInfo.files[i]
    const {
      fileURI: file,
      filename,
      type: fileType,
      info: fileLabel,
    } = fileInfo
    const stats = fs.statSync(file)
    const fileSizeInBytes = stats.size
    const extension = path.extname(file)
    const mimeType = mime.contentType(extension)
    const uuid = uuidv4()
    promises.push(
      uploadFile(
        `${uuid}${extension}`,
        filename,
        extension,
        manuscript,
        file,
        fileSizeInBytes,
        mimeType,
        fileType,
        fileLabel,
        submitter,
      ),
    )
  }

  /*
  if (htmlManuscript) {
    const fileURI = `${tmpPath}/preview.html`
    promises.push(
      fs.writeFile(fileURI, htmlManuscript, err => {
        if (err) throw err
        const stats = fs.statSync(tmpPath)
        const fileSizeInBytes = stats.size
        const uuid = uuidv4()
        promises.push(
          uploadFile(
            `${uuid}.html`,
            'preview.html',
            'html',
            manuscript,
            fileURI,
            fileSizeInBytes,
            'text/html',
            'source',
            'HTML preview',
            submitter,
          ),
        )
      }),
    )
  }
*/

  let manuscriptdb
  await Promise.all(promises)
    .catch(error => {
      logger.error(error)
    })
    .then(async () => {
      manuscriptdb = await new Manuscript(manuscript).saveWithUserAndRole(
        {
          relate: true,
          unrelate: true,
        },
        submitter.userId,
        'submitter',
      )

      const { files } = parsedInfo
      const manuscriptFileURI = files.filter(
        file => file.type === 'manuscript',
      )[0].fileURI

      await xsweetConverter.xsweetConvert(manuscriptFileURI, manuscriptdb.id)
      await submitManuscript(manuscriptdb, submitter)
    })
  return manuscriptdb
}

/*
function convertFromInk(pubsweetServer, parsedInfo, token) {
  const manuscript = parsedInfo.files.find(file => file.type === 'manuscript')
  if (manuscript && manuscript.filename.endsWith('.docx')) {
    const url = `${pubsweetServer}/api/ink?recipe=editoria-typescript`
    const form = new FormData()

    const readStream = fs.createReadStream(manuscript.fileURI)
    form.append('file', readStream)
    return new Promise((resolve, reject) => {
      fetch(url, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: form,
      })
        .then(response => response.json())
        .then(data => resolve(data.converted))
        .catch(err => {
          logger.error(err)
          resolve(null)
        })
    })
  }
  return new Promise((resolve, reject) => {
    resolve(null)
  })
}
*/

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}
