<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [FTP integration](#ftp-integration)
  - [Directory monitoring utility](#directory-monitoring-utility)
  - [FTP container](#ftp-container)
  - [FTP user creation](#ftp-user-creation)
    - [FTP user for tagging](#ftp-user-for-tagging)
  - [Database user creation](#database-user-creation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# FTP integration

## Directory monitoring utility
The monitoring utility server/ftp-integration/api.js monitors a local folder for changes. 
The folder path is configured with an `ftp_root` configuration parameter, currently defined in the 
development.js as ftpdata under the user's home:
 
`ftp_root: 'ftpdata'`

This location should be mapped to the FTP path in the corresponding container. 
The extract below is taken from docker-compose.yml:

```
volumes: 
    - "~/ftpdata/:/home/ftpusers/"
    - "~/passwd:/etc/pure-ftpd/passwd"
```

The utility runs automatically with the server is started up (`yarn server`) or manually by ```node server/ftp-integration/api.js ```. 

## FTP container
The configuration for the the FTP container can be found in the docker-compose.yml. 
Basically, it maps the ftp location to a local folder in the hosting maching, like showing above. 

## FTP user creation
To create an FTP user we first need to open a bash in the container:

`docker exec -it xpubepmc_ftpd_server_1 bash`

and then create the user (e.g. 'ieee'):

`pure-pw useradd ieee -u ftpuser -d /home/ftpusers/ieee -m`

This will create user `ieee` under the default ftp user group `ftpuser` with a pertinent home directory in the ftp container. 
The container configuration maps this to a location on the application container so that it will be made available in the application.
The `-m` flag commits the changes.
We will be asked for the password (twice).

More information, along with more commands for managing users, can be found here: https://github.com/stilliard/docker-pure-ftpd and https://download.pureftpd.org/pure-ftpd/doc/README.Virtual-Users 

### FTP user for tagging
Part of the application is the construction of a tar package for every manuscript to be send 
to the taggers. We need to create an ftp account, like above, so that the packages will be made 
available to that location for the tagger to download from.

`pure-pw useradd taggers -m -u ftpuser -d /home/ftpusers/taggers`

The `ftp_tagger: 'taggers'` value (currently in `development.js`) should match this FTP username.

## Database user creation
The uploaded manuscripts should belong to PubSweet users. 
Therefore, we need to create them. This can be accomplished by 
using the user seeding feature described in the [main README file](../../../README.md). 
They should have the same username as the ftp users. Note that these are not the FTP credentials. 
 The FTP accounts are created separately on the FTP service (container). The FTP Bulk upload service
 will monitor the "ftp_root" local folder for changes (file uploads).