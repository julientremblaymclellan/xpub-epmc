const AnnotationManager = require('../xpub-model/entities/annotation')
const { entityToRow } = require('../xpub-model/entities/util')
const authorization = require('pubsweet-server/src/helpers/authorization')

const columnNames = ['id', 'quote', 'text', 'ranges']

module.exports = app => {
  const authBearer = app.locals.passport.authenticate('bearer', {
    session: false,
  })
  const ANNOTATIONS_URI = '/annotations'
  // GET single annotation
  app.get(`${ANNOTATIONS_URI}/:id`, authBearer, (req, res, next) => {
    authorization
      .can(req.user, 'read', { id: req.params.id, type: 'Annotation' })
      .then(result => {
        AnnotationManager.find(req.params.id)
          .then(annotation => {
            const row = entityToRow(annotation, columnNames)
            res.status(200).json(row)
          })
          .catch(error => {
            next(error)
          })
      })
      .catch(error => {
        next(error)
      })
  })

  // GET review annotations
  app.get(`${ANNOTATIONS_URI}/review/:id`, authBearer, (req, res, next) => {
    authorization
      .can(req.user, 'read', { id: req.params.id, type: 'Review' })
      .then(result => {
        AnnotationManager.findByReviewId(req.params.id)
          .then(annotations => {
            const rows = annotations.map(a => entityToRow(a, columnNames))
            res.status(200).json({ rows })
          })
          .catch(error => {
            next(error)
          })
      })
      .catch(error => {
        next(error)
      })
  })

  // GET file annotations
  app.get(`${ANNOTATIONS_URI}/file/:file`, authBearer, (req, res, next) => {
    authorization
      .can(req.user, 'read', { id: req.params.file, type: 'File' })
      .then(result => {
        AnnotationManager.findByFileId(req.params.file)
          .then(annotations => {
            const rows = annotations.map(a => entityToRow(a, columnNames))
            res.status(200).json({ rows })
          })
          .catch(error => {
            next(error)
          })
      })
      .catch(error => {
        next(error)
      })
  })

  // POST annotation
  app.post(`${ANNOTATIONS_URI}`, (req, res, next) => {
    authorization
      .can(req.user, 'create', { id: null, type: 'Annotation', ...req.body })
      .then(result => {
        AnnotationManager.save(req.body)
          .then(annotation => {
            const row = entityToRow(annotation, columnNames)
            res.status(200).json(row)
          })
          .catch(error => {
            next(error)
          })
      })
      .catch(error => {
        next(error)
      })
  })

  // PUT annotation
  app.put(`${ANNOTATIONS_URI}/:id`, authBearer, (req, res, next) => {
    authorization
      .can(req.user, 'update', { id: req.params.id, type: 'Annotation' })
      .then(result => {
        AnnotationManager.save(req.body)
          .then(annotation => {
            const row = entityToRow(annotation, columnNames)
            res.status(200).json(row)
          })
          .catch(error => {
            next(error)
          })
      })
      .catch(error => {
        next(error)
      })
  })

  // DELETE annotation
  app.delete(`${ANNOTATIONS_URI}/:id`, authBearer, (req, res, next) => {
    authorization
      .can(req.user, 'delete', { id: req.params.id, type: 'Annotation' })
      .then(result => {
        AnnotationManager.delete(req.params.id)
          .then(() => {
            res.status(200).json({ message: 'deleted successfully' })
          })
          .catch(error => {
            next(error)
          })
      })
      .catch(error => {
        next(error)
      })
  })
}
