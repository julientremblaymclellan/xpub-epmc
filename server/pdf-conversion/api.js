const logger = require('@pubsweet/logger')
const fs = require('fs')
const uuidv4 = require('uuid/v4')
require('dotenv').config()
const https = require('https')
const { minioClient } = require('express-middleware-minio')
const config = require('config')
const fetch = require('node-fetch')
// const ManuscriptManager = require('../xpub-model/entities/manuscript')
const Manuscript = require('../xpub-model/entities/manuscript/data-access')
// const FileManager = require('../xpub-model/entities/file')

const pubsweetServer = config.get('pubsweet-server.baseUrl')
const pdfTransformerApi = config.get('ncbiPdfTransformerApi')

;(async () => {
  const beforeUpdate = Date.now()
  await getDeposits()
  Manuscript.knex().destroy()
  logger.info(
    `PDF conversion process finished in ${Date.now() - beforeUpdate} ms`,
  )
})()

// runMonitor()

/*
async function runMonitor() {
  /!* eslint-disable no-await-in-loop *!/
  while (true) {
    getDeposits()
    await sleep(pdfTransformerApi.freq)
  }
}

function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms)
  })
}
*/

async function getDeposits() {
  logger.info('Polling our database for manuscripts to be converted to PDF...')

  await Manuscript.findByFieldEager(
    'pdf_deposit_state',
    'WAITING_FOR_PDF_CONVERSION',
    'files',
  ).then(async resultSet => {
    /* eslint-disable no-await-in-loop */
    /* eslint-disable no-restricted-syntax */
    for (const manuscript of resultSet) {
      await deposit(manuscript)
    }
  })

  logger.info('Polling NCBI for replies from the PDF conversion service...')
  const manuscript = new Manuscript()
  const pdfDepositStates = ['COMMITTED', 'WAITING_FOR_RESULT']

  await manuscript
    .findByFieldValuesIn('pdf_deposit_state', pdfDepositStates, 'files')
    .then(async resultSet => {
      /* eslint-disable no-await-in-loop */
      /* eslint-disable no-restricted-syntax */
      for (const manuscript of resultSet) {
        logger.info(`Check status of conversion: ${manuscript.id}`)
        await ncbiApiCall(
          '/deposits',
          'GET',
          null,
          `/${manuscript.pdfDepositId}`,
        ).then(async ncbiResponse => {
          await processDeposit(manuscript, ncbiResponse)
        })
      }
    })
}

function getFile(url, dest, cb) {
  return new Promise((resolve, reject) => {
    const file = fs.createWriteStream(dest)
    https
      .get(url, response => {
        const stream = response.pipe(file)
        stream.on('finish', () => {
          resolve(true)
        })
        /*

      await response.pipe(file)
      await file.on('finish', async () => {
        await file.close(cb) // close() is async, call cb after close completes.
      })
*/
      })
      .on('error', err => {
        // Handle errors
        fs.unlink(dest) // Delete the file async. (But we don't check the result)
        reject(err)
      })
  })
}

async function uploadToMinio(filename, manuscript, filePath, item_size, item) {
  const Role = require('../xpub-model/entities/role/data-access')
  const roles = await Role.findByFieldEager('name', 'admin', 'users')
  const adminUser = roles[0].users[0] // get the first available admin
  await minioClient
    .uploadFile(
      filename,
      `${manuscript.id}.pdf`,
      'application/pdf',
      filePath,
      async (error, etag) => {
        if (error) {
          await fs.unlink(filePath, () => {
            manuscript.pdfDepositState = 'RESULT_FAILED_TO_BE_RETRIEVED'
            new Manuscript(manuscript).save()
          })
          logger.error(error)
        }
      },
    )
    .then(async () => {
      logger.info('PDF file uploaded to minio')
      // todo: need to delete this but minio still needs it
      // await fs.unlink(filePath, async () => {
      const pdf4print = 'pdf4print'

      // delete it if already exists
      manuscript.files = manuscript.files.filter(
        file => file.type !== pdf4print,
      )
      const pdfFile = {
        manuscriptId: manuscript.id,
        url: `/download/${filename}`,
        size: item_size,
        filename: item,
        type: pdf4print,
        label: 'PDF Conversion',
        mimeType: 'application/pdf',
        updatedBy: adminUser.id,
      }
      manuscript.files.push(pdfFile)
      manuscript.pdfDepositState = 'RESULT_RETRIEVED'
      await new Manuscript(manuscript).save().then(() => {
        logger.info(
          `PDF retrieved and saved successfully for manuscript ${
            manuscript.id
          }`,
        )
      })
    })
    .then(() => {
      logger.debug('fulfilled')
    })
  // })
}

async function processDeposit(manuscript, deposit) {
  logger.info(`NCBI conversion status for : ${manuscript.id}: ${deposit.state}`)
  if (deposit.state === 'RESULT_IS_READY') {
    logger.info(`PDF conversion is ready: ${manuscript.id}. Fetching...`)
    const pdfItem = deposit.items.filter(item => item.result)[0]
    const { item, s3_get_url, item_size } = pdfItem

    const uuidv = await uuidv4()
    const filename = `${uuidv}.pdf`
    const filePath = `/tmp/${filename}`
    await getFile(s3_get_url, filePath)
      .then(async () =>
        uploadToMinio(filename, manuscript, filePath, item_size, item),
      )
      .catch(async error => {
        manuscript.pdfDepositState = 'RESULT_FAILED_TO_BE_RETRIEVED'
        manuscript.formState = error.toString()
        if (manuscript.status === 'tagging') {
          manuscript.status = 'xml-triage'
        }
        await new Manuscript(manuscript).save()
        logger.error(error)
      })
  } else if (deposit.state === 'NO_RESULT') {
    manuscript.pdfDepositState = deposit.state
    manuscript.formState = deposit.details
    if (manuscript.status === 'tagging') {
      manuscript.status = 'xml-triage'
    }
    await new Manuscript(manuscript).save()
    logger.error(
      `Error getting result from PDF conversion service: ${deposit.details}`,
    )
  } else {
    logger.info(`Nothing found to be processed.`)
  }
}

/*
function deleteDeposit(deposit_id) {
  return ncbiApiCall('/deposits', 'DELETE', null, `/${deposit_id}`)
}
*/

function storeIds(manuscript, depositId, status) {
  manuscript.pdfDepositId = depositId
  manuscript.pdfDepositState = status
  return new Manuscript(manuscript).save()
}

async function deposit(manuscript) {
  if (manuscript.files) {
    logger.info(`Start depositing ${manuscript.id}`)
    const depositObj = {
      action: 'commit',
      depositor: 'ebi',
      domain: 'ukpmcpa',
      items: [],
    }
    manuscript.files
      .filter(
        file =>
          !file.deleted &&
          (file.type === 'PMCfinal' || file.type === 'IMGprint'),
      )
      .forEach(file => {
        depositObj.items.push({
          item: `${pubsweetServer}${file.url}`,
          item_size: file.size,
        })
      })

    await ncbiApiCall('/deposits/', 'POST', depositObj).then(
      async ncbiResponse => {
        logger.info(ncbiResponse)
        await storeIds(
          manuscript,
          ncbiResponse.deposit_id,
          ncbiResponse.state,
        ).then(databaseResponse => {
          logger.info(
            `Deposit done. Stored depositId: ${ncbiResponse.deposit_id}`,
          )
        })
      },
    )
  } else {
    logger.info(`No files to deposit for manuscript ${manuscript.id}`)
  }
}

function ncbiApiCall(uri, method, body, parameters) {
  let localParams = ''
  if (parameters) localParams = parameters
  const url = `${pdfTransformerApi.url}${uri}${localParams}`
  logger.info(`Calling ${url}`)
  return new Promise((resolve, reject) => {
    fetch(url, {
      method,
      body: body ? JSON.stringify(body) : null,
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then(response => response.json())
      .then(data => resolve(data))
      .catch(err => reject(err))
  })
}
