const FileManager = require('./entities/file')
const IdentityManager = require('./entities/identity')
const ManuscriptManager = require('./entities/manuscript')
const NoteManager = require('./entities/note')
const RoleManager = require('./entities/role')
const TeamManager = require('./entities/team')
const UserManager = require('./entities/user')
const JournalManager = require('./entities/journal')
const OrganizationManager = require('./entities/organization')
const AuditManager = require('./entities/audit')
const ReviewManager = require('./entities/review')

module.exports = {
  FileManager,
  NoteManager,
  IdentityManager,
  ManuscriptManager,
  RoleManager,
  TeamManager,
  UserManager,
  JournalManager,
  OrganizationManager,
  AuditManager,
  ReviewManager,
}
