const Team = require('./data-access')
const Note = require('../note/data-access')
const logger = require('@pubsweet/logger')

const TeamManager = {
  find: Team.selectById,
  findByUserIdOrManuscriptId: Team.selectByUserIdOrManuscriptId,
  selectByUserId: async id => Team.selectByUserId(id),
  selectByManuscriptId: async id => Team.selectByManuscriptId(id),
  addNewReviewer: async (noteId, userId) => {
    try {
      const note = await Note.selectById(noteId)
      if (note) {
        const { manuscriptId } = note
        await Team.insert(
          {
            manuscriptId,
            userId,
            roleName: 'reviewer',
          },
          userId,
        )
        await Note.delete(noteId, userId)
        return true
      }
    } catch (error) {
      logger.error('Set reviewer error: ', error)
      return false
    }
  },
  delete: Team.delete,
  save: async (team, userId) => {
    let { id } = team
    if (team.id) {
      const updated = await Team.update(team, userId)
      if (!updated) {
        throw new Error('team not found')
      }
    } else {
      id = await Team.insert(team, userId)
    }

    return { ...team, id }
  },
  modelName: 'EpmcTeam',

  model: Team,
}

module.exports = TeamManager
