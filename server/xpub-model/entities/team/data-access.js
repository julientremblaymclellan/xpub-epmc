const { Model } = require('objection')
const uuid = require('uuid')
const EpmcBaseModel = require('../epmc-base-model')
const { rowToEntity, entityToRow, buildQuery, runQuery } = require('../util')
const Manuscript = require('../manuscript/data-access')
const Role = require('../role/data-access')
const User = require('../user/data-access')

const columnNames = ['id', 'manuscript_id', 'user_id', 'role_name']

class Team extends EpmcBaseModel {
  static get tableName() {
    return 'team'
  }

  // static idColumn = ['article_id', 'member_id'];

  static get idColumn() {
    return ['manuscript_id', 'user_id', 'role_name']
  }

  /*
  static get dbRefProp() {
    return ['manuscript_id', 'user_id', 'role_name']
  }
*/

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        /*
        created: { type: 'timestamp' },
        updated: { type: 'timestamp' },
*/
        deleted: { type: 'timestamp' },
        manuscriptId: { anyOf: [{ type: 'string' }, { type: 'null' }] },
        userId: { type: 'uuid' },
        roleName: { type: 'string' },
        updatedBy: { type: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: Model.BelongsToOneRelation,
        modelClass: Manuscript,
        join: {
          from: 'team.manuscriptId',
          to: 'manuscript.id',
        },
      },
      roles: {
        relation: Model.BelongsToOneRelation,
        modelClass: Role,
        join: {
          from: 'team.roleName',
          to: 'role.name',
        },
      },
      users: {
        relation: Model.HasManyRelation,
        modelClass: User,
        join: {
          from: 'team.userId',
          to: 'users.id',
        },
      },
    }
  }

  static async selectById(id) {
    const rows = await runQuery(
      buildQuery
        .select()
        .from('team')
        .where({ id })
        .whereNull('deleted'),
    )
    if (!rows.length) {
      throw new Error('manuscript_team_user not found')
    }
    return rowToEntity(rows[0])
  }

  static async selectByManuscriptId(manId) {
    const manuscriptRoles = await Team.query()
      .where({
        'team.manuscript_id': manId,
      })
      .whereNull('team.deleted')
    return manuscriptRoles
  }

  static async selectByUserId(userId, manId) {
    const roles = await Team.query()
      .where({
        'team.user_id': userId,
      })
      .whereNull('team.deleted')

    return roles.map(team => ({
      id: team.id,
      created: team.created,
      updated: team.updated,
      role: team.roleName,
      objectId: team.manuscriptId ? team.manuscriptId : '',
      objectType: team.manuscriptId ? 'Manuscript' : 'Organization',
    }))
  }

  static async selectByUserIdOrManuscriptId(userId, manId) {
    let manuscriptRoles = []
    let organizationRoles = []
    if (manId) {
      manuscriptRoles = await Team.query()
        .where({
          'team.manuscript_id': manId,
          'team.user_id': userId,
        })
        .whereNull('team.deleted')
    }
    organizationRoles = await Team.query()
      .where({
        'team.user_id': userId,
      })
      .whereNull('team.manuscript_id')
      .whereNull('team.deleted')
    return organizationRoles.concat(manuscriptRoles)
  }

  static async selectAll() {
    const rows = await runQuery(
      buildQuery
        .select()
        .from('team')
        .whereNull('deleted'),
    )
    return rows.map(rowToEntity)
  }

  static async insert(team, userId) {
    const row = entityToRow(team, columnNames)
    row.id = uuid.v4()
    row.updated_by = userId
    const query = buildQuery.insert(row).into('team')
    await runQuery(query)
    return row.id
  }

  static update(team, userId) {
    const row = entityToRow(team, columnNames)
    row.updated_by = userId
    const query = buildQuery
      .update(row)
      .table('team')
      .where('id', team.id)
      .whereNull('deleted')
    return runQuery(query)
  }

  static delete(id, userId) {
    return Team.query().updateAndFetchById(id, {
      deleted: new Date().toISOString(),
      updatedBy: userId,
    })
  }
}
module.exports = Team
