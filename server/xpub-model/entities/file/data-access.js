const { Model } = require('objection')
const uuid = require('uuid')
const EpmcBaseModel = require('../epmc-base-model')
const { rowToEntity, entityToRow, buildQuery, runQuery } = require('../util')

const columnNames = [
  'manuscript_id',
  'filename',
  'label',
  'type',
  'mime_type',
  'size',
  'url',
]

class File extends EpmcBaseModel {
  static get tableName() {
    return 'file'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        manuscriptId: { type: 'string' },
        filename: { type: 'string' },
        mimeType: { type: 'string' },
        type: { type: 'string' },
        size: { type: 'int' },
        url: { type: 'string' },
        label: { type: ['string', 'null'] },
        deleted: { type: 'timestamp' },
        updatedBy: { type: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    const Manuscript = require('../manuscript/data-access')
    return {
      manuscript: {
        relation: Model.BelongsToOneRelation,
        modelClass: Manuscript,
        join: {
          from: 'file.manuscriptId',
          to: 'manuscript.id',
        },
      },
    }
  }

  static async selectById(id) {
    const rows = await runQuery(
      buildQuery
        .select()
        .from('file')
        .where({ id })
        .whereNull('deleted'),
    )
    if (!rows.length) {
      throw new Error('File not found')
    }
    return rowToEntity(rows[0])
  }

  static async selectByManuscriptId(manId) {
    const rows = await runQuery(
      buildQuery
        .select()
        .from('file')
        .where({ 'file.manuscript_id': manId })
        .whereNull('file.deleted'),
    )
    return rows.map(rowToEntity)
  }

  static async selectByManIdConvertedFiles(manId) {
    const rows = await runQuery(
      buildQuery
        .select()
        .from('file')
        .where({ 'file.manuscript_id': manId })
        .andWhere('file.type', 'in', ['tempHTML', 'PMCfinal'])
        .whereNull('file.deleted'),
    )
    return rows.map(rowToEntity)
  }

  static async updateFileUrl(id, fileurl) {
    const fileUpdated = File.query()
      .patch({ url: fileurl })
      .where('id', id)
    return fileUpdated
  }

  static async selectAll() {
    const rows = await runQuery(
      buildQuery
        .select()
        .from('file')
        .whereNull('deleted'),
    )
    return rows.map(rowToEntity)
  }

  static async insert(file, userId) {
    const row = entityToRow(file, columnNames)
    row.updated_by = userId
    row.id = uuid.v4()
    const query = buildQuery.insert(row).into('file')
    await runQuery(query)
    return row.id
  }

  static update(file, userId) {
    const row = entityToRow(file, columnNames)
    row.updated_by = userId
    const query = buildQuery
      .update(row)
      .table('file')
      .where('id', file.id)
      .whereNull('deleted')
    return runQuery(query)
  }

  static delete(id, userId) {
    return File.query().updateAndFetchById(id, {
      deleted: new Date().toISOString(),
      updatedBy: userId,
    })
  }

  static deleteByManIdAndType(manuscript_id, type, userId) {
    return File.query()
      .update({
        deleted: new Date().toISOString(),
        updatedBy: userId,
      })
      .where({ manuscript_id, type })
      .whereNull('deleted')
  }

  static async deleteByManuscriptId(id, userId, trx) {
    await File.query(trx)
      .update({
        deleted: new Date().toISOString(),
        updatedBy: userId,
      })
      .where('manuscript_id', id)
      .whereNull('deleted')
  }

  static async deleteById(id, userId) {
    return File.query().updateAndFetchById(id, {
      deleted: new Date().toISOString(),
      updatedBy: userId,
    })
  }
}
module.exports = File
