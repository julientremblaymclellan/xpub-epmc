const lodash = require('lodash')
const FileAccess = require('./data-access')

// const dataAccess = new FileAccess()

const empty = {
  filename: '',
  label: '',
  type: 0,
  mime_type: '',
  size: 0,
  url: '',
}

const FileManager = {
  find: FileAccess.selectById,
  findByManuscriptId: FileAccess.selectByManuscriptId,
  delete: FileAccess.delete,
  deleteById: FileAccess.deleteById,
  deleteByManIdAndType: FileAccess.deleteByManIdAndType,
  new: (props = {}) => lodash.merge({}, empty, props),
  save: async (file, userId) => {
    let id = { file }
    if (file.id) {
      const updated = await FileAccess.update(file, userId)
      if (!updated) {
        throw new Error('File not found')
      }
    } else {
      id = await FileAccess.insert(file, userId)
    }

    return { ...file, id }
  },

  modelName: 'File',

  model: FileAccess,
}

module.exports = FileManager
