const { Model } = require('objection')
const logger = require('@pubsweet/logger')
const EpmcBaseModel = require('../epmc-base-model')

class Role extends EpmcBaseModel {
  static get tableName() {
    return 'role'
  }

  static get schema() {
    return {
      properties: {
        name: { type: 'string' },
        created: { type: 'timestamp' },
        updated: { type: 'timestamp' },
        organization: { type: 'boolean' },
      },
    }
  }

  static get idColumn() {
    return 'name'
  }

  static get dbRefProp() {
    return 'name'
  }

  static get relationMappings() {
    const Manuscript = require('../manuscript/data-access')
    const Team = require('../team/data-access')
    const User = require('../user/data-access')

    return {
      manuscripts: {
        relation: Model.ManyToManyRelation,
        modelClass: Manuscript,
        join: {
          from: 'role.name',
          // ManyToMany relation needs the `through` object
          // to describe the join table.
          through: {
            modelClass: Team,
            from: 'team.roleName',
            to: 'team.manuscriptId',
          },
          to: 'manuscript.user_id',
        },
      },
      users: {
        relation: Model.ManyToManyRelation,
        modelClass: User,
        join: {
          from: 'role.name',
          // ManyToMany relation needs the `through` object
          // to describe the join table.
          through: {
            modelClass: Team,
            from: 'team.roleName',
            to: 'team.userId',
          },
          to: 'users.id',
        },
      },
    }
  }

  static async selectByName(name) {
    const roles = await Role.query().where('name', name)
    logger.debug('roles: ', roles)
    return roles && roles.length > 0 ? roles[0] : null
  }

  static async selectAll() {
    const roles = await Role.query()
    logger.debug('roles: ', roles)
    return roles && roles.length > 0 ? roles[0] : null
  }

  static async selectByType(organization) {
    const roles = await Role.query().where('organization', organization)
    return roles
  }

  static async delete(name) {
    const numberOfDeletedRows = await Role.query()
      .delete()
      .where('name', name)
    logger.debug('numberOfDeletedRows: ', numberOfDeletedRows)
    return numberOfDeletedRows
  }
}

module.exports = Role
