const uuid = require('uuid')
const EpmcBaseModel = require('../epmc-base-model')
const { rowToEntity, entityToRow, buildQuery, runQuery } = require('../util')

const columnNames = ['name']

class Organization extends EpmcBaseModel {
  static get tableName() {
    return 'organization'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        created: { type: 'timestamp' },
        updated: { type: 'timestamp' },
        name: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {}
  }

  static async selectById(id) {
    const rows = await runQuery(
      buildQuery
        .select()
        .from('organization')
        .where({ id }),
    )
    if (!rows.length) {
      throw new Error('Organization not found')
    }
    return rowToEntity(rows[0])
  }

  static async selectAll() {
    return Organization.query()
  }

  static async insert(organization) {
    const row = entityToRow(organization, columnNames)
    row.id = uuid.v4()
    const query = buildQuery.insert(row).into('organization')
    await runQuery(query)
    return row.id
  }

  static update(organization) {
    const row = entityToRow(organization, columnNames)
    const query = buildQuery
      .update(row)
      .table('organization')
      .where('id', organization.id)
    return runQuery(query)
  }

  static delete(id) {
    return runQuery(
      buildQuery
        .delete()
        .from('organization')
        .where({ id }),
    )
  }
}
module.exports = Organization
