const { Model } = require('objection')
const uuid = require('uuid')
const EpmcBaseModel = require('../epmc-base-model')
const { rowToEntity, entityToRow, buildQuery, runQuery } = require('../util')

const columnNames = ['notes_type', 'content', 'manuscript_id']

class Note extends EpmcBaseModel {
  static get tableName() {
    return 'note'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        created: { type: 'timestamp' },
        updated: { type: 'timestamp' },
        deleted: { type: 'timestamp' },
        manuscriptId: { type: 'string' },
        content: { type: 'string' },
        notesType: { type: 'string' },
        updatedBy: { type: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    const Manuscript = require('../manuscript/data-access')
    const User = require('../user/data-access')
    return {
      manuscript: {
        relation: Model.BelongsToOneRelation,
        modelClass: Manuscript,
        join: {
          from: 'note.manuscriptId',
          to: 'manuscript.id',
        },
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'note.updated_by',
          to: 'users.id',
        },
      },
    }
  }

  static async selectById(id) {
    const rows = await runQuery(
      buildQuery
        .select()
        .from('note')
        .where({ id })
        .whereNull('deleted'),
    )
    if (!rows.length) {
      throw new Error('Note not found')
    }
    return rowToEntity(rows[0])
  }

  static async selectByManuscriptId(manId) {
    const notes = await Note.query()
      .where('manuscript_id', manId)
      .whereNull('deleted')
      .eager('user.identities')
    return notes
  }

  static async selectAll() {
    const rows = await runQuery(
      buildQuery
        .select()
        .from('note')
        .whereNull('deleted'),
    )
    return rows.map(rowToEntity)
  }

  static async insert(note, userId) {
    const row = entityToRow(note, columnNames)
    row.id = uuid.v4()
    row.updated_by = userId
    const query = buildQuery.insert(row).into('note')
    await runQuery(query)
    const manuscript = {
      id: note.manuscriptId,
      meta: {
        notes: [rowToEntity(row)],
      },
    }
    return manuscript
  }

  static async update(note, userId) {
    const row = entityToRow(note, columnNames)
    row.updated_by = userId
    const query = buildQuery
      .update(row)
      .returning('*')
      .table('note')
      .where('id', note.id)
      .whereNull('deleted')
    const newNote = await runQuery(query)
    const manuscript = {
      id: newNote[0].manuscriptId,
      meta: {
        notes: newNote,
      },
    }
    return manuscript
  }

  static delete(id, userId) {
    return Note.query().updateAndFetchById(id, {
      deleted: new Date().toISOString(),
      updatedBy: userId,
    })
  }

  static async deleteByManuscriptId(id, userId, trx) {
    await Note.query(trx)
      .update({
        deleted: new Date().toISOString(),
        updatedBy: userId,
      })
      .where('manuscript_id', id)
      .whereNull('deleted')
  }
}
module.exports = Note
