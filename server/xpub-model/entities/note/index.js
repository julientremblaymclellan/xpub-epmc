const Note = require('./data-access')

const NoteManager = {
  find: Note.selectById,
  findByManuscriptId: Note.selectByManuscriptId,
  delete: Note.delete,
  deleteByManuscriptId: Note.deleteByManuscriptId,
  create: async (note, userId) => Note.insert(note, userId),
  update: async (note, userId) => {
    if (note.id) {
      const updated = await Note.update(note, userId)
      if (!updated) {
        throw new Error('Note not found')
      }
      return updated
    }
  },
  modelName: 'Note',

  model: Note,
}

module.exports = NoteManager
