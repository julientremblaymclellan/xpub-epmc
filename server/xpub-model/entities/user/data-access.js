const { Model } = require('objection')
const config = require('config')
const EpmcBaseModel = require('../epmc-base-model')
const logger = require('@pubsweet/logger')
const BaseModel = require('@pubsweet/base-model')

const knex = BaseModel.knex()

class User extends EpmcBaseModel {
  static get tableName() {
    return 'users'
  }

  static get idColumn() {
    return 'id'
  }

  /*
  static get dbRefProp() {
    return 'id'
  }
*/

  static get schema() {
    return {
      properties: {
        id: { type: ['uuid'] },
        defaultIdentity: { type: 'string' },
        title: { type: 'string' },
        givenNames: { type: 'string' },
        surname: { type: 'string' },
        deleted: { type: 'timestamp' },
        updatedBy: { type: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    const Identity = require('../identity/data-access')
    const Manuscript = require('../manuscript/data-access')
    const Team = require('../team/data-access')
    const Role = require('../role/data-access')

    return {
      identities: {
        relation: Model.HasManyRelation,
        modelClass: Identity,
        join: {
          from: 'users.id',
          to: 'identity.userId',
        },
      },
      teams: {
        relation: Model.HasManyRelation,
        modelClass: Team,
        join: {
          from: 'users.id',
          to: 'team.userId',
        },
      },
      manuscripts: {
        relation: Model.ManyToManyRelation,
        modelClass: Manuscript,
        join: {
          from: 'users.id',
          through: {
            modelClass: Team,
            from: 'team.userId',
            to: 'team.manuscriptId',
          },
          to: 'manuscript.userId',
        },
      },
      roles: {
        relation: Model.ManyToManyRelation,
        modelClass: Role,
        join: {
          from: 'users.id',
          // ManyToMany relation needs the `through` object
          // to describe the join table.
          through: {
            from: 'team.userId',
            to: 'team.roleName',
            modelClass: Team,
          },
          to: 'role.name',
        },
      },
    }
  }

  // We decide not to support username
  static async findByEmail(email) {
    const user = await User.query()
      .join('identity', 'users.id', 'identity.user_id')
      .where('identity.email', email)
      .whereNull('users.deleted')
      .eager('identities')
    logger.debug('findByEmail: ', user)
    return user && user.length > 0 ? user[0] : null
  }
  static async findListByEmail(email) {
    const users = await User.query()
      .join('identity', 'users.id', 'identity.user_id')
      .where('identity.email', 'ilike', `%${email}%`)
      .whereNull('users.deleted')
      .eager('identities')
    return users
  }
  static async findListByName(name) {
    const users = await User.query()
      .where('surname', 'ilike', `%${name}%`)
      .orWhere('given_names', 'ilike', `%${name}%`)
      .whereNull('deleted')
      .eager('identities')
    return users
  }
  static async validateUser(email, password) {
    const user = await User.findByEmail(email)
    if (!user) {
      throw new Error('No user with that email')
    }

    const identities = user.identities.filter(id => id.email === email)
    const identity = identities[0]

    const Identity = require('../identity/data-access')
    const valid = await Identity.isPasswordSame(password, identity.passwordHash)

    if (!valid) {
      throw new Error('Incorrect password')
    }

    return { email: identity.email, id: identity.userId }
  }

  static async findById(id, relations = ['identities']) {
    const user = await User.query()
      .where('id', id)
      .whereNull('deleted')
      .eager(`[${relations.toString()}]`)
    logger.debug('user: ', user)
    return user && user.length > 0 ? user[0] : null
  }

  static async mergeUser(from, to, updatedBy) {
    // no need to "where deleted"
    const result = await knex.raw('SELECT merge(?, ?, ?)', [
      from,
      to,
      updatedBy,
    ])
    return result.rows[0].merge === true
  }

  // used in signup
  async addIdentity(email, password) {
    const Identity = require('../identity/data-access')
    this.updateProperties({
      identities: {
        email,
        passwordHash: await Identity.hashPassword(password),
        type: config.user.identity.default,
      },
    })
  }

  // unused
  async addTeam(roleName, manuscriptId) {
    this.updateProperties({
      teams: {
        roleName,
        manuscriptId,
      },
    })
  }
}

module.exports = User
