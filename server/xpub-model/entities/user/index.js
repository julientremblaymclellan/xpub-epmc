const logger = require('@pubsweet/logger')
const config = require('config')
const User = require('./data-access')
const Team = require('../team/data-access')
const EmailValidator = require('email-validator')
const IdentityManager = require('../identity')

const UserManager = {
  findById: async (id, relations) => {
    const dbUser = await User.findById(id, relations)
    return dbUser
  },

  findByEmail: async email => {
    const dbUser = await User.findByEmail(email)
    return dbUser
  },
  findListByEmail: async email => {
    const dbUsers = await User.findListByEmail(email)
    return dbUsers
  },
  findListByName: async name => {
    const dbUsers = await User.findListByName(name)
    return dbUsers
  },
  findEmail: async id => {
    const user = await UserManager.findById(id)
    if (!user) {
      throw new Error('User not found')
    }
    if (!user.identities || user.identities.length <= 0) {
      throw new Error('User email not available')
    }

    const localIdentity = user.identities.find(
      identity =>
        identity.type === config.user.identity.local &&
        EmailValidator.validate(identity.email),
    )
    if (!localIdentity) {
      throw new Error('User email not found')
    }
    return localIdentity.email
  },
  async updateUser(id, input, relations = ['identities'], updatedBy) {
    const duplicateUser = await User.findByEmail(input.email)
    if (duplicateUser && duplicateUser.id !== id) {
      throw new Error('Email already exists.')
    }
    const dbUser = await User.findById(id, relations)
    dbUser.title = input.title
    dbUser.givenNames = input.givenNames
    dbUser.surname = input.surname
    dbUser.identities.forEach(identity => {
      Object.keys(identity).forEach(
        key => identity[key] == null && delete identity[key],
      )
    })
    const identity = dbUser.identities.find(
      identity => identity.type === 'local',
    )

    // handle update of current user and user updated by admin
    if (input.newPassword) {
      try {
        await User.validateUser(identity.email, input.currentPassword)
      } catch (error) {
        throw new Error('Current Password is invalid')
      }
      identity.updateProperties({
        passwordHash: await IdentityManager.model.hashPassword(
          input.newPassword,
        ),
        email: input.email,
        updatedBy,
      })
    } else {
      identity.updateProperties({
        email: input.email,
        updatedBy,
      })
    }
    const identities = dbUser.identities.filter(
      identity => identity.type !== 'local',
    )
    identities.push(identity)
    dbUser.updateProperties({
      identities,
      updatedBy,
    })
    // handle change in privilage level by admin
    if (input.team !== undefined) {
      dbUser.teams.forEach(team => {
        Object.keys(team).forEach(key => team[key] == null && delete team[key])
      })
      let team = dbUser.teams.find(team => !team.manuscriptId)
      const teams = dbUser.teams.filter(team => team.manuscriptId)
      // if privilege is assigned
      if (input.team) {
        // if user has already a role: update, else: create
        if (!team) {
          team = new Team()
        }
        team.updateProperties({
          roleName: input.team,
          manuscriptId: null,
          updatedBy,
        })
        teams.push(team)
      }
      dbUser.updateProperties({
        teams,
        updatedBy,
      })
    }
    await dbUser.save()
    return dbUser
  },
  mergeUser: async (from, to, updatedBy) => {
    const result = await User.mergeUser(from, to, updatedBy)
    return result
  },
  signUp: async ({ title = '', givenNames, surname, email, password }) => {
    let savedUser
    try {
      const duplicateUser = await User.findByEmail(email)
      if (duplicateUser) {
        throw new Error('Email already exists.')
      }
      const user = new User({
        title,
        givenNames,
        surname,
        defaultIdentity: config.user.identity.default,
      })
      await user.addIdentity(email, password)
      savedUser = await user.save()
    } catch (error) {
      logger.error('Forwarding error: ', error)
      throw error
    }
    logger.debug('savedUser: ', savedUser)
    return savedUser
  },

  modelName: 'EpmcUser',

  model: User,
}

module.exports = UserManager
