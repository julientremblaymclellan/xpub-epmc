const Audit = require('./data-access')

const AuditManager = {
  modelName: 'Audit',
  model: Audit,
  getMetrics: async user => {
    const metrics = await Audit.getMetrics()
    return metrics
  },
}

module.exports = AuditManager
