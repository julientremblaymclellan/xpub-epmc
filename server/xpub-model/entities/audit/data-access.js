const { Model } = require('objection')
const EpmcBaseModel = require('../epmc-base-model')

const BaseModel = require('@pubsweet/base-model')

const knex = BaseModel.knex()

class Audit extends EpmcBaseModel {
  static get tableName() {
    return 'audit.audit_log'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        created: { type: 'timestamp' },
        userId: { type: 'uuid' },
        action: { type: 'string' },
        objectId: { type: 'uuid' },
        originalData: { type: 'object' },
        changes: { type: 'object' },
        objectType: { type: 'string' },
        manuscriptId: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    const User = require('../user/data-access')
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'audit.audit_log.user_id',
          to: 'users.id',
        },
      },
    }
  }

  static async getMetrics() {
    // display_mth, submitted, xml_review, xml_review_within_10_days, xml_review_within_10_days_perc, xml_review_within_3_days, xml_review_within_3_days_perc, published, ncbi_ready_median
    const metrics = await knex.raw(
      `SELECT display_mth, submitted, xml_review, xml_review_within_10_days,
       CASE WHEN xml_review=0 THEN 0 ELSE xml_review_within_10_days*100/xml_review END xml_review_within_10_days_perc,
       xml_review_within_3_days,
       CASE WHEN xml_review=0 THEN 0 ELSE xml_review_within_3_days*100/xml_review END xml_review_within_3_days_perc,
       published, ncbi_ready_median
FROM
(SELECT TO_CHAR(date_trunc('month', current_date) + -1*n * INTERVAL '1 month', 'YYYYMM') mth,
        TO_CHAR(date_trunc('month', current_date) + -1*n * INTERVAL '1 month', 'Mon YYYY') display_mth,
       SUM(CASE WHEN TO_CHAR(d.submitted_date, 'Mon YYYY') = TO_CHAR(date_trunc('month', current_date) + -1*n.n * INTERVAL '1 month', 'Mon YYYY') THEN 1 ELSE 0 END) submitted,
       SUM(CASE WHEN TO_CHAR(d.xml_review_date, 'Mon YYYY') = TO_CHAR(date_trunc('month', current_date) + -1*n.n * INTERVAL '1 month', 'Mon YYYY') THEN 1 ELSE 0 END) xml_review,
       SUM(CASE WHEN TO_CHAR(d.xml_review_date, 'Mon YYYY') = TO_CHAR(date_trunc('month', current_date) + -1*n.n * INTERVAL '1 month', 'Mon YYYY')
                     THEN CASE WHEN DATE_PART('DAY', xml_review_date-submitted_date)<11 THEN 1
                     ELSE 0
                    END
                ELSE 0
               END) xml_review_within_10_days,
       SUM(CASE WHEN TO_CHAR(d.xml_review_date, 'Mon YYYY') = TO_CHAR(date_trunc('month', current_date) + -1*n.n * INTERVAL '1 month', 'Mon YYYY')
                     THEN CASE WHEN DATE_PART('DAY', xml_review_date-submitted_date)<4 THEN 1
                     ELSE 0
                    END
                ELSE 0
               END) xml_review_within_3_days,
       SUM(CASE WHEN TO_CHAR(d.first_published_date, 'Mon YYYY') = TO_CHAR(date_trunc('month', current_date) + -1*n.n * INTERVAL '1 month', 'Mon YYYY') THEN 1 ELSE 0 END) published,
       percentile_cont(0.5) within group ( order by CASE WHEN TO_CHAR(d.ncbi_ready_date, 'Mon YYYY') = TO_CHAR(date_trunc('month', current_date) + -1*n.n * INTERVAL '1 month', 'Mon YYYY')
                        THEN DATE_PART('DAY', ncbi_ready_date-submitted_date)
                   ELSE NULL END) ncbi_ready_median
FROM   generate_series(1, 12) n, audit.manuscript_process_dates d
GROUP BY TO_CHAR(date_trunc('month', current_date) + -1*n.n * INTERVAL '1 month', 'Mon YYYY'), n.n) AS a
order by mth desc`,
    )

    const { rows } = metrics

    rows.forEach((row, index) => {
      row.id = index
      row.created = '2018-05-17'
    })

    return rows
  }
}

module.exports = Audit
