const { Model, transaction } = require('objection')
const uuid = require('uuid')
const EpmcBaseModel = require('../epmc-base-model')
const { rowToEntity, entityToRow, buildQuery, runQuery } = require('../util')
const BaseModel = require('@pubsweet/base-model')
const logger = require('@pubsweet/logger')

const knex = BaseModel.knex()

const columnNames = ['journalTitle', 'meta,publisher_name']

class Journal extends EpmcBaseModel {
  static get tableName() {
    return 'journal'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        created: { type: 'timestamp' },
        updated: { type: ['timestamp', 'null'] },
        dateRevised: { type: ['timestamp', 'null'] },
        journalTitle: { type: 'string' },
        'meta,publisherName': { type: ['string', 'null'] },
        'meta,issn': { type: ['string', 'null'] },
        'meta,nlmta': { type: ['string', 'null'] },
        'meta,nlmuniqueid': { type: ['string', 'null'] },
        'meta,pmcStatus': { type: 'boolean' },
        'meta,pubmedStatus': { type: 'boolean' },
        'meta,firstYear': { type: 'string' },
        'meta,endYear': { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    const Manuscript = require('../manuscript/data-access')
    return {
      manuscripts: {
        relation: Model.HasManyRelation,
        modelClass: Manuscript,
        join: {
          from: 'journal.id',
          to: 'manuscript.journalId',
        },
      },
    }
  }

  static async selectById(id) {
    const row = await runQuery(
      buildQuery
        .select('journal.*')
        .from('journal')
        .where('id', id)
        .first(),
    )
    if (!row) {
      throw new Error('journal not found')
    }
    return rowToEntity(row)
  }

  static async selectByNlmId(nlmId) {
    const row = await runQuery(
      buildQuery
        .select('journal.*')
        .from('journal')
        .where('meta,nlmuniqueid', nlmId)
        .first(),
    )
    if (!row) {
      throw new Error('journal not found')
    }
    return rowToEntity(row)
  }

  static async searchByTitle(query) {
    const rows = await runQuery(
      buildQuery
        .select('journal.*')
        .from('journal')
        .where('journalTitle', 'ILIKE', `${query}%`)
        .orderBy('journalTitle')
        .limit(50),
    )
    if (!rows) {
      throw new Error('journal not found')
    }
    return rowToEntity(rows)
  }

  static async selectAll() {
    const rows = await runQuery(buildQuery.select().from('journal'))
    return rows.map(rowToEntity)
  }

  static async insert(journal) {
    const row = entityToRow(journal, columnNames)
    row.id = uuid.v4()
    const query = buildQuery.insert(row).into('journal')
    await runQuery(query)
    return row.id
  }

  static async upsertMulti(journals) {
    const promises = [] // array of promises
    let trx
    try {
      trx = await transaction.start(knex)
      let counter = 0
      const util = require('util')
      journals.forEach(journal => {
        const insert = knex('journal')
          .insert(journal)
          .toString()

        const update = knex('journal')
          .update(journal)
          .whereRaw(
            `journal."meta,nlmuniqueid" = '${
              journal['meta,nlmuniqueid']
            }' AND journal.date_revised < '${journal.dateRevised}'`,
          )
        const query = util.format(
          '%s ON CONFLICT ("meta,nlmuniqueid") DO UPDATE SET %s',
          insert.toString(),
          update.toString().replace(/^update\s.*\sset\s/i, ''),
          'RETURNING id',
        )
        counter += 1

        promises.push(knex.raw(query).transacting(trx))
      })

      return Promise.all(promises).then(() => {
        trx.commit()
        logger.info(`${counter} records were updated.`)
      })
    } catch (error) {
      if (trx) {
        await trx.rollback()
      }
      logger.error('No journals were inserted')
      logger.error(error)
      throw error
    }
  }

  static update(journal) {
    const row = entityToRow(journal, columnNames)
    const query = buildQuery
      .update(row)
      .table('journal')
      .where('id', journal.id)
    return runQuery(query)
  }

  static delete(id) {
    return runQuery(
      buildQuery
        .delete()
        .from('journal')
        .where({ id }),
    )
  }
}
module.exports = Journal
