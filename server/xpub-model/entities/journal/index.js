const lodash = require('lodash')
const Journal = require('./data-access')

const empty = {
  journalTitle: '',
  publisher_name: '',
}

const JournalManager = {
  find: Journal.selectById,
  delete: Journal.delete,
  new: (props = {}) => lodash.merge({}, empty, props),
  save: async journal => {
    let { id } = journal
    if (id) {
      const updated = await Journal.update(journal)
      if (!updated) {
        throw new Error('Journal not found')
      }
    } else {
      id = await Journal.insert(journal)
    }

    return { ...journal, id }
  },
  selectWithNLM: async nlmId => Journal.selectByNlmId(nlmId),
  searchByTitle: async query => Journal.searchByTitle(query),
}

module.exports = JournalManager
