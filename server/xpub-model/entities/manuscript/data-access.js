const { transaction, Model, raw } = require('objection')
const logger = require('@pubsweet/logger')
const config = require('config')
const EpmcBaseModel = require('../epmc-base-model')
const BaseModel = require('@pubsweet/base-model')

const knex = BaseModel.knex()

const PAGE_SIZE = config.pageSize || 50

class Manuscript extends EpmcBaseModel {
  static get tableName() {
    return 'manuscript'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'string', format: 'regex', pattern: 'EMS\\d+' },
        organizationId: { type: 'uuid' },
        journalId: { type: 'uuid' },
        created: { type: 'timestamp' },
        updated: { type: 'timestamp' },
        deleted: { type: 'timestamp' },
        updatedBy: { type: 'uuid' },
        claimedBy: { type: ['string', 'null'] },
        previousVersion: { type: 'uuid' },
        status: { type: ['string', 'null'] },
        formState: { type: ['string', 'null'] },
        decision: { type: ['string', 'null'] },
        pdfDepositId: { type: 'uuid' },
        pdfDepositState: { type: ['string', 'null'] },
        ncbiState: { type: ['string', 'null'] },
        'meta,title': { type: ['string', 'null'] },
        'meta,articleType': { type: ['string', 'null'] },
        'meta,articleIds': {
          type: ['array', 'null'],
          items: {
            type: 'object',
            properties: {
              pubIdType: { type: 'string' },
              id: { type: 'string' },
            },
          },
        },
        'meta,abstract': { type: ['string', 'null'] },
        'meta,subjects': { type: ['string', 'null'] },
        'meta,publicationDates': {
          type: ['array', 'null'],
          items: {
            type: 'object',
            properties: {
              type: { type: 'string' },
              date: { type: 'string' },
              jatsDate: {
                type: 'object',
                properties: {
                  year: { type: 'string' },
                  month: { type: 'string' },
                  day: { type: 'string' },
                  season: { type: 'string' },
                },
              },
            },
          },
        },
        'meta,notes': {
          type: ['array', 'null'],
          items: {
            type: ['object', 'null'],
            properties: {
              id: { type: 'string' },
              content: { type: 'string' },
              notesType: { type: 'string' },
            },
          },
        },
        'meta,volume': { type: ['string', 'null'] },
        'meta,issue': { type: ['string', 'null'] },
        'meta,location': {
          type: ['object', 'null'],
          properties: {
            fpage: { type: ['string', 'null'] },
            lpage: { type: ['string', 'null'] },
            elocationId: { type: ['string', 'null'] },
          },
        },
        'meta,fundingGroup': {
          type: ['array', 'null'],
          items: {
            type: 'object',
            properties: {
              fundingSource: { type: 'string' },
              awardId: { type: 'string' },
              title: { type: ['string', 'null'] },
              pi: {
                type: 'object',
                properties: {
                  surname: { type: 'string' },
                  givenNames: { type: ['string', 'null'] },
                  prefix: { type: ['string', 'null'] },
                  email: { type: 'string' },
                },
              },
            },
          },
        },
        'meta,releaseDelay': { type: ['string', 'null'] },
        'meta,unmatchedJournal': { type: ['string', 'null'] },
        teams: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              role: { type: 'string' },
              teamMembers: { type: 'array' },
            },
          },
        },
        files: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'string' },
              type: { type: 'string' },
              label: { type: 'string' },
              filename: { type: 'string' },
              url: { type: 'string' },
              mimeType: { type: 'string' },
            },
          },
        },
      },
    }
  }

  static get jsonAttributes() {
    return ['meta,articleIds', 'meta,publicationDates', 'meta,fundingGroup']
  }

  static get relationMappings() {
    const File = require('../file/data-access')
    const Note = require('../note/data-access')
    const Journal = require('../journal/data-access')
    const Review = require('../review/data-access')
    const Team = require('../team/data-access')
    const User = require('../user/data-access')
    const Audit = require('../audit/data-access')
    return {
      reviews: {
        relation: Model.HasManyRelation,
        modelClass: Review,
        join: {
          from: 'manuscript.id',
          to: 'review.manuscriptId',
        },
      },
      files: {
        relation: Model.HasManyRelation,
        modelClass: File,
        join: {
          from: 'manuscript.id',
          to: 'file.manuscriptId',
        },
      },
      notes: {
        relation: Model.HasManyRelation,
        modelClass: Note,
        join: {
          from: 'manuscript.id',
          to: 'note.manuscriptId',
        },
      },
      journal: {
        relation: Model.BelongsToOneRelation,
        modelClass: Journal,
        join: {
          from: 'manuscript.journalId',
          to: 'journal.id',
        },
      },
      teams: {
        relation: Model.HasManyRelation,
        modelClass: Team,
        join: {
          from: 'manuscript.id',
          to: 'team.manuscriptId',
        },
      },
      claiming: {
        relation: Model.HasOneRelation,
        modelClass: User,
        join: {
          from: 'manuscript.claimedBy',
          to: 'users.id',
        },
      },
      audits: {
        relation: Model.HasManyRelation,
        modelClass: Audit,
        join: {
          from: 'manuscript.id',
          to: 'audit.audit_log.manuscriptId',
        },
      },
      users: {
        relation: Model.ManyToManyRelation,
        modelClass: User,
        join: {
          from: 'manuscript.id',
          // ManyToMany relation needs the `through` object
          // to describe the join table.
          through: {
            modelClass: Team,
            from: 'team.manuscriptId',
            to: 'team.userId',
          },
          to: 'users.id',
        },
      },
    }
  }

  async saveWithUserAndRole(options, userId, roleName) {
    const Team = require('../team/data-access')
    let trx
    let saved
    try {
      trx = await transaction.start(knex)
      saved = await this.saveWithTrx(trx, options)

      await new Team({
        manuscriptId: saved.id,
        userId,
        roleName,
        updatedBy: userId,
      }).saveWithTrx(trx)

      await trx.commit()
    } catch (error) {
      await trx.rollback()
      logger.error('Nothing was inserted')
      logger.error(error)
      throw error
    }

    return saved
  }

  static async selectById(id, eager = false) {
    let manuscripts
    if (eager) {
      // don't whereNull deleted here
      manuscripts = await Manuscript.query()
        .select('manuscript.*')
        .where('manuscript.id', id)
        .eager('[journal, teams.users, notes, files, claiming]')
        .modifyEager('teams', builder => {
          builder.whereNull('deleted')
        })
        .modifyEager('notes', builder => {
          builder.whereNull('deleted')
        })
        .modifyEager('files', builder => {
          builder.whereNull('deleted')
        })
    } else {
      manuscripts = await Manuscript.query().where('id', id)
    }
    return manuscripts && manuscripts.length > 0 ? manuscripts[0] : null
  }

  static async selectByStatus(statuses, page = 0, pageSize = PAGE_SIZE, user) {
    const { isAdmin, manuscriptIds } = await Manuscript.isAdmin(user)
    let manuscripts
    if (isAdmin) {
      manuscripts =
        page === -1
          ? await Manuscript.query()
              .distinct('manuscript.id')
              .select('manuscript.*')
              .eager('[teams.users, claiming, journal]')
              .whereIn('manuscript.status', statuses)
              .whereNull('manuscript.deleted')
              .orderBy('manuscript.updated', 'desc')
          : await Manuscript.query()
              .distinct('manuscript.id')
              .select('manuscript.*')
              .eager('[teams.users, claiming, journal]')
              .whereIn('manuscript.status', statuses)
              .whereNull('manuscript.deleted')
              .orderBy('manuscript.updated', 'desc')
              .page(page, pageSize)
    } else {
      manuscripts =
        page === -1
          ? await Manuscript.query()
              .distinct('manuscript.id')
              .select('manuscript.*')
              .whereIn('manuscript.id', manuscriptIds)
              .eager('[teams.users, claiming, journal]')
              .whereIn('manuscript.status', statuses)
              .whereNull('manuscript.deleted')
              .orderBy('manuscript.updated', 'desc')
          : await Manuscript.query()
              .distinct('manuscript.id')
              .select('manuscript.*')
              .whereIn('manuscript.id', manuscriptIds)
              .eager('[teams.users, claiming, journal]')
              .whereIn('manuscript.status', statuses)
              .whereNull('manuscript.deleted')
              .orderBy('manuscript.updated', 'desc')
              .page(page, pageSize)
    }
    logger.debug('manuscripts: ', manuscripts)
    return manuscripts
  }

  static async countByStatus(statuses, user) {
    const { isAdmin, manuscriptIds } = await Manuscript.isAdmin(user)

    const count = isAdmin
      ? await knex('manuscript')
          .count('*')
          .where('status', '=', statuses)
          .whereNull('deleted')
      : await knex('manuscript')
          .count('*')
          .where('status', '=', statuses)
          .whereIn('id', manuscriptIds)
          .whereNull('deleted')

    return parseInt(count[0].count, 10)
  }

  static async countDeleted(user) {
    const { isAdmin } = await Manuscript.isAdmin(user)

    const count = isAdmin
      ? await knex('manuscript')
          .count('*')
          .whereNotNull('deleted')
      : [{ count: 0 }]

    return parseInt(count[0].count, 10)
  }

  static async getDeleted(page = 0, pageSize = PAGE_SIZE, user) {
    const manuscripts =
      page === -1
        ? await Manuscript.query()
            .distinct('manuscript.id')
            .select('manuscript.*')
            .eager('[teams.users, claiming, journal]')
            .whereNotNull('manuscript.deleted')
            .orderBy('manuscript.updated', 'desc')
        : await Manuscript.query()
            .distinct('manuscript.id')
            .select('manuscript.*')
            .eager('[teams.users, claiming, journal]')
            .whereNotNull('manuscript.deleted')
            .orderBy('manuscript.updated', 'desc')
            .page(page, pageSize)
    logger.debug('manuscripts: ', manuscripts)
    return manuscripts
  }

  static async selectActivityById(id) {
    const manuscripts = await Manuscript.query()
      .where('manuscript.id', id)
      .eager('[journal, notes, teams.users, audits.user.identities]')
      .modifyEager('audits', q => {
        q.where(q => {
          q.whereNot('audit_log.object_type', 'note').orWhere(q => {
            q.where('audit_log.object_type', 'note').whereJsonSupersetOf(
              'audit_log.changes',
              { notes_type: 'userMessage' },
            )
          })
        }).orderBy('audit_log.created', 'asc')
      })
    return manuscripts && manuscripts.length > 0 ? manuscripts[0] : null
  }

  static async getManuscriptById(id) {
    const manuscript = Manuscript.query().where('manuscript.id', id)
    return manuscript
  }

  static async selectByPdfDepositStatesNull() {
    const manuscripts = Manuscript.query()
      .whereNull('pdf_deposit_state')
      .whereNull('deleted')
    logger.debug('manuscripts: ', manuscripts)
    return manuscripts
  }

  static async selectByPdfDepositStates(depositStates) {
    const manuscripts = Manuscript.query()
      .whereIn('pdf_deposit_state', depositStates)
      .whereNull('deleted')
    logger.debug('manuscripts: ', manuscripts)
    return manuscripts
  }

  static async selectAll(user) {
    const manuscripts = await Manuscript.query()
      .distinct('manuscript.id')
      .select('manuscript.*')
      .leftJoin('team', 'team.manuscript_id', 'manuscript.id')
      .groupBy('manuscript.id', 'team.user_id', 'team.role_name')
      .whereNull('team.deleted')
      .where('team.user_id', user)
      .whereNull('manuscript.deleted')
      .orderBy('manuscript.updated', 'desc')
      .eager('[teams.users, journal]')
    return manuscripts
  }

  static async searchArticleIds(id, user) {
    const { isAdmin, manuscriptIds } = await Manuscript.isAdmin(user)

    const manuscripts = isAdmin
      ? await Manuscript.query()
          .whereJsonSupersetOf('meta,article_ids', [{ id }])
          .whereNull('manuscript.deleted')
          .eager('[journal, notes]')
      : await Manuscript.query()
          .whereJsonSupersetOf('meta,article_ids', [{ id }])
          .whereIn('id', manuscriptIds)
          .whereNull('manuscript.deleted')
          .eager('[journal, notes]')
    return manuscripts
  }

  static async searchByTitle(query) {
    const manuscripts = await Manuscript.query()
      .distinct('manuscript.id')
      .select('manuscript.*')
      .eager('[teams.users, claiming, journal, notes]')
      .whereNull('manuscript.deleted')
      .where(
        raw(`(? ilike concat("meta,title", '%') or "meta,title" ilike ?)`, [
          query,
          `${query}%`,
        ]),
      )
      .orderBy('manuscript.updated', 'desc')
    return manuscripts
  }

  static async searchByTitleOrUser(
    query,
    page = 0,
    pageSize = PAGE_SIZE,
    user,
  ) {
    const { isAdmin, manuscriptIds } = await Manuscript.isAdmin(user)

    const manuscripts = isAdmin
      ? await Manuscript.query()
          .distinct('manuscript.id')
          .select('manuscript.*')
          .whereNull('manuscript.deleted')
          .eager('[teams.users, claiming, journal]')
          .join('team', 'team.manuscript_id', 'manuscript.id')
          .join('users', 'team.user_id', 'users.id')
          .join('identity', 'identity.user_id', 'users.id')
          .where(
            raw(
              `("identity"."email" ilike '${query}%' or "users"."surname" ilike '${query}%' or "meta,title" ilike '${query}%')`,
            ),
          )
          .orderBy('manuscript.updated', 'desc')
          .page(page, pageSize)
      : await Manuscript.query()
          .distinct('manuscript.id')
          .select('manuscript.*')
          .whereIn('manuscript.id', manuscriptIds)
          .whereNull('manuscript.deleted')
          .eager('[teams.users, claiming, journal]')
          .join('team', 'team.manuscript_id', 'manuscript.id')
          .join('users', 'team.user_id', 'users.id')
          .join('identity', 'identity.user_id', 'users.id')
          .where(
            raw(
              `("identity"."email" ilike '${query}%' or "users"."surname" ilike '${query}%' or "meta,title" ilike '${query}%')`,
            ),
          )
          .orderBy('manuscript.updated', 'desc')
          .page(page, pageSize)

    return manuscripts
  }

  static async findNcbiReady() {
    const manuscripts = Manuscript.query()
      .where('status', config.get('ncbiInitialState'))
      .whereNull('ncbiState')
      .whereNull('deleted')
      .eager('[journal, files]')
    logger.debug('manuscripts: ', manuscripts)
    return manuscripts
  }
  static async insert(manuscript) {
    const { id } = await Manuscript.query().insertGraph(manuscript)
    const manuscriptInserted = await Manuscript.query()
      .where('id', id)
      .whereNull('deleted')
      .eager()
    return manuscriptInserted[0]
  }

  static async delete(id, userId) {
    return Manuscript.query()
      .patchAndFetchById(id, {
        deleted: new Date().toISOString(),
        updatedBy: userId,
      })
      .returning('*')
  }

  static async updateErrorMsg(id, errorMsg) {
    const manuscriptUpdated = await Manuscript.query()
      .patch({ formState: errorMsg, status: 'xml-triage' })
      .where('id', id)
    return manuscriptUpdated
  }

  static async updatePdfDepositState(id) {
    const manuscriptUpdated = await Manuscript.query()
      .patch({ pdfDepositState: 'WAITING_FOR_PDF_CONVERSION' })
      .where('id', id)
    return manuscriptUpdated
  }

  static async clearFormState(id) {
    const manuscriptUpdated = await Manuscript.query()
      .patch({ formState: '' })
      .where('id', id)
    return manuscriptUpdated
  }

  static async isAdmin(user) {
    const Team = require('../team/')
    const roles = await Team.selectByUserId(user)
    const isAdmin = roles.find(r => r.role === 'admin')
    const manuscriptIds = roles
      .map(r => r.objectId)
      .filter(objectId => objectId)
    return { isAdmin, manuscriptIds }
  }
}

module.exports = Manuscript
