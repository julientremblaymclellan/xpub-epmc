/* eslint-disable no-prototype-builtins */
const Manuscript = require('../data-access')

// Transform from GraphQL data to Database model
const dManuscriptUpdate = (data, userId) => {
  const keys = Object.keys(data)
  const props = {}
  keys.forEach(k => {
    if (k === 'meta') {
      const metaKeys = Object.keys(data[k])
      metaKeys.forEach(mk => {
        props[`meta,${mk}`] = data.meta[mk]
      })
    } else {
      props[k] = data[k]
    }
  })
  if (userId) {
    props.updatedBy = userId
  }
  return new Manuscript(props)
}

const gJournal = model => {
  const journal = {
    id: model.id,
    journalTitle: model.journalTitle,
    meta: {
      publisherName: model['meta,publisherName'],
      issn: model['meta,issn'],
      nlmta: model['meta,nlmta'],
      nlmuniqueid: model['meta,nlmuniqueid'],
      pmcStatus: model['meta,pmcStatus'],
      pubmedStatus: model['meta,pubmedStatus'],
      firstYear: model['meta,firstYear'],
      endYear: model['meta,endYear'],
    },
  }
  return journal
}

const gTeams = model =>
  model.map(t => ({
    role: t.roleName,
    teamMembers: t.users.map(u => ({
      user: {
        id: u.id,
      },
      alias: {
        name: {
          title: u.title,
          surname: u.surname,
          givenNames: u.givenNames,
        },
      },
    })),
  }))

// Transform from Database model to GraphQL data
const gManuscript = model => {
  const keys = Object.keys(model).filter(key => model[key])
  const manuscript = {}
  manuscript.meta = {}
  keys.forEach(k => {
    switch (k) {
      case 'notes':
        manuscript.meta.notes = model[k]
        break
      case 'journal':
        manuscript[k] = gJournal(model[k])
        break
      case 'teams':
        manuscript[k] = gTeams(model[k])
        break
      default:
        if (k.startsWith('meta,')) {
          manuscript.meta[k.replace('meta,', '')] = model[k]
        } else {
          manuscript[k] = model[k]
        }
        break
    }
  })
  return manuscript
}

module.exports = {
  dManuscriptUpdate,
  gManuscript,
}
