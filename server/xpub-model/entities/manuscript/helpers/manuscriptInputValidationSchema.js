const Joi = require('joi')

const manuscriptInputSchema = Joi.object()
  .keys({
    id: Joi.string().required(),
    meta: Joi.object()
      .keys({
        title: Joi.string().required(),
        articleIds: Joi.array().items(
          Joi.object().keys({
            type: Joi.string(),
            date: Joi.date(),
          }),
        ),
        publicationDates: Joi.array().items(
          Joi.object().keys({
            pubIdType: Joi.string().required(),
            id: Joi.string().required(),
          }),
        ),
        unmatchedJournal: Joi.string(),
      })
      .required(),
  })
  .required()

module.exports = manuscriptInputSchema
