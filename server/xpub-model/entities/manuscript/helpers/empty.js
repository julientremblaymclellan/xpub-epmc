/**
 * this should be kept in sync with the schema
 */
const emptyManuscript = {
  files: [],
  status: 'INITIAL',
}

module.exports = emptyManuscript
