const { transaction } = require('objection')
const config = require('config')
const lodash = require('lodash')
const rfr = require('rfr')
const authorization = require('pubsweet-server/src/helpers/authorization')
const logger = require('@pubsweet/logger')
const ManuscriptAccess = require('./data-access')
const NoteAccess = require('../note/data-access')
const UserAccess = require('../user/data-access')
const ReviewAccess = require('../review/data-access')
const Team = require('../team/data-access')
const { dManuscriptUpdate, gManuscript } = require('./helpers/transform')

const { createPackageForTaggers } = rfr('server/ftp-integration/toTaggers')

const {
  userMessage,
  reviewerEmail,
  submitterRejectEmail,
  removeDuplicateEmail,
  finalReviewEmail,
  taggerErrorEmail,
} = rfr('server/email')

const Manuscript = {
  selectActivityById: async id => {
    const manuscript = await ManuscriptAccess.selectActivityById(id)
    const gMan = gManuscript(manuscript)
    return gMan
  },

  all: async user => {
    const Manuscripts = await ManuscriptAccess.selectAll(user)
    return Manuscripts.map(manuscript => gManuscript(manuscript))
  },

  findByStatus: async (query, page, pageSize, user) => {
    const res =
      query[0] === 'deleted'
        ? await ManuscriptAccess.getDeleted(page, pageSize, user)
        : await ManuscriptAccess.selectByStatus(query, page, pageSize, user)

    const total = res.total || res.total === 0 ? res.total : res.length
    const manuscripts = res.results ? res.results : res
    const rtn = {
      total,
      manuscripts: manuscripts.map(manuscript => gManuscript(manuscript)),
    }
    return rtn
  },

  findById: async (id, userId) => {
    const manuscript = await ManuscriptAccess.selectById(id, true)
    await authorization.can(userId, 'read', {
      id,
      type: 'Manuscript',
      status: manuscript.status,
    })
    return gManuscript(manuscript)
  },

  findByArticleId: async (id, userId) => {
    let manuscript = null
    if (id.toUpperCase().startsWith('EMS')) {
      manuscript = await ManuscriptAccess.selectById(id)
    } else {
      const manuscripts = await ManuscriptAccess.searchArticleIds(id, userId)
      manuscript = manuscripts.pop()
    }
    try {
      await authorization.can(userId, 'read', {
        id: manuscript.id,
        type: 'Manuscript',
        status: manuscript.status,
      })
    } catch (error) {
      logger.error(error)
      return {
        manuscript: null,
        errors: [{ message: `No active manuscript with ID: ${id}` }],
      }
    }
    return {
      manuscript,
      errors: manuscript
        ? null
        : [{ message: `No manuscript found with ID: ${id}` }],
    }
  },

  checkDuplicates: async (id, articleIds, title, user) => {
    let manuscripts = []
    if (articleIds && articleIds.length > 0) {
      const idMatches = []
      const matchLists = articleIds.map(aid =>
        ManuscriptAccess.searchArticleIds(aid, user),
      )
      await Promise.all(matchLists)
      matchLists.forEach(list => idMatches.concat(list))
      manuscripts = manuscripts.concat(
        idMatches.reduce((other, each) => {
          if (each.id !== id && !manuscripts.some(m => m.id === each.id)) {
            other.push(each)
          }
          return other
        }, []),
      )
    }
    const titleMatches = await ManuscriptAccess.searchByTitle(title)
    manuscripts = manuscripts.concat(
      (titleMatches &&
        titleMatches.reduce((other, each) => {
          if (each.id !== id && !manuscripts.some(m => m.id === each.id)) {
            other.push(each)
          }
          return other
        }, [])) ||
        [],
    )
    return {
      total: manuscripts.length,
      manuscripts: manuscripts.map(manuscript => gManuscript(manuscript)),
    }
  },

  countByStatus: async user => {
    const { states } = config
    const counts = await states.map(async type => {
      const count = await ManuscriptAccess.countByStatus(type, user)
      return { type, count }
    })
    const deleted = await ManuscriptAccess.countDeleted(user)
    counts.push({ type: 'deleted', count: deleted })
    return counts
  },

  search: async (query, page, pageSize, user) => {
    const manuscripts = await ManuscriptAccess.searchByTitleOrUser(
      query,
      page,
      pageSize,
      user,
    )
    logger.debug('manuscripts: ', manuscripts)
    return {
      total: manuscripts.total,
      manuscripts: manuscripts.results.map(manuscript =>
        gManuscript(manuscript),
      ),
    }
  },
  findByDepositStates: ManuscriptAccess.selectByPdfDepositStates,
  findByDepositStatesNull: ManuscriptAccess.selectByPdfDepositStatesNull,
  findNcbiReady: ManuscriptAccess.findNcbiReady,
  delete: async (id, userId) => {
    try {
      const manuscript = await ManuscriptAccess.selectById(id)
      if (manuscript) {
        await ManuscriptAccess.delete(id, userId)
        return true
      }
      return false
    } catch (error) {
      logger.error(`Nothing was deleted: ${error}`)
      return false
    }
  },

  create: async (data, userId, organizationId) => {
    await authorization.can(userId, 'create', {
      id: null,
      type: 'Manuscript',
      status: config.manuscript.status.initial,
    })

    let trx
    let savedMan
    try {
      trx = await transaction.start(ManuscriptAccess.knex())
      const manuscript = new ManuscriptAccess({
        organizationId,
        updatedBy: userId,
        status: config.manuscript.status.initial,
      })
      savedMan = await manuscript.saveWithTrx(trx)
      const input = dManuscriptUpdate(data, userId)
      lodash.assign(savedMan, input)
      await savedMan.saveWithTrx(trx)
      const team = new Team({
        manuscriptId: savedMan.id,
        userId,
        roleName: config.authsome.teams.submitter.name,
        updatedBy: userId,
      })
      await team.saveWithTrx(trx)
      await trx.commit()
    } catch (error) {
      if (trx) {
        await trx.rollback()
      }
      logger.error('Nothing was created: ', error)
      throw error
    }
    return savedMan
  },

  changeClaim: async (manId, userId, unclaim = false) => {
    const originalMan = await ManuscriptAccess.selectById(manId)
    let manuscriptUpdate = {}
    const errors = []
    if (originalMan.claimedBy && unclaim) {
      if (originalMan.claimedBy === userId) {
        manuscriptUpdate = dManuscriptUpdate({ claimedBy: null }, userId)
      } else {
        errors.push({ message: 'You have not claimed this manuscript' })
      }
    } else if (originalMan.claimedBy) {
      errors.push({ message: 'Manuscript already claimed' })
    } else {
      manuscriptUpdate = dManuscriptUpdate({ claimedBy: userId }, userId)
    }
    lodash.assign(originalMan, manuscriptUpdate)
    await originalMan.save()
    const updatedMan = await ManuscriptAccess.selectById(manId, true)
    return {
      manuscript: gManuscript(updatedMan),
      errors,
    }
  },

  update: async (input, userId) => {
    const originalMan = await ManuscriptAccess.selectById(input.id)
    if (!originalMan) {
      throw new Error('Manuscript not found')
    }
    const manuscriptUpdate = dManuscriptUpdate(input, userId)
    lodash.assign(originalMan, manuscriptUpdate)
    await originalMan.save()
    const updatedMan = await ManuscriptAccess.selectById(input.id, true)
    return gManuscript(updatedMan)
  },

  replace: async (keepId, throwId, userId) => {
    const keepMan = await ManuscriptAccess.selectById(keepId)
    const throwMan = await ManuscriptAccess.selectById(throwId, true)
    const emailId =
      (throwMan.teams.find(t => t.roleName === 'reviewer') &&
        throwMan.teams.find(t => t.roleName === 'reviewer').users[0].id) ||
      throwMan.teams.find(t => t.roleName === 'submitter').users[0].id
    const user = await UserAccess.findById(emailId)
    const keepFunds = keepMan['meta,fundingGroup'] || []
    const throwFunds = throwMan['meta,fundingGroup'] || []
    const newFunds = keepFunds.concat(
      throwFunds.reduce((combo, t) => {
        if (
          !keepFunds.some(
            k =>
              k.pi.email === t.pi.email &&
              k.awardId === t.awardId &&
              k.fundingSource === t.fundingSource,
          )
        ) {
          combo.push(t)
        }
        return combo
      }, []),
    )
    const input = {
      'meta,fundingGroup': newFunds,
      updatedBy: userId,
    }
    lodash.assign(keepMan, input)
    try {
      await keepMan.save()
      await ManuscriptAccess.delete(throwId, userId)
      await removeDuplicateEmail(user, throwMan, keepMan)
      return true
    } catch (error) {
      logger.error(error)
      return false
    }
  },

  submit: async (input, userId) => {
    const originalMan = await ManuscriptAccess.selectById(input.id)
    const errors = []
    if (!originalMan) {
      errors.push({ message: 'Manuscript not found' })
      return {
        manuscript: null,
        errors,
      }
    }
    if (input.status === 'tagging') {
      input.formState = null
      createPackageForTaggers(input.id)
    } else if (input.status === 'xml-triage') {
      input.formState =
        'Returned from submitter/reviewer after missing materials reported. Check activity page for details.'
    } else if (input.status === 'submitted') {
      const notes = await NoteAccess.selectByManuscriptId(input.id)
      const reviewerNote = notes.find(n => n.notesType === 'selectedReviewer')
      const teams = await Team.selectByManuscriptId(input.id)
      const subTeam = teams.find(t => t.roleName === 'submitter')
      const revTeam = teams.find(t => t.roleName === 'reviewer')
      const submitter = await UserAccess.findById(subTeam.userId)
      let inReview = false
      let reviewer = {}
      let token = ''
      if (reviewerNote) {
        reviewer = JSON.parse(reviewerNote.content)
        token = reviewerNote.id
        if (reviewer.id) {
          let trx
          try {
            trx = await transaction.start(Team.knex())
            const team = new Team({
              manuscriptId: input.id,
              userId: reviewer.id,
              roleName: 'reviewer',
              updatedBy: userId,
            })
            await team.saveWithTrx(trx)
            await NoteAccess.delete(reviewerNote.id, userId)
            if (revTeam) {
              await Team.delete(revTeam.id, userId)
            }
            await trx.commit()
          } catch (error) {
            if (trx) {
              await trx.rollback()
            }
            logger.error('Nothing was created: ', error)
            errors.push(error)
            return {
              manuscript: null,
              errors,
            }
          }
          if (reviewer.id !== submitter.id) {
            inReview = true
          }
        } else {
          inReview = true
        }
      } else if (
        originalMan.status === 'submission-error' &&
        revTeam &&
        revTeam.userId !== submitter.id &&
        userId !== revTeam.userId
      ) {
        const revUser = await UserAccess.findById(revTeam.userId)
        const { id, title, givenNames, surname, identities } = revUser
        const { email } = identities[0]
        inReview = true
        reviewer = {
          id,
          name: {
            title,
            givenNames,
            surname,
          },
          email,
        }
        token = 'correction'
      } else if (!revTeam) {
        errors.push({ message: 'Reviewer must be selected' })
        return {
          manuscript: null,
          errors,
        }
      }
      if (inReview) {
        input.status = 'in-review'
        await reviewerEmail({
          reviewer,
          manInfo: {
            id: input.id,
            title: originalMan['meta,title'],
          },
          submitter,
          token,
        })
      }
    }
    input.claimedBy = null
    try {
      const manuscriptUpdate = dManuscriptUpdate(input, userId)
      lodash.assign(originalMan, manuscriptUpdate)
      await originalMan.save()
    } catch (error) {
      logger.error(error)
      errors.push(error)
    }
    const updatedMan = await ManuscriptAccess.selectById(input.id, true)
    return {
      manuscript: gManuscript(updatedMan),
      errors,
    }
  },

  reject: async (input, userId) => {
    const originalMan = await ManuscriptAccess.selectById(input.manuscriptId)
    const teams = await Team.selectByManuscriptId(input.manuscriptId)
    const subTeam = teams.find(t => t.roleName === 'submitter')
    const revTeam = teams.find(t => t.roleName === 'reviewer')
    const submitter = await UserAccess.findById(subTeam.userId)
    const reviewer = await UserAccess.findById(revTeam.userId)
    const email = JSON.parse(input.content)
    const errors = []
    try {
      await NoteAccess.insert(input, userId)
      const manUpdate = {
        claimedBy: null,
        formState: email.message ? email.message : email,
        status: 'submission-error',
        updatedBy: userId,
      }
      lodash.assign(originalMan, manUpdate)
      await originalMan.save()
    } catch (error) {
      logger.error(error)
      errors.push(error)
      return {
        manuscript: null,
        errors,
      }
    }
    if (email.to) {
      const uniqueTo = [...new Set(email.to)]
      const sendTo = uniqueTo.map(
        u =>
          (submitter.id === u && submitter.identities[0].email) ||
          (reviewer.id === u && reviewer.identities[0].email),
      )
      await userMessage(sendTo, email.subject, email.message)
    } else {
      await submitterRejectEmail({
        reviewer,
        manInfo: {
          id: originalMan.id,
          title: originalMan['meta,title'],
        },
        submitter,
        message: email,
      })
    }
    const updatedMan = await ManuscriptAccess.selectById(originalMan.id, true)
    return {
      manuscript: gManuscript(updatedMan),
      errors,
    }
  },

  review: async (input, userId) => {
    const originalMan = await ManuscriptAccess.selectById(input.id)
    const errors = []
    if (!originalMan) {
      throw new Error('Manuscript not found')
    }
    if (input.status === 'xml-complete') {
      if (
        (originalMan['meta,articleIds'] &&
          originalMan['meta,articleIds'].some(
            aid => aid.pubIdType === 'pmid',
          )) ||
        (originalMan['meta,volume'] &&
          originalMan['meta,location'] &&
          originalMan['meta,publicationDates'])
      ) {
        input.status = 'ncbi-ready'
      }
    }
    try {
      if (['xml-review', 'xml-qa'].includes(input.status)) {
        await ReviewAccess.deleteByManuscriptId(input.id, userId)
      }
      input.claimedBy = null
      const manuscriptUpdate = dManuscriptUpdate(input, userId)
      lodash.assign(originalMan, manuscriptUpdate)
      await originalMan.save()
    } catch (error) {
      logger.error(error)
      errors.push(error)
      return {
        manuscript: null,
        errors,
      }
    }
    if (input.status === 'xml-review') {
      try {
        const teams = await Team.selectByManuscriptId(input.id)
        const revTeam = teams.find(t => t.roleName === 'reviewer')
        const reviewer = await UserAccess.findById(revTeam.userId)
        await finalReviewEmail(reviewer, {
          id: originalMan.id,
          title: originalMan['meta,title'],
          releaseDelay: originalMan['meta,releaseDelay'],
        })
      } catch (error) {
        logger.error(error)
        errors.push(error)
      }
    }
    const updatedMan = await ManuscriptAccess.selectById(input.id, true)
    return {
      manuscript: gManuscript(updatedMan),
      errors,
    }
  },

  retag: async (input, userId) => {
    const originalMan = await ManuscriptAccess.selectById(input.manuscriptId)
    const email = JSON.parse(input.content)
    try {
      await ReviewAccess.deleteByManuscriptId(input.id, userId)
      await NoteAccess.insert(input, userId)
      await taggerErrorEmail(email.subject, email.message)
      const manUpdate = {
        claimedBy: null,
        status: 'tagging',
        updatedBy: userId,
      }
      lodash.assign(originalMan, manUpdate)
      await originalMan.save()
    } catch (error) {
      logger.error(error)
      return false
    }
    return true
  },

  modelName: 'Manuscript',

  model: ManuscriptAccess,
}

module.exports = Manuscript
