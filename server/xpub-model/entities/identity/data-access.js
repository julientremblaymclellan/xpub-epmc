const { Model } = require('objection')
// const uuid = require('uuid')
const bcrypt = require('bcrypt')
// const config = require('config')
const EpmcBaseModel = require('../epmc-base-model')
const User = require('../user/data-access')

const BCRYPT_COST = 12

class Identity extends EpmcBaseModel {
  static get tableName() {
    return 'identity'
  }

  static get relationMappings() {
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'identity.user_id',
          to: 'users.id',
        },
      },
    }
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        email: { type: 'string' },
        passwordHash: { type: 'string' },
        name: { type: 'object' },
        userId: { type: 'uuid' },
        identifier: { type: 'string' },
        meta: { type: 'object' },
        deleted: { type: 'timestamp' },
        updatedBy: { type: 'uuid' },
      },
    }
  }

  /*
  static get idColumn() {
    return 'email'
  }

  static get dbRefProp() {
    return 'id'
  }
*/
  static async isPasswordSame(password, hash) {
    let valid = false

    try {
      const epmcPwd = require('epmc-pwd') // eslint-disable-line
      const encrypted = epmcPwd.generatePwd(password)
      valid = encrypted === hash || (await bcrypt.compare(encrypted, hash))
    } catch (e) {
      valid = await bcrypt.compare(password, hash)
    }

    return valid
  }

  static async hashPassword(password) {
    let encrypted

    try {
      const epmcPwd = require('epmc-pwd') // eslint-disable-line
      encrypted = epmcPwd.generatePwd(password)
    } catch (e) {
      encrypted = password
    }

    const salt = await bcrypt.genSalt(
      process.env.BCRYPT_SALT_ROUNDS
        ? await parseInt(process.env.BCRYPT_SALT_ROUNDS, 10)
        : BCRYPT_COST,
    )
    const hash = await bcrypt.hash(encrypted, salt)

    return hash
  }
}

module.exports = Identity
