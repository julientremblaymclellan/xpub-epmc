const config = require('config')
const Email = require('@pubsweet/component-send-email')
const logger = require('@pubsweet/logger')
const {
  htmlEmailBase,
  setReviewerTemplate,
  newReviewerTemplate,
  checkReviewerTemplate,
  rejectSubmissionTemplate,
  removeDupeTemplate,
  newPackageForTaggingTemplate,
  processedTaggingFilesTemplate,
  bulkUploadTemplate,
  manuscriptHasBeenPublishedTemplate,
  finalReviewTemplate,
} = require('./templates')

const tagger = config.ftp_tagger.email
const { sender, url, testAddress, system } = config['epmc-email']

const sendMail = (to, subject, message, from = null, cc = null, bcc = null) => {
  const mailData = {
    from: from || sender,
    to: testAddress || to,
    subject: `[Europe PMC plus] ${subject}`,
    html: htmlEmailBase(message, url),
  }
  logger.info(`Email recipient: ${to}`)
  if (cc) {
    mailData.cc = testAddress || cc
    logger.info(`Email CC: ${cc}`)
  }
  if (bcc) {
    mailData.bcc = testAddress || bcc
    logger.info(`Email BCC: ${bcc}`)
  }
  Email.send(mailData)
}

const userMessage = (email, subject, message, from = null) => {
  const regex = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()[\]{};:'".,<>?«»“”‘’]))/i
  const linkAdded = message.replace(
    regex,
    '<a href="$&" style="color:#20699C">$&</a>',
  )
  const messageDiv = `<div style="white-space:pre-wrap">${linkAdded}</div>`
  sendMail(email, subject, messageDiv, from)
}

const taggerErrorEmail = (subject, message) => {
  const html = `<p>This manuscript has been returned to you. Please correct the following tagging errors.</p>${message}`
  sendMail(tagger, subject, html, null, null, system)
}

const taggerEmail = (email, manId, title, link) => {
  const html = newPackageForTaggingTemplate(manId, title, link)
  const subject = 'New Package Available for Tagging'
  sendMail(email, subject, html, null, null, system)
}

const processedTaggerEmail = (email, manId, title, packageName) => {
  const html = processedTaggingFilesTemplate(manId, title, packageName)
  const subject = 'Tagging files have been uploaded'
  sendMail(email, subject, html, null, null, system)
}

const bulkUploaderEmail = (email, message) => {
  const html = bulkUploadTemplate(message)
  const subject = 'Error while processing package'
  sendMail(email, subject, html, null, null, system)
}

const manuscriptHasBeenPublishedEmail = (email, message, link) => {
  const html = manuscriptHasBeenPublishedTemplate(message, link)
  const subject = 'Your manuscript has been published'
  sendMail(email, subject, html, null, null, system)
}

const reviewerEmail = ({ reviewer, manInfo, submitter, token }) => {
  const { title, givenNames, surname } = submitter
  const submitterName = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const { title: t, givenNames: g, surname: s } = reviewer.name
  const salutation = `${t ? `${t} ` : ''}${g} ${s}`
  let link = `${url}submission/${manInfo.id}/submit`
  let html = ''
  if (token === 'correction') {
    html = checkReviewerTemplate(salutation, manInfo.title, submitterName, link)
  } else if (reviewer.id) {
    html = setReviewerTemplate(salutation, manInfo.title, submitterName, link)
  } else {
    link = `${url}dashboard?accept=${token}`
    html = newReviewerTemplate(salutation, manInfo.title, submitterName, link)
  }
  const subject = `Approve submission of ${manInfo.id}`
  sendMail(reviewer.email, subject, html, null, null, system)
}

const submitterRejectEmail = ({ reviewer, manInfo, submitter, message }) => {
  const { title, givenNames, surname } = submitter
  const { email } = submitter.identities[0]
  const salutation = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const { title: t, givenNames: g, surname: s } = reviewer
  const reviewerName = `${t ? `${t} ` : ''}${g} ${s}`
  const link = `${url}submission/${manInfo.id}/submit`
  const html = rejectSubmissionTemplate(
    salutation,
    manInfo.title,
    reviewerName,
    message,
    link,
  )
  const subject = `${manInfo.id}: Submission rejected`
  sendMail(email, subject, html, null, null, system)
}

const removeDuplicateEmail = (user, badInfo, goodInfo) => {
  const { title, givenNames, surname } = user
  const { email } = user.identities[0]
  const salutation = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const pmcid =
    goodInfo['meta,articleIds'] &&
    goodInfo['meta,articleIds'].find(id => id.pubIdType === 'pmcid')
      ? goodInfo['meta,articleIds'].find(id => id.pubIdType === 'pmcid').id
      : null
  const html = removeDupeTemplate(
    salutation,
    badInfo.id,
    badInfo['meta,title'],
    goodInfo.id,
    pmcid,
  )
  const subject = `${badInfo.id}: Duplicate submission`
  sendMail(email, subject, html, null, null, system)
}

const finalReviewEmail = (reviewer, manInfo) => {
  const { title, givenNames, surname } = reviewer
  const { email } = reviewer.identities[0]
  const salutation = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const link = `${url}submission/${manInfo.id}/review`
  const releaseDelay =
    manInfo.releaseDelay === '0'
      ? 'immediately'
      : `${manInfo.releaseDelay} month${
          manInfo.releaseDelay === '1' ? '' : 's'
        }`
  const html = finalReviewTemplate(
    salutation,
    manInfo.title,
    link,
    releaseDelay,
  )
  const subject = `${manInfo.id}: Ready for final review`
  sendMail(email, subject, html, null, null, system)
}

module.exports = {
  sendMail,
  userMessage,
  taggerEmail,
  processedTaggerEmail,
  taggerErrorEmail,
  bulkUploaderEmail,
  manuscriptHasBeenPublishedEmail,
  reviewerEmail,
  submitterRejectEmail,
  removeDuplicateEmail,
  finalReviewEmail,
}
