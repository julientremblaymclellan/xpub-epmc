const resetPasswordTemplate = ({ title, givenNames, surname }, link) => `
  <h1 style="font-weight:600;">Reset your password</h1>
	<p>Dear ${title ? `${title} ` : ''}${givenNames} ${surname},</p>
	<p>Please click the link to <a style="color:#20699C" href="${link}">reset the password</a> of your Europe PMC Plus account.</p>
  <p>If you did not request a password reset, please disregard this message.</p>
	<p>Kind regards,</p>
	<p>The Europe PMC Helpdesk</p>
`

module.exports = {
  resetPasswordTemplate,
}
