const removeDupeTemplate = (salutation, badId, title, goodId, pmcid) => `
  <h1 style="font-weight:600;">Duplicate submission</h1>
  <p>Dear ${salutation},</p>
  <p>Your manuscript submission, <b>${badId}: ${title}</b> has been removed as a duplicate of ${goodId}. Any grants attached to your submission have been added to the existing record.</p>
  <p>Please use ${pmcid || goodId} for grant reporting purposes${
  pmcid ? '' : ' until a PMCID becomes available'
}.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

module.exports = {
  removeDupeTemplate,
}
