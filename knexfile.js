// Update with your config settings.
const config = require('config')
const path = require('path')

const connection = config['pubsweet-server'] && config['pubsweet-server'].db

module.exports = {
  development: {
    client: 'postgresql',
    connection,
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      directory: path.join(__dirname, '/server/xpub-model/migrations'),
    },
    seeds: {
      directory: path.join(__dirname, '/server/xpub-model/seeds'),
    },
  },

  staging: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user: 'username',
      password: 'password',
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
    },
  },

  production: {
    client: 'postgresql',
    connection,
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      directory: path.join(__dirname, '/server/xpub-model/migrations'),
    },
    seeds: {
      directory: path.join(__dirname, '/server/xpub-model/seeds'),
    },
  },
}
