FROM xpub/xpub:base

WORKDIR ${HOME}

# install Chrome
RUN curl -sL http://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN echo 'deb http://dl.google.com/linux/chrome/deb/ stable main' >> /etc/apt/sources.list.d/google.list
RUN apt-get update && apt-get install -y google-chrome-stable cron vim

# install Firefox - apparently no debian package for firefox 57
RUN apt-get install -y libdbus-glib-1-2
RUN cd /opt && wget http://ftp.mozilla.org/pub/firefox/releases/57.0.4/linux-x86_64/en-GB/firefox-57.0.4.tar.bz2 && \
    tar xjf firefox-*.tar.bz2 && \
    ln -s /opt/firefox/firefox /usr/local/bin/

# RUN apt-get install -y default-jdk libstdc++6
RUN apt-get install -y default-jdk
#COPY node-libxml node-libxml
#WORKDIR node-libxml
#RUN ["npm", "install"]
WORKDIR ${HOME}

COPY package.json yarn.lock ./
ENV NODE_ENV "development"

RUN npm install -g node-pre-gyp

# We do a development install because react-styleguidist is a dev dependency and we want to run tests
RUN [ "yarn", "install" ]

# Remove cache and offline mirror
RUN [ "yarn", "cache", "clean"]
RUN [ "rm", "-rf", "/npm-packages-offline-cache"]

# Add crontab file in the cron directory
ADD crontab /etc/cron.d/crontab

# Add the env variables at the beginning of the cron file
RUN env | cat - /etc/cron.d/crontab > temp && mv temp /etc/cron.d/crontab

# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/crontab

# Apply cron job
RUN crontab /etc/cron.d/crontab

COPY app.js .babelrc .eslintignore .eslintrc .prettierrc .stylelintignore .stylelintrc ./

COPY app app
COPY config config
COPY scripts scripts
COPY static static
COPY test test
COPY webpack webpack
COPY server server
COPY ./*.sh ./
COPY ./*.js ./

ENV NODE_ENV ${NODE_ENV}

RUN [ "npx", "pubsweet", "build"]

EXPOSE ${PORT:-80}

CMD []
