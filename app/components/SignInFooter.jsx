import React from 'react'
import { H4, Link } from '@pubsweet/ui'
import { A } from './ui'

const SignInFooter = () => (
  <div>
    <H4>Need help depositing a manuscript?</H4>
    <ul>
      <li>
        See the Europe PMC plus <Link to="/user-guide">User Guide</Link> for
        more information.
      </li>
      <li>
        {`Contact the Europe PMC helpdesk at `}
        <A href="mailto:helpdesk@europepmc.org">helpdesk@europepmc.org</A>
        {` or +44 1223 494118.`}
      </li>
    </ul>
  </div>
)

export default SignInFooter
