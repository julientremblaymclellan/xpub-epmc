import React from 'react'
import PropTypes from 'prop-types'
import { Redirect, withRouter } from 'react-router-dom'
import { UserContext } from './App'

const AuthenticatedComponent = ({ children, location }) => (
  <UserContext.Consumer>
    {currentUser => {
      if (!currentUser || !localStorage.getItem('token')) {
        const { pathname, search = '' } = location
        const url = pathname + search
        return <Redirect to={`/login?next=${url}`} />
      }
      return React.cloneElement(children, { currentUser })
    }}
  </UserContext.Consumer>
)

AuthenticatedComponent.propTypes = {
  children: PropTypes.node.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
    search: PropTypes.string.isRequired,
  }).isRequired,
}

export default withRouter(AuthenticatedComponent)
