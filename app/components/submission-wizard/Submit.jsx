import React from 'react'
import styled, { withTheme } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, Action, H1, H2, H3, Icon } from '@pubsweet/ui'
import {
  Portal,
  Page,
  Buttons,
  Close,
  CloseButton,
  PreviewPage,
  PreviewPanel,
  EditPanel,
  PanelHeader,
  PanelContent,
  SectionContent as Content,
  SectionHeader as Header,
} from '../ui'
import UploadFiles, { SubmissionTypes } from '../upload-files'
import ManuscriptPreview from '../ManuscriptPreview'
import { NoteMutations } from '../SubmissionMutations'
import ResolveDuplicates from '../ResolveDuplicates'
import SubmitForm from './SubmitForm'
import SubmitComplete from './SubmitComplete'
import submitSections, { ErrorMessage } from './SubmitSections'

const Alert = withTheme(({ children, theme }) => (
  <Icon color={theme.colorError} size={2.75}>
    {children}
  </Icon>
))

const CloseEdit = styled(Close)`
  margin: calc(${th('gridUnit')} * 6) auto calc(${th('gridUnit')} * 2);
`

const ErrorReport = styled.p`
  white-space: pre-wrap;
  font-style: italic;
  margin-top: 0;
  color: ${th('colorError')};
`
const DuplicatesWithMutations = NoteMutations(ResolveDuplicates)

class Submit extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      cancel: false,
      editing: null,
      highlights: '',
      error: '',
      prune: false,
    }
  }
  componentDidMount() {
    if (
      this.props.currentUser.admin &&
      this.props.manuscript.status === 'submitted' // &&
      // this.props.currentVersion.source
    ) {
      const fake = document.createElement('div')
      // fake.innerHTML = this.props.currentVersion.source
      this.setState({ highlights: fake.textContent })
    }
  }
  changeCitation = citation => {
    this.props.changeCitation(citation)
    this.setState({ editing: null })
  }
  pruneDupes = () => this.setState({ prune: true })
  render() {
    const { currentUser, manuscript, duplicates: checkDupes = [] } = this.props
    const {
      id: mId,
      meta,
      files: allfiles,
      status,
      teams,
      formState,
    } = manuscript
    if (teams && allfiles) {
      const { editing, highlights, prune, cancel } = this.state
      const sections = submitSections(
        manuscript,
        checkDupes,
        this.changeCitation,
        currentUser,
        highlights,
        this.pruneDupes,
        this.props.updateEmbargo,
        this.props.updateGrants,
      )
      const { notes } = meta
      const files = allfiles
        ? allfiles.filter(
            file =>
              !file.type ||
              file.type === 'manuscript' ||
              SubmissionTypes.some(t => t.value === file.type),
          )
        : []
      let reviewer = null
      if (teams && teams.find(team => team.role === 'reviewer')) {
        const rev = teams.find(team => team.role === 'reviewer').teamMembers[0]
        reviewer = {
          id: rev.user.id,
          name: rev.alias.name,
        }
      }
      const submitter =
        teams && teams.find(team => team.role === 'submitter')
          ? teams.find(team => team.role === 'submitter').teamMembers[0]
          : null
      const dupeNote = notes
        ? notes.find(n => n.notesType === 'notDuplicates')
        : null
      const notDupes = dupeNote ? JSON.parse(dupeNote.content) : []
      const duplicates = checkDupes.filter(d => !notDupes.includes(d.id))
      return (
        <React.Fragment>
          {(() => {
            if (
              (reviewer &&
                reviewer.id === currentUser.id &&
                !['in-review', 'submission-error'].includes(status)) ||
              (submitter &&
                submitter.user.id === currentUser.id &&
                !['INITIAL', 'READY', 'submission-error'].includes(status))
            ) {
              window.scrollY = 0
              window.pageYOffset = 0
              document.scrollingElement.scrollTop = 0
              return (
                <SubmitComplete
                  cancel={() => this.setState({ cancel: true })}
                  currentUser={currentUser}
                  history={this.props.history}
                  manuscript={manuscript}
                />
              )
            } else if (editing) {
              return (
                <Page>
                  <CloseEdit>
                    <CloseButton
                      onClick={() => this.setState({ editing: null })}
                    />
                  </CloseEdit>
                  {editing.props['data-upload'] ? (
                    <div>
                      <H2>Files</H2>
                      <UploadFiles
                        checked
                        files={files}
                        manuscript={mId}
                        types={SubmissionTypes}
                      />
                    </div>
                  ) : (
                    <React.Fragment>{editing}</React.Fragment>
                  )}
                  {this.state.error && (
                    <ErrorMessage>
                      <Icon color="currentColor" size={2}>
                        alert_circle
                      </Icon>
                      {this.state.error}
                    </ErrorMessage>
                  )}
                  <Buttons>
                    <Button
                      onClick={() => this.setState({ editing: null })}
                      primary={!editing.props['data-citation']}
                    >
                      {editing.props['data-citation'] ? 'Cancel' : 'Save'}
                    </Button>
                  </Buttons>
                </Page>
              )
            }
            return (
              <PreviewPage>
                <PreviewPanel>
                  <div>
                    <PanelHeader>
                      <H1>Preview submission</H1>
                    </PanelHeader>
                    <PanelContent>
                      <ManuscriptPreview
                        file={files.find(file => file.type === 'manuscript')}
                        textContent={highlights =>
                          currentUser.admin && status === 'submitted'
                            ? this.setState({ highlights })
                            : false
                        }
                      />
                    </PanelContent>
                  </div>
                </PreviewPanel>
                <EditPanel>
                  <div>
                    <PanelHeader>
                      <H2>Check submission details</H2>
                    </PanelHeader>
                    <PanelContent>
                      <Content>
                        {status === 'submission-error' && formState && (
                          <ErrorReport>{formState}</ErrorReport>
                        )}
                      </Content>
                      {sections.map(sec => (
                        <React.Fragment key={sec.title}>
                          <Header error={!!sec.error}>
                            <H3>{sec.title}</H3>
                            {sec.edit && (
                              <Action
                                id={`edit-${sec.title}`}
                                onClick={() =>
                                  this.setState({ editing: sec.edit })
                                }
                                style={{
                                  display: 'inline-flex',
                                  alignItems: 'center',
                                }}
                              >
                                {!!sec.error && <Alert>alert_circle</Alert>}
                                <Icon color="currentColor" size={2.5}>
                                  edit
                                </Icon>
                                Edit
                              </Action>
                            )}
                          </Header>
                          <Content>
                            <div>
                              {sec.content}
                              {sec.error}
                            </div>
                          </Content>
                        </React.Fragment>
                      ))}
                      <SubmitForm
                        currentUser={currentUser}
                        manuscript={this.props.manuscript}
                        sections={sections}
                      />
                    </PanelContent>
                  </div>
                </EditPanel>
                {duplicates.length > 0 && prune && (
                  <DuplicatesWithMutations
                    close={() => this.setState({ prune: false })}
                    duplicates={duplicates}
                    manuscript={manuscript}
                    note={dupeNote}
                  />
                )}
              </PreviewPage>
            )
          })()}
          {cancel && (
            <Portal transparent>
              <p>
                {`Are you sure you want to cancel this submission? The submission will be removed and processing will not be completed.`}
              </p>
              <Buttons right>
                <Button
                  onClick={() =>
                    this.props.deleteMan(() => this.props.history.push('/'))
                  }
                  primary
                >
                  Yes
                </Button>
                <Button onClick={() => this.setState({ cancel: false })}>
                  No
                </Button>
              </Buttons>
            </Portal>
          )}
        </React.Fragment>
      )
    }
    return null
  }
}

export default Submit
