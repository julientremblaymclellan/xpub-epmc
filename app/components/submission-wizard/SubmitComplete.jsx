import { states } from 'config'
import React from 'react'
import { Action, Icon, Button, H1, H2, H3 } from '@pubsweet/ui'
import { B, Buttons, Page } from '../ui'
import { SubmissionTypes } from '../upload-files'
import { FileThumbnails } from '../preview-files'
import Citation from './Citation'

const SubmitComplete = ({ currentUser, cancel, manuscript, history }) => {
  const { status, meta, files, journal, teams } = manuscript
  const { fundingGroup, releaseDelay } = meta
  const submitter =
    teams && teams.find(team => team.role === 'submitter')
      ? teams.find(team => team.role === 'submitter').teamMembers[0]
      : null
  const showFiles = files
    ? files.filter(
        file =>
          !file.type ||
          file.type === 'manuscript' ||
          SubmissionTypes.some(t => t.value === file.type),
      )
    : []
  const curr = states.indexOf(status)
  const done = states.indexOf('xml-complete')
  const sent = states.indexOf('ncbi-ready')
  return (
    <Page withHeader>
      <H1>Thank you for your submission</H1>
      <p>
        {`Your submission is `}
        {curr >= done
          ? 'complete and accepted for archiving. If you have any questions, or require changes to be made, please click the link above to contact the Helpdesk.'
          : `${
              ['submitted', 'in-review', 'xml-review'].includes(status)
                ? 'in review'
                : 'being processed'
            }. You will receive email updates as it is processed. You can also log in to Europe PMC plus at any time to check the status of your submission. If you require any changes to the following information, please click the link above to contact the Helpdesk.`}
      </p>
      <Buttons>
        <Button onClick={() => history.push('/')} primary>
          Done
        </Button>
        {submitter && submitter.user.id === currentUser.id && curr < sent && (
          <Action
            onClick={() => cancel()}
            style={{ display: 'inline-flex', alignItems: 'center' }}
          >
            <Icon color="currentColor" size={2.5}>
              trash-2
            </Icon>
            Cancel submission
          </Action>
        )}
      </Buttons>
      <H2>Submission details</H2>
      <H3>Citation</H3>
      <Citation journal={journal} metadata={meta} />
      <H3>Files</H3>
      <FileThumbnails files={showFiles} />
      <H3>Funding</H3>
      {fundingGroup && fundingGroup.length > 0 && (
        <p>
          <B>Grants: </B>
          {fundingGroup.map((f, t) => (
            <span key={f.awardId}>
              {`${f.fundingSource} ${f.awardId}`}
              {t !== fundingGroup.length - 1 && ', '}
            </span>
          ))}
        </p>
      )}
      {(releaseDelay || typeof releaseDelay === 'number') && (
        <p>
          <B>Embargo: </B>
          {`${releaseDelay} month${
            parseInt(releaseDelay, 10) === 1 ? '' : 's'
          }`}
        </p>
      )}
    </Page>
  )
}

export default SubmitComplete
