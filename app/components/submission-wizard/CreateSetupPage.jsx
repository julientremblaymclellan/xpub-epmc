import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { graphql, compose } from 'react-apollo'
import { H2, Icon } from '@pubsweet/ui'
import { SplitPage, InfoPanel, StepPanel } from '../ui'
import PubMedSearch from '../citation-search'
import CreateHeader from './CreateHeader'
import CreateInfo from './CreateInfo'
import { CREATE_NOTE } from '../operations'
import { CREATE_MANUSCRIPT } from './operations'

const SplitPageShort = styled(SplitPage)`
  min-height: calc(100vh - (${th('gridUnit')} * 20));
`

const manuscriptSetup = async (
  citationData,
  createNote,
  createManuscript,
  history,
) => {
  if (citationData) {
    const { note, ...citation } = citationData
    const created = await createManuscript({
      variables: { data: { ...citation } },
    })
    const { id } = created.data.createManuscript
    if (note) {
      await createNote({
        variables: {
          data: {
            manuscriptId: id,
            ...note,
          },
        },
      })
    }
    const route = `/submission/${id}/create`
    history.push(route)
  }
}

const showInfo = e => {
  e.currentTarget.classList.toggle('showInfo')
  e.currentTarget.children[0].classList.toggle('hidden')
  e.currentTarget.children[1].classList.toggle('hidden')
}

const CreateSetup = ({ history, createNote, createManuscript }) => (
  <SplitPageShort>
    <StepPanel>
      <div>
        <CreateHeader currentStep={0} />
        <H2>Citation</H2>
        <PubMedSearch
          changeNote
          citationData={d =>
            manuscriptSetup(d, createNote, createManuscript, history)
          }
        />
      </div>
    </StepPanel>
    <InfoPanel>
      <div className="hidden" id="more-info" onClick={e => showInfo(e)}>
        More Info
        <Icon>chevron-right</Icon>
        <Icon className="hidden">chevron-down</Icon>
      </div>
      <CreateInfo currentStep={0} />
    </InfoPanel>
  </SplitPageShort>
)

export default compose(
  graphql(CREATE_MANUSCRIPT, { name: 'createManuscript' }),
  graphql(CREATE_NOTE, { name: 'createNote' }),
)(CreateSetup)
