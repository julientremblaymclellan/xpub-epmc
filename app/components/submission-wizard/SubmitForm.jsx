import React from 'react'
import { withRouter } from 'react-router'
import { Mutation } from 'react-apollo'
import styled from 'styled-components'
import { Button, CheckboxGroup, H3, Icon } from '@pubsweet/ui'
import {
  A,
  B,
  Buttons,
  Portal,
  Close,
  CloseButton,
  SectionContent as Content,
  SectionHeader as Header,
} from '../ui/'
import SubmissionErrorReport, { adminOptions } from '../SubmissionErrorReport'
import { SUBMIT_MANUSCRIPT } from './operations'
import ReviewerErrorReport from './ReviewerErrorReport'

const Help = styled.p`
  font-style: italic;
  display: flex;
  align-items: center;
`
class SubmitForm extends React.Component {
  state = {
    checkedBoxes: [],
    reject: false,
    message: '',
  }
  render() {
    const { manuscript, currentUser, sections } = this.props
    const { id, status, meta, teams, journal, files } = manuscript
    const { releaseDelay = '', unmatchedJournal } = meta
    const { checkedBoxes, reject, message } = this.state
    const submitter = teams.find(team => team.role === 'submitter')
      ? teams.find(team => team.role === 'submitter').teamMembers[0]
      : null
    const reviewer = teams.find(team => team.role === 'reviewer')
      ? teams.find(team => team.role === 'reviewer').teamMembers[0]
      : null
    const { title, givenNames, surname } = submitter.alias.name
    const submitterName = `${title && `${title} `}${givenNames} ${surname}`
    const options = [
      {
        value: '1',
        label: `This manuscript has been accepted for publication in ${
          journal ? journal.meta.nlmta : unmatchedJournal
        } and includes all modifications resulting from the peer review process.`,
      },
      {
        value: '2',
        label: `I request that this manuscript be publicly accessible through Europe PMC ${releaseDelay &&
          (releaseDelay === '0'
            ? 'immediately'
            : `${releaseDelay} month${
                releaseDelay === '1' ? '' : 's'
              }`)} after the publisher's official date of final publication, and I confirm that the publisher is aware of, and has agreed to, this action.`,
      },
      {
        value: '3',
        label:
          'This manuscript is the result of research supported, in whole or in part, by a member of the Europe PMC Funders Group.',
      },
    ]
    const xml = files.find(f => f.type === 'PMC') || null
    return (
      <Mutation mutation={SUBMIT_MANUSCRIPT}>
        {(submitManuscript, { data }) => {
          const submit = async newStatus => {
            const variables = { data: { id, status: newStatus } }
            if (newStatus === 'submitted' && xml) {
              variables.data.status = 'xml-triage'
            }
            await submitManuscript({ variables })
            if (currentUser.admin) {
              this.props.history.push('/')
            }
          }
          const Certify = () => (
            <div>
              <p>Please certify the following statements are true:</p>
              <CheckboxGroup
                name="reviewer-checklist"
                onChange={c => this.setState({ checkedBoxes: c })}
                options={options}
                value={checkedBoxes}
              />
              <Help>
                <Icon size={2}>help-circle</Icon>
                <span>
                  What is the{' '}
                  <A href="https://europepmc.org/Funders/" target="_blank">
                    Europe PMC Funder&apos;s Group?
                  </A>
                </span>
              </Help>
            </div>
          )
          const ApproveButton = props => (
            <Button
              disabled={
                checkedBoxes.length <= 0 ||
                checkedBoxes.reduce(
                  (acc, val) => parseInt(acc, 10) + parseInt(val, 10),
                ) < 6 ||
                sections.some(sec => sec.error)
              }
              onClick={() => submit('submitted')}
              primary
            >
              {props.text}
            </Button>
          )
          if (currentUser.admin && status === 'submitted') {
            return (
              <React.Fragment>
                <Header>
                  <H3>Review submission</H3>
                </Header>
                <Content>
                  <div>
                    <p>
                      Check the following and select anything that is missing:
                    </p>
                    <CheckboxGroup
                      name="admin-checklist"
                      onChange={c => this.setState({ checkedBoxes: c })}
                      options={adminOptions}
                      value={checkedBoxes}
                    />
                    <Buttons>
                      <Button
                        disabled={
                          checkedBoxes.length > 0 ||
                          sections.some(sec => sec.error)
                        }
                        onClick={() => submit('tagging')}
                        primary
                      >
                        Approve for tagging
                      </Button>
                      {checkedBoxes.length > 0 && (
                        <Button onClick={() => this.setState({ reject: true })}>
                          Reject submission
                        </Button>
                      )}
                    </Buttons>
                  </div>
                </Content>
                {reject && (
                  <Portal transparent>
                    <Close style={{ margin: 0 }}>
                      <CloseButton
                        onClick={() => this.setState({ reject: false })}
                      />
                    </Close>
                    <SubmissionErrorReport
                      checkedBoxes={checkedBoxes}
                      close={() => this.setState({ reject: false })}
                      manuscript={manuscript}
                      reviewer={reviewer}
                      submitter={submitter}
                    />
                  </Portal>
                )}
              </React.Fragment>
            )
          } else if (submitter && submitter.user.id === currentUser.id) {
            return (
              <React.Fragment>
                <Header>
                  <H3>Approve submission</H3>
                </Header>
                <Content>
                  <div>
                    {status === 'submission-error' && (
                      <p>
                        {`Errors have been reported with this submission. You may edit the manuscript submission to correct these errors by clicking the pencil icon next to any section. When you are done, resubmit the manuscript.`}
                      </p>
                    )}
                    <Certify />
                    <Buttons right>
                      <ApproveButton text="Submit manuscript" />
                    </Buttons>
                  </div>
                </Content>
              </React.Fragment>
            )
          } else if (reviewer && reviewer.user.id === currentUser.id) {
            return (
              <React.Fragment>
                <Header>
                  <H3>Review submission</H3>
                </Header>
                <Content>
                  <div>
                    <p>
                      {`You have been designated as the reviewer for this submission.`}
                    </p>
                    {status === 'in-review' && (
                      <React.Fragment>
                        <p>
                          {`Approve the submission for processing, or reject it to send it back to the submitter with comments. Alternatively, you may edit the manuscript submission yourself by clicking the pencil icon next to any section.`}
                        </p>
                        <p>
                          {`You will be responsible for approving the final web version. You must be an author of the manuscript. If you do not meet the requirements to review and approve this manuscript submission, please return the manuscript to the submitter, or contact the helpdesk.`}
                        </p>
                      </React.Fragment>
                    )}
                    {status === 'submission-error' && (
                      <React.Fragment>
                        <p>
                          {`Errors have been reported with this submission. If you are able to fix the errors, you may edit the manuscript submission yourself by clicking the pencil icon next to any section, then approve it to send it back to the helpdesk.`}
                        </p>
                        <p>
                          {`If you require the submitter to fix the errors, no further action is required; they have already been contacted, and you will be contacted for an additional review once they have made their corrections.`}
                        </p>
                      </React.Fragment>
                    )}
                    <Certify />
                    <Buttons>
                      <ApproveButton text="Approve submission" />
                      {status === 'in-review' && (
                        <Button onClick={() => this.setState({ reject: true })}>
                          Reject submission
                        </Button>
                      )}
                    </Buttons>
                  </div>
                </Content>
                {reject && (
                  <Portal transparent>
                    <Close style={{ margin: 0 }}>
                      <CloseButton
                        onClick={() => this.setState({ reject: false })}
                      />
                    </Close>
                    <ReviewerErrorReport
                      close={() => this.setState({ reject: false })}
                      manuscriptId={id}
                      message={message}
                      onChange={message => this.setState({ message })}
                      submitter={submitterName}
                    />
                  </Portal>
                )}
              </React.Fragment>
            )
          } else if (currentUser.admin) {
            return (
              <React.Fragment>
                <Header>
                  <H3>Attention Helpdesk</H3>
                </Header>
                <Content>
                  <div>
                    <p>
                      <B>This manuscript has not yet been submitted!</B>
                      {` It is '${status}'`}
                    </p>
                    <p>
                      {`You may submit this manuscript, but this should only be done if you are changing the reviewer, or in some other special circumstance.`}
                    </p>
                    <Buttons>
                      <Button onClick={() => submit('submitted')} primary>
                        Submit
                      </Button>
                    </Buttons>
                  </div>
                </Content>
              </React.Fragment>
            )
          }
          this.props.history.push('/')
          return null
        }}
      </Mutation>
    )
  }
}

export default withRouter(SubmitForm)
