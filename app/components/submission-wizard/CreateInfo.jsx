import React from 'react'
import { H3, Link } from '@pubsweet/ui'
import { A } from '../ui'

const CreateInfo = ({ currentStep }) => (
  <React.Fragment>
    {currentStep === 0 && (
      <div className="createInfo">
        <H3>Getting started</H3>
        <p>
          {`The first step to submitting a manuscript is to find the manuscript title and citation. We provide a title lookup function which will search Pubmed for a citation. If you can't find the manuscript, enter the information manually.`}
        </p>
        <H3>What you’ll need to complete the submission</H3>
        <ul>
          <li>Manuscript title</li>
          <li>Journal name if not in PubMed</li>
          <li>Grant numbers</li>
          <li>All manuscript files including supplemental data</li>
        </ul>
        <H3>Overview of the process</H3>
        <ol>
          <li>Submit manuscript</li>
          <li>Review PDF for completeness</li>
          <li>Review final web version</li>
        </ol>
        <H3>How long it will take</H3>
        <p>
          {`If all manuscript files are submitted in appropriate formats, and if approval and communication are prompt, the process typically takes about six to eight weeks.`}
        </p>
      </div>
    )}
    {currentStep === 1 && (
      <div className="createInfo">
        <H3>Which versions of files should I upload?</H3>
        <p>
          {`Authors can check the `}
          <A href="http://www.sherpa.ac.uk/romeo/index.php" target="_blank">
            SHERPA/RoMEO
          </A>
          {` database for publishers' open access policies.`}
        </p>
        <H3>How should I label files?</H3>
        <p>
          {`The labels are intended to help reviewers put your manuscript together as intended. Provide a label that matches the corresponding file as mentioned in the text.`}
        </p>
        <H3>What file formats can I use?</H3>
        <p>
          {`For the manuscript text, only Microsoft Word or PDF files can be used. For figures, tables, and supplementary files, check `}
          <Link to="/user-guide/allowedfiles">
            our list of acceptable file types
          </Link>
          .
        </p>
      </div>
    )}
    {currentStep === 2 && (
      <div className="createInfo">
        <H3>
          {`What if a grant or project that supported this manuscript is not in the list?`}
        </H3>
        <p>
          {`If a particular grant or project that you are expecting to see is not listed, please verify that the proper PI surname was entered. You must enter the surname first. In addition to the surname you may enter the initial of the PI&apos;s given name (e.g. 'Smith R', not 'Smith Robert').`}
        </p>
        <p>
          {`If you can’t locate your grant and/or project in the list, contact the Helpdesk to ask for help locating your grant. You'll be asked to provide the grant/contract number, project title, and contact PI's name.`}
        </p>
        <H3>Selecting an embargo period</H3>
        <p>
          {`The embargo period lets us know the delay from the time the manuscript is published in the journal until it can be published in Europe PMC. The embargo periods listed are those allowed by your funder. If there is a conflict between the embargo period required by your journal and those allowed by your funder, please contact the Helpdesk.`}
        </p>
      </div>
    )}
    {currentStep === 3 && (
      <div className="createInfo">
        <H3>What is a reviewer?</H3>
        <p>
          {`The reviewer will have final approval over the web version of this manuscript submission created for Europe PMC. Most commonly, you, the submitter, will also be the reviewer. If you are submitting this manuscript on behalf of someone else, please designate that person as the reviewer.`}
        </p>
        <p>
          {`The designated reviewer must be one of the authors of the manuscript.`}
        </p>
        <H3>What if the correct person isn&apos;t listed?</H3>
        <p>
          {`The options provided for the reviewer include you and the PIs of your selected grants. If the reviewer should be someone other than you or one of your grant PIs, please select the last option, 'Other reviewer', and enter the person's name and email address in the fields provided.`}
        </p>
      </div>
    )}
  </React.Fragment>
)

export default CreateInfo
