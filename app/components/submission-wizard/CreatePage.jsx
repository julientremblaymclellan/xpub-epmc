import React from 'react'
import { withRouter } from 'react-router'
import { Query } from 'react-apollo'
import { omit } from 'lodash'
import styled, { createGlobalStyle } from 'styled-components'
import { th, lighten } from '@pubsweet/ui-toolkit'
import { Icon, Button, Checkbox, H2 } from '@pubsweet/ui'
import { UserContext } from '../App'
import {
  Page,
  Buttons,
  InfoPanel,
  Loading,
  LoadingIcon,
  Notification,
  SplitPage,
  StepPanel,
} from '../ui/'
import PubMedSearch from '../citation-search'
import UploadFiles, { SubmissionTypes } from '../upload-files'
import SubmissionHeader from '../SubmissionHeader'
import { NoteMutations, ManuscriptMutations } from '../SubmissionMutations'
import { GET_MANUSCRIPT } from '../operations'
import GrantSearch from '../GrantSearch'
import CreatePageHeader from './CreateHeader'
import CreateInfo from './CreateInfo'
import Citation from './Citation'
import SelectReviewer from './SelectReviewer'

const FadeIn = createGlobalStyle`
  @keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }
  .createInfo {
    opacity: 1;
    animation-name: fadeIn;
    animation-delay: 0s;
    animation-duration: .75s;
  }
`
const Status = styled.div`
  margin-right: calc(${th('gridUnit')} * 2);
  font-style: italic;
  display: inline-flex;
  align-items: center;
  color: ${lighten('colorText', 28)};
`
const Confirm = styled.div`
  display: flex;
  align-items: flex-start;
  & > p {
    margin-top: ${th('gridUnit')};
  }
  & > span {
    margin-left: calc(${th('gridUnit')} * 2);
    color: ${th('colorSuccess')};
  }
`
const ErrorText = styled.span`
  color: ${th('colorError')};
`
const ReviewerWithMutations = NoteMutations(SelectReviewer)
const PubMedWithMutations = NoteMutations(PubMedSearch)

class Created extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      currentStep: this.getCurrentStep(),
      showSearch: false,
      checked: false,
      showInfo: false,
      status: '',
      error: '',
    }
  }
  static contextType = UserContext
  getCurrentStep = () => {
    const { meta, files } = this.props.manuscript
    const { notes } = meta
    const test1 = files && files.length > 0
    const test2 = meta.fundingGroup && meta.fundingGroup.length > 0
    const test3 =
      meta.releaseDelay &&
      notes &&
      notes.some(n => n.notesType === 'selectedReviewer')
    if (test1 && test2 && test3) {
      return 3
    } else if (test1 && test2) {
      return 2
    } else if (test1) {
      return 1
    }
    return 0
  }
  getDisableNext = step => {
    const { meta, files } = this.props.manuscript
    const { notes } = meta
    switch (step) {
      case 3:
        if (!notes || !notes.some(n => n.notesType === 'selectedReviewer')) {
          return true
        }
        return false
      case 2:
        if (
          !meta.fundingGroup ||
          meta.fundingGroup.length < 1 ||
          !meta.releaseDelay
        ) {
          return true
        }
        return false
      case 1:
        if (!files || !this.state.checked) {
          return true
        } else if (
          files &&
          files
            .filter(
              file =>
                !file.type || SubmissionTypes.some(t => t.value === file.type),
            )
            .some(file => !file.label || !file.type)
        ) {
          return true
        }
        return false
      default:
        return false
    }
  }
  checkFiles = () => {
    const { checked } = this.state
    const { files } = this.props.manuscript
    const newState = {}

    // hacky way
    let hasEmptyInputs = false
    const inputs = document.querySelectorAll('input[type=text]')
    for (let i = 0; i < inputs.length; i += 1) {
      if (!inputs[i].value.trim()) {
        hasEmptyInputs = true
        break
      }
    }
    if (
      !checked &&
      files &&
      hasEmptyInputs &&
      files
        .filter(
          file =>
            !file.type || SubmissionTypes.some(t => t.value === file.type),
        )
        .some(file => !file.label || !file.type)
    ) {
      newState.checked = false
      newState.error =
        'Error: All indicated files must have a type and a label.'
    } else {
      newState.checked = !checked
      newState.error = ''
    }
    this.setState(newState)
  }
  goPrev = () => {
    const newState = { status: '' }
    if (this.state.currentStep > 0) {
      newState.currentStep = this.state.currentStep - 1
    } else {
      newState.showSearch = true
    }
    this.setState(newState)
    window.scrollY = 0
    window.pageYOffset = 0
    document.scrollingElement.scrollTop = 0
  }
  goNext = () => {
    const newState = { status: '' }
    if (this.state.showSearch) {
      newState.showSearch = false
    } else if (this.state.currentStep < 3) {
      newState.currentStep = this.state.currentStep + 1
    } else {
      const { id } = this.props.manuscript
      this.props.setStatus('READY', () => {
        this.props.history.push(`/submission/${id}/submit`)
      })
    }
    this.setState(newState)
    window.scrollY = 0
    window.pageYOffset = 0
    document.scrollingElement.scrollTop = 0
  }
  changeCitation = citationData => {
    this.props.changeCitation(citationData)
    this.setState({ showSearch: false })
  }
  render() {
    const currentUser = this.context
    const { currentStep, status, error, checked } = this.state
    const { manuscript, history } = this.props
    const { id: mId, meta, journal, files: allfiles, teams } = manuscript
    const { notes, fundingGroup: grants } = meta
    const files = allfiles
      ? allfiles.filter(
          file =>
            !file.type ||
            file.type === 'manuscript' ||
            SubmissionTypes.some(t => t.value === file.type),
        )
      : []
    const fundingGroup = grants
      ? grants.map(g => {
          const n = omit(g, '__typename')
          n.pi = omit(g.pi, '__typename')
          return n
        })
      : []
    const reviewerNote = notes
      ? notes.find(n => n.notesType === 'selectedReviewer')
      : null
    const submitter =
      teams && teams.find(team => team.role === 'submitter')
        ? teams.find(team => team.role === 'submitter').teamMembers[0]
        : null
    if (manuscript.status === 'INITIAL') {
      return (
        <SplitPage>
          <StepPanel>
            <div>
              <CreatePageHeader currentStep={currentStep} />
              {currentStep === 0 && <H2>Citation</H2>}
              {currentStep === 1 && <H2>Files</H2>}
              {error && <Notification type="error">{error}</Notification>}
              {currentStep === 0 && (
                <React.Fragment>
                  {this.state.showSearch ? (
                    <PubMedWithMutations
                      citationData={this.changeCitation}
                      manuscript={manuscript}
                    />
                  ) : (
                    <Confirm>
                      <Citation journal={journal} metadata={meta} />
                      <Icon color="currentColor" size={5}>
                        check
                      </Icon>
                    </Confirm>
                  )}
                </React.Fragment>
              )}
              {currentStep === 1 && (
                <React.Fragment>
                  <UploadFiles
                    checked={checked}
                    files={files}
                    manuscript={mId}
                    parentStatus={status}
                    types={SubmissionTypes}
                  />
                  {files.length > 0 && (
                    <React.Fragment>
                      <p>
                        {!checked && <ErrorText>* </ErrorText>}
                        Please certify the following statement is true:
                      </p>
                      <Checkbox
                        checked={checked}
                        label="I have provided all of the files that are part of this manuscript, including figures, tables, appendices, and all supplemental files. I understand that the processing of the manuscript will be delayed if any of the manuscript files are missing."
                        onChange={() => this.checkFiles()}
                      />
                    </React.Fragment>
                  )}
                </React.Fragment>
              )}
              {currentStep === 2 && (
                <GrantSearch
                  changedEmbargo={this.props.updateEmbargo}
                  changedGrants={this.props.updateGrants}
                  selectedEmbargo={meta.releaseDelay}
                  selectedGrants={fundingGroup}
                />
              )}
              {currentStep === 3 && (
                <ReviewerWithMutations
                  currentUser={currentUser}
                  funding={fundingGroup}
                  manuscriptId={mId}
                  reviewer={null}
                  reviewerNote={reviewerNote}
                  submitter={submitter}
                />
              )}
              <Buttons>
                <Buttons right>
                  <Button
                    disabled={this.getDisableNext(currentStep)}
                    onClick={this.goNext}
                    primary={!this.state.showSearch}
                    title={
                      this.state.showSearch
                        ? 'Click to proceed with previously selected citation'
                        : ''
                    }
                  >
                    {this.state.showSearch ? 'Cancel citation change' : 'Next'}
                  </Button>
                  {!this.state.showSearch && (
                    <Button onClick={this.goPrev}>
                      {currentStep === 0 ? 'Change citation' : 'Previous'}
                    </Button>
                  )}
                </Buttons>
                <Buttons left>
                  {!this.state.showSearch && (
                    <Button onClick={() => history.push('/dashboard')}>
                      Save and complete later
                    </Button>
                  )}
                  {status && (
                    <Status>
                      <Icon color="currentColor" size={2}>
                        info
                      </Icon>
                      {status}
                    </Status>
                  )}
                </Buttons>
              </Buttons>
            </div>
          </StepPanel>
          <InfoPanel>
            <FadeIn />
            <div
              className={`hidden ${this.state.showInfo && 'showInfo'}`}
              id="more-info"
              onClick={() => this.setState({ showInfo: !this.state.showInfo })}
            >
              More Info
              <Icon>{`chevron-${this.state.showInfo ? 'down' : 'right'}`}</Icon>
            </div>
            <CreateInfo currentStep={currentStep} />
          </InfoPanel>
        </SplitPage>
      )
    }
    history.push(`/submission/${manuscript.id}/submit`)
    return null
  }
}

const CreatedWithMutatons = ManuscriptMutations(Created)
const CreatedWithHeader = SubmissionHeader(CreatedWithMutatons)

const CreatePage = ({ match, ...props }) => (
  <Query
    fetchPolicy="cache-and-network"
    query={GET_MANUSCRIPT}
    variables={{ id: match.params.id }}
  >
    {({ data, loading }) => {
      if (loading || !data || !data.manuscript) {
        return (
          <Page>
            <Loading>
              <LoadingIcon />
            </Loading>
          </Page>
        )
      }
      return (
        <CreatedWithHeader
          manuscript={data.manuscript}
          match={match}
          {...props}
        />
      )
    }}
  </Query>
)

export default withRouter(CreatePage)
