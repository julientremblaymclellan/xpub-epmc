import React from 'react'
import styled, { css } from 'styled-components'
import * as mime from 'mime-types'
import { Icon } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { B } from '../ui'
import { FileLightbox, fileSort, ImageTypes } from './'

const Thumbnails = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  margin: 0;
  padding: 0;
`
const Thumbnail = styled.div`
  display: flex;
  width: 50%;
  align-items: center;
  padding: calc(${th('gridUnit')} / 2);
  @media screen and (max-width: 1370px) {
    width: 100%;
  }
  @media screen and (max-width: 870px) {
    width: 33%;
  }
  @media screen and (max-width: 750px) {
    width: 50%;
  }
  @media screen and (max-width: 450px) {
    width: 100%;
  }
`
const Thumb = styled.div`
  width: 33%;
  max-width: 200px;
  @media screen and (min-width: 870px) and (max-width: 1370px) {
    width: 40%;
  }
  @media screen and (max-width: 450px) {
    width: 40%;
  }
`
const ImageBox = styled.div`
  height: 0;
  padding-top: 98%;
  color: ${th('colorTextReverse')};
  background-color: ${th('colorBorder')};
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  ${props => props.image && `background-image: ${props.image};`}
  background-size: cover;
  margin: calc(${th('gridUnit')} / 2);
  @media screen and (min-width: 870px) and (max-width: 1370px) {
    padding-top: 45%;
  }
  @media screen and (min-width: 650px) and (max-width: 750px) {
    padding-top: 45%;
  }
  @media screen and (max-width: 450px) {
    padding-top: 45%;
  }
  p {
    margin: -70% 0 0;
    text-align: center;
    font-size: ${th('fontSizeHeading2')};
    @media screen and (min-width: 1150px) and (max-width: 1370px) {
      margin-top: -35%;
      font-size: calc(${th('fontSizeHeading1')} * 1.25);
    }
    @media screen and (min-width: 1000px) and (max-width: 1150px) {
      margin-top: -35%;
      font-size: ${th('fontSizeHeading1')};
    }
    @media screen and (min-width: 870px) and (max-width: 1000px) {
      margin-top: -38%;
    }
    @media screen and (min-width: 650px) and (max-width: 750px) {
      margin-top: -38%;
    }
    @media screen and (max-width: 450px) {
      margin-top: -35%;
      font-size: ${th('fontSizeHeading1')};
    }
  }
`
const FileInfo = styled.p`
  width: 66%;
  margin: 0 calc(${th('gridUnit')} / 2);
  font-size: ${th('fontSizeBaseSmall')};
  word-break: break-all;
  span {
    display: inline-block;
    margin-right: ${th('gridUnit')};
  }
  @media screen and (min-width: 870px) and (max-width: 1370px) {
    width: 60%;
  }
  @media screen and (max-width: 450px) {
    width: 60%;
  }
`
const error = css`
  display: inline-flex;
  align-items: flex-start;
  color: ${th('colorError')};
`
const Bold = styled(B)`
  ${props => props.error && error}
`
const FileThumbnails = ({ files }) => (
  <Thumbnails>
    {fileSort(files).map(file => (
      <Thumbnail key={file.url}>
        <Thumb>
          <ImageBox
            image={ImageTypes.includes(file.mimeType) && `url('${file.url}')`}
          >
            {!ImageTypes.includes(file.mimeType) && (
              <p>.{mime.extension(file.mimeType)}</p>
            )}
          </ImageBox>
        </Thumb>
        <FileInfo>
          <FileLightbox file={file} />
          <br />
          <span>
            <Bold error={!file.type}>
              Type:
              {!file.type && (
                <Icon color="currentColor" size={2}>
                  x-circle
                </Icon>
              )}
            </Bold>
            {file.type && ` ${file.type}`}
          </span>
          {file.type !== 'manuscript' && (
            <span>
              <Bold error={!file.label}>
                Label:
                {!file.label && (
                  <Icon color="currentColor" size={2}>
                    x-circle
                  </Icon>
                )}
              </Bold>
              {file.label && ` ${file.label}`}
            </span>
          )}
        </FileInfo>
      </Thumbnail>
    ))}
  </Thumbnails>
)

export default FileThumbnails
