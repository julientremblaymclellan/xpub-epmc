import React from 'react'
import { Field } from 'formik'
import {
  ErrorText,
  H1,
  Button,
  Checkbox,
  TextField,
  Link,
  Icon,
} from '@pubsweet/ui'
import styled from 'styled-components'
import { Page, Notification } from '../ui'
import UserCore from '../UserCore'

const PasswordField = styled.div`
  max-width: 400px;
  margin-bottom: 32px;
  .dAHGiG {
    position: absolute;
    right: 0;
    top: 0;
  }
  .new-password {
    position: relative;
  }
`
const MyAccountPage = styled(Page)`
  a {
    display: block;
    margin-top: 42px;
    margin-bottom: 32px;
    width: fit-content;
    span {
      vertical-align: middle;
    }
  }
`
const CurrentPasswordInput = props => (
  <TextField
    invalidTest={props.invalidTest}
    label="Current Password"
    {...props.field}
    type="password"
  />
)
const NewPasswordInput = props => (
  <TextField
    invalidTest={props.invalidTest}
    label="New Password"
    {...props.field}
    type="password"
  />
)
const NewPlainPasswordInput = props => (
  <TextField
    invalidTest={props.invalidTest}
    label="New Password (at least 8 characters)"
    {...props.field}
  />
)

class MyAccount extends React.Component {
  state = {
    showPassword: false,
    showPasswordSection: false,
  }
  toggleShowPassword = () => {
    const { showPasswordSection, showPassword } = this.state
    this.setState({
      showPasswordSection,
      showPassword: !showPassword,
    })
  }
  toggleShowPasswordSection = () => {
    const { showPasswordSection, showPassword } = this.state
    this.setState({
      showPassword,
      showPasswordSection: !showPasswordSection,
    })
  }
  render() {
    const { values, errors, handleSubmit, touched } = this.props
    return (
      <MyAccountPage>
        <H1>My Account</H1>
        {values.success && (
          <Notification type="success"> {values.success} </Notification>
        )}
        {values.error && (
          <Notification type="error"> {values.error} </Notification>
        )}
        <form onSubmit={handleSubmit}>
          <UserCore {...this.props} />
          <Link onClick={() => this.toggleShowPasswordSection()} to="#">
            Change password
            <Icon color="currentColor" size={3}>
              {this.state.showPasswordSection
                ? 'chevron-down'
                : 'chevron-right'}
            </Icon>
          </Link>
          {this.state.showPasswordSection && (
            <PasswordField>
              <Field
                component={CurrentPasswordInput}
                invalidTest={errors.currentPassword && touched.currentPassword}
                name="currentPassword"
              />
              {errors.currentPassword && (
                <ErrorText>{errors.currentPassword}</ErrorText>
              )}
              <div className="new-password">
                {!this.state.showPassword && (
                  <Field
                    component={NewPasswordInput}
                    invalidTest={errors.newPassword && touched.newPassword}
                    name="newPassword"
                  />
                )}

                {this.state.showPassword && (
                  <Field
                    component={NewPlainPasswordInput}
                    invalidTest={errors.newPassword && touched.newPassword}
                    name="password"
                  />
                )}
                {errors.newPassword && touched.newPassword && (
                  <ErrorText>{errors.newPassword}</ErrorText>
                )}
                <Checkbox
                  checked={this.state.showPassword}
                  label="Show password"
                  onChange={() => this.toggleShowPassword()}
                />
              </div>
            </PasswordField>
          )}
          <Button primary type="submit">
            Confirm
          </Button>
        </form>
      </MyAccountPage>
    )
  }
}

export default MyAccount
