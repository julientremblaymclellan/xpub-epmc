import gql from 'graphql-tag'

const UPDATE_CURRENT_USER = gql`
  mutation($input: UpdateCurrentUserInput) {
    epmc_updateCurrentUser(input: $input) {
      id
      identities {
        type
        email
        name {
          title
          givenNames
          surname
        }
      }
    }
  }
`

module.exports = {
  UPDATE_CURRENT_USER,
}
