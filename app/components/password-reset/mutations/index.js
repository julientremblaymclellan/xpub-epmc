import config from 'config'

const { resetPassword, emailPasswordResetLink } = config['pubsweet-client'][
  'password-reset'
].graphql.mutations

module.exports = {
  emailPasswordResetLink,
  resetPassword,
}
