import { compose } from 'recompose'
import { withFormik } from 'formik'
import { graphql } from 'react-apollo'
import * as yup from 'yup' // for everything
import PasswordResetEmail from './PasswordResetEmail'
import mutations from './mutations'

const handleSubmit = (
  values,
  { props, setSubmitting, setErrors, resetForm, setStatus },
) =>
  props
    .emailPasswordResetLink({ variables: { email: values.email } })
    .then(({ data, errors }) => {
      resetForm()
      !errors && setStatus({ email: values.email })
    })
    .catch(e => {
      if (e.graphQLErrors) {
        setSubmitting(false)
        setErrors(e.graphQLErrors[0].message)
      }
    })

const enhancedFormik = withFormik({
  initialValues: {
    email: '',
  },
  mapPropsToValues: props => ({
    email: props.email,
  }),
  validationSchema: yup.object().shape({
    email: yup
      .string()
      .email('Email not valid')
      .required('Email is required'),
  }),
  displayName: 'password-reset-email',
  handleSubmit,
})(PasswordResetEmail)

export default compose(
  graphql(mutations.emailPasswordResetLink, {
    name: 'emailPasswordResetLink',
  }),
)(enhancedFormik)
