import React from 'react'
import { Field } from 'formik'
import { isEmpty } from 'lodash'
import config from 'config'
import { H1, Link, Button, TextField } from '@pubsweet/ui'
import { Page, Notification } from '../ui/'

const { appName } = config['pubsweet-client']['password-reset']

const PasswordInput = props => (
  <TextField
    className="double-column last-column"
    label="Password *"
    {...props.field}
    type="password"
  />
)
const ConfirmPasswordInput = props => (
  <TextField
    className="double-column"
    label="Confirm Password *"
    {...props.field}
    type="password"
  />
)

const PasswordReset = ({ errors, handleSubmit, touched, status }) => (
  <Page>
    <H1>Reset your {appName} password</H1>
    <p>Enter a new password for your {appName} account</p>
    {errors.password && touched.password && (
      <Notification type="error">{errors.password}</Notification>
    )}
    {!errors.password && errors.confirmPassword && touched.confirmPassword && (
      <Notification type="error">{errors.confirmPassword}</Notification>
    )}

    {!isEmpty(errors) && typeof errors === 'string' && (
      <Notification type="error">{errors}</Notification>
    )}

    <form onSubmit={handleSubmit} style={{ maxWidth: '60ch' }}>
      <Field component={PasswordInput} name="password" />
      <Field component={ConfirmPasswordInput} name="confirmPassword" />
      <Button primary type="submit">
        Reset password
      </Button>

      {status && status.reset && (
        <p>
          <strong>Password reset!</strong>
          <br />
          Your {appName} account password has been reset. Sign in&nbsp;
          <Link to="/login">here</Link> using your new password.
        </p>
      )}
    </form>
  </Page>
)

// used by consumers
export default PasswordReset
