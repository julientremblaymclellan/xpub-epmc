import React from 'react'
import annotator from 'annotator'
import AnnotatorStyle from './AnnotatorStyle'

const Annotator = BaseComponent =>
  class AnnotatorDiv extends React.Component {
    state = { app: null }
    setRef = elem => {
      this.elem = elem
    }
    startAnnotator = () => {
      const { file, userId, revId } = this.props
      const { app } = this.state
      const annOpts = {
        user_id: userId,
        file_id: file.id,
        review_id: revId,
      }
      if (app) {
        app.destroy().then(() => {
          this.start(this.elem, annOpts)
        })
      } else {
        this.start(this.elem, annOpts)
      }
    }
    async start(elm, options) {
      if (this.elem) {
        const app = new annotator.App()
        await app
          .include(annotator.ui.main, { element: elm })
          .include(annotator.storage.http, {
            prefix: '/annotations',
            urls: {
              create: '/',
              update: `/{id}`,
              destroy: `/{id}`,
              search: `/file/${options.file_id}`,
            },
          })
          .include(this.addHooks, options)
          .start()
        await app.annotations.store.setHeader(
          'Authorization',
          `Bearer ${window.localStorage.getItem('token')}`,
        )
        await app.annotations.load()
        this.setState({ app })
      }
    }
    addHooks = options => ({
      annotationsLoaded: annotations => {},
      beforeAnnotationCreated: annotation => {
        this.packAnnotation(annotation, options)
      },
      annotationCreated: annotation => {
        this.props.reload && this.props.reload(true)
      },
      beforeAnnotationUpdated: annotation => {
        this.packAnnotation(annotation, options)
      },
      annotationUpdated: annotation => {
        this.props.reload && this.props.reload(true)
      },
      beforeAnnotationDeleted: annotation => {
        this.packAnnotation(annotation, options)
      },
      annotationDeleted: annotation => {
        this.props.reload && this.props.reload(true)
      },
    })

    packAnnotation = (annotation, options) => {
      annotation.user_id = options.user_id
      annotation.file_id = options.file_id
      annotation.review_id = options.review_id
      annotation.ranges = annotation.ranges
    }
    render() {
      const { file, revId, userId, children, style, ...props } = this.props
      return (
        <div className="annotator" ref={this.setRef} style={style}>
          <AnnotatorStyle />
          <BaseComponent loaded={this.startAnnotator} url={file.url} {...props}>
            {children}
          </BaseComponent>
        </div>
      )
    }
  }

export default Annotator
