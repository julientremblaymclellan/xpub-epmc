import React from 'react'
import moment from 'moment'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Action, H5, Icon } from '@pubsweet/ui'
import { ZebraList, ZebraListItem } from '../ui'

const History = styled.div`
  padding: 0 0 calc(${th('gridUnit')} * 2);
`
const ListItem = styled(ZebraListItem)`
  font-size: ${th('fontSizeBaseSmall')};
`
const ErrorReport = styled.div`
  padding: ${th('gridUnit')} 0 calc(${th('gridUnit')} * 2);
  h5 {
    margin-top: 0;
  }
`
const Drop = styled(Action)`
  font-weight: 600;
  display: flex;
  align-items: flex-start;
`
const DeletedErrors = ({ annotations }) => {
  const list = { tempHTML: [], pdf4print: [] }
  annotations.forEach(a => list[a.file.type].push(a))
  return (
    <ErrorReport>
      {list.tempHTML.length > 0 && (
        <React.Fragment>
          <H5>Web preview errors</H5>
          <AList list={list.tempHTML} />
        </React.Fragment>
      )}
      {list.pdf4print.length > 0 && (
        <React.Fragment>
          <H5>PDF preview errors</H5>
          <AList list={list.pdf4print} />
        </React.Fragment>
      )}
    </ErrorReport>
  )
}

const AList = ({ list }) => (
  <ZebraList>
    {list.map(a => (
      <ListItem key={a.id}>
        <em>{a.quote}</em>
        <br />
        {a.text}
      </ListItem>
    ))}
  </ZebraList>
)

class ReviewHistory extends React.Component {
  state = {
    showList: false,
    show: this.props.reviews.map(r => false),
  }
  render() {
    const { reviews } = this.props
    const { showList, show } = this.state
    return (
      <History>
        <Drop onClick={() => this.setState({ showList: !showList })}>
          Previous error reports
          <Icon color="currentColor" size={2.5}>
            {showList ? 'chevron-down' : 'chevron-right'}
          </Icon>
        </Drop>
        {showList && (
          <React.Fragment>
            {reviews.map((review, i) => {
              const { created, user, annotations } = review
              const { givenNames, surname } = user.identities[0].name
              return (
                <React.Fragment key={review.id}>
                  <Drop
                    onClick={() =>
                      this.setState({
                        show: show.map((s, n) => (n === i ? !s : s)),
                      })
                    }
                  >
                    {givenNames} {surname} {moment(created).format('D/M/YY')}
                    <Icon color="currentColor" size={2.5}>
                      {show[i] ? 'chevron-down' : 'chevron-right'}
                    </Icon>
                  </Drop>
                  {show[i] && (
                    <DeletedErrors annotations={annotations} key={review.id} />
                  )}
                </React.Fragment>
              )
            })}
          </React.Fragment>
        )}
      </History>
    )
  }
}

export default ReviewHistory
