import React from 'react'
import moment from 'moment'
import styled from 'styled-components'
import { th, lighten } from '@pubsweet/ui-toolkit'
import { H4 } from '@pubsweet/ui'

const Annotations = styled.ul`
  font-size: ${th('fontSizeBaseSmall')};
  list-style-type: none;
  padding-left: 0;
  margin-left: 0;
  li {
    padding: calc(${th('gridUnit')} * 2);
    border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
    border-radius: calc(${th('borderRadius')} * 3);
    background-color: ${lighten('colorWarning', 85)};
    & + li {
      margin-top: calc(${th('gridUnit')} * 2);
    }
    p {
      font-size: ${th('fontSizeBaseSmall')};
      margin-bottom: 0;
    }
    &:after {
      content: '';
      display: block;
      clear: both;
    }
  }
`
const TimeStamp = styled.div`
  float: right;
  margin: 0 0;
  font-size: calc(${th('gridUnit')} * 1.5);
`

const AList = ({ list }) => (
  <Annotations>
    {list.map(a => (
      <li key={a.id}>
        <em>{a.quote}</em>
        <p>{a.text}</p>
        <TimeStamp>{moment(a.updated).format('D/M/YY H:m')}</TimeStamp>
      </li>
    ))}
  </Annotations>
)

const ListErrors = ({ annotations }) => {
  const list = { tempHTML: [], pdf4print: [] }
  annotations.forEach(a => list[a.file.type].push(a))
  return (
    <React.Fragment>
      {list.tempHTML.length > 0 && (
        <React.Fragment>
          <H4>Web preview errors</H4>
          <AList list={list.tempHTML} />
        </React.Fragment>
      )}
      {list.pdf4print.length > 0 && (
        <React.Fragment>
          <H4>PDF preview errors</H4>
          <AList list={list.pdf4print} />
        </React.Fragment>
      )}
    </React.Fragment>
  )
}

export default ListErrors
