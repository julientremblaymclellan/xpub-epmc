import React from 'react'
import { withRouter } from 'react-router'
import { Mutation, Query } from 'react-apollo'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Action, Button, H3 } from '@pubsweet/ui'
import {
  Notification,
  Loading,
  LoadingIcon,
  Buttons,
  SectionContent as Content,
  SectionHeader as Header,
} from '../ui'
import { FileThumbnails } from '../preview-files'
import ListErrors from './ListErrors'
import ReviewHistory from './ReviewHistory'
import SubmissionError from './SubmissionError'
import { REVIEW_MANUSCRIPT, GET_REVIEWS, CURRENT_REVIEW } from './operations'

const Option = styled.div`
  display: flex;
  flex-flow: row wrap;
  align-items: center;
  justify-content: space-between;
  min-height: calc(${th('gridUnit')} * 6);
  padding: ${th('gridUnit')} calc(${th('gridUnit')} * 3);
  & > * {
    margin: ${th('gridUnit')} auto;
  }
  @media screen and (max-width: 870px) {
    padding: ${th('gridUnit')} calc(${th('gridUnit')} * 4);
  }
`
const SmallAction = styled(Action)`
  font-size: ${th('fontSizeBaseSmall')};
  font-weight: 600;
`
const showPopup = button => {
  button.parentNode.nextElementSibling.classList.remove('hidden')
}

const ReviewFormDisplay = ({
  manuscript,
  previews,
  review,
  deleted,
  files,
  history,
  reportError = false,
}) => (
  <Mutation
    mutation={REVIEW_MANUSCRIPT}
    refetchQueries={() => [
      {
        query: GET_REVIEWS,
        variables: { manuscriptId: manuscript.id },
      },
      {
        query: CURRENT_REVIEW,
        variables: { manuscriptId: manuscript.id },
      },
    ]}
  >
    {(reviewManuscript, { data }) => {
      const { annotations } = review || []
      const { id, status, teams } = manuscript
      const reviewer = teams.find(t => t.role === 'reviewer').teamMembers[0]
      const setStatus = async newStatus => {
        await reviewManuscript({
          variables: { data: { id, status: newStatus } },
        })
      }
      if (status === 'tagging') {
        return (
          <React.Fragment>
            <Option>
              <H3>Tagging complete?</H3>
              <Button
                disabled={!previews}
                onClick={() => setStatus('xml-qa')}
                primary
              >
                Send for XML QA
              </Button>
            </Option>
            <Content>
              <p>
                {`When all files have been uploaded and both the Web and PDF previews have been generated, please click the button above to move the manuscript to XML QA.`}
              </p>
            </Content>
          </React.Fragment>
        )
      } else if (manuscript.status === 'xml-triage') {
        const user = review ? review.user : null
        const { givenNames, surname } = (user && user.identities[0].name) || ''
        return (
          <React.Fragment>
            <Header>
              <H3>
                Review
                {(review && ' correction request') ||
                  (manuscript.formState && ' errors') ||
                  ' and route'}
              </H3>
            </Header>
            <Content>
              <div>
                {review && (
                  <p>
                    Reported by {givenNames} {surname}
                    {user.id === reviewer.user.id && ' (Reviewer)'}
                  </p>
                )}
                {annotations && <ListErrors annotations={annotations} />}
                {deleted.length > 0 && <ReviewHistory reviews={deleted} />}
                <p>
                  {`You can fix any errors and approve for review, or send this list of issues to the taggers to fix. You may edit the annotation text to provide more detail if necessary.`}
                </p>
                <p>
                  {`If an error is caused by missing or incomplete files, click 'Return for user upload' and you will be given the opportunity to write a message explaining what is needed, and send the manuscript back to the submitter or reviewer to upload files.`}
                </p>
                <Option>
                  <H3 style={{ width: '100%', textAlign: 'center' }}>
                    Errors fixed? Approve for:
                  </H3>
                  <Button onClick={e => showPopup(e.currentTarget)}>
                    NCBI
                  </Button>
                  <Button onClick={() => setStatus('xml-qa')} primary>
                    QA
                  </Button>
                  <Button
                    onClick={async () => {
                      await setStatus('xml-review')
                      history.push('/')
                    }}
                    primary
                  >
                    Final review
                  </Button>
                </Option>
                <Notification className="hidden" type="warning">
                  Are you sure you want to skip author review?{' '}
                  <SmallAction onClick={() => setStatus('xml-complete')}>
                    I&apos;m sure!
                  </SmallAction>
                </Notification>
                <SubmissionError
                  annotations={annotations}
                  manuscript={manuscript}
                />
              </div>
            </Content>
          </React.Fragment>
        )
      } else if (status === 'xml-qa' || status === 'xml-review') {
        if (status === 'xml-review') {
          deleted = deleted.filter(r => r.user.id === reviewer.user.id)
        }
        return (
          <React.Fragment>
            <Header>
              <H3>Review web versions</H3>
            </Header>
            <Content>
              <div>
                {status === 'xml-review' && (
                  <p>
                    {`Please review the web versions of your manuscript. This is the final step in the manuscript submission process.`}
                  </p>
                )}
                <p>
                  {`Preview both the PDF and Web versions ${
                    status === 'xml-review' ? 'of your' : 'generated from the'
                  } manuscript. Click '`}
                  <span className="hide-mobile">
                    Compare with submitted file&apos;, above,
                  </span>
                  <span className="show-mobile">Submitted file&apos;</span>
                  {` to view the originally submitted manuscript file. Other files submitted with the manuscript can be checked below.`}
                </p>
              </div>
            </Content>
            {files && files.length > 0 && (
              <React.Fragment>
                <Header>
                  <H3>Other submitted files</H3>
                </Header>
                <Content>
                  <FileThumbnails files={files} />
                </Content>
              </React.Fragment>
            )}
            <Header>
              <H3>Review</H3>
            </Header>
            <Content>
              <div>
                <p>
                  {`Approve Web and PDF previews, or mark any errors and request corrections.`}
                </p>
                {annotations && <ListErrors annotations={annotations} />}
                {deleted.length > 0 && <ReviewHistory reviews={deleted} />}
                <Buttons>
                  <Button
                    disabled={annotations}
                    onClick={async () => {
                      await setStatus(
                        status === 'xml-review' ? 'xml-complete' : 'xml-review',
                      )
                      history.push('/')
                    }}
                    primary
                  >
                    Approve
                  </Button>
                  {annotations && (
                    <Button
                      onClick={async () => {
                        await setStatus('xml-triage')
                        history.push('/')
                      }}
                    >
                      Send error report
                    </Button>
                  )}
                </Buttons>
              </div>
            </Content>
          </React.Fragment>
        )
      }
      history.push('/')
      return null
    }}
  </Mutation>
)

const ReviewFormRouter = withRouter(ReviewFormDisplay)

const ReviewForm = ({ manuscript, ...props }) => (
  <Query query={GET_REVIEWS} variables={{ manuscriptId: manuscript.id }}>
    {({ data: { deletedReviews }, loading }) => {
      if (loading || !deletedReviews) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }
      return (
        <ReviewFormRouter
          deleted={deletedReviews}
          manuscript={manuscript}
          {...props}
        />
      )
    }}
  </Query>
)

export default ReviewForm
