import React from 'react'
import { Query } from 'react-apollo'
import { Page, Loading, LoadingIcon } from '../ui'
import { GET_MANUSCRIPT } from '../operations'
import SubmissionHeader from '../SubmissionHeader'
import Review from './Review'
import { CURRENT_REVIEW } from './operations'

const ReviewWithHeader = SubmissionHeader(Review)

const ReviewPage = ({ match, ...props }) => (
  <Query
    fetchPolicy="cache-and-network"
    query={GET_MANUSCRIPT}
    variables={{ id: match.params.id }}
  >
    {({ data: { manuscript }, loading: loadingOne }) => (
      <Query
        query={CURRENT_REVIEW}
        variables={{ manuscriptId: match.params.id }}
      >
        {({ data: { currentReview }, loading, refetch }) => {
          if (loadingOne || !manuscript) {
            return (
              <Page>
                <Loading>
                  <LoadingIcon />
                </Loading>
              </Page>
            )
          }
          return (
            <ReviewWithHeader
              manuscript={manuscript}
              match={match}
              reload={() => refetch()}
              review={currentReview}
              {...props}
            />
          )
        }}
      </Query>
    )}
  </Query>
)

export default ReviewPage
