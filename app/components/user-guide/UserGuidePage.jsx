import React from 'react'
import { Route, Switch } from 'react-router-dom'
import styled from 'styled-components'
import { Link, H1, H2, H3, H4 } from '@pubsweet/ui'
import { Page, Toggle, A } from '../ui'
import { Requirements, AllowedFiles } from './user-guide-pages/'
import MSSHorizontal from '../../assets/mss_process_horizontal.png'
import MSSVertical from '../../assets/mss_process_vertical.png'

const Image = styled.img`
  width: 100%;
  max-width: 750px;
  &.vertical {
    display: none;
  }
  @media screen and (max-width: 740px) {
    &.horizontal {
      display: none;
    }
    &.vertical {
      display: block;
      max-width: 550px;
    }
  }
`
const altText = `Author or submitter: Upload the final version of a manuscript, modified and accepted for publication. Europe PMC plus: Transform the submission into archival and web versions for reading, indexing, and searching. Author or submitter: Review the final web versions of the manuscript. Europe PMC plus: Make the manuscript freely available on Europe PMC and PubMed Central.`

const SigninLink = () => <Link to="/login">sign into</Link>

const SignupLink = () => <Link to="/signup">create a new account for</Link>

const UserGuide = () => (
  <div>
    <H2>Process overview</H2>
    <Image alt={altText} className="horizontal" src={MSSHorizontal} />
    <Image alt={altText} className="vertical" src={MSSVertical} />
    <H2>How to submit a manuscript</H2>
    <H3>For authors</H3>
    <H4>Authors who have been asked to review a manuscript</H4>
    <p>
      {`If you have received an email invitation to review a manuscript, simply click on the emailed link and `}
      <SigninLink />, or <SignupLink />
      {`, the Plus manuscript submission system. Follow the instructions to preview the submission materials. Later, you will receive an invitation to review the final web version of the manuscript.`}
    </p>
    <H4>Authors submitting their own manuscript</H4>
    <p>
      {`An author submitting their own manuscript should `}
      <SigninLink />, or <SignupLink />
      {`, the Plus manuscript submission system. Follow the instructions to submit the manuscript files, and name yourself or another author as the reviewer. The reviewer is responsible for making sure the web version of the manuscript is correct.`}
    </p>
    <H3>For those submitting on behalf of an author</H3>
    <H4>Submitting an individual manuscript</H4>
    <p>
      {`To submit an individual manuscript on behalf of someone else, simply `}
      <SigninLink />, or <SignupLink />
      {`, the Plus manuscript submission system. Follow the instructions to submit the manuscript files, and identify the author as the reviewer.`}
    </p>
    <H4>Publishers and bulk submitters</H4>
    <p>
      {`Europe PMC plus accepts multiple manuscripts submitted on behalf of authors via FTP. This offers publishers a way to use in-house automated processes to organize and upload manuscript submissions in bulk. For more information, please download the `}
      <A download="Plus Bulk Upload" href="/assets/nihms-bulk-sub.tar.gz">
        Plus Bulk Upload
      </A>
      {` package.`}
    </p>
    <H2>What you will need</H2>
    <ul>
      <li>The final manuscript file in a PDF or Microsoft Word format</li>
      <li>
        {`The figures and supplementary files in an `}
        <Link to="/user-guide/allowedfiles">accepted format</Link>
      </li>
      <li>The grant numbers</li>
      <li>
        {`If the citation isn’t found, you will need the name of the publishing journal`}
      </li>
      <li>The embargo period required by the publishing journal</li>
    </ul>
    <H2>Process and timeframe</H2>
    <H3>The process</H3>
    <p>
      {`During the submission process, the submitter, and reviewer if one has been designated, will be able to check the full list of submitted files for completeness. The Europe PMC plus system allows users to track the status and progress of manuscripts.`}
    </p>
    <H3>Timeframe</H3>
    <p>
      {`In the best case scenario, if a manuscript is submitted correctly, and is reviewed promptly by the author, the process can take as little as two weeks.`}
    </p>
    <H3>Embargo period</H3>
    <p>
      {`If an embargo period is required by the journal or funder, and if it is indicated during the submission, the manuscript will be released to Europe PMC after the embargo period has passed. Europe PMC strongly encourages authors to deposit their manuscript as soon as possible.`}
    </p>
    <H3>Release to Europe PMC</H3>
    <p>
      {`After the submission process has been completed, and once the embargo period has passed, manuscripts will be released to Europe PMC and PubMed Central (PMC). Europe PMC will archive the manuscript as XML and will display the manuscript in three forms: abstract, online full-text, and PDF.`}
    </p>
  </div>
)

const UserGuidePage = ({ children }) => {
  const subPage = window.location.pathname

  return (
    <Page>
      <H1>User guide</H1>
      <Toggle>
        <Link
          className={
            !subPage.includes('requirements') &&
            !subPage.includes('allowedfiles')
              ? 'current'
              : ''
          }
          to="/user-guide"
        >
          How to
        </Link>
        <Link
          className={subPage.includes('requirements') ? 'current' : ''}
          to="/user-guide/requirements"
        >
          Requirements
        </Link>
        <Link
          className={subPage.includes('allowedfiles') ? 'current' : ''}
          to="/user-guide/allowedfiles"
        >
          Acceptable file types
        </Link>
      </Toggle>
      <Switch>
        <Route component={Requirements} path="/user-guide/requirements" />
        <Route component={AllowedFiles} path="/user-guide/allowedfiles" />
        <Route component={UserGuide} path="/user-guide" />
      </Switch>
    </Page>
  )
}

export default UserGuidePage
