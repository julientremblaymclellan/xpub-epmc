import React from 'react'
import * as mime from 'mime-types'
import { H2, H3, H4 } from '@pubsweet/ui'
import { FileTypes } from '../../upload-files'
import { Table } from '../../ui'

export const AllowedFiles = () => {
  const mimetypes = FileTypes.reduce((types, type) => {
    const mimetype = type.substring(0, type.indexOf('/'))
    if (mime.extension(type) && !types.some(t => t === mimetype)) {
      types.push(mimetype)
    }
    return types
  }, [])
  return (
    <div>
      <H2>Acceptable file types</H2>
      <H3>Manuscript text file</H3>
      <p>This file can be either a PDF or Microsoft Word file.</p>
      <H3>Figures and supplemental files</H3>
      <H4>Application</H4>
      {mimetypes.map(type => (
        <div key={type}>
          <H2>{type.charAt(0).toUpperCase() + type.slice(1)}</H2>
          <Table>
            <tbody>
              <tr>
                <th>File extension</th>
                <th>MIME type</th>
              </tr>
              {FileTypes.filter(subtype => subtype.startsWith(type)).map(
                subtype =>
                  mime.extension(subtype) && (
                    <tr key={subtype}>
                      <td>.{mime.extension(subtype)}</td>
                      <td>{subtype}</td>
                    </tr>
                  ),
              )}
            </tbody>
          </Table>
        </div>
      ))}
    </div>
  )
}

export default AllowedFiles
