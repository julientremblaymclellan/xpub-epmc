import React from 'react'
import { H1, H2 } from '@pubsweet/ui'
import { CreateInfo } from '../../submission-wizard'

const titles = ['Citation', 'Files', 'Funding', 'Reviewer']

export const SubmitManuscript = () => (
  <div>
    <H1>Submitting a manuscript to Europe PMC</H1>
    {titles.map((title, i) => (
      <div key={title}>
        <H2>
          {i + 1}. {title}
        </H2>
        <CreateInfo currentStep={i} />
      </div>
    ))}
  </div>
)

export default SubmitManuscript
