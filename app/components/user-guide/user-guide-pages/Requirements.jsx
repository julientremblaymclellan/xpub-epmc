import React from 'react'
import { H2, H3 } from '@pubsweet/ui'
import { A } from '../../ui'

export const Requirements = () => (
  <div>
    <H2>Submission requirements</H2>
    <H3>Funding and open access policies</H3>
    <p>
      {`The manuscripts submitted must be peer-reviewed, original (primary) research publications that have direct costs supported, in whole or in part, by one or more `}
      <A href="//http://europepmc.org/Funders" target="_blank">
        Europe PMC funders
      </A>
      {`. Typically this applies to all research grant and career development award mechanisms, cooperative agreements and contracts.`}
    </p>
    <p>
      {`Europe PMC plus has been developed to allow submitters to easily identify grant numbers (both past and current). Please `}
      <A href="mailto:helpdesk@europepmc.org">
        contact the Europe PMC Helpdesk
      </A>
      {` if you require assistance or if your grant is not listed in Europe PMC
      plus.`}
    </p>
    <p>
      {`Europe PMC funders expect research outputs they have funded to be freely available. For more information about the open access policies of individual funders, please see the `}
      <A href="//http://europepmc.org/Funders" target="_blank">
        Europe PMC Funders page
      </A>
      {`. Manuscripts are then made freely available through Europe PMC and PubMed Central (PMC).`}
    </p>
    <H3>Which version to submit</H3>
    <p>
      {`Generally, the accepted peer reviewed version of the manuscript, or the version of record for creative commons licensed articles, should be submitted. Check your journal’s version policy at `}
      <A href="//www.sherpa.ac.uk/romeo/index.php" target="_blank">
        SHERPA/RoMEO
      </A>
      {`. Supplementary material that has been submitted to the journal in support of the manuscript should also be submitted.`}
    </p>
    <p>
      {`If the author wishes to submit a PDF created by the publisher to Europe PMC plus, then permission for this particular use must have been granted to the author by the publisher.`}
    </p>
    <H3>Who can submit</H3>
    <p>
      {`Manuscript files may be submitted to Europe PMC plus by the author, the publisher, or anyone given access to the author's files (administrative personnel, graduate students, librarians, etc.). Approval  of the submitted files and web version of the manuscript requires PI authorisation.`}
    </p>
    <H2>When a manuscript should NOT be submitted</H2>
    <p>
      {`A separate submission to the Europe PMC Plus system is not necessary if a manuscript has been accepted by a participating `}
      <A href="//europepmc.org/journalList" target="_blank">
        PubMed Central journal
      </A>
      .
    </p>
    <p>
      {`If the research was not at least part funded by one of the Europe PMC Funders, then it cannot be accepted into Europe PMC.`}
    </p>
  </div>
)

export default Requirements
