import React from 'react'
import { withRouter } from 'react-router'
import { Mutation } from 'react-apollo'
import styled from 'styled-components'
import { th, override } from '@pubsweet/ui-toolkit'
import { Button, CheckboxGroup, TextField } from '@pubsweet/ui'
import { Buttons, TextArea } from './ui'
import { REJECT_MANUSCRIPT } from './operations'

export const Fieldset = styled.fieldset`
  border: 0;
  padding: 0;
  margin-bottom: calc(${th('gridUnit')} * 3);
  & > label {
    ${override('ui.Label')};
  }
`
export const adminOptions = [
  {
    value: 'title',
    label: 'Manuscript title matching submission',
  },
  {
    value: 'affiliations',
    label: 'Complete title page with authors & affiliations',
  },
  {
    value: 'figures',
    label: 'Figures, with captions',
  },
  {
    value: 'tables',
    label: 'Tables, with captions',
  },
  {
    value: 'supp',
    label: 'Supplementary material',
  },
  {
    value: 'refs',
    label: 'Complete reference list',
  },
  {
    value: 'display',
    label: 'Legible text and graphics',
  },
  {
    value: 'english',
    label: 'Content in English',
  },
  {
    value: 'grants',
    label: 'Appropriate grant linking',
  },
  {
    value: 'other',
    label: 'Something else',
  },
]

const messages = title => ({
  title: {
    subject: 'manuscript title mismatch',
    message: `We have found that the file you have submitted is for a manuscript entitled "[INSERT TITLE HERE]", while your submission was made under the title "${title}".

Please contact us at helpdesk@europepmc.org to confirm the manuscript title and whether the file as uploaded is correct.`,
  },
  affiliations: {
    subject: 'incomplete title page',
    message:
      'The manuscript is missing a complete title page. We do not have author and affiliation information for this manuscript and are therefore unable to proceed with processing of this manuscript for Europe PMC.',
  },
  figures: {
    subject: 'missing figures',
    message:
      'This submission makes reference to figures that have not been uploaded with the manuscript.',
  },
  tables: {
    subject: 'missing tables',
    message:
      'This submission makes reference to tables that have not been uploaded with the manuscript.',
  },
  supp: {
    subject: 'missing supplementary material',
    message:
      'This submission makes reference to supplementary files that have not been uploaded with the manuscript.',
  },
  refs: {
    subject: 'incomplete reference list',
    message: 'The manuscript is missing a complete reference list.',
  },
  display: {
    subject: 'poor display of content',
    message:
      'The figures supplied are of low resolution and contain blurry or illegible text. Please provide images of higher quality. If you are unable to provide higher quality image files because such files are not available, please let us know.',
  },
  grants: {
    subject: 'linked grants mismatch',
    message:
      'There is a problem with the grants linked to the manuscript. [DESCRIBE PROBLEM HERE]',
  },
  english: {
    subject: 'English is not the primary language',
    message:
      'Unfortunately your submission to Europe PMC cannot be processed because we currently only accept peer-reviewed manuscripts in English. If such a version is available, please contact the Europe PMC Helpdesk at helpdesk@europepmc.org.',
  },
})

const endErrs = ['title', 'english']

const formatName = name =>
  `${name.title ? `${name.title} ` : ''}${name.givenNames} ${name.surname}`

class SubmissionErrorReport extends React.Component {
  state = {
    subject: '',
    people: [],
    selected: [],
    message: '',
  }
  componentDidMount() {
    const { submitter, reviewer } = this.props
    const people = []
    if (submitter) {
      people.push({
        label: `${formatName(submitter.alias.name)} (Submitter${reviewer &&
          reviewer.user.id === submitter.user.id &&
          '/Reviewer'})`,
        value: JSON.stringify(submitter),
      })
    }
    if (reviewer && reviewer.user.id !== submitter.user.id) {
      people.push({
        label: `${formatName(reviewer.alias.name)} (Reviewer)`,
        value: JSON.stringify(reviewer),
      })
    }
    this.setState({ people, selected: [JSON.stringify(submitter)] })
    this.generateMessage()
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.selected !== this.state.selected ||
      prevProps.checkedBoxes !== this.props.checkedBoxes
    ) {
      this.generateMessage()
    }
  }
  generateMessage = () => {
    const { checkedBoxes, manuscript } = this.props
    const { id, meta } = manuscript
    const errors = messages(meta.title)
    const subject = `${id} submission error:${checkedBoxes.map(
      err => ` ${errors[err] ? errors[err].subject : ''}`,
    )}.`
    const link = `${window.location.origin}/submission/${id}/submit`
    const message = `Dear ${this.state.selected
      .map(s => formatName(JSON.parse(s).alias.name))
      .join(', ')},

Thank you for your submission to Europe PMC plus.

${checkedBoxes
  .map(err =>
    errors[err]
      ? `${errors[err].message}

`
      : '',
  )
  .join('')}${
      !checkedBoxes.some(err => endErrs.includes(err))
        ? `We have returned the manuscript to a state where you can upload and submit the missing material. To provide the required material and resubmit, please visit:

${link}

`
        : ''
    }Kind regards,

Europe PMC Helpdesk`

    this.setState({ message, subject })
  }
  render() {
    const { subject, message, people, selected } = this.state
    const { close, history, manuscript } = this.props
    return (
      <Mutation mutation={REJECT_MANUSCRIPT}>
        {(rejectManuscript, { data }) => {
          const reject = async () => {
            await rejectManuscript({
              variables: {
                data: {
                  manuscriptId: manuscript.id,
                  notesType: 'userMessage',
                  content: JSON.stringify({
                    to: selected.map(s => JSON.parse(s).user.id),
                    subject,
                    message,
                  }),
                },
              },
            })
            history.push('/')
          }
          return (
            <div>
              <Fieldset>
                <label>Recipient(s)</label>
                <CheckboxGroup
                  onChange={checked => this.setState({ selected: checked })}
                  options={people}
                  value={selected}
                />
              </Fieldset>
              <TextField
                label="Subject"
                onChange={e => this.setState({ subject: e.target.value })}
                value={subject}
              />
              <TextArea
                label="Message"
                onChange={e => this.setState({ message: e.target.value })}
                rows={15}
                value={message}
              />
              <Buttons right>
                <Button
                  disabled={selected.length === 0 || !message}
                  onClick={reject}
                  primary
                >
                  Send & Reject
                </Button>
                <Button onClick={close}>Cancel</Button>
              </Buttons>
            </div>
          )
        }}
      </Mutation>
    )
  }
}

export default withRouter(SubmissionErrorReport)
