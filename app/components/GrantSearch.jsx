/* eslint-disable no-template-curly-in-string */
import React from 'react'
import { withRouter } from 'react-router-dom'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, ErrorText, H2, H3 } from '@pubsweet/ui'
import { SearchSelect, Select, Center } from './ui'

const GrantInfo = styled.span`
  display: block;
  font-size: ${th('fontSizeBaseSmall')};
  span {
    margin-right: ${th('gridUnit')};
  }
`
function mapGrants(grants) {
  if (Array.isArray(grants)) {
    return grants.map(grant => ({
      fundingSource: grant.Grant.Funder.Name,
      awardId: grant.Grant.Id,
      title: grant.Grant.Title,
      pi: {
        surname: grant.Person.FamilyName,
        givenNames: grant.Person.Initials,
        title: grant.Person.Title,
        email: grant.Person.Email,
      },
    }))
  }
}

class GrantSearch extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      query: '',
      hitcount: 0,
      page: 0,
      availableGrants: [],
      grantsErr: '',
      loading: false,
      // Embargo state props
      embargoDisabled: true,
      embargo: this.props.selectedEmbargo ? this.props.selectedEmbargo : '',
      embargoOptions: [
        {
          value: '',
          label: 'Please select an embargo period',
        },
      ],
      funderInfoList: [],
    }
    this.onQueryChange = this.onQueryChange.bind(this)
    this.onLoadMore = this.onLoadMore.bind(this)
    this.grantsAdded = this.grantsAdded.bind(this)

    this.loadFunders = this.loadFunders.bind(this)
    this.updateEmbargoOptions = this.updateEmbargoOptions.bind(this)
    this.selectEmbargo = this.selectEmbargo.bind(this)
  }
  componentDidMount() {
    if (this.props.selectedGrants && this.props.selectedGrants.length > 0) {
      this.loadFunders(this.props.selectedGrants)
    }
  }
  grantsAdded(grants) {
    if (grants.length > 0) {
      this.setState({ grantsErr: '' })
      this.props.changedGrants(grants)
      this.loadFunders(grants)
    } else {
      this.props.changedGrants([])
      this.props.changedEmbargo(null)
      this.setState({
        grantsErr: 'At least one grant is required.',
        embargo: '',
        embargoDisabled: true,
      })
    }
  }
  updateEmbargoOptions(grants) {
    if (grants) {
      const releaseDelays = []
      grants.forEach(grant => {
        const { releaseDelay } = this.state.funderInfoList.find(
          record => record.name === grant.fundingSource,
        )
        releaseDelays.push(parseInt(releaseDelay, 10))
      })
      const minReleaseDelay = Math.min(...releaseDelays)
      const embargoOptions = [
        {
          value: '',
          label: 'Please select an embargo period',
        },
      ]
      for (let i = 0; i <= minReleaseDelay; i += 1) {
        embargoOptions.push({
          value: i,
          label: `${i}  month${i !== 1 ? 's' : ''}`,
        })
      }
      this.setState({ embargoOptions })
    }
  }
  selectEmbargo(event) {
    this.props.changedEmbargo(event.target.value)
    this.setState({ embargo: event.target.value })
  }
  async loadFunders(grants) {
    const newState = { embargoDisabled: false }
    if (this.state.funderInfoList.length < 1) {
      const fundersUrl = `https://www.ebi.ac.uk/europepmc/GristAPI/rest/api/get/funders`
      const response = await fetch(fundersUrl)
      const json = await response.json()
      newState.funderInfoList = json.funderInfoList
    }
    this.setState(newState)
    this.updateEmbargoOptions(grants)
  }
  async onLoadMore() {
    const { query } = this.state
    const page = this.state.page + 1
    const url = `/grist/rest/api/search?query=gid:%22${query}%22%7Cpi:%22${query}%22&page=${page}`
    const response = await fetch(url, {
      headers: new Headers({
        Authorization: `Bearer ${window.localStorage.getItem('token')}`,
      }),
    })
    const json = await response.json()
    const availableGrants = this.state.availableGrants.concat(
      mapGrants(
        Array.isArray(json.RecordList.Record)
          ? json.RecordList.Record
          : [json.RecordList.Record],
      ),
    )
    this.setState({
      page,
      availableGrants,
      loading: false,
    })
  }
  async onQueryChange(e) {
    const query = e.target.value
    this.setState({ query: e ? query : '' })
    const newState = {
      page: 1,
      availableGrants: [],
      hitcount: 0,
    }
    if (query.trim().length > 0) {
      const url = `/grist/rest/api/search?query=gid:%22${query}%22%7Cpi:%22${query}%22&page=1`
      const response = await fetch(url, {
        headers: new Headers({
          Authorization: `Bearer ${window.localStorage.getItem('token')}`,
        }),
      })
      const json = await response.json()
      const hitcount = json.HitCount
      if (hitcount) {
        if (parseInt(hitcount, 10) === 0) {
          this.setState(newState)
        } else if (parseInt(hitcount, 10) === 1) {
          newState.availableGrants = mapGrants([json.RecordList.Record])
          newState.hitcount = hitcount
          this.setState(newState)
        } else {
          newState.availableGrants = mapGrants(json.RecordList.Record)
          newState.hitcount = hitcount
          this.setState(newState)
        }
      } else {
        this.setState({
          grantsErr:
            "We're experiencing a problem with our grants search. Please try again later.",
        })
      }
    } else {
      this.setState(newState)
    }
  }
  render() {
    return (
      <div>
        <H2>Funding</H2>
        <H3>Add all grants that support this manuscript</H3>
        <SearchSelect
          displaySelected="`${option.fundingSource}, ${option.awardId} (${option.pi.surname})`"
          icon="chevron_down"
          invalidTest={this.state.grantsErr}
          label="Search by Grant # or by PI surname followed by initial(s), e.g. Smith A"
          onInput={this.onQueryChange}
          optionsOnChange={this.grantsAdded}
          query={this.state.query}
          selectedOptions={this.props.selectedGrants}
        >
          {this.state.availableGrants.map(grant => (
            <SearchSelect.Option
              data-option={grant}
              key={`${grant.pi.title}_${grant.pi.givenNames}_${
                grant.pi.surname
              }_${grant.awardId}_${grant.fundingSource}`}
              propKey={`${grant.pi.title}_${grant.pi.givenNames}_${
                grant.pi.surname
              }_${grant.awardId}`}
            >
              {grant.title}
              <GrantInfo>
                <span>
                  <b>PI:</b>
                  {` ${grant.pi.title} ${grant.pi.givenNames} ${
                    grant.pi.surname
                  }`}
                </span>
                <span>
                  <b>Funder:</b>
                  {` ${grant.fundingSource}`}
                </span>
                <span>
                  <b>ID:</b>
                  {` ${grant.awardId}`}
                </span>
              </GrantInfo>
            </SearchSelect.Option>
          ))}
          {this.state.hitcount > this.state.availableGrants.length && (
            <SearchSelect.Option propKey="loadMore">
              <Center>
                <Button
                  disabled={this.state.loading}
                  onClick={e => {
                    e.stopPropagation()
                    this.setState({ loading: true })
                    this.onLoadMore()
                  }}
                >
                  {this.state.loading ? 'Loading...' : 'Load more results'}
                </Button>
              </Center>
            </SearchSelect.Option>
          )}
        </SearchSelect>
        {this.state.grantsErr && <ErrorText>{this.state.grantsErr}</ErrorText>}
        {!this.props.hideEmbargo && (
          <div style={{ paddingTop: '1px' }}>
            <H3>
              Select the embargo period required by your journal and funder
            </H3>
            <Select
              disabled={this.state.embargoDisabled}
              icon="chevron_down"
              label="Select embargo period after publication date"
              onChange={this.selectEmbargo}
              options={this.state.embargoOptions}
              value={this.state.embargo}
              width="400px"
            />
          </div>
        )}
      </div>
    )
  }
}

export default withRouter(GrantSearch)
