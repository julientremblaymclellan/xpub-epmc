import { compose } from 'recompose'
import { withFormik } from 'formik'
import { graphql } from 'react-apollo'
import * as yup from 'yup' // for everything
import mutations from './mutations'
import passwordMutations from '../password-reset/mutations'
import { NO_PRIVILEGE, AccountDetails } from './AccountDetails'

const handleSubmit = (values, { props, setSubmitting, setValues, setErrors }) =>
  props
    .epmc_updateUser({
      variables: {
        input: {
          title: values.title,
          givenNames: values.givenNames,
          surname: values.surname,
          email: values.email,
          team: values.team === NO_PRIVILEGE ? null : values.team,
          id: values.id,
        },
      },
    })
    .then(({ data, errors }) => {
      if (!errors) {
        setSubmitting(true)
        setValues(
          Object.assign(values, {
            success: 'Account details are updated successfully!',
          }),
        )
        setTimeout(() => {
          setValues(
            Object.assign(values, {
              success: '',
            }),
          )
        }, 1000)
      } else {
        setSubmitting(true)
        throw new Error()
      }
    })
    .catch(e => {
      if (e.graphQLErrors) {
        setSubmitting(false)
        setValues(Object.assign(values, { error: true }))
        setTimeout(() => {
          setValues(
            Object.assign(values, {
              error: false,
            }),
          )
        }, 1000)
        setErrors(e.graphQLErrors[0].message)
      }
    })
const resetPassword = ({
  values,
  setValues,
  setErrors,
  emailPasswordResetLink,
}) => {
  emailPasswordResetLink({ variables: { email: values.email } })
    .then(({ data, errors }) => {
      if (!errors) {
        setValues(
          Object.assign(values, {
            success: 'Password reset email will be sent shortly',
          }),
        )
      } else {
        setValues(Object.assign(values, { error: true }))
      }
    })
    .catch(e => {
      if (e.graphQLErrors) {
        setValues(Object.assign(values, { error: true }))
        setErrors(e.graphQLErrors[0].message)
      }
    })
}
const enhancedFormik = withFormik({
  mapPropsToValues: props => {
    const currentIdentity = props.user.identities.filter(
      identity => identity.type === 'local',
    )[0]
    const teamOptions = props.roles
      .map(role => role.charAt(0).toUpperCase() + role.slice(1))
      .concat(NO_PRIVILEGE)
    return {
      email: currentIdentity.email,
      title: currentIdentity.name.title ? currentIdentity.name.title : '',
      givenNames: currentIdentity.name.givenNames,
      surname: currentIdentity.name.surname,
      manageAccount: true,
      id: props.user.id,
      resetPassword,
      team: props.user.teams[0] ? props.user.teams[0] : NO_PRIVILEGE,
      teamOptions,
    }
  },
  validationSchema: yup.object().shape({
    email: yup
      .string()
      .email('Email not valid')
      .required('Email is required'),
    givenNames: yup.string().required('Given names is required'),
    surname: yup.string().required('Surname is required'),
  }),
  displayName: 'login',
  handleSubmit,
})(AccountDetails)

export default compose(
  graphql(mutations.UPDATE_USER, { name: 'epmc_updateUser' }),
  graphql(passwordMutations.emailPasswordResetLink, {
    name: 'emailPasswordResetLink',
  }),
)(enhancedFormik)
