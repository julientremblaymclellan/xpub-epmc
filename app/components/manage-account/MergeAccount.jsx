import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Link, Icon } from '@pubsweet/ui'
import { withApollo } from 'react-apollo'
import {
  SearchForm,
  ZebraList,
  ZebraListItem,
  ConfirmDialog,
  Notification,
} from '../ui/'
import { GET_USER } from './queries'
import { MERGE_USER } from './mutations'

const SearchArea = styled.div`
  float: right;
  width: 100%;
  margin-bottom: calc(${th('gridUnit')} * 2);
  form {
    display: inline-block;
    width: calc(${th('gridUnit')} * 45);
    div:first-child {
      margin-bottom: 0;
    }
    button {
      top: 8px;
    }
  }
  .separator {
    margin-right: 16px;
    margin-left: 16px;
    font-size: 16px;
  }
  @media screen and (max-width: 733px) {
    float: none;
    form {
      display: block;
      margin-left: 0 !important;
      margin-bottom: calc(${th('gridUnit')} * 2);
      width: 100% !important;
    }
  }
`
const ListItem = styled(ZebraListItem)`
  display: flex;
  align-items: top;
  justify-content: space-between;
  div > span {
    margin: 8px;
    width: 200px;
    display: inline-flex;
  }
  a {
    width: 300px;
    margin: 4px;
  }
  @media screen and (max-width: 733px) {
    span {
      display: block;
    }
  }
`
class MergeAccount extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchById: props.searchById ? props.searchById : '',
      results: [],
      isOpen: false,
    }
    this.onSearchValChanged = this.onSearchValChanged.bind(this)
    this.onSearchValSubmitted = this.onSearchValSubmitted.bind(this)
  }
  toggleMerge = () => {
    const { isOpen, searchById, success, error, results } = this.state
    this.setState({
      isOpen: !isOpen,
      searchById,
      results,
      success,
      error,
    })
  }
  mergeAccount = input => {
    if (!input) {
      this.toggleMerge()
      return
    }
    this.setState({
      results: [],
      success: '',
      error: '',
    })
    const options = {
      mutation: MERGE_USER,
      variables: {
        input: {
          from: this.state.results[0].id,
          to: this.props.user.id,
        },
      },
    }
    this.props.client
      .mutate(options)
      .then(response => {
        if (response.data.mergeUser) {
          this.setState({ success: 'Account has been merged successfully!' })
        } else {
          this.setState({ error: 'Error occured. please try again' })
        }
        this.toggleMerge()
      })
      .catch(e => {
        this.setState({ error: 'Error occured. please try again' })
        this.toggleMerge()
      })
  }
  onSearchValSubmitted(searchForm, e) {
    e.preventDefault()
    this.setState({
      results: [],
      success: '',
      error: '',
    })
    const options = {
      query: GET_USER,
      variables: { id: this.state.searchById },
    }
    this.props.client
      .query(options)
      .then(response => this.setState({ results: [response.data.epmc_user] }))
      .catch(e => {
        const error = e.graphQLErrors
          ? 'Please enter a valid existing ID'
          : 'Error occured. please try again'
        this.setState({ error })
      })
  }
  onSearchValChanged(e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  render() {
    const { searchById, results } = this.state
    const cancelResult = () => {
      this.state.results = []
    }
    const merge = async input => {
      this.mergeAccount(input)
    }
    return (
      <div>
        {this.state.success && (
          <Notification type="success"> {this.state.success} </Notification>
        )}
        {this.state.error && (
          <Notification type="error">{this.state.error}</Notification>
        )}
        {!this.state.results.length && (
          <SearchArea>
            <SearchForm
              name="searchById"
              noButton="true"
              onChange={this.onSearchValChanged}
              onSubmit={e => this.onSearchValSubmitted('usersById', e)}
              placeholder="Enter account ID"
              value={searchById}
            />
          </SearchArea>
        )}
        <ZebraList>
          {results.map(user => {
            let localIdentity = user.identities.filter(
              identity => identity.type === 'local',
            )[0]
            if (!localIdentity) {
              ;[localIdentity] = user.identities
            }
            return (
              <ListItem key={user.id}>
                <div>
                  <Link to={`/manage-account/${user.id}`}>
                    <span>{user.id}</span>
                  </Link>
                  <span>
                    {`
                      ${
                        localIdentity.name.title ? localIdentity.name.title : ''
                      }
                      ${localIdentity.name.givenNames}
                      ${localIdentity.name.surname}
                    `}
                  </span>
                  <span>{localIdentity.email}</span>
                  <Link onClick={this.toggleMerge} to="#">
                    <Icon color="currentColor" size={2}>
                      minimize
                    </Icon>
                    Merge
                  </Link>
                  <Link onClick={cancelResult} to="#">
                    <Icon color="currentColor" size={2}>
                      minus-square
                    </Icon>
                    Cancel
                  </Link>
                </div>
              </ListItem>
            )
          })}
        </ZebraList>
        <ConfirmDialog
          action={merge}
          isOpen={this.state.isOpen}
          message="Once the content is merged, this account will be deactivated. Are you sure you want to merge?"
        />
      </div>
    )
  }
}
export default withApollo(MergeAccount)
