import gql from 'graphql-tag'

const GET_USER = gql`
  query($id: ID!) {
    epmc_user(id: $id) {
      id
      identities {
        type
        email
        name {
          title
          givenNames
          surname
        }
      }
      teams
    }
  }
`
const GET_ROLES_BY_TYPE = gql`
  query($organization: Boolean!) {
    rolesByType(organization: $organization)
  }
`
module.exports = {
  GET_USER,
  GET_ROLES_BY_TYPE,
}
