import React from 'react'
import { Query } from 'react-apollo'
import styled from 'styled-components'
import { H1, H3, H5, Icon, Link } from '@pubsweet/ui'
import { Loading, LoadingIcon, Page } from '../ui/'
import AccountDetails from './AccountDetailsContainer'
import MergeAccount from './MergeAccount'
import { GET_USER, GET_ROLES_BY_TYPE } from './queries'

const Container = styled.div`
  .reset-pass {
    margin-top: 20px;
  }
  > div {
    padding: 0px;
  }
  .search-again {
    float: right;
    font-size: 14px;
  }
`

const ManageAccount = props => (
  <Query
    fetchPolicy="cache-and-network"
    query={GET_USER}
    variables={{ id: props.location.pathname.split('/')[2] }}
  >
    {({ data, loading }) => {
      if (loading) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }
      const userData = data
      return (
        <Query
          fetchPolicy="cache-and-network"
          query={GET_ROLES_BY_TYPE}
          variables={{ organization: true }}
        >
          {({ data, loading }) => {
            if (loading) {
              return (
                <Loading>
                  <LoadingIcon />
                </Loading>
              )
            }
            return (
              <Page>
                <Container>
                  <H1>Manage User: {props.location.pathname.split('/')[2]}</H1>
                  <Link className="search-again" to="/manage-users">
                    <Icon color="currentColor" size={2}>
                      search
                    </Icon>
                    Search again
                  </Link>
                  <H3>Profile</H3>
                  <AccountDetails
                    {...props}
                    roles={data.rolesByType}
                    user={userData.epmc_user}
                  />
                  <H3>Admin</H3>
                  <H5>Merge data from another account</H5>
                  <MergeAccount {...props} user={userData.epmc_user} />
                </Container>
              </Page>
            )
          }}
        </Query>
      )
    }}
  </Query>
)

export default ManageAccount
