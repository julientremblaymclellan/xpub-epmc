import gql from 'graphql-tag'

const UPDATE_USER = gql`
  mutation($input: UpdateUserInput) {
    epmc_updateUser(input: $input) {
      id
      identities {
        type
        email
        name {
          title
          givenNames
          surname
        }
      }
      teams
    }
  }
`

const MERGE_USER = gql`
  mutation($input: MergeRequest) {
    mergeUser(input: $input)
  }
`

module.exports = {
  UPDATE_USER,
  MERGE_USER,
}
