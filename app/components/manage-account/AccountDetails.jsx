import React from 'react'
import { H1, Link, Button, Icon } from '@pubsweet/ui'
import { Notification, ConfirmDialog, Select } from '../ui'
import UserCore from '../UserCore'

const NO_PRIVILEGE = 'No Privilege'

class AccountDetails extends React.Component {
  state = {
    isOpen: false,
    teamOptions: [],
    team: '',
  }

  toggleReset = () => {
    const { isOpen } = this.state
    this.setState({
      isOpen: !isOpen,
    })
  }

  render() {
    const { values, handleSubmit } = this.props
    // handling letter casing in admin user management
    if (values.manageAccount) {
      this.state.teamOptions = values.teamOptions
      this.state.team =
        values.team.charAt(0).toUpperCase() + values.team.slice(1)
    }
    const action = async input => {
      if (input) {
        values.success = await values.resetPassword(this.props)
      }
      this.toggleReset()
    }
    const selectTeam = e => {
      values.team =
        e.target.value !== NO_PRIVILEGE
          ? e.target.value.charAt(0).toLowerCase() + e.target.value.slice(1)
          : e.target.value
      this.setState({
        team: values.team,
      })
    }
    return (
      <React.Fragment>
        {values.signup && <H1>Create a Europe PMC plus account</H1>}
        {values.myAccount && <H1>My Account</H1>}

        {values.success && (
          <Notification type="success"> {values.success} </Notification>
        )}
        {values.error && (
          <Notification type="error">
            Error occured, please try again.
          </Notification>
        )}
        <form onSubmit={handleSubmit}>
          <UserCore {...this.props} />
          <Select
            icon="chevron_down"
            label="Privilege level"
            onChange={selectTeam}
            options={this.state.teamOptions}
            value={this.state.team}
            width="400px"
          />
          <Button primary type="submit">
            Confirm
          </Button>
        </form>
        <div className="reset-pass">
          <Icon color="currentColor" size={2}>
            rotate-ccw
          </Icon>
          <Link onClick={() => this.toggleReset()} to="#">
            Reset password
          </Link>
          <ConfirmDialog
            action={action}
            isOpen={this.state.isOpen}
            message="Are you sure you want to reset password?"
          />
        </div>
      </React.Fragment>
    )
  }
}

export { NO_PRIVILEGE, AccountDetails }
