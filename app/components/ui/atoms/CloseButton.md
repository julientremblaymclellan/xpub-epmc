Icon used to close something (like a modal window). Use the `Close` section to position it in the top right.

```js
<Close>
  <CloseIcon />
</Close>
```
