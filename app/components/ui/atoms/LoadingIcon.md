Spinning icon used when loading or waiting for API calls to finish. EPMC version of `@pubsweet/ui/Spinner`

```js
<LoadingIcon/>
```
