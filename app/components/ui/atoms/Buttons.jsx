import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const Buttons = styled.div`
  display: flex;
  flex-direction: ${props => (props.left ? 'row' : 'row-reverse')};
  align-items: center;
  justify-content: ${props =>
    props.right || props.left ? 'flex-start' : 'space-between'}};
  button {
    margin-${props => (props.top ? 'bottom' : 'top')}: calc(${th(
  'gridUnit',
)} * 3);
  }
  *:first-child + * {
    margin-${props => (props.left ? 'left' : 'right')}: calc(${th(
  'gridUnit',
)} * 4);
  }

  @media screen and (max-width: 680px) {
    flex-direction: column;
    justify-content: center
    align-items: ${props => (props.left ? 'flex-start' : 'flex-end')};
    *:first-child + * {
      margin-${props => (props.left ? 'left' : 'right')}: 0;
    }
  }
`
export { Buttons }
