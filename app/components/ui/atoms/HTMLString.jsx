import React from 'react'
import ReactHtmlParser from 'react-html-parser'

const HTMLString = ({ string, element: Element = 'span' }) => {
  let parsed = ReactHtmlParser(string)
  if (parsed.length > 0 && typeof parsed[0] === 'string') {
    parsed = ReactHtmlParser(parsed)
  }
  return <Element>{parsed}</Element>
}

export default HTMLString
