import React from 'react'
import styled from 'styled-components'
import { Icon, Action } from '@pubsweet/ui'

const Close = styled.p`
  display: flex;
  align-items: center;
  justify-content: flex-end;
`

const CloseButton = ({ size = 4, ...props }) => (
  <Action {...props}>
    <Icon color="currentColor" size={size}>
      x
    </Icon>
  </Action>
)

export { Close, CloseButton }
