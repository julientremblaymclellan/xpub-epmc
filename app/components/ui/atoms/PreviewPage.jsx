import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export const PreviewPage = styled.div`
  display: flex;
  justify-content: center;
  align-items: stretch;
  background-color: ${th('colorBackground')};
  min-height: calc(100vh - (${th('gridUnit')} * 20));
  box-sizing: border-box;

  @media screen and (max-width: 870px) {
    flex-wrap: wrap;
    min-height: 0;
  }
`
export const PreviewPanel = styled.div`
  flex: 1 1 1000px;
  max-width: 100%;
  min-width: 0;
  & > div {
    width: 100%;
    max-width: 1000px;
    margin: 0 0 0 auto;
    @media screen and (min-width: 870px) {
      position: -webkit-sticky;
      position: sticky;
      top: calc(-${th('gridUnit')} * 8);
    }
    & > div {
      padding: 0 calc(${th('gridUnit')} * 2);
    }
    & > div:last-child {
      padding-bottom: calc(${th('gridUnit')} * 2);
    }
  }
`
export const EditPanel = styled.div`
  flex: 1 1 500px;
  min-height: 100vh;
  & > div {
    height: 100%;
    width: 100%;
    max-width: 500px;
    margin: 0 auto 0 0;
    border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
    border-top: 0;
    border-bottom: 0;
    background-color: ${th('colorTextReverse')};
    padding-bottom: calc(${th('gridUnit')} * 3);
    & > div:first-child {
      padding-left: calc(${th('gridUnit')} * 3);
    }
    @media screen and (max-width: 870px) {
      max-width: 100%;
    }
  }
`
export const PanelHeader = styled.div`
  height: calc(${th('gridUnit')} * 10);
  display: flex;
  align-items: flex-end;
`
export const PanelContent = styled.div`
  .editor-wrapper,
  .annotator,
  .pdf-viewer,
  #html-preview {
    box-sizing: border-box;
    height: calc(100vh - (${th('gridUnit')} * 4));
    @media screen and (max-width: 870px) {
      height: 90vh;
    }
  }
  .pdf-viewer-pane {
    overflow: auto;
    height: calc(100% - (${th('gridUnit')} * 3));
    padding: calc(${th('gridUnit')} / 2) 0;
  }
  #html-preview {
    overflow: auto;
    background-color: ${th('colorTextReverse')};
    border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  }
`
export const SectionHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: ${th('colorBackgroundHue')};
  min-height: calc(${th('gridUnit')} * 6);
  padding: ${th('gridUnit')} calc(${th('gridUnit')} * 3);
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  border-right: 0;
  border-left: 0;
  & > * {
    margin: 0;
  }
  @media screen and (max-width: 870px) {
    padding: ${th('gridUnit')} calc(${th('gridUnit')} * 4);
  }
`
export const SectionContent = styled.div`
  max-width: 100%;
  & > * {
    padding: ${th('gridUnit')} calc(${th('gridUnit')} * 3);
  }
`
