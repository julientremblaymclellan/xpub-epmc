import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export const Toggle = styled.div`
  button,
  a {
    vertical-align: top;
    font-weight: 600;
  }
  button + button,
  a + a {
    margin-left: calc(${th('gridUnit')} * 4);
  }
  @media screen and (max-width: ${props =>
      (props.children.length > 3 && 800) ||
      (props.children.length > 2 && 500) ||
      400}px) {
    display: flex;
    align-items: center;
    justify-content: space-between;
    flex-wrap: wrap;
    button,
    a {
      padding: ${th('gridUnit')} ${th('gridUnit')} 0 0;
    }
    button + button,
    a + a {
      margin: 0;
    }
  }
`
