import React from 'react'
import ReactDOM from 'react-dom'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import { Cover } from './'

export const Modal = styled.div`
  box-shadow: 0 0 8px ${th('colorBorder')};
  background-color: ${props =>
    props.accent ? th('colorBackgroundHue') : th('colorTextReverse')};
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  padding: calc(${th('gridUnit')} * 6);
  width: 800px;
  max-width: 85%;
  box-sizing: border-box;
  margin: calc(${th('gridUnit')} * 4) auto;
  & > p:first-child {
    & + h2 {
      margin-top: 0;
    }
  }
  h2:first-child {
    margin-top: 0;
  }
`
export const Portal = React.forwardRef(
  ({ children, transparent, ...props }, ref) =>
    ReactDOM.createPortal(
      <Cover ref={ref} transparent={transparent}>
        <Modal {...props}>{children}</Modal>
      </Cover>,
      document.getElementById('root').firstChild,
    ),
)
