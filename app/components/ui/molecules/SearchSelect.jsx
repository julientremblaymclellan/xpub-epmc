/* eslint no-eval: 0 */
import React from 'react'
import styled from 'styled-components'
import { Icon, Button } from '@pubsweet/ui'
import { th, override } from '@pubsweet/ui-toolkit'
import { isEqual, isEmpty } from 'lodash'

const Root = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: ${props =>
    props.inline ? '0' : `calc(${props.theme.gridUnit} * 3)`};
  width: ${props => (props.width ? props.width : 'auto')};
  max-width: 100%;
  ${override('ui.SearchSelect')};
`
const Label = styled.label`
  font-size: ${th('fontSizeBaseSmall')};
  display: block;
  ${override('ui.Label')};
  ${override('ui.Select.Label')};
  ${override('ui.SearchSelect.Label')};
`
const borderColor = ({ theme, validationStatus = 'default' }) =>
  ({
    error: theme.colorError,
    success: theme.colorSuccess,
    default: theme.colorBorder,
  }[validationStatus])

const SelectedContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  min-height: calc(${th('gridUnit')} * 7);
  ${override('ui.SearchSelect.SelectedContainer')};
`
const SelectedLabel = styled.span`
  display: inline-flex;
  align-items: center;
  font-size: ${th('fontSizeBaseSmall')};
  ${override('ui.Label')};
  ${override('ui.SearchSelect.Label')};
  ${override('ui.SearchSelect.SelectedLabel')};
`
const Selected = styled(Button)`
  display: inline-flex;
  align-items: center;
  margin: 0 ${th('gridUnit')} ${th('gridUnit')} 0;
  ${override('ui.SearchSelect.Selected')};
`
const Dropdown = styled.div`
  max-width: 100%;
  box-sizing: border-box;
  position: relative;

  ul {
    display: none;
  }
  &.focus {
    outline-offset: -2px;
    outline: -webkit-focus-ring-color auto 5px;
    ul {
      border: ${th('borderWidth')} ${th('borderStyle')} ${borderColor};
      border-top: 0;
      display: block;
    }
  }

  ${override('ui.Select.Dropdown')};
  ${override('ui.SearchSelect.Dropdown')};
`
const DropdownText = styled.div`
  width: 100%;
  border: ${th('borderWidth')} ${th('borderStyle')} ${borderColor};
  height: calc(${th('gridUnit')} * 6);
  border-radius: 6px;
  font-family: inherit;
  font-size: inherit;
  -moz-appearance: none;
  -webkit-appearance: none;
  box-sizing: border-box;
  align-items: center;
  white-space: pre;
  color: ${th('colorText')};
  background-color: ${th('colorBackgroundHue')};
  &.disabled {
    opacity: 0.6;
  }
  &.disabled + * {
    opacity: 0.6;
  }
  ${override('ui.Select.DropdownText')};
  ${override('ui.SearchSelect.DropdownText')};
`
const SearchInput = styled.input`
  width: 100%
  height: 100%;
  border: none;
  background-color: transparent;
  padding: 0 ${th('gridUnit')};
  box-sizing: border-box;
  font-family: inherit;
  font-size: inherit;
  &:focus {
    outline: none;
  }
`
const DropdownIcon = styled(Icon)`
  border: ${th('borderWidth')} ${th('borderStyle')} ${borderColor};
  background-color: ${th('colorBackgroundHue')};
  height: 100%;
  box-sizing: border-box;
  display: inline-flex;
  align-items: center;
  position: absolute;
  right: 0;
  top: 0;
  ${override('ui.Select.DropdownIcon')};
  ${override('ui.SearchSelect.DropdownIcon')};
`
const OptionList = styled.ul`
  max-height: calc(${th('gridUnit')} * 24);
  background-color: ${th('colorBackground')};
  overflow: auto;
  max-width: 100%;
  list-style-type: none;
  margin: 0;
  padding: 0;
  width: 100%;
  position: absolute;
  top: calc(${th('gridUnit')} * 6);
  z-index: 1;
  ${override('ui.SearchSelect.OptionList')};
`
const Option = styled.li`
  cursor: default;
  display: flex;
  padding: ${th('gridUnit')};
  &:hover,
  &:focus {
    color: ${th('colorTextReverse')};
    background-color: ${th('colorPrimary')};
  }
  &.selected {
    font-style: italic;
  }
  & > div {
    flex: 1 1 100%;
  }
  & > div:first-child {
    flex: 1 1 calc(${th('gridUnit')} * 5);
  }
  ${override('ui.SearchSelect.Option')};
`
const DefaultDropIcon = ({ icon }) => (
  <DropdownIcon color="currentColor">{icon}</DropdownIcon>
)

class SearchSelect extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedOptions: this.props.selectedOptions
        ? this.props.selectedOptions
        : [],
      inFocus: false,
    }
    this.addOption = this.addOption.bind(this)
    this.keyPressed = this.keyPressed.bind(this)
    this.keyRemove = this.keyRemove.bind(this)
    this.removeOption = this.removeOption.bind(this)
    this.addFocus = this.addFocus.bind(this)
    this.unFocus = this.unFocus.bind(this)
    this.setFocusRef = this.setFocusRef.bind(this)
  }
  componentDidMount() {
    // generate a unique ID to link the label to the input
    // note this may not play well with server rendering
    this.inputId = `searchsel-${Math.round(Math.random() * 1e12).toString(36)}`
    document.addEventListener('mousedown', this.unFocus)
    document.addEventListener('focus', this.unFocus, true)
  }
  componentWillUnmount() {
    document.removeEventListener('mousedown', this.unFocus)
    document.removeEventListener('focus', this.unFocus)
  }
  setFocusRef(focus) {
    this.focus = focus
  }
  removeOption(index) {
    const selectedOptions = [...this.state.selectedOptions]
    selectedOptions.splice(index, 1)
    this.setState({ selectedOptions })
    this.props.optionsOnChange(selectedOptions)
  }
  keyRemove(event, index) {
    return event.keyCode !== 13 || this.removeOption(index)
  }
  keyPressed(event, option) {
    const key = event.keyCode
    switch (key) {
      case 13:
        this.addOption(event, option)
        break
      case 40:
      case 39:
        if (event.target.id === this.inputId) {
          event.target.parentElement.parentElement.lastChild.firstChild.tabIndex = 0
          event.target.parentElement.parentElement.lastChild.firstChild.focus()
        } else if (event.target.nextSibling) {
          event.target.tabIndex = -1
          event.target.nextSibling.tabIndex = 0
          event.target.nextSibling.focus()
        }
        break
      case 38:
        if (event.target.id !== this.inputId && event.target.previousSibling) {
          event.target.tabIndex = -1
          event.target.previousSibling.tabIndex = 0
          event.target.previousSibling.focus()
        }
        break
      case 36:
        if (event.target.id !== this.inputId) {
          event.target.tabIndex = -1
          event.target.parentElement.firstChild.tabIndex = 0
          event.target.parentElement.firstChild.focus()
        }
        break
      case 35:
        if (event.target.id !== this.inputId) {
          event.target.tabIndex = -1
          event.target.parentElement.lastChild.tabIndex = 0
          event.target.parentElement.lastChild.focus()
        }
        break
      default:
        break
    }
  }
  addOption(event, option) {
    event.preventDefault()
    event.stopPropagation()
    if (option) {
      if (!this.props.singleSelect) {
        const selectedOptions = [...this.state.selectedOptions]
        if (!this.state.selectedOptions.some(opt => isEqual(opt, option))) {
          selectedOptions.push(option)
          this.setState({ selectedOptions })
          this.props.optionsOnChange(selectedOptions)
        }
      } else {
        this.setState({
          selectedOptions: option,
          inFocus: false,
        })
        this.props.optionsOnChange(option)
      }
    }
    if (event.currentTarget) {
      event.currentTarget.blur()
      this.focus.blur()
      this.setState({ inFocus: false })
    }
  }
  addFocus(event) {
    if (!this.props.disabled) {
      this.setState({ inFocus: true })
    }
  }
  unFocus(event) {
    const select = document.getElementById(`${this.inputId}select`)
    if (select && !select.contains(event.target)) {
      this.setState({ inFocus: false })
    }
  }
  render() {
    const {
      children,
      dropIcon: DropIcon = DefaultDropIcon,
      icon = 'chevrons_down',
      inline,
      label,
      query,
      onInput,
      displaySelected,
      disabled,
      placeholder = 'Find as you type...',
      singleSelect = false,
      width,
      ...props
    } = this.props
    let selectStyle = {}
    if (
      children.some(child => child.length > 0 || !isEmpty(child)) &&
      this.state.inFocus
    ) {
      selectStyle = {
        borderBottomRightRadius: '0',
        borderBottomLeftRadius: '0',
      }
    }
    return (
      <Root inline={inline} width={width}>
        {!singleSelect && (
          <SelectedContainer>
            <SelectedLabel>Selected:</SelectedLabel>
            {this.state.selectedOptions.map((option, i) => (
              <Selected
                key={JSON.stringify(option)}
                onClick={() => this.removeOption(i)}
                onKeyDown={e => this.keyRemove(e, i)}
                tabIndex={0}
              >
                {displaySelected ? eval(displaySelected) : option}
                <Icon color="currentColor" size={2}>
                  x
                </Icon>
              </Selected>
            ))}
          </SelectedContainer>
        )}
        {label && <Label htmlFor={this.inputId}>{label}</Label>}
        <Dropdown
          className={this.state.inFocus ? 'focus' : ''}
          id={`${this.inputId}select`}
          onBlur={this.unFocus}
          onClick={this.addFocus}
          ref={this.setFocusRef}
        >
          <DropdownText
            className={disabled ? 'disabled' : ''}
            style={selectStyle}
            {...props}
          >
            <SearchInput
              disabled={disabled}
              id={this.inputId}
              onChange={onInput}
              onFocus={this.addFocus}
              onKeyDown={e => this.keyPressed(e)}
              placeholder={placeholder}
              type="text"
              value={query}
            />
          </DropdownText>
          <DropIcon icon={icon} />
          {children.some(child => child.length > 0 || !isEmpty(child)) && (
            <OptionList>
              {React.Children.map(children, child => {
                const selected =
                  child &&
                  child.props['data-option'] &&
                  Array.isArray(this.state.selectedOptions) &&
                  this.state.selectedOptions.some(opt =>
                    isEqual(opt, child.props['data-option']),
                  )
                if (child) {
                  return (
                    <Option
                      className={selected ? 'selected' : ''}
                      key={child.props.propKey}
                      onClick={e =>
                        this.addOption(e, child.props['data-option'])
                      }
                      onKeyDown={e =>
                        this.keyPressed(e, child.props['data-option'])
                      }
                      tabIndex={-1}
                    >
                      <div>
                        {selected && <Icon color="currentColor">check</Icon>}
                      </div>
                      <div>{child.props.children}</div>
                    </Option>
                  )
                }
                return child
              })}
            </OptionList>
          )}
        </Dropdown>
      </Root>
    )
  }
}

SearchSelect.Option = Option

export default SearchSelect
