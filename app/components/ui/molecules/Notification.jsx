import React from 'react'
import styled from 'styled-components'
import { th, darken } from '@pubsweet/ui-toolkit'
import { Icon } from '@pubsweet/ui'

const Container = styled.div`
  padding: ${th('gridUnit')};
  margin: ${th('gridUnit')} 0;
  width: 100%;
  display: flex;
  align-items: flex-start;
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  box-sizing: border-box;
  &.hidden {
    display: none;
  }
  &.warning {
    color: ${th('colorText')};
    background-color: ${th('colorWarning')};
  }
  &.error {
    color: ${th('colorTextReverse')};
    background-color: ${th('colorError')};
  }
  &.info {
    color: ${th('colorText')};
    background-color: ${darken('colorBackgroundHue', 7)};
  }
  &.success {
    color: ${th('colorTextReverse')};
    background-color: ${th('colorSuccess')};
  }
  & > div,
  & > div * {
    font-size: ${th('fontSizeBaseSmall')};
  }
`
const NotifIcon = {
  warning: 'alert-triangle',
  error: 'alert-circle',
  info: 'info',
  success: 'check-circle',
}

const Notification = ({ children, type, ...props }) => (
  <Container className={`${props.className ? props.className : ''} ${type}`}>
    <Icon color="currentColor" size={2}>
      {NotifIcon[type]}
    </Icon>
    <div>{children}</div>
  </Container>
)
export { Notification }
