import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Action, Icon } from '@pubsweet/ui'

const Indicator = styled(Action)`
  margin: 0 calc(${th('gridUnit')} / 2);
  cursor: pointer;
  span {
    display: inline-block;
    vertical-align: middle;
    &.current {
      text-decoration: underline;
    }
  }
`

const ArrowIcon = styled(Icon)`
  padding: 6px 0 0;
`

// props: totalSize(required), pageSize(required), maxNumberOfDisplayedPages(default 5), currentPage(required), onPageEntered(param: currentPage)
const Pagination = ({ ...props }) => {
  const { totalSize, pageSize, currentPage } = props
  // inc. the last page
  const maxNumberOfDisplayedPages = props.maxNumberOfDisplayedPages
    ? props.maxNumberOfDisplayedPages
    : 5

  const paginationRequired = totalSize >= pageSize
  const numberOfpages =
    totalSize % pageSize === 0
      ? totalSize / pageSize
      : parseInt(totalSize / pageSize + 1, 10)
  const dotsRequired = numberOfpages > maxNumberOfDisplayedPages
  // always display the same number of pages
  // inc. the last page
  const numberOfDisplayedPages = dotsRequired
    ? maxNumberOfDisplayedPages
    : numberOfpages
  const start =
    dotsRequired && currentPage <= numberOfpages - numberOfDisplayedPages + 1
      ? currentPage
      : numberOfpages - numberOfDisplayedPages + 1
  const hasDots =
    dotsRequired && start + numberOfDisplayedPages <= numberOfpages

  const onPageEntered = currentPage => {
    props.onPageEntered(currentPage)
  }

  const previous = paginationRequired && currentPage !== 1 && (
    <React.Fragment>
      <Indicator onClick={() => onPageEntered(1)}>
        <ArrowIcon color="currentColor" size={3}>
          chevrons-left
        </ArrowIcon>
      </Indicator>
      <Indicator onClick={() => onPageEntered(currentPage - 1)}>
        <ArrowIcon color="currentColor" size={3}>
          chevron-left
        </ArrowIcon>
        <span>Previous</span>
      </Indicator>
    </React.Fragment>
  )

  let pages = []
  if (paginationRequired) {
    pages = Array.from(new Array(numberOfDisplayedPages - 1), (val, index) => (
      <React.Fragment>
        <Indicator key={index} onClick={() => onPageEntered(index + start)}>
          <span className={currentPage === index + start ? 'current' : ''}>
            {index + start}
          </span>
        </Indicator>
        {hasDots && index === numberOfDisplayedPages - 2 && (
          <Indicator
            key="dots"
            onClick={() => onPageEntered(index + start + 1)}
          >
            <span>...</span>
          </Indicator>
        )}
      </React.Fragment>
    ))
    pages.push(
      <Indicator
        key={numberOfpages}
        onClick={() => onPageEntered(numberOfpages)}
      >
        <span className={currentPage === numberOfpages ? 'current' : ''}>
          {numberOfpages}
        </span>
      </Indicator>,
    )
  }

  const next = paginationRequired && currentPage !== numberOfpages && (
    <React.Fragment>
      <Indicator onClick={() => onPageEntered(currentPage + 1)}>
        <span>Next</span>
        <ArrowIcon color="currentColor" size={3}>
          chevron-right
        </ArrowIcon>
      </Indicator>
      <Indicator onClick={() => onPageEntered(numberOfpages)}>
        <ArrowIcon color="currentColor" size={3}>
          chevrons-right
        </ArrowIcon>
      </Indicator>
    </React.Fragment>
  )

  return (
    <div>
      {previous}
      {pages}
      {next}
    </div>
  )
}

export default Pagination
