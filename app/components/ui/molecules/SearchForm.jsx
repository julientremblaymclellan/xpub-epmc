import React from 'react'
import styled, { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { TextField, Icon, Button } from '@pubsweet/ui'

const iconButton = css`
  position: relative;
  input {
    padding-right: calc(${th('gridUnit')} * 3);
  }
  button {
    position: absolute;
    top: 0;
    right: 0;
    border: none;
    color: ${th('colorPrimary')}
    background-color: transparent;
    min-width: initial;
    padding: 0;
    line-height: initial;
    height: calc(100% - (${th('gridUnit')} * 3));
    &:hover,
    &:active,
    &:focus {
      color: ${th('colorPrimary')};
      background-color: transparent !important;
    }
    &:focus {
      border: none;
      box-shadow: none;
    }
  }
`
const iconButtonOffset = css`
  button {
    top: calc(${th('gridUnit')} * 3);
    height: calc(100% - (${th('gridUnit')} * 6));
  }
`
const sepButton = css`
  div:first-child {
    max-width: 100% !important;
    width: calc(100% - (${th('gridUnit')} * 12));
    margin-right: calc(${th('gridUnit')} * 3);
  }
`
const Form = styled.form`
  display: flex;
  align-items: center;
  justify-content: space-between;

  ${props => (props.noButton ? iconButton : sepButton)};
  ${props => props.noButton && props.hasLabel && iconButtonOffset};
`
const SubmitButton = styled(Button)`
  max-width: calc(${th('gridUnit')} * 12);
`
const Text = styled(TextField)`
  width: 100%;
`
const SearchForm = props => (
  <Form
    hasLabel={props.label}
    noButton={props.noButton}
    onSubmit={props.onSubmit}
  >
    <Text
      label={props.label}
      name={props.name}
      onChange={props.onChange}
      placeholder={props.placeholder}
      value={props.value}
    />
    <SubmitButton disabled={props.disabled} primary type="submit">
      {props.buttonLabel ? (
        props.buttonLabel
      ) : (
        <Icon color="currentColor" size={3}>
          search
        </Icon>
      )}
    </SubmitButton>
  </Form>
)

export default SearchForm
