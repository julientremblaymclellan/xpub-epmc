import React from 'react'
import { Mutation } from 'react-apollo'
import {
  CREATE_NOTE,
  UPDATE_NOTE,
  GET_MANUSCRIPT,
  UPDATE_MANUSCRIPT,
  DELETE_MANUSCRIPT,
} from './operations'

export const ManuscriptMutations = BaseComponent => ({
  manuscript,
  children,
  refetch = [
    {
      query: GET_MANUSCRIPT,
      variables: { id: manuscript.id },
    },
  ],
  ...props
}) => (
  <Mutation mutation={UPDATE_MANUSCRIPT} refetchQueries={() => refetch}>
    {(updateManuscript, { data }) => {
      const { id } = manuscript
      const changeCitation = async citationData => {
        await updateManuscript({
          variables: { data: { id, ...citationData } },
        })
      }
      const updateEmbargo = async releaseDelay => {
        await updateManuscript({
          variables: { data: { id, meta: { releaseDelay } } },
        })
      }
      const updateGrants = async fundingGroup => {
        await updateManuscript({
          variables: { data: { id, meta: { fundingGroup } } },
        })
      }
      const setStatus = async (status, callBack) => {
        await updateManuscript({
          variables: { data: { id, status } },
        })
        callBack && callBack()
      }
      return (
        <Mutation mutation={DELETE_MANUSCRIPT} refetchQueries={() => refetch}>
          {(deleteManuscript, { data }) => {
            const deleteMan = async callBack => {
              await deleteManuscript({
                variables: {
                  id: manuscript.id,
                },
              })
              callBack && callBack()
            }
            return (
              <BaseComponent
                changeCitation={changeCitation}
                deleteMan={deleteMan}
                manuscript={manuscript}
                setStatus={setStatus}
                updateEmbargo={updateEmbargo}
                updateGrants={updateGrants}
                {...props}
              >
                {children}
              </BaseComponent>
            )
          }}
        </Mutation>
      )
    }}
  </Mutation>
)

export const NoteMutations = BaseComponent => ({
  manuscriptId,
  children,
  refetch,
  ...props
}) => (
  <Mutation mutation={UPDATE_NOTE} refetchQueries={() => refetch}>
    {(updateNote, { data }) => {
      const changeNote = async note => {
        await updateNote({
          variables: {
            data: {
              ...note,
            },
          },
        })
      }
      return (
        <Mutation mutation={CREATE_NOTE} refetchQueries={() => refetch}>
          {(createNote, { data }) => {
            const newNote = async note => {
              const data = { ...note }
              if (manuscriptId) {
                data.manuscriptId = manuscriptId
              }
              await createNote({
                variables: { data },
              })
            }
            return (
              <BaseComponent
                changeNote={changeNote}
                newNote={newNote}
                {...props}
              >
                {children}
              </BaseComponent>
            )
          }}
        </Mutation>
      )
    }}
  </Mutation>
)
