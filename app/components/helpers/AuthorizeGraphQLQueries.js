import gql from 'graphql-tag'

const CURRENT_USER = gql`
  query {
    epmc_currentUser {
      id
      identities {
        type
        email
        name {
          title
          givenNames
          surname
        }
      }
    }
  }
`

const GET_USER = gql`
  query GetUser($id: ID!) {
    user(id: $id) {
      id
    }
  }
`

const GET_USER_TEAMS = gql`
  query GetUserTeams($userId: ID!) {
    userTeams(id: $userId) {
      id
      role
      objectType
    }
  }
`

module.exports = {
  CURRENT_USER,
  GET_USER,
  GET_USER_TEAMS,
}
