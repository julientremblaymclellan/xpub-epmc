import React from 'react'
import styled, { createGlobalStyle } from 'styled-components'
import { Icon, Button } from '@pubsweet/ui'
import { th, darken, lighten } from '@pubsweet/ui-toolkit'
import * as PDFJS from 'pdfjs-dist/webpack.js'
import Viewer from './Viewer'

const Limit = createGlobalStyle`
  body {
    overflow: hidden;
    div {
      position: relative !important;
    }
  }
`
const OuterContainer = styled.div`
  max-width: 100%;
  background-color: ${th('colorBackgroundHue')};
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  position: relative;
  height: 100vh;
  margin: 0;
  overflow: hidden;
  &.full {
    position: fixed !important;
    top: 0;
    right: 0;
    left: 0;
    z-index: 5;
    height: 100vh;
  }
`
const Toolbar = styled.div`
  height: calc(${th('gridUnit')} * 3);
  background-color: ${darken('colorBackgroundHue', 1)};
  box-shadow: 0 0 8px ${darken('colorBackgroundHue', 30)};
  position: relative;
  z-index: 2;
  display: flex;
  justify-content: space-between;
`
const Zoom = styled(Button)`
  display: inline-flex;
  vertical-align: top;
  margin: 0;
  height: calc(${th('gridUnit')} * 3);
  padding: 0;
  min-width: 0;
  line-height: calc(${th('gridUnit')} * 3);
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  background-color: ${th('colorBackgroundHue')};
  font-weight: normal;
  color: ${th('colorText')};
  font-size: ${th('fontSizeBaseSmall')}
  &:hover {
    background-color: ${lighten('colorBackgroundHue', 4)};
  }
`
const Info = styled.span`
  display: inline-block;
  vertical-align: top;
  margin: 0 ${th('gridUnit')};
  height: calc(${th('gridUnit')} * 3);
  line-height: calc(${th('gridUnit')} * 3);
  font-size: ${th('fontSizeBaseSmall')};
`
class PDFViewer extends React.Component {
  state = {
    pdf: null,
    text: [],
    page: 1,
    scale: 0,
    fullscreen: false,
    loading: true,
  }
  async componentDidMount() {
    const pdf = await PDFJS.getDocument(this.props.url)
    if (this.viewer) {
      this.setState({ pdf, loading: false })
    }
  }
  zoomIn = () => {
    const { scale } = this.state
    const rounded = Math.round(scale * 4) / 4
    if (rounded < 5) {
      if (rounded >= 2) {
        this.setState({ scale: rounded + 1 })
      } else {
        this.setState({ scale: rounded + 0.25 })
      }
    }
  }
  zoomOut = () => {
    const { scale } = this.state
    const rounded = Math.round(scale * 4) / 4
    if (rounded > 0.5) {
      if (rounded >= 3) {
        this.setState({ scale: rounded - 1 })
      } else {
        this.setState({ scale: rounded - 0.25 })
      }
    }
  }
  setFit = fit => {
    if (this.viewer && fit) this.setState({ scale: fit })
  }
  setRef = viewer => {
    this.viewer = viewer
  }
  countPages = () => {
    const { top } = this.viewer.getBoundingClientRect()
    const pages = this.viewer.getElementsByClassName('pdf-page')
    const topPages = Array.prototype.filter.call(
      pages,
      page => page.getBoundingClientRect().top <= top,
    )
    if (topPages.length > 0) {
      const page = topPages[topPages.length - 1].dataset.pageNumber
      if (this.state.page !== page) {
        this.setState({ page })
      }
    }
  }
  deliverText = pageText => {
    if (this.viewer && (this.props.textContent || this.props.loaded)) {
      this.setState({ text: [...this.state.text, pageText] }, () => {
        const { text, pdf } = this.state
        if (text.length === pdf._pdfInfo.numPages) {
          const sorted = text.sort((a, b) => a.page - b.page)
          if (this.props.textContent) {
            this.props.textContent(sorted.map(p => p.text).join(' '))
          }
          if (this.props.loaded) {
            this.props.loaded(true)
          }
        }
      })
    }
  }
  render() {
    const { pdf, scale, page, fullscreen, loading } = this.state
    return (
      <React.Fragment>
        {fullscreen && <Limit />}
        <OuterContainer
          className={`pdf-viewer ${fullscreen && 'full'}`}
          data-pages={pdf && pdf._pdfInfo && pdf._pdfInfo.numPages}
          ref={this.setRef}
        >
          <Toolbar>
            {pdf && pdf._pdfInfo && (
              <Info>
                Page {page} of {pdf._pdfInfo.numPages}
              </Info>
            )}
            <div>
              <Zoom onClick={this.zoomOut}>
                <Icon size={2}>minus</Icon>
              </Zoom>
              <Zoom onClick={this.zoomIn}>
                <Icon size={2}>plus</Icon>
              </Zoom>
              <Info>{(scale * 100).toFixed()}%</Info>
              <Zoom
                onClick={() => this.setState({ scale: 0 })}
                style={{ padding: '0 4px', marginRight: '4px' }}
              >
                Fit to width
              </Zoom>
            </div>
            <div>
              <Zoom
                onClick={() => this.setState({ fullscreen: !fullscreen })}
                style={{ padding: '0 4px' }}
              >
                {fullscreen ? 'Exit full screen' : 'Full screen'}
                <Icon size={2}>{fullscreen ? 'minimize' : 'maximize'}</Icon>
              </Zoom>
            </div>
          </Toolbar>
          {pdf && (
            <Viewer
              fitWidth={this.setFit}
              loading={loading}
              onScroll={this.countPages}
              pdf={pdf}
              scale={scale}
              textContent={this.deliverText}
            />
          )}
        </OuterContainer>
      </React.Fragment>
    )
  }
}

export default PDFViewer
