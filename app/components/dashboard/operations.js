import gql from 'graphql-tag'

const ClaimFragment = gql`
  fragment ClaimFragment on Manuscript {
    claiming {
      id
      givenNames
      surname
    }
  }
`
export const CLAIM_MANUSCRIPT = gql`
  mutation ClaimManuscript($id: ID!) {
    claimManuscript(id: $id) {
      manuscript {
        id
        updated
        ...ClaimFragment
      }
      errors {
        message
      }
    }
  }
  ${ClaimFragment}
`
export const UNCLAIM_MANUSCRIPT = gql`
  mutation UnclaimManuscript($id: ID!) {
    unclaimManuscript(id: $id) {
      manuscript {
        id
        updated
        ...ClaimFragment
      }
      errors {
        message
      }
    }
  }
  ${ClaimFragment}
`
export const CURRENT_USER = gql`
  query($email: String) {
    epmc_currentUser(email: $email) {
      id
    }
  }
`
export const ADD_REVIEWER = gql`
  mutation AddReviewer($noteId: ID!) {
    addReviewer(noteId: $noteId)
  }
`
export const COUNT_MANUSCRIPTS = gql`
  query CountByStatus {
    countByStatus {
      type
      count
    }
  }
`
export const GET_MANUSCRIPT = gql`
  query SearchArticleIds($id: String!) {
    searchArticleIds(id: $id) {
      manuscript {
        id
        status
      }
      errors {
        message
      }
    }
  }
`

const DashboardFragment = gql`
  fragment DashboardManuscript on Manuscript {
    id
    status
    updated
    deleted
    formState
    journal {
      meta {
        nlmta
      }
    }
    meta {
      title
      articleIds {
        pubIdType
        id
      }
      publicationDates {
        type
        date
      }
      notes {
        notesType
        content
      }
      fundingGroup {
        fundingSource
        awardId
        title
        pi {
          surname
          givenNames
          title
          email
        }
      }
      releaseDelay
      unmatchedJournal
    }
    teams {
      role
      teamMembers {
        user {
          id
        }
        alias {
          name {
            title
            surname
            givenNames
          }
        }
      }
    }
  }
`

export const ALL_MANUSCRIPTS = gql`
  query LoadManuscripts {
    manuscripts {
      ...DashboardManuscript
    }
  }
  ${DashboardFragment}
`

export const ADMIN_MANUSCRIPTS = gql`
  query LoadAdminManuscripts($external: Boolean) {
    adminManuscripts(external: $external) {
      total
      manuscripts {
        ...DashboardManuscript
        ...ClaimFragment
      }
    }
  }
  ${ClaimFragment}
  ${DashboardFragment}
`

export const MANUSCRIPTS_BY_STATUS = gql`
  query FindManuscriptsByStatus($query: String!, $page: Int, $pageSize: Int) {
    findByStatus(query: $query, page: $page, pageSize: $pageSize) {
      total
      manuscripts {
        ...DashboardManuscript
        ...ClaimFragment
      }
    }
  }
  ${ClaimFragment}
  ${DashboardFragment}
`

export const MANUSCRIPTS_BY_EMAIL_OR_LASTNAME = gql`
  query MANUSCRIPTS_BY_EMAIL_OR_LASTNAME(
    $query: String!
    $page: Int
    $pageSize: Int
  ) {
    searchManuscripts(query: $query, page: $page, pageSize: $pageSize) {
      total
      manuscripts {
        ...DashboardManuscript
        ...ClaimFragment
      }
    }
  }
  ${ClaimFragment}
  ${DashboardFragment}
`

export const METRICS = gql`
  query METRICS {
    getMetrics {
      id
      created
      updated
      display_mth
      submitted
      xml_review
      xml_review_within_10_days
      xml_review_within_10_days_perc
      xml_review_within_3_days
      xml_review_within_3_days_perc
      published
      ncbi_ready_median
    }
  }
`
