import React from 'react'
import { withRouter } from 'react-router'
import { ApolloConsumer } from 'react-apollo'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Notification, SearchForm } from '../ui'
import { GET_MANUSCRIPT } from './operations'
import { States } from './'

const SearchArea = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  input {
    height: calc(${th('gridUnit')} * 5);
  }
  & > *:first-child {
    max-width: calc(${th('gridUnit')} * 15);
    margin-right: calc(${th('gridUnit')} * 3);
  }
  & > *:nth-child(2) {
    min-width: calc(${th('gridUnit')} * 38);
  }
  @media screen and (max-width: 880px) {
    justify-content: flex-start;
    margin-top: ${th('gridUnit')};
    flex-wrap: wrap;
  }
`
class SearchBoxes extends React.Component {
  state = {
    id: SearchBoxes.id ? SearchBoxes.id : '',
    search: SearchBoxes.search ? SearchBoxes.search : '',
    errors: [],
  }
  componentDidUpdate() {
    if (this.state.errors) {
      setTimeout(() => {
        this.setState({ errors: [] })
      }, 10000)
    }
  }
  static id = ''
  static search = ''
  onSearchValChanged = e => {
    this.setState({ [e.target.name]: e.target.value })
    SearchBoxes[e.target.name] = e.target.value
  }
  onSearchValSubmitted = (where, e) => {
    e.preventDefault()
    const val = this.state[where]
    const route = `/search?${where}=${val}`
    this.props.history.push(route)
  }
  render() {
    const { id, search } = this.state

    return (
      <React.Fragment>
        <SearchArea>
          <ApolloConsumer>
            {client => {
              const goToArticle = async e => {
                e.preventDefault()
                const { id } = this.state
                const { data } = await client.query({
                  query: GET_MANUSCRIPT,
                  variables: { id },
                })
                const { errors, manuscript } = data.searchArticleIds
                if (errors) {
                  this.setState({ errors: data.searchArticleIds.errors })
                } else if (manuscript) {
                  this.props.history.push(
                    `submission/${manuscript.id}/${
                      States.admin[manuscript.status].url
                    }`,
                  )
                }
              }
              return (
                <SearchForm
                  disabled={!id}
                  name="id"
                  noButton="true"
                  onChange={this.onSearchValChanged}
                  onSubmit={e => goToArticle(e)}
                  placeholder="ID"
                  value={id}
                />
              )
            }}
          </ApolloConsumer>
          <SearchForm
            disabled={!search}
            name="search"
            noButton="true"
            onChange={this.onSearchValChanged}
            onSubmit={e => this.onSearchValSubmitted('search', e)}
            placeholder="Title, or author last name or email"
            value={search}
          />
        </SearchArea>
        {this.state.errors.map(e => (
          <Notification key={e.message} type="error">
            {e.message}
          </Notification>
        ))}
      </React.Fragment>
    )
  }
}

export default withRouter(SearchBoxes)
