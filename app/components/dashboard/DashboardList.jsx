import React from 'react'
import styled from 'styled-components'
import { graphql, compose } from 'react-apollo'
import { th, lighten } from '@pubsweet/ui-toolkit'
import { Action, Button, Icon, Link } from '@pubsweet/ui'
import { B, HTMLString, ZebraList, ZebraListItem } from '../ui'
import { States, timeSince } from './'
import { CLAIM_MANUSCRIPT, UNCLAIM_MANUSCRIPT } from './operations'

const ListItem = styled(ZebraListItem)`
  display: flex;
  align-items: top;
  justify-content: space-between;
  padding: ${th('gridUnit')} calc(${th('gridUnit')} * 2);
  @media screen and (max-width: 800px) {
    display: block;
  }
  a {
    font-weight: 600;
  }
`
const Title = styled.p`
  margin: 0 0 ${th('gridUnit')};
`
const ItemDetails = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: start;
  flex-wrap: wrap;
  font-size: ${th('fontSizeBaseSmall')};
  * {
    margin-right: calc(${th('gridUnit')} * 2);
  }
`
const Column = styled.div`
  font-size: ${th('fontSizeBaseSmall')};
  margin-left: calc(${th('gridUnit')} * 4);
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  @media screen and (max-width: 800px) {
    margin: 0;
    align-items: flex-start;
  }
  & > *:not(:first-child) {
    margin-top: calc(${th('gridUnit')} / 2);
  }
  button {
    margin-top: 0;
  }
  div {
    width: 100%;
    font-size: ${th('fontSizeBaseSmall')};
    display: flex;
    justify-content: space-between;
    align-items: baseline;
  }
`
const ItemStatus = styled.div`
  white-space: nowrap;
  padding: 0 ${th('gridUnit')};
  color: ${th('colorTextReverse')};
  font-size: ${th('fontSizeBaseSmall')};
  min-width: calc(${th('gridUnit')} * 27);
  text-align: center;
  &.warning {
    background-color: ${th('colorWarning')};
    color: ${th('colorText')};
  }
  &.error {
    background-color: ${th('colorError')};
  }
  &.normal {
    background-color: ${lighten('colorPrimary', 50)};
  }
  &.success {
    background-color: ${th('colorSuccess')};
  }
  &.removed {
    background-color: ${th('colorBorder')};
  }
  @media screen and (max-width: 800px) {
    min-width: 0;
    width: calc(${th('gridUnit')} * 27);
    white-space: normal;
    margin-top: ${th('gridUnit')};
  }
`
const ClaimAction = styled(Button)`
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('fontSize')};
  padding: 0 calc(${th('gridUnit')} * 1.5);
  margin-top: ${th('gridUnit')};
  min-width: 0;
`
const UnclaimAction = styled(Action)`
  display: flex;
  align-items: center;
  font-size: ${th('fontSizeBaseSmall')};
  &.disabled,
  &:disabled {
    opacity: 1;
    color: ${th('colorText')};
    cursor: default;
    &:hover {
      text-decoration: none;
      background-color: transparent;
    }
  }
  & > *:nth-child(even) {
    display: none;
  }
  &.active:hover {
    & > *:nth-child(even) {
      display: inline-flex;
    }
    & > *:nth-child(odd) {
      display: none;
    }
  }
`
const claimStates = ['submitted', 'xml-qa', 'xml-triage']

const DashboardList = ({
  currentUser,
  listData,
  sectionRole,
  claimManuscript,
  unclaimManuscript,
  errors,
}) => (
  <ZebraList>
    {listData.map(manuscript => {
      if (manuscript.teams) {
        const {
          claiming,
          meta,
          journal,
          updated,
          deleted,
          status,
          teams,
        } = manuscript
        const { articleIds, releaseDelay, unmatchedJournal } = meta
        const pmid =
          articleIds && articleIds.find(aid => aid.pubIdType === 'pmid')
        const submitter = teams.find(team => team.role === 'submitter')
        const { title: st, givenNames: sg, surname: ss } = submitter
          ? submitter.teamMembers[0].alias.name
          : ''
        const reviewer = teams.find(team => team.role === 'reviewer')
        const { title: rt, givenNames: rg, surname: rs } = reviewer
          ? reviewer.teamMembers[0].alias.name
          : ''
        const userRole =
          sectionRole ||
          (currentUser.admin && 'admin') ||
          (reviewer &&
            reviewer.teamMembers[0].user.id === currentUser.id &&
            'reviewer') ||
          (submitter &&
            submitter.teamMembers[0].user.id === currentUser.id &&
            'submitter')
        const state =
          (deleted && {
            color: 'removed',
            status: 'Deleted',
            url: 'activity',
          }) ||
          States[userRole][status]
        const { url } = state
        return (
          <ListItem key={manuscript.id}>
            <div>
              <Title>
                <Link to={`/submission/${manuscript.id}/${url}`}>
                  <HTMLString string={meta.title} />
                </Link>
              </Title>
              <ItemDetails>
                {reviewer &&
                reviewer.teamMembers[0].user.id ===
                  submitter.teamMembers[0].user.id ? (
                  <span>
                    {`SUBMITTER/REVIEWER:${st ? ` ${st} ` : ' '}${sg} ${ss}`}
                  </span>
                ) : (
                  <React.Fragment>
                    <span>
                      {`SUBMITTER:${st ? ` ${st} ` : ' '}${sg} ${ss}`}
                    </span>
                    {reviewer && (
                      <span>
                        {`REVIEWER:${rt ? ` ${rt} ` : ' '}${rg} ${rs}`}
                      </span>
                    )}
                  </React.Fragment>
                )}
              </ItemDetails>
              <ItemDetails>
                <span>{manuscript.id}</span>
                {pmid && <span>PMID: {pmid.id}</span>}
                <span>
                  JOURNAL: {journal ? journal.meta.nlmta : unmatchedJournal}
                </span>
                {releaseDelay && (
                  <span>
                    EMBARGO: {releaseDelay} month
                    {parseInt(releaseDelay, 10) !== 1 && 's'}
                  </span>
                )}
              </ItemDetails>
            </div>
            <Column>
              <ItemStatus className={state.color}>{state.status}</ItemStatus>
              <B>{timeSince(updated)}</B>
              <div>
                {currentUser && currentUser.admin && (
                  <Link to={`/submission/${manuscript.id}/activity`}>
                    Activity
                  </Link>
                )}
                {userRole === 'admin' && claimStates.includes(status) && (
                  <React.Fragment>
                    {claiming ? (
                      <UnclaimAction
                        className={claiming.id === currentUser.id && 'active'}
                        disabled={claiming.id !== currentUser.id}
                        onClick={async () => {
                          const newData = await unclaimManuscript({
                            variables: { id: manuscript.id },
                          })
                          errors(newData.data.unclaimManuscript.errors)
                          return newData
                        }}
                      >
                        <Icon color="currentColor" size={2}>
                          check
                        </Icon>
                        <Icon color="currentColor" size={2}>
                          x
                        </Icon>
                        <span>{claiming.givenNames}</span>
                        <span>Unclaim</span>
                      </UnclaimAction>
                    ) : (
                      <ClaimAction
                        onClick={async () => {
                          const newData = await claimManuscript({
                            variables: { id: manuscript.id },
                          })
                          errors(newData.data.claimManuscript.errors)
                          return newData
                        }}
                        primary
                      >
                        Claim
                      </ClaimAction>
                    )}
                  </React.Fragment>
                )}
              </div>
            </Column>
          </ListItem>
        )
      }
      return null
    })}
  </ZebraList>
)

export default compose(
  graphql(CLAIM_MANUSCRIPT, { name: 'claimManuscript' }),
  graphql(UNCLAIM_MANUSCRIPT, { name: 'unclaimManuscript' }),
)(DashboardList)
