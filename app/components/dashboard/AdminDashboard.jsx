import React from 'react'
import { Query } from 'react-apollo'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Link, H3, Icon } from '@pubsweet/ui'
import { Loading, LoadingIcon, Table } from '../ui'
import { COUNT_MANUSCRIPTS } from './operations'
import DashboardBase from './DashboardBase'

const ListTable = styled(Table)`
  width: 100%;
  margin-bottom: calc(${th('gridUnit')} * 3);
  tr > *:first-child {
    width: calc(${th('gridUnit')} * 10);
    text-align: right;
  }
`
const HelpdeskQueue = {
  'needs submission review': ['submitted'],
  'needs xml review': ['xml-qa'],
  'xml error reported': ['xml-triage'],
  'needs citation': ['xml-complete'],
}

const SubmitterQueue = {
  'not yet submitted': ['INITIAL', 'READY'],
  'needs reviewer approval': ['in-review'],
  'submission error': ['submission-error'],
  'needs final review': ['xml-review'],
}

const TaggerQueue = { tagging: ['tagging'] }

const Completed = {
  'approved for PMC': ['ncbi-ready'],
  published: ['published'],
  'being withdrawn': ['being-withdrawn'],
  deleted: ['deleted'],
}

const QueueTable = ({ title, queue, data }) => {
  let total = 0
  const tableData = Object.keys(queue).map(label => {
    const items = data.filter(s => queue[label].includes(s.type))
    let count = 0
    const states = items.map(i => {
      count += i.count
      total += i.count
      return i.type
    })
    return { label, status: queue[label], count, states }
  })
  return (
    <React.Fragment>
      <H3 style={{ marginTop: 0 }}>{title}</H3>
      <ListTable>
        <tbody>
          <tr>
            <th>{total}</th>
            <th>All</th>
          </tr>
          {tableData.map(row => (
            <tr key={row.label}>
              <td>{row.count}</td>
              <td>
                {row.count ? (
                  <Link to={`/search?status=${row.status}`}>{row.label}</Link>
                ) : (
                  row.label
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </ListTable>
    </React.Fragment>
  )
}

const Dashboard = ({ currentUser }) => (
  <Query fetchPolicy="cache-and-network" query={COUNT_MANUSCRIPTS}>
    {({ data, loading }) => {
      if (loading) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }
      return (
        <React.Fragment>
          <QueueTable
            data={data.countByStatus}
            queue={HelpdeskQueue}
            title="Helpdesk queue"
          />
          <QueueTable
            data={data.countByStatus}
            queue={SubmitterQueue}
            title="Submitter/Reviewer queue"
          />
          <QueueTable
            data={data.countByStatus}
            queue={TaggerQueue}
            title="Tagger queue"
          />
          <QueueTable
            data={data.countByStatus}
            queue={Completed}
            title="Completed"
          />
          <Link
            style={{ display: 'flex', alignItems: 'center' }}
            to="/admin-metrics"
          >
            <span>Go to metrics</span>
            <Icon color="currentColor">trending-up</Icon>
          </Link>
        </React.Fragment>
      )
    }}
  </Query>
)

const AdminDashboard = DashboardBase(Dashboard)

const AdminDashboardPage = ({ currentUser }) => {
  if (currentUser.admin) {
    return <AdminDashboard currentUser={currentUser} pageTitle="Dashboard" />
  }
  return null
}

export default AdminDashboardPage
