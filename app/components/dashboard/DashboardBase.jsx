import React from 'react'
import styled from 'styled-components'
import { Link, H1, Icon } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { Page } from '../ui'
import SearchBoxes from './SearchBoxes'

const Top = styled.div`
  display: flex
  justify-content: space-between;
  align-items: flex-end;
  & > *:first-child {
    margin-right: calc(${th('gridUnit')} * 4);
  }
  @media screen and (max-width: 800px) {
    display: block;
  }
`
const LinkAction = styled(Link)`
  display: inline-flex;
  align-items: center;
  font-weight: 600;
`
const LinkArea = styled.p`
  @media screen and (max-width: 800px) {
    margin: 0;
    display: inline-block;
  }
`

const CreateButton = () => (
  <LinkArea>
    <LinkAction to="/create">
      <Icon color="currentColor" size={3}>
        plus
      </Icon>
      <span>Submit a new manuscript</span>
    </LinkAction>
  </LinkArea>
)

const DashboardBase = BaseComponent => ({
  children,
  currentUser,
  pageTitle,
  hideSearchBoxes,
  ...props
}) => (
  <Page>
    <Top>
      <H1>{pageTitle}</H1>
      <CreateButton />
    </Top>
    {currentUser.admin && !hideSearchBoxes && <SearchBoxes />}
    <BaseComponent currentUser={currentUser} {...props}>
      {children}
    </BaseComponent>
  </Page>
)

export default DashboardBase
