import React from 'react'
import { Query, compose, graphql } from 'react-apollo'
import styled from 'styled-components'
import { H4, Link } from '@pubsweet/ui'
import { pageSize } from 'config'
import { Loading, LoadingIcon, Notification, Toggle, Pagination } from '../ui'
import {
  ALL_MANUSCRIPTS,
  ADMIN_MANUSCRIPTS,
  MANUSCRIPTS_BY_STATUS,
  ADD_REVIEWER,
  COUNT_MANUSCRIPTS,
} from './operations'
import DashboardBase from './DashboardBase'
import DashboardList from './DashboardList'
import SearchBoxes from './SearchBoxes'

const ToggleBar = styled(Toggle)`
  float: left;
  @media screen and (max-width: 880px) {
    float: none;
  }
`

const PaginationPane = styled.div`
  display: flex;
  justify-content: flex-end;
`

const MyQueue = ({ currentUser, errors, setErrors }) => (
  <Query
    fetchPolicy="cache-and-network"
    query={ADMIN_MANUSCRIPTS}
    variables={{ external: currentUser.external }}
  >
    {({ data, loading }) => {
      if (loading) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }
      const mine = []
      const claimed = []
      const unclaimed = []
      const { manuscripts } = data.adminManuscripts
      manuscripts.forEach(m => {
        if (m.claiming) {
          if (m.claiming.id === currentUser.id) {
            mine.push(m)
          } else {
            claimed.push(m)
          }
        } else {
          unclaimed.push(m)
        }
      })
      return (
        <React.Fragment>
          {errors.map(e => (
            <Notification key="message" type="warning">
              {e.message}
            </Notification>
          ))}
          {mine.length > 0 && (
            <React.Fragment>
              <H4>My claimed manuscripts ({mine.length})</H4>
              <DashboardList
                currentUser={currentUser}
                errors={setErrors}
                listData={mine}
                sectionRole="admin"
              />
            </React.Fragment>
          )}
          {unclaimed.length > 0 && (
            <React.Fragment>
              <H4>Unclaimed ({unclaimed.length})</H4>
              <DashboardList
                currentUser={currentUser}
                errors={setErrors}
                listData={unclaimed}
                sectionRole="admin"
              />
            </React.Fragment>
          )}
          {claimed.length > 0 && (
            <React.Fragment>
              <H4>In process ({claimed.length})</H4>
              <DashboardList
                currentUser={currentUser}
                errors={setErrors}
                listData={claimed}
                sectionRole="admin"
              />
            </React.Fragment>
          )}
        </React.Fragment>
      )
    }}
  </Query>
)

const MyManuscripts = ({ currentUser, errors, history }) => {
  const onPageEntered = p => {
    history.push(`/dashboard?completed=true&page=${p}`)
  }

  return (
    <Query fetchPolicy="cache-and-network" query={COUNT_MANUSCRIPTS}>
      {({ data, loading }) => {
        if (loading) {
          return (
            <Loading>
              <LoadingIcon />
            </Loading>
          )
        }

        // get the total number of manuscripts to decide whether the toggles are required
        const { countByStatus } = data
        let totalTount = 0
        countByStatus.forEach(status => {
          totalTount += status.count
        })
        const togglesRequired = totalTount > pageSize

        // determine the query and variables used to get manuscripts
        const query = togglesRequired ? MANUSCRIPTS_BY_STATUS : ALL_MANUSCRIPTS
        const params = new URLSearchParams(history.location.search)
        // togglesRequired and on the completed toggle
        const completed = params.get('completed')

        const variables = {}
        if (togglesRequired) {
          if (completed) {
            variables.query = `xml-complete,ncbi-ready,published`
            variables.page = params.get('page') ? params.get('page') - 1 : 0
            variables.pageSize = pageSize
          } else {
            variables.query = `INITIAL,READY,submission-error,in-review,submitted,tagging,xml-qa,xml-triage,xml-review,being-withdrawn`
            variables.page = -1
          }
        }

        return (
          <Query
            fetchPolicy="cache-and-network"
            query={query}
            variables={variables}
          >
            {({ data, loading }) => {
              if (loading) {
                return (
                  <Loading>
                    <LoadingIcon />
                  </Loading>
                )
              }

              const { total, manuscripts } = togglesRequired
                ? data.findByStatus
                : data

              // continue here...style later!!!
              const toggles = togglesRequired && (
                <React.Fragment>
                  <ToggleBar>
                    <Link
                      className={!completed ? 'current' : ''}
                      to="/dashboard"
                    >
                      In-process
                    </Link>
                    <Link
                      className={completed ? 'current' : ''}
                      to="/dashboard?completed=true"
                    >
                      Completed
                    </Link>
                  </ToggleBar>
                  <SearchBoxes />
                </React.Fragment>
              )

              if (completed) {
                const currentPage = params.get('page')
                  ? parseInt(params.get('page'), 10)
                  : 1
                return (
                  <React.Fragment>
                    {toggles}
                    {total ? (
                      <PaginationPane>
                        <Pagination
                          currentPage={currentPage}
                          onPageEntered={onPageEntered}
                          pageSize={pageSize}
                          totalSize={total}
                        />
                      </PaginationPane>
                    ) : (
                      ''
                    )}
                    <DashboardList
                      currentUser={currentUser}
                      listData={manuscripts}
                    />
                  </React.Fragment>
                )
              }

              const attention = []
              const review = []
              const complete = []
              const other = []
              manuscripts.forEach(m => {
                const reviewer = m.teams.find(t => t.role === 'reviewer')
                const submitter = m.teams.find(t => t.role === 'submitter')
                if (
                  reviewer &&
                  reviewer.teamMembers[0].user.id === currentUser.id &&
                  ['in-review', 'xml-review'].includes(m.status)
                ) {
                  review.push(m)
                } else if (
                  submitter.teamMembers[0].user.id === currentUser.id &&
                  ['INITIAL', 'READY', 'submission-error'].includes(m.status)
                ) {
                  attention.push(m)
                } else if (
                  !togglesRequired &&
                  ['xml-complete', 'ncbi-ready', 'published'].includes(m.status)
                ) {
                  complete.push(m)
                } else {
                  other.push(m)
                }
              })
              return (
                <React.Fragment>
                  {toggles}
                  {manuscripts.length === 0 && (
                    <Notification type="info">
                      There are currently no manuscripts connected to this
                      account.
                    </Notification>
                  )}
                  {errors.map(e => (
                    <Notification
                      key="message"
                      type={e.type ? e.type : 'warning'}
                    >
                      {e.message}
                    </Notification>
                  ))}
                  {attention.length > 0 && (
                    <React.Fragment>
                      <H4>Needs attention ({attention.length})</H4>
                      <DashboardList
                        listData={attention}
                        sectionRole="submitter"
                      />
                    </React.Fragment>
                  )}
                  {review.length > 0 && (
                    <React.Fragment>
                      <H4>Waiting for author review ({review.length})</H4>
                      <DashboardList listData={review} sectionRole="reviewer" />
                    </React.Fragment>
                  )}
                  {other.length > 0 && (
                    <React.Fragment>
                      <H4>In process at Europe PMC ({other.length})</H4>
                      <DashboardList
                        currentUser={currentUser}
                        listData={other}
                      />
                    </React.Fragment>
                  )}
                  {complete.length > 0 && (
                    <React.Fragment>
                      <H4>Submission complete ({complete.length})</H4>
                      <DashboardList
                        listData={complete}
                        sectionRole="submitter"
                      />
                    </React.Fragment>
                  )}
                </React.Fragment>
              )
            }}
          </Query>
        )
      }}
    </Query>
  )
}

const QueuePage = DashboardBase(MyQueue)
const ManuscriptsPage = DashboardBase(MyManuscripts)

class DashboardPage extends React.Component {
  state = { errors: [] }
  componentDidMount() {
    const { search, pathname } = this.props.location
    if (search) {
      const params = new URLSearchParams(search)
      const noteId = params.get('accept')
      if (noteId) {
        this.setReviewer(noteId)
        params.delete('accept')
        this.props.history.replace(`${pathname}?${params.toString()}`)
      }
    }
  }
  componentDidUpdate() {
    if (this.state.errors.length > 0) {
      setTimeout(() => {
        this.setState({ errors: [] })
      }, 4000)
    }
  }
  async setReviewer(noteId) {
    const set = await this.props.addReviewer({
      variables: { noteId },
      refetchQueries: [{ query: ALL_MANUSCRIPTS }],
      awaitRefetchQueries: true,
    })
    if (!set.data.addReviewer) {
      this.setState({
        errors: [
          {
            message: 'Manuscript already accepted for review.',
            type: 'warning',
          },
        ],
      })
    } else {
      this.setState({
        errors: [
          {
            message: 'Manuscript accepted for review.',
            type: 'success',
          },
        ],
      })
    }
  }
  render() {
    const { currentUser, history } = this.props
    if (currentUser.admin || currentUser.external) {
      return (
        <QueuePage
          currentUser={currentUser}
          errors={this.state.errors}
          pageTitle="My queue"
          setErrors={errors => this.setState({ errors })}
        />
      )
    }
    return (
      <ManuscriptsPage
        currentUser={currentUser}
        errors={this.state.errors}
        history={history}
        pageTitle="My manuscripts"
      />
    )
  }
}

export default compose(graphql(ADD_REVIEWER, { name: 'addReviewer' }))(
  DashboardPage,
)
