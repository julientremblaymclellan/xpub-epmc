import React from 'react'
import { Query } from 'react-apollo'
import styled from 'styled-components'
import { METRICS } from './operations'
import { Loading, LoadingIcon, Table } from '../ui'
import DashboardBase from './DashboardBase'

const MetricsTable = styled.div`
  overflow-x: auto;
`

const Metrics = ({ currentUser }) => (
  <Query fetchPolicy="cache-and-network" query={METRICS}>
    {({ data, loading }) => {
      if (loading) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }

      const cols = [
        {
          text: 'display_mth',
          helpText: 'Month',
        },
        {
          text: 'xml_review',
          helpText: 'Total manuscripts processed',
        },
        {
          text: 'xml_review_within_10_days',
          helpText: 'Number of manuscripts processed in 10 days',
        },
        {
          text: 'xml_review_within_10_days_perc',
          helpText: 'Percent of manuscripts processed in 10 days',
        },
        {
          text: 'xml_review_within_3_days',
          helpText: 'Number of manuscripts processed in 3 days',
        },
        {
          text: 'xml_review_within_3_days_perc',
          helpText: 'Percent of manuscripts processed in 3 days',
        },
        {
          text: 'submitted',
          helpText: 'Manuscripts submitted to Europe PMC',
        },
        {
          text: 'published',
          helpText: 'Manuscripts published to Europe PMC',
        },
        {
          text: 'ncbi_ready_median',
          helpText:
            'Median processing times from submission to NCBI packet (days)',
        },
      ]

      const metrics = data.getMetrics.map(row => {
        const entity = {}
        cols.forEach(col => (entity[col.text] = row[col.text]))
        return entity
      })

      return (
        <MetricsTable>
          <Table>
            <tbody>
              <tr>
                {cols.map(col => (
                  <th key={col.text} title={col.helpText}>
                    {col.text}
                  </th>
                ))}
              </tr>
              {metrics.map(entry => (
                <tr key={entry.display_mth}>
                  {Object.keys(entry).map(key => (
                    <td key={key}>{entry[key]}</td>
                  ))}
                </tr>
              ))}
            </tbody>
          </Table>
        </MetricsTable>
      )
    }}
  </Query>
)

const MetricsPage = DashboardBase(Metrics)

export default MetricsPage
