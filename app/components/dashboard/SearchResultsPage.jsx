import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { H2 } from '@pubsweet/ui'
import { Query } from 'react-apollo'
import { pageSize } from 'config'
import {
  MANUSCRIPTS_BY_STATUS,
  MANUSCRIPTS_BY_EMAIL_OR_LASTNAME,
} from './operations'
import { Loading, LoadingIcon, Pagination } from '../ui'
import SearchBoxes from './SearchBoxes'
import DashboardList from './DashboardList'
import DashboardBase from './DashboardBase'

const QueryLabel = styled(H2)`
  float: left;
  max-width: 50%;
  margin: 0 auto calc(${th('gridUnit')} * 2);
  @media screen and (max-width: 880px) {
    float: none;
    max-width: 100%;
  }
`
const PaginationPane = styled.div`
  display: flex;
  justify-content: flex-end;
`

const Dashboard = ({ currentUser, history }) => {
  const params = new URLSearchParams(history.location.search)
  const key = params.has('status') ? 'status' : 'search'
  const query = key === 'status' ? params.get('status') : params.get('search')
  const currentPage = params.get('page') ? parseInt(params.get('page'), 10) : 1
  const page = params.has('page') ? currentPage - 1 : 0

  const onPageEntered = p => {
    history.push(`/search?${key}=${query}&page=${p}`)
  }

  return (
    <Query
      fetchPolicy="cache-and-network"
      query={
        key === 'status'
          ? MANUSCRIPTS_BY_STATUS
          : MANUSCRIPTS_BY_EMAIL_OR_LASTNAME
      }
      variables={{ query, page, pageSize }}
    >
      {({ data, loading }) => {
        if (loading) {
          return (
            <Loading>
              <LoadingIcon />
            </Loading>
          )
        }

        const src =
          key === 'status' ? data.findByStatus : data.searchManuscripts
        const { total, manuscripts } = src

        return (
          <React.Fragment>
            <QueryLabel>
              {query} ({total})
            </QueryLabel>
            <SearchBoxes />
            <div style={{ clear: 'both' }} />
            {total ? (
              <PaginationPane>
                <Pagination
                  currentPage={currentPage}
                  onPageEntered={onPageEntered}
                  pageSize={pageSize}
                  totalSize={total}
                />
              </PaginationPane>
            ) : (
              ''
            )}
            <DashboardList currentUser={currentUser} listData={manuscripts} />
          </React.Fragment>
        )
      }}
    </Query>
  )
}

const SearchResults = DashboardBase(Dashboard)

const SearchResultsPage = ({ currentUser, history }) => (
  <SearchResults
    currentUser={currentUser}
    hideSearchBoxes="true"
    history={history}
    pageTitle="Search results"
  />
)

export default SearchResultsPage
