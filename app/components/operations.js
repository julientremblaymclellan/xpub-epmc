import gql from 'graphql-tag'

export const ManuscriptFragment = gql`
  fragment ManuscriptFragment on Manuscript {
    id
    status
    updated
    formState
    journal {
      journalTitle
      meta {
        nlmta
        pmcStatus
        pubmedStatus
      }
    }
    meta {
      title
      articleIds {
        pubIdType
        id
      }
      publicationDates {
        type
        date
      }
      fundingGroup {
        fundingSource
        awardId
        title
        pi {
          surname
          givenNames
          title
          email
        }
      }
      releaseDelay
      unmatchedJournal
      notes {
        id
        notesType
        content
      }
    }
    files {
      id
      type
      label
      filename
      url
      mimeType
    }
    teams {
      role
      teamMembers {
        user {
          id
        }
        alias {
          name {
            title
            surname
            givenNames
          }
        }
      }
    }
  }
`

export const GET_MANUSCRIPT = gql`
  query GetManuscript($id: ID!) {
    manuscript(id: $id) {
      ...ManuscriptFragment
    }
  }
  ${ManuscriptFragment}
`

export const UPDATE_MANUSCRIPT = gql`
  mutation UpdateManuscript($data: ManuscriptInput!) {
    updateManuscript(data: $data) {
      status
    }
  }
`

export const REJECT_MANUSCRIPT = gql`
  mutation RejectManuscript($data: NewNoteInput!) {
    rejectManuscript(data: $data) {
      manuscript {
        ...ManuscriptFragment
      }
      errors {
        message
      }
    }
  }
  ${ManuscriptFragment}
`

export const CREATE_NOTE = gql`
  mutation CreateNote($data: NewNoteInput!) {
    createNote(data: $data) {
      id
      meta {
        notes {
          id
          notesType
          content
        }
      }
    }
  }
`

export const UPDATE_NOTE = gql`
  mutation UpdateNote($data: NoteInput!) {
    updateNote(data: $data) {
      id
      meta {
        notes {
          id
          notesType
          content
        }
      }
    }
  }
`

export const CHECK_DUPES = gql`
  query($id: ID!, $articleIds: [String], $title: String!) {
    checkDuplicates(id: $id, articleIds: $articleIds, title: $title) {
      manuscripts {
        ...ManuscriptFragment
      }
    }
  }
  ${ManuscriptFragment}
`

export const REPLACE_MANUSCRIPT = gql`
  mutation ReplaceManuscript($keepId: ID!, $throwId: ID!) {
    replaceManuscript(keepId: $keepId, throwId: $throwId)
  }
`

export const DELETE_MANUSCRIPT = gql`
  mutation DeleteManuscript($id: ID!) {
    deleteManuscript(id: $id)
  }
`
