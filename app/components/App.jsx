import React from 'react'
import styled, { createGlobalStyle } from 'styled-components'
import { Query } from 'react-apollo'
import { th } from '@pubsweet/ui-toolkit'
import { GlobalStyle, AppBar, Action, Icon, Link } from '@pubsweet/ui'
import createHistory from 'history/createBrowserHistory'
import { Loading, LoadingIcon } from './ui'
import mainLogo from '../assets/epmcpluslogo.png'
import { CURRENT_USER, GET_USER_TEAMS } from './helpers/AuthorizeGraphQLQueries'
import userHelper from './helpers/userHelper'
import Footer from './Footer'

const Global = createGlobalStyle`
  html,
  body,
  #root,
  #root > div,
  #root > div > div { min-height: 100vh; }
  body {
    font-family: ${th('fontInterface')};
      background-color: ${th('colorBackground')};
  }
  div, p {
    font-size: ${th('fontSizeBase')};
    line-height: ${th('lineHeightBase')};
  }
  *::-webkit-scrollbar {
    width: 8px !important;
    height: 8px !important;
  }
`
const NavSec = styled.div`
  height: calc(${th('gridUnit')} * 12);
  background-color: ${th('colorTextReverse')};
  border-bottom: ${th('borderWidth')} ${th('borderStyle')}
    ${th('colorFurniture')};
  @media screen and (max-width: 500px) {
    height: calc(${th('gridUnit')} * 9);
  }
`
const BrandingSection = styled.span`
  display: flex;
  flex-wrap: no-wrap;
  align-items: center;
  height: 100%;
  padding: 0 calc(${th('gridUnit')} * 2);
  @media screen and (max-width: 375px) {
    width: calc(${th('gridUnit')} * 5);
    overflow: hidden;
    margin-left: calc(${th('gridUnit')} * 4);
  }
`
const MainTitle = styled.img`
  width: 300px;
  @media screen and (max-width: 1300px) {
    width: 240px;
  }
  @media screen and (max-width: 900px) {
    width: 220px;
  }
  @media screen and (max-width: 800px) {
    width: 200px;
  }
`
const Tag = styled.span`
  display: inline-block;
  white-space: nowrap;
  margin-top: calc(-${th('gridUnit')} / 2);
  &:before {
    content: '|';
    display: inline-block;
    padding: 0 ${th('gridUnit')};
  }}
  @media screen and (max-width: 1200px) {
    display: none;
  }
`
const MobileButton = styled(Action)`
  display: none;
  @media screen and (max-width: 900px) {
    display: flex;
    padding: 0 calc(${th('gridUnit')} * 2);
  }
`
const RightSection = styled.div`
  display: flex;
  flex-wrap: no-wrap;
  align-items: center;
  justify-content: flex-end;
  height: 100%;
  padding: 0 calc(${th('gridUnit')} * 2);
  @media screen and (min-width: 1532px) {
    padding-right: 0;
  }
  @media screen and (max-width: 1300px) {
    padding-left: 0;
  }
  @media screen and (min-width: 901px) {
    &.hidden {
      display: flex !important;
    }
  }
  @media screen and (max-width: 900px) {
    flex-direction: column;
    width: 100%;
    height: auto;
    position: absolute;
    top: calc(100% + ${th('borderWidth')});
    z-index: 10;
    padding: 0;
  }
`
const Section = styled.span`
  display: inline-flex;
  white-space: nowrap;
  align-items: center;
  height: 100%;
  padding: 0 calc(${th('gridUnit')} * 2);
  a,
  button {
    display: flex;
    align-items: center;
    white-space: nowrap;
    &:focus {
      box-shadow: none;
      text-decoration: underline;
    }
  }
  & > a,
  & > button {
    font-weight: 600;
  }
  &:last-of-type {
    padding-right: 0;
  }
  @media screen and (max-width: 1100px) {
    padding: 0 calc(${th('gridUnit')} * 1.5);
  }
  @media screen and (max-width: 900px) {
    display: block;
    height: auto;
    background-color: ${th('colorTextReverse')};
    width: 100%;
    padding: 0;
    box-shadow: 0 4px 4px -4px ${th('colorBorder')};
    a,
    button {
      display: block;
      width: 100%;
      padding: calc(${th('gridUnit')} * 2) calc(${th('gridUnit')} * 4);
      border-top: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
      &.current {
        border-bottom: 0;
      }
    }
    & > button {
      display: none;
    }
    .hidden {
      display: block;
    }
  }
`
const Dropdown = styled.div`
  position: absolute;
  top: 70%;
  right: ${th('gridUnit')};
  z-index: 5;
  width: 100%;
  min-width: calc(${th('gridUnit')} * 16);
  box-sizing: border-box;
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  border-top: 0;
  & > * {
    width: 100%;
    height: calc(${th('gridUnit')} * 6);
    padding: ${th('gridUnit')};
    background-color: ${th('colorTextReverse')};
    border-top: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
    &:hover {
      background-color: ${th('colorTextReverse')} !important;
      border-top: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
    }
  }
  @media screen and (max-width: 900px) {
    position: relative;
    top: auto;
    right: auto;
    border-right: 0;
    border-left: 0;
    & > * {
      height: auto;
      font-weight: 600;
    }
  }
`
const Branding = () => (
  <BrandingSection>
    <MainTitle alt="Europe PMC plus" src={mainLogo} />
    <Tag>manuscript submission system</Tag>
  </BrandingSection>
)

const toggleDropdown = sec => {
  sec.classList.toggle('hidden')
}

const logoutUser = () => {
  userHelper.clearCurrentUser()
  createHistory({ forceRefresh: true }).push('/login')
}

const RightComponent = ({ user, loginLink, onLogoutClick }) => (
  <React.Fragment>
    <MobileButton onClick={e => toggleDropdown(e.currentTarget.nextSibling)}>
      <Icon color="currentColor" size={4}>
        menu
      </Icon>
    </MobileButton>
    <RightSection className="hidden">
      {user && (
        <React.Fragment>
          {user.admin && (
            <Section>
              <Link
                className={
                  window.location.pathname === '/admin-dashboard' && 'current'
                }
                to="/admin-dashboard"
              >
                Dashboard
              </Link>
            </Section>
          )}
          <Section>
            <Link
              className={window.location.pathname === '/dashboard' && 'current'}
              to="/dashboard"
            >
              {user.admin ? 'My queue' : 'My manuscripts'}
            </Link>
          </Section>
        </React.Fragment>
      )}
      <Section>
        {user && user.admin ? (
          <Link
            className={
              window.location.pathname === '/manage-users' && 'current'
            }
            to="/manage-users"
          >
            Manage Users
          </Link>
        ) : (
          <Link
            className={window.location.pathname === '/user-guide' && 'current'}
            to="/user-guide"
          >
            User Guide
          </Link>
        )}
      </Section>
      {user ? (
        <Section
          onClick={e => toggleDropdown(e.currentTarget.lastChild)}
          onMouseEnter={e =>
            e.currentTarget.lastChild.classList.remove('hidden')
          }
          onMouseLeave={e => e.currentTarget.lastChild.classList.add('hidden')}
          style={{ position: 'relative' }}
        >
          <Action>
            <Icon color="currentColor" size={3}>
              user
            </Icon>
            <span>{user.identities[0].name.givenNames}</span>
            <Icon color="currentColor" size={3}>
              chevron-down
            </Icon>
          </Action>
          <Dropdown className="hidden">
            <Link to="/my-account">My account</Link>
            <Action onClick={onLogoutClick}>Logout</Action>
          </Dropdown>
        </Section>
      ) : (
        <Section>
          <Action to={loginLink}>
            <Icon color="currentColor" size={3}>
              log-in
            </Icon>
            <span>Login</span>
          </Action>
        </Section>
      )}
    </RightSection>
  </React.Fragment>
)

export const UserContext = React.createContext({
  currentUser: {},
  setCurrentUser: () => {},
})

const App = ({ children, journal, history, match }) => (
  <Query fetchPolicy="cache-and-network" query={CURRENT_USER}>
    {({ data, loading }) => {
      if (loading) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }
      const currentUser = data ? data.epmc_currentUser : undefined
      const userId = currentUser ? currentUser.id : undefined
      return (
        <Query
          fetchPolicy="cache-and-network"
          query={GET_USER_TEAMS}
          variables={{ userId }}
        >
          {({ data, loading }) => {
            if (loading) {
              return (
                <Loading>
                  <LoadingIcon />
                </Loading>
              )
            }

            const userTeams = data ? data.userTeams : []
            if (userTeams.some(t => t.role === 'admin')) {
              currentUser.admin = true
            } else if (userTeams.some(t => t.role === 'external-admin')) {
              currentUser.external = true
            }

            return (
              <UserContext.Provider value={currentUser}>
                <React.Fragment>
                  <GlobalStyle />
                  <Global />
                  <NavSec>
                    <AppBar
                      brand={Branding()}
                      onLogoutClick={logoutUser}
                      rightComponent={RightComponent}
                      user={currentUser}
                    />
                  </NavSec>
                  {children}
                  <Footer />
                </React.Fragment>
              </UserContext.Provider>
            )
          }}
        </Query>
      )
    }}
  </Query>
)

export default App
