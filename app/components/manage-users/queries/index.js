import gql from 'graphql-tag'

const USERS_BY_NAME = gql`
  query($name: String!) {
    usersByName(name: $name) {
      id
      identities {
        type
        email
        name {
          title
          givenNames
          surname
        }
      }
    }
  }
`
const USERS_BY_EMAIL = gql`
  query($email: String!) {
    usersByEmail(email: $email) {
      id
      identities {
        type
        email
        name {
          title
          givenNames
          surname
        }
      }
    }
  }
`

module.exports = {
  USERS_BY_NAME,
  USERS_BY_EMAIL,
}
