import React from 'react'
import { Field } from 'formik'
import { ErrorText, H1, Link, Button, Checkbox, TextField } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import { Page, Notification } from '../ui'
import SignInFooter from '../SignInFooter'
import UserCore from '../UserCore'

const SignIn = styled.p``

const Notice = styled.p`
  margin: ${th('gridUnit')} 0 0;
  text-indent: calc(3 * ${th('gridUnit')});
`
const ShowPassword = styled(Checkbox)`
  position: absolute;
  right: 0;
  top: 0;
  font-size: ${th('fontSizeBaseSmall')};
`
const PasswordDetails = styled.div`
  display: flex;
  align-items: center;
  width: 50%;
  max-width: 100%;
  @media screen and (max-width: 1000px) {
    width: 600px;
  @media screen and (max-width: 800px) {
    display: block;
    width: 408px;
  }
`
const PasswordField = styled.div`
  position: relative;
  width: 308px;
  max-width: 100%;
  margin-right: calc(${th('gridUnit')} * 1.5);
  @media screen and (max-width: 1000px) {
    width: 408px;
  }
  @media screen and (max-width: 800px) {
    width: 100%;
  }
`
const PasswordRules = styled.ul`
  font-size: ${th('fontSizeBaseSmall')};
  padding-left: calc(${th('gridUnit')} * 4);
  @media screen and (max-width: 800px) {
    display: flex;
    justify-content: space-between;
    margin: calc(-${th('gridUnit')} * 3) 0 calc(${th('gridUnit')} * 3);
    padding: 0 ${th('gridUnit')};
    li {
      margin-left: calc(${th('gridUnit')} * 2);
    }
  }
`

class Signup extends React.Component {
  state = {
    accept: false,
    showPassword: false,
    isValidLength: false,
  }
  setButtonRef = button => {
    this.button = button
  }
  toggleAccept = () => {
    const { accept, showPassword } = this.state
    this.setState({
      accept: !accept,
      showPassword,
    })
  }
  toggleShowPassword = () => {
    const { showPassword } = this.state
    this.setState({
      showPassword: !showPassword,
    })
  }
  validatePassword = e => {
    if (!e.target.value) {
      return
    }
    const isValidLength = e.target.value.length >= 8
    this.setState({
      isValidLength,
    })
  }
  PasswordInput = props => (
    <TextField
      invalidTest={props.invalidTest}
      label="Password"
      {...props.field}
      onBlur={e => this.validatePassword(e)}
      onKeyUp={e => this.validatePassword(e)}
      type="password"
    />
  )
  PlainPasswordInput = props => (
    <TextField
      invalidTest={props.invalidTest}
      label="Password"
      onBlur={e => this.validatePassword(e)}
      onKeyUp={e => this.validatePassword(e)}
      {...props.field}
    />
  )
  render() {
    const { values, errors, handleSubmit, touched, location } = this.props
    return (
      <Page>
        <H1>Create a Europe PMC plus account</H1>

        {values.success && (
          <Notification type="success"> {values.success} </Notification>
        )}
        {values.error && (
          <Notification type="error">{values.error}</Notification>
        )}
        <form onSubmit={handleSubmit}>
          <UserCore {...this.props}>
            <PasswordDetails>
              <PasswordField>
                {!this.state.showPassword && (
                  <Field component={this.PasswordInput} name="password" />
                )}

                {this.state.showPassword && (
                  <Field component={this.PlainPasswordInput} name="password" />
                )}
                <ShowPassword
                  checked={this.state.showPassword}
                  label="Show password"
                  onChange={() => this.toggleShowPassword()}
                />
                {errors.password && touched.password && (
                  <ErrorText>{errors.password}</ErrorText>
                )}
              </PasswordField>
              <PasswordRules>
                <li style={{ opacity: this.state.isValidLength ? 1 : 0.5 }}>
                  8 characters
                </li>
              </PasswordRules>
            </PasswordDetails>
          </UserCore>
          <div className="accept">
            <Checkbox
              checked={this.state.accept}
              label="I have read and accept the privacy notice."
              onChange={() => this.toggleAccept()}
            />
            <Notice>
              {`See `}
              <Link
                target="_blank"
                to="//www.ebi.ac.uk/data-protection/privacy-notice/europe-pmc-plus"
              >
                Privacy notice
              </Link>
              {` for Europe PMC’s Advanced User Services.`}
            </Notice>
          </div>
          <p>
            <Button
              disabled={!this.state.accept}
              primary
              ref={this.setButtonRef}
              type="submit"
            >
              Create account
            </Button>
          </p>
        </form>
        <SignIn>
          {`Already have a Europe PMC plus account? `}
          <Link to={`/login${location.search}`}>Sign in.</Link>
        </SignIn>
        <SignInFooter />
      </Page>
    )
  }
}

export default Signup
