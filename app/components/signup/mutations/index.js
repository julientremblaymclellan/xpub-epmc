import gql from 'graphql-tag'

const SIGNUP_USER = gql`
  mutation($input: SignupUserInput) {
    epmc_signupUser(input: $input) {
      user {
        email
      }
      token
    }
  }
`

module.exports = {
  SIGNUP_USER,
}
