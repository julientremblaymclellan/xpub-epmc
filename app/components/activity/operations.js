import gql from 'graphql-tag'
import { ManuscriptFragment } from '../operations'

export const GET_USER = gql`
  query GetUser($id: ID!) {
    user(id: $id) {
      id
      identities {
        ... on Local {
          name {
            givenNames
            surname
          }
        }
      }
    }
  }
`

export const GET_JOURNAL = gql`
  query($id: ID!) {
    journal(id: $id) {
      id
      journalTitle
    }
  }
`

export const QUERY_ACTIVITY_INFO = gql`
  query QueryActivitiesByManuscriptId($id: ID!) {
    activities: epmc_queryActivitiesByManuscriptId(id: $id) {
      ...ManuscriptFragment
      meta {
        volume
        issue
        location {
          fpage
          lpage
          elocationId
        }
      }
      claiming {
        givenNames
        surname
      }
      audits {
        id
        created
        updated
        user {
          id
          title
          givenNames
          surname
          identities {
            email
          }
        }
        action
        originalData
        changes
        objectType
        objectId
        manuscriptId
      }
    }
  }
  ${ManuscriptFragment}
`
