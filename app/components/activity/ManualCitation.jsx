import React from 'react'
import { Field, withFormik } from 'formik'
import * as yup from 'yup'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, TextField, Action, H3, H4, ErrorText } from '@pubsweet/ui'
import { Buttons, Toggle } from '../ui'

const Flex = styled.div`
  display: flex;
  align-items: flex-end;
  min-width: 0;
  &.hidden {
    display: none;
  }
  & > div {
    flex: 1 1 50%;
    min-width: 0;
    margin-left: ${th('gridUnit')};
    margin-right: ${th('gridUnit')};
    &:first-child {
      margin-left: 0;
      & ~ * {
        margin-right: 0;
        & + * {
          margin-left: 0;
        }
      }
    }
  }
`
const SmallToggle = styled(Toggle)`
  button {
    font-size: ${th('fontSizeBaseSmall')};
  }
  margin-bottom: ${th('gridUnit')};
`

const Title = props => (
  <TextField invalidTest={props.invalidTest} label="Title" {...props.field} />
)
const Volume = props => <TextField label="Volume" {...props.field} />
const Issue = props => <TextField label="Issue" {...props.field} />
const FPage = props => (
  <TextField className={props.className} label="First page" {...props.field} />
)
const LPage = props => (
  <TextField className={props.className} label="Last page" {...props.field} />
)
const Elocation = props => (
  <TextField className={props.className} label="eLocation" {...props.field} />
)
const DOI = props => <TextField label="DOI" {...props.field} />
const PYear = props => (
  <TextField invalidTest={props.invalidTest} label="Year" {...props.field} />
)
const PMonth = props => (
  <TextField invalidTest={props.invalidTest} label="Month" {...props.field} />
)
const PDay = props => (
  <TextField invalidTest={props.invalidTest} label="Day" {...props.field} />
)
const PSeason = props => (
  <TextField className={props.className} label="Season" {...props.field} />
)
const EYear = props => <TextField label="Year" {...props.field} />
const EMonth = props => (
  <TextField invalidTest={props.invalidTest} label="Month" {...props.field} />
)
const EDay = props => (
  <TextField invalidTest={props.invalidTest} label="Day" {...props.field} />
)
const ESeason = props => (
  <TextField className={props.className} label="Season" {...props.field} />
)

const toggleSwitch = button => {
  if (!button.classList.contains('current')) {
    const buttons = Array.from(button.parentNode.children)
    buttons.forEach(element => {
      element.classList.toggle('current')
    })
    const things = Array.from(button.parentNode.nextElementSibling.children)
    things.forEach(element => {
      element.classList.toggle('hidden')
    })
  }
}

const CitationForm = ({ values, errors, touched, ...props }) => (
  <form onSubmit={props.handleSubmit}>
    <Flex>
      <div>
        <H3>Enter citation</H3>
        <Flex>
          <Field component={Volume} name="volume" />
          <Field component={Issue} name="issue" />
        </Flex>
      </div>
      <div>
        <SmallToggle>
          <Action
            className={(!values.elocationId || values.fpage) && 'current'}
            onClick={e => toggleSwitch(e.currentTarget)}
          >
            Pages
          </Action>
          <Action
            className={!values.fpage && values.elocationId && 'current'}
            onClick={e => toggleSwitch(e.currentTarget)}
          >
            Elocation
          </Action>
        </SmallToggle>
        <Flex>
          <Field
            className={!values.fpage && values.elocationId && 'hidden'}
            component={FPage}
            name="fpage"
          />
          <Field
            className={!values.fpage && values.elocationId && 'hidden'}
            component={LPage}
            name="lpage"
          />
          <Field
            className={(!values.elocationId || values.fpage) && 'hidden'}
            component={Elocation}
            name="elocationId"
          />
        </Flex>
      </div>
    </Flex>
    <Field component={DOI} name="doi" />
    <div>
      <Field
        component={Title}
        invalidTest={errors.title && touched.title}
        name="title"
      />
      {errors.title && touched.title && <ErrorText>{errors.title}</ErrorText>}
    </div>
    <Flex>
      <div>
        <H4>Print date</H4>
        <Field component={PYear} name="printyear" />
      </div>
      <div>
        <SmallToggle>
          <Action
            className={!values.printseason && 'current'}
            onClick={e => toggleSwitch(e.currentTarget)}
          >
            Month
          </Action>
          <Action
            className={values.printseason && 'current'}
            onClick={e => toggleSwitch(e.currentTarget)}
          >
            Season
          </Action>
        </SmallToggle>
        <Flex>
          <div className={values.printseason && 'hidden'}>
            <Field
              component={PMonth}
              invalidTest={errors.printmonth && touched.printmonth}
              name="printmonth"
            />
            {errors.printmonth && touched.printmonth && (
              <ErrorText>{errors.printmonth}</ErrorText>
            )}
          </div>
          <div className={values.printseason && 'hidden'}>
            <Field
              component={PDay}
              invalidTest={errors.printday && touched.printday}
              name="printday"
            />
            {errors.printday && touched.printday && (
              <ErrorText>{errors.printday}</ErrorText>
            )}
          </div>
          <Field
            className={!values.printseason && 'hidden'}
            component={PSeason}
            name="printseason"
          />
        </Flex>
      </div>
    </Flex>
    <Flex>
      <div>
        <H4>Electronic date</H4>
        <Field component={EYear} name="electronicyear" />
      </div>
      <div>
        <SmallToggle>
          <Action
            className={!values.electronicseason && 'current'}
            onClick={e => toggleSwitch(e.currentTarget)}
          >
            Month
          </Action>
          <Action
            className={values.electronicseason && 'current'}
            onClick={e => toggleSwitch(e.currentTarget)}
          >
            Season
          </Action>
        </SmallToggle>
        <Flex>
          <div className={values.electronicseason && 'hidden'}>
            <Field
              component={EMonth}
              invalidTest={errors.electronicmonth && touched.electronicmonth}
              name="electronicmonth"
            />
            {errors.electronicmonth && touched.electronicmonth && (
              <ErrorText>{errors.electronicmonth}</ErrorText>
            )}
          </div>
          <div className={values.electronicseason && 'hidden'}>
            <Field
              component={EDay}
              invalidTest={errors.electronicday && touched.electronicday}
              name="electronicday"
            />
            {errors.electronicday && touched.electronicday && (
              <ErrorText>{errors.electronicday}</ErrorText>
            )}
          </div>
          <Field
            className={!values.electronicseason && 'hidden'}
            component={ESeason}
            name="electronicseason"
          />
        </Flex>
      </div>
    </Flex>
    <Buttons right>
      <Button primary type="submit">
        Save
      </Button>
      <Button onClick={props.close}>Exit</Button>
    </Buttons>
  </form>
)

const handleSubmit = async (
  values,
  { props, setSubmitting, setErrors, resetForm, setStatus },
) => {
  const { meta: prevMeta, citationData } = props
  const { articleIds } = prevMeta
  const meta = {
    title: values.title,
    volume: values.volume,
    issue: values.issue,
    location: {
      fpage: values.fpage,
      lpage: values.fpage,
      elocationId: values.elocationId,
    },
    publicationDates: [],
  }
  if (values.printyear) {
    const ppub = {
      type: 'ppub',
      jatsDate: {
        year: values.printyear,
      },
    }
    if (values.printseason) {
      ppub.jatsDate.season = values.printseason
    } else {
      ppub.jatsDate.month = values.printmonth
      ppub.jatsDate.day = values.printday
    }
    meta.publicationDates.push(ppub)
  }
  if (values.electronicyear) {
    const epub = {
      type: 'epub',
      jatsDate: {
        year: values.electronicyear,
      },
    }
    if (values.electronicseason) {
      epub.jatsDate.season = values.electronicseason
    } else {
      epub.jatsDate.month = values.electronicmonth
      epub.jatsDate.day = values.electronicday
    }
    meta.publicationDates.push(epub)
  }
  meta.articleIds = articleIds.filter(
    aid => !['doi', 'pmid'].includes(aid.pubIdType),
  )
  if (values.doi) {
    meta.articleIds.push({
      pubIdType: 'doi',
      id: values.doi,
    })
  }
  citationData({ meta })
  resetForm()
  props.close()
}

const enhancedFormik = withFormik({
  initialValues: {
    title: '',
    volume: '',
    issue: '',
    fpage: '',
    lpage: '',
    elocationId: '',
    doi: '',
    printyear: '',
    printmonth: '',
    printday: '',
    printseason: '',
    electronicyear: '',
    electronicmonth: '',
    electronicday: '',
    electronicseason: '',
  },
  mapPropsToValues: props => {
    const { meta } = props
    const {
      articleIds,
      volume,
      issue,
      location,
      publicationDates,
      title,
    } = meta
    const doi =
      (articleIds && articleIds.find(aid => aid.pubIdType === 'doi')) || ''
    const ppub =
      publicationDates && publicationDates.find(d => d.type === 'ppub')
    const epub =
      publicationDates && publicationDates.find(d => d.type === 'epub')
    const p = (ppub && ppub.jatsDate) || null
    const e = (epub && epub.jatsDate) || null
    return {
      title,
      volume: volume || '',
      issue: issue || '',
      fpage: (location && location.fpage) || '',
      lpage: (location && location.lpage) || '',
      elocationId: (location && location.elocationId) || '',
      doi: doi.id || '',
      printyear: (p && p.year) || '',
      printmonth: (p && p.month) || '',
      printday: (p && p.day) || '',
      printseason: (p && p.season) || '',
      electronicyear: (e && e.year) || '',
      electronicmonth: (e && e.month) || '',
      electronicday: (e && e.day) || '',
      electronicseason: (e && e.season) || '',
    }
  },
  validationSchema: yup.object().shape({
    title: yup.string().required('Title is required'),
    volume: yup.string(),
    issue: yup.string(),
    fpage: yup.string(),
    lpage: yup.string(),
    elocationId: yup.string(),
    doi: yup.string(),
    printyear: yup.date('Invalid year').min('2000', 'Invalid year'),
    printmonth: yup
      .number()
      .integer()
      .min(1, 'Invalid month')
      .max(12, 'Invalid month'),
    printday: yup
      .number()
      .integer()
      .min(1, 'Invalid day')
      .max(31, 'Invalid day'),
    printseason: yup.string(),
    electronicyear: yup.date('Invalid year').min('2000', 'Invalid year'),
    electronicmonth: yup
      .number()
      .integer()
      .min(1, 'Invalid month')
      .max(12, 'Invalid month'),
    electronicday: yup
      .number()
      .integer()
      .min(1, 'Invalid day')
      .max(31, 'Invalid day'),
    electronicseason: yup.string(),
  }),
  displayName: 'manual-citation',
  handleSubmit,
})(CitationForm)

export default enhancedFormik
