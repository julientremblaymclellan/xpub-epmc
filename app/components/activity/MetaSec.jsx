import React from 'react'
import { Query } from 'react-apollo'
import { Icon, Action, Button, H2 } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import moment from 'moment'
import { CHECK_DUPES } from '../operations'
import MetaEdit from './MetaEdit'

import { QUERY_ACTIVITY_INFO } from './operations'

const Heading = styled(H2)`
  border-bottom: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  padding-bottom: ${th('gridUnit')};
`
const Container = styled.div`
  margin: 0 0 calc(${th('gridUnit')} * 2);
  column-count: 3;
  @media screen and (max-width: 1000px) {
    column-count: 2;
  }
  @media screen and (max-width: 600px) {
    column-count: 1;
  }
`
const DL = styled.dl`
  margin: 0;
  dt {
    font-weight: 600;
    margin-right: ${th('gridUnit')};
    display: inline;
  }
  dd {
    margin: 0;
    display: inline;
  }
`
const DupeButton = styled(Button)`
  padding: calc(${th('gridUnit')} / 2) ${th('gridUnit')};
  font-size: ${th('fontSizeBaseSmall')};
`
const EditIcon = () => (
  <Icon color="currentColor" size={1.75}>
    edit-2
  </Icon>
)

class MetaSec extends React.Component {
  state = { edit: null }
  render() {
    const { manuscript } = this.props
    const { teams, audits, meta, journal } = manuscript
    const {
      fundingGroup,
      releaseDelay,
      articleIds,
      volume,
      issue,
      location,
      publicationDates,
      title,
      notes,
    } = meta
    let duplicates = []
    const pmid = articleIds && articleIds.find(aid => aid.pubIdType === 'pmid')
    const submitter = teams.find(team => team.role === 'submitter')
    const { title: st, givenNames: sg, surname: ss } = submitter
      ? submitter.teamMembers[0].alias.name
      : ''
    const reviewer = teams.find(team => team.role === 'reviewer')
    const { title: rt, givenNames: rg, surname: rs } = reviewer
      ? reviewer.teamMembers[0].alias.name
      : ''
    const auditInfo = audits.reduce((info, audit) => {
      if (!info.timeInProcess) {
        info.timeInProcess = moment(audit.created).fromNow(true)
      }
      info.lastActivity = moment(audit.created).fromNow(true)
      return info
    }, {})
    const year =
      (publicationDates &&
        publicationDates.find(d => d.type === 'ppub') &&
        (publicationDates.find(d => d.type === 'ppub').jatsDate
          ? publicationDates.find(d => d.type === 'ppub').jatsDate.year
          : moment(
              publicationDates.find(d => d.type === 'ppub').date,
            ).year())) ||
      null
    const { edit } = this.state
    return (
      <React.Fragment>
        <DL style={{ float: 'right', marginTop: '1rem' }}>
          <dt>Status:</dt>
          <dd>
            {manuscript.status}
            <Action onClick={() => this.setState({ edit: 'status' })}>
              <Icon color="currentColor" size={1.75}>
                send
              </Icon>
            </Action>
          </dd>
        </DL>
        <Heading>Quick view</Heading>
        <Container>
          {reviewer &&
          reviewer.teamMembers[0].user.id ===
            submitter.teamMembers[0].user.id ? (
            <DL>
              <dt>Submitter/Reviewer:</dt>
              <dd>{`${st ? `${st} ` : ''}${sg} ${ss}`}</dd>
            </DL>
          ) : (
            <React.Fragment>
              <DL>
                <dt>Submitter:</dt>
                <dd>{`${st ? `${st} ` : ''}${sg} ${ss}`}</dd>
              </DL>
              {reviewer && (
                <DL>
                  <dt>Reviewer:</dt>
                  <dd>{`${rt ? `${rt} ` : ''}${rg} ${rs}`}</dd>
                </DL>
              )}
            </React.Fragment>
          )}
          <DL>
            <dt>Time in process:</dt>
            <dd>{auditInfo.timeInProcess || 'N/A'}</dd>
          </DL>
          <DL>
            <dt>Last activity:</dt>
            <dd>{`${auditInfo.lastActivity} ago` || 'N/A'}</dd>
          </DL>
          {fundingGroup && (
            <DL>
              <dt>Grants:</dt>
              <dd>
                {fundingGroup.map((f, t) => (
                  <span key={f.awardId}>
                    {`${f.fundingSource} ${f.awardId}`}
                    {t !== fundingGroup.length - 1 && ', '}
                  </span>
                ))}
                <Action onClick={() => this.setState({ edit: 'grants' })}>
                  <EditIcon />
                </Action>
              </dd>
            </DL>
          )}
          {releaseDelay && (
            <DL>
              <dt>Embargo:</dt>
              <dd>
                {releaseDelay} month{releaseDelay !== '1' && 's'}
                <Action onClick={() => this.setState({ edit: 'embargo' })}>
                  <EditIcon />
                </Action>
              </dd>
            </DL>
          )}
          <DL>
            <dt>Citation:</dt>
            <dd>
              {(journal && journal.meta.nlmta) || 'Unmatched'}
              {year && `. ${year}`}
              {volume && `, ${volume}`}
              {issue && `(${issue})`}
              {location && location.fpage ? (
                <React.Fragment>
                  {`:${location.fpage}`}
                  {location.lpage && `-${location.lpage}`}.
                </React.Fragment>
              ) : (
                <React.Fragment>
                  {location &&
                    location.elocationId &&
                    `:${location.elocationId}.`}
                </React.Fragment>
              )}
              {pmid && ` PMID: ${pmid.id}`}
              <Action onClick={() => this.setState({ edit: 'citation' })}>
                <EditIcon />
              </Action>
            </dd>
          </DL>
          <Query
            fetchPolicy="cache-and-network"
            query={CHECK_DUPES}
            variables={{
              id: manuscript.id,
              articleIds: articleIds ? articleIds.map(aid => aid.id) : null,
              title,
            }}
          >
            {({ data, loading }) => {
              if (loading || !data || !data.checkDuplicates) {
                return '...'
              }
              const { manuscripts } = data.checkDuplicates
              const dupeNote = notes
                ? notes.find(n => n.notesType === 'notDuplicates')
                : null
              const notDupes = dupeNote ? JSON.parse(dupeNote.content) : []
              duplicates = manuscripts.filter(m => !notDupes.includes(m.id))
              return (
                <React.Fragment>
                  {duplicates && duplicates.length > 0 && (
                    <DupeButton
                      onClick={() => this.setState({ edit: 'dupes' })}
                      primary
                    >
                      Resolve duplicates
                    </DupeButton>
                  )}
                  {edit && (
                    <MetaEdit
                      close={() => this.setState({ edit: null })}
                      duplicates={duplicates}
                      manuscript={manuscript}
                      refetch={[
                        {
                          query: QUERY_ACTIVITY_INFO,
                          variables: { id: manuscript.id },
                        },
                      ]}
                      toEdit={edit}
                    />
                  )}
                </React.Fragment>
              )
            }}
          </Query>
        </Container>
      </React.Fragment>
    )
  }
}

export default MetaSec
