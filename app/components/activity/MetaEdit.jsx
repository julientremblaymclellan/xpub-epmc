import React from 'react'
import { omit } from 'lodash'
import styled, { withTheme } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Action, H2, Button, Icon, TextField } from '@pubsweet/ui'
import { states } from 'config'
import { Portal, Buttons, Close, CloseButton, Notification } from '../ui'
import { ManuscriptMutations, NoteMutations } from '../SubmissionMutations'
import ResolveDuplicates from '../ResolveDuplicates'
import GrantSearch from '../GrantSearch'
import CitationEdit, { Exit } from './CitationEdit'

const RouteButton = styled(Button)`
  align-items: center;
  display: inline-flex;
`
const FlexP = styled.p`
  display: flex;
  align-items: center;
  & > button {
    flex: 0 0 auto;
  }
  & > span {
    flex: 1 1 50%;
    margin-left: ${th('gridUnit')};
  }
`
class EmbargoEdit extends React.Component {
  state = {
    embargo: this.props.embargo,
    error: false,
  }
  render() {
    return (
      <React.Fragment>
        <TextField
          label="Enter embargo in months"
          name="embargo"
          onChange={e => this.setState({ embargo: e.target.value })}
          value={this.state.embargo}
        />
        {this.state.error && (
          <Notification type="error">
            Input must be a whole number, in digits
          </Notification>
        )}
        <Buttons right>
          <Button
            onClick={() => {
              if (this.state.embargo % 1 !== 0) {
                this.setState({ error: true })
              } else {
                this.props.change(this.state.embargo)
                this.props.close()
              }
            }}
            primary
          >
            Save
          </Button>
          <Exit close={this.props.close} />
        </Buttons>
      </React.Fragment>
    )
  }
}

const DuplicatesWithMutations = NoteMutations(ResolveDuplicates)

const MetaEdit = withTheme(
  ({ theme, close, manuscript, duplicates, toEdit, ...props }) => {
    const { meta } = manuscript
    const { fundingGroup: grants, releaseDelay, notes } = meta
    const fundingGroup = grants
      ? grants.map(g => {
          const n = omit(g, '__typename')
          n.pi = omit(g.pi, '__typename')
          return n
        })
      : []
    const qa = states.indexOf('submitted')
    const xml = states.indexOf('xml-triage')
    const done = states.indexOf('ncbi-ready')
    const curr = states.indexOf(manuscript.status)
    if (toEdit === 'dupes' && duplicates) {
      return (
        <DuplicatesWithMutations
          close={close}
          duplicates={duplicates}
          manuscript={manuscript}
          note={notes ? notes.find(n => n.notesType === 'notDuplicates') : null}
        />
      )
    }
    return (
      <Portal style={{ backgroundColor: theme.colorBackground }} transparent>
        <Close>
          <CloseButton onClick={close} />
        </Close>
        <React.Fragment>
          {(() => {
            switch (toEdit) {
              case 'grants':
                return (
                  <React.Fragment>
                    <GrantSearch
                      changedGrants={props.updateGrants}
                      hideEmbargo
                      selectedGrants={fundingGroup}
                    />
                    <Exit close={close} />
                  </React.Fragment>
                )
              case 'embargo':
                return (
                  <EmbargoEdit
                    change={props.updateEmbargo}
                    close={close}
                    embargo={releaseDelay}
                  />
                )
              case 'citation':
                return (
                  <CitationEdit
                    change={props.changeCitation}
                    close={close}
                    manuscript={manuscript}
                  />
                )
              case 'status':
                return (
                  <React.Fragment>
                    <H2>Send/Remove</H2>
                    <FlexP>
                      <RouteButton
                        disabled={!(curr > qa && curr !== xml)}
                        onClick={() =>
                          props.setStatus(
                            curr > xml ? 'xml-triage' : 'submitted',
                            close,
                          )
                        }
                      >
                        <Icon color="currentColor" size={2.5}>
                          send
                        </Icon>
                        Send back
                      </RouteButton>
                      {curr > qa && curr !== xml ? (
                        <span>
                          {` to most recent editing status:`}
                          {curr > xml ? ' xml-triage' : ' submitted'}
                        </span>
                      ) : (
                        <span>
                          {` The submission is already in an editing state (`}
                          {manuscript.status}
                          {`). You may send it backward or forward from the manuscript view.`}
                        </span>
                      )}
                    </FlexP>
                    <FlexP>
                      <RouteButton
                        disabled={curr >= done}
                        onClick={e =>
                          e.currentTarget.nextElementSibling.classList.remove(
                            'hidden',
                          )
                        }
                      >
                        <Icon color="currentColor" size={2.5}>
                          trash-2
                        </Icon>
                        Delete manuscript
                      </RouteButton>
                      <span className="hidden">
                        {` Are you certain? `}
                        <Action
                          onClick={e => {
                            const sec =
                              e.currentTarget.parentElement.parentElement
                                .nextElementSibling
                            props.deleteMan(() =>
                              sec.classList.remove('hidden'),
                            )
                          }}
                        >
                          Confirm deletion
                        </Action>
                      </span>
                      {curr >= done &&
                        ' Manuscript has been sent to PMC and must be withdrawn.'}
                    </FlexP>
                    <Notification className="hidden" type="info">
                      {`Manuscript succesfully deleted. If not already sent, please send a message to the user(s) explaining the deletion.`}
                    </Notification>
                    <Exit close={close} />
                  </React.Fragment>
                )
              default:
                return null
            }
          })()}
        </React.Fragment>
      </Portal>
    )
  },
)

export default ManuscriptMutations(MetaEdit)
