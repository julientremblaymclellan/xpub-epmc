import React from 'react'
import styled, { withTheme } from 'styled-components'
import { Action, Button, H2, H3, Icon } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { Buttons, Toggle } from '../ui'
import PubMedSearch, { JournalSearch } from '../citation-search'
import ManualCitation from './ManualCitation'

const JournalCheck = styled.div`
  display: flex;
  align-items: start;
  & > div:first-child {
    flex: 9;
    padding-right: ${th('gridUnit')};
    & + div {
      flex: 1;
      text-align: center;
    }
  }
`
const CitationDiv = styled.div`
  max-width: 100%;
  box-sizing: border-box;
  display: flex;
  alogn-items: start;
  & > div:first-child {
    flex: 2;
    min-width: 0;
  }
`
const UserCitation = styled.div`
  flex: 1;
  min-width: 0;
  background-color: ${th('colorBackgroundHue')};
  margin-left: calc(${th('gridUnit')} * 2);
  padding: 0 ${th('gridUnit')} ${th('gridUnit')};
`
const Small = styled.span`
  display: block;
  font-size: ${th('fontSizeBaseSmall')}
  font-weight: 600;
  & + p {
    margin: ${th('gridUnit')} auto;
  }
`
const IndexedIcon = withTheme(({ theme, indexed }) => (
  <Icon color={indexed ? theme.colorSuccess : theme.colorError}>
    {indexed ? 'check' : 'x'}
  </Icon>
))

export const Exit = ({ close }) => (
  <Buttons right>
    <Button onClick={close}>Exit</Button>
  </Buttons>
)

class CitationEdit extends React.Component {
  state = {
    show: 'search',
  }
  componentDidMount() {
    const { manuscript } = this.props
    const { journal } = manuscript
    const newStatus = {
      show: journal.meta.pubmedStatus ? 'search' : 'enter',
    }
    this.setState(newStatus)
  }
  componentDidUpdate(prevProps) {
    const oldStatus = prevProps.manuscript.journal.meta.pubmedStatus
    const newStatus = this.props.manuscript.journal.meta.pubmedStatus
    if (oldStatus !== newStatus) {
      ;(() => this.setState({ show: newStatus ? 'search' : 'enter' }))()
    }
  }
  render() {
    const { manuscript, change, close } = this.props
    const { journal, meta } = manuscript
    const { notes } = meta
    const note =
      (notes && notes.find(n => n.notesType === 'userCitation')) || null
    const { show } = this.state
    return (
      <React.Fragment>
        <H2>Journal</H2>
        <JournalCheck>
          <div>
            <JournalSearch
              journal={journal}
              setJournal={e => change({ journalId: e.id })}
            />
          </div>
          <div>
            <Small>Indexed?</Small>
            <p>
              <IndexedIcon indexed={journal.meta.pubmedStatus} />
            </p>
          </div>
        </JournalCheck>
        <H2>Edit citation</H2>
        <Toggle>
          <Action
            className={show === 'search' && 'current'}
            onClick={() => this.setState({ show: 'search' })}
          >
            Search
          </Action>
          <Action
            className={show === 'enter' && 'current'}
            onClick={() => this.setState({ show: 'enter' })}
          >
            Enter manually
          </Action>
        </Toggle>
        {show === 'search' ? (
          <React.Fragment>
            <H3>Citation search</H3>
            <PubMedSearch
              citationData={e => {
                change(e)
                close()
              }}
            />
            <Exit close={close} />
          </React.Fragment>
        ) : (
          <CitationDiv>
            <div>
              <ManualCitation citationData={change} close={close} meta={meta} />
            </div>
            {note && (
              <UserCitation>
                <H3>User citation</H3>
              </UserCitation>
            )}
          </CitationDiv>
        )}
      </React.Fragment>
    )
  }
}

export default CitationEdit
