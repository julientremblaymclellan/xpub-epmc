import React from 'react'
import { withRouter } from 'react-router'
import { Query } from 'react-apollo'
import { H1 } from '@pubsweet/ui'
import { Page, Loading, LoadingIcon } from '../ui'
import { QUERY_ACTIVITY_INFO } from './operations'
import SubmissionHeader from '../SubmissionHeader'
import ActivityDetails from './ActivityDetails'
import QuickView from './QuickView'

const ActivityPage = props => (
  <Page>
    <H1>Manuscript activity</H1>
    <QuickView manuscript={props.manuscript} />
    <ActivityDetails {...props} />
  </Page>
)

const ActivityPageWithHeader = SubmissionHeader(ActivityPage)

const ActivityPageContainer = ({ currentUser, history, match, ...props }) => (
  <Query query={QUERY_ACTIVITY_INFO} variables={{ id: match.params.id }}>
    {({ data, loading }) => {
      if (loading || !data || !data.activities) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }
      if (currentUser.admin) {
        return (
          <ActivityPageWithHeader
            currentUser={currentUser}
            manuscript={data.activities}
            {...props}
          />
        )
      }
      history.push('/')
      return null
    }}
  </Query>
)

export default withRouter(ActivityPageContainer)
