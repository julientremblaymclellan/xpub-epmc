import React from 'react'
import { Icon } from '@pubsweet/ui'
import { th, darken } from '@pubsweet/ui-toolkit'
import styled, { withTheme } from 'styled-components'
import { states } from 'config'
import MetaSec from './MetaSec'

const Checklist = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  border-right: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  @media screen and (max-width: 970px) {
    flex-wrap: wrap;
  }
`
const Col = styled.div`
  flex: ${props => props.flex};
  display: flex;
  flex-direction: column;
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  border-right: 0;

  & > div {
    flex: 1 1 calc(${th('gridUnit')} * 6.25);
    text-align: center;
    padding: ${th('gridUnit')};
    &:first-child {
      flex: 1;
      font-weight: 600;
      background-color: ${darken('colorBackgroundHue', 7)};
      border-bottom: ${th('borderWidth')} ${th('borderStyle')}
        ${th('colorBorder')};
    }
  }

  @media screen and (max-width: 970px) {
    div {
      font-size: ${th('fontSizeBaseSmall')};
      white-space: nowrap;
    }
  }
`

const StatusIcon = withTheme(({ theme }) => (
  <Icon color={theme.colorSuccess} size={3}>
    check
  </Icon>
))

const Status = ({ current, mark }) => {
  const curr = states.indexOf(current)
  const compare = states.indexOf(mark)
  return (
    <div>
      {curr === compare && '...'}
      {curr > compare && <StatusIcon />}
    </div>
  )
}

const CheckCitation = ({ manuscript }) => {
  const { meta, journal } = manuscript
  const { articleIds, volume, location, publicationDates } = meta
  return (
    <div>
      {articleIds && articleIds.find(aid => aid.pubIdType === 'pmid') ? (
        <React.Fragment>{volume ? <StatusIcon /> : '...'}</React.Fragment>
      ) : (
        <React.Fragment>
          {journal && volume && location && publicationDates.length > 0 ? (
            <StatusIcon />
          ) : (
            '...'
          )}
        </React.Fragment>
      )}
    </div>
  )
}

const QuickView = ({ manuscript }) => (
  <React.Fragment>
    <MetaSec manuscript={manuscript} />
    <Checklist>
      <Col flex={1}>
        <div>Submitted</div>
        <Status current={manuscript.status} mark="READY" />
      </Col>
      <Col flex={1.5}>
        <div>Author review</div>
        <Status current={manuscript.status} mark="in-review" />
      </Col>
      <Col flex={1.5}>
        <div>Submission QA</div>
        <Status current={manuscript.status} mark="submitted" />
      </Col>
      <Col flex={1.5}>
        <div>XML tagging</div>
        <Status current={manuscript.status} mark="tagging" />
      </Col>
      <Col flex={1}>
        <div>XML QA</div>
        <Status current={manuscript.status} mark="xml-qa" />
      </Col>
      <Col flex={1.5}>
        <div>Final review</div>
        <Status current={manuscript.status} mark="xml-review" />
      </Col>
      <Col flex={1}>
        <div>Citation</div>
        <CheckCitation manuscript={manuscript} />
      </Col>
      <Col flex={1}>
        <div>Published</div>
        <Status current={manuscript.status} mark="ncbi-ready" />
      </Col>
    </Checklist>
  </React.Fragment>
)

export default QuickView
