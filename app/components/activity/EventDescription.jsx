import React from 'react'
import { Query } from 'react-apollo'
import styled from 'styled-components'
import moment from 'moment'
import { Icon, Action, Button, Link } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { B, A, HTMLString } from '../ui'
import { AllTypes } from '../upload-files'
import { UserContext } from '../App'
import Mailer from '../mailer'
import { GET_USER, GET_JOURNAL } from './operations'

const NoteEvent = styled.div`
  display: flex;
  align-items: flex-end;
  flex-wrap: wrap;
  justify-content: space-between;
  div {
    max-width: calc(100% - (${th('gridUnit')} * 4));
    font-size: ${th('fontSizeBaseSmall')};
  }
  button {
    line-height: 0;
  }
  .hidden {
    display: none;
  }
`
const NoPadding = styled(Action)`
  span {
    padding-top: 0;
    padding-bottom: 0;
  }
`
const EmailBody = styled.div`
  white-space: pre-wrap;
  padding-top: calc(${th('gridUnit')} * 2);
  &,
  & div,
  & div * {
    font-size: ${th('fontSizeBaseSmall')};
  }
`
const ReplyButton = styled(Button)`
  min-width: 0;
  padding: calc(${th('gridUnit')} / 2) ${th('gridUnit')};
  font-size: ${th('fontSizeBaseSmall')};
  margin-left: ${th('gridUnit')};
  float: right;
`
const Username = ({ id }) => (
  <Query query={GET_USER} variables={{ id }}>
    {({ data, loading, error }) => {
      if (error) {
        return <em>[User has been deleted]</em>
      }
      if (loading || !data) {
        return `...`
      }
      const { givenNames, surname } = data.user.identities[0].name
      return (
        <Link to={`/manage-account/${id}`}>
          {givenNames} {surname}
        </Link>
      )
    }}
  </Query>
)

const Journal = ({ id }) => (
  <Query query={GET_JOURNAL} variables={{ id }}>
    {({ data, loading }) => {
      if (loading || !data || !data.journal) {
        return `...`
      }
      return <em>{data.journal.journalTitle}</em>
    }}
  </Query>
)

const toggleHidden = button => {
  const array = Array.from(button.children)
  array.forEach(element => {
    element.classList.toggle('hidden')
  })
  button.parentNode.nextElementSibling.classList.toggle('hidden')
}

const cleanUp = column => column.replace('meta,', '').replace('_', ' ')
const isJson = str => {
  try {
    JSON.parse(str)
  } catch (e) {
    return false
  }
  return true
}

const ParseJson = ({ col, val }) => {
  const set = isJson(val) && JSON.parse(val)
  switch (col) {
    case 'journal id':
      return val ? <Journal id={val} /> : '(unmatched)'
    case 'title':
      return <em>{val}</em>
    case 'article ids':
      return set.map(
        (p, i) =>
          `${p.pubIdType.toUpperCase()} ${p.id}${
            i !== set.length - 1 ? ', ' : ''
          }`,
      )
    case 'funding group':
      return set.map(
        (p, i) =>
          `${p.pi.surname}, ${p.fundingSource} ${p.awardId}${
            i !== set.length - 1 ? '; ' : ''
          }`,
      )
    case 'publication dates':
      return set.map((p, i) => {
        const type =
          (p.type === 'epub' && 'Electronic') || (p.type === 'ppub' && 'Print')
        const date = p.date
          ? moment(p.date).format('DD MMM YYYY')
          : `${p.jatsDate.year} ${p.jatsDate.season}`
        return `${date} (${type})${i !== set.length - 1 ? '; ' : ''}`
      })
    case 'location':
      if (set.fpage) {
        return `page ${set.fpage}${set.lpage && `-${set.lpage}`}`
      } else if (set.elocationId) {
        return `elocation ${set.elocationId}`
      }
      return ''
    case 'form_state':
      return (
        <React.Fragment>
          <NoteEvent>
            <div>Errors in submission:</div>
            <NoPadding
              onClick={e => toggleHidden(e.currentTarget)}
              title="Message body"
            >
              <Icon color="currentColor" size={3}>
                chevron-right
              </Icon>
              <Icon className="hidden" color="currentColor" size={3}>
                chevron-down
              </Icon>
            </NoPadding>
          </NoteEvent>
          <EmailBody className="hidden">{val}</EmailBody>
        </React.Fragment>
      )
    default:
      return val
  }
}

class EmailMessage extends React.Component {
  state = { open: false, mail: false }
  static contextType = UserContext
  render() {
    const currentUser = this.context
    const { email, manuscript, sender } = this.props
    const { open, mail } = this.state
    return (
      <React.Fragment>
        <NoteEvent>
          <div>
            <B>Sent email to: </B>
            {email.to.map((to, i) => (
              <React.Fragment key={to}>
                {(to === 'helpdesk' && 'Helpdesk') ||
                  (to === 'tagger' && 'Taggers') || <Username id={to} />}
                {i !== email.to.length - 1 && ', '}
              </React.Fragment>
            ))}
            <br />
            <B>Subject: </B>
            {email.subject}
          </div>
          <Action
            onClick={() => this.setState({ open: !open })}
            title="Message body"
          >
            <Icon color="currentColor" size={3}>
              chevron-{open ? 'down' : 'right'}
            </Icon>
          </Action>
        </NoteEvent>
        {open && (
          <div>
            <ReplyButton onClick={() => this.setState({ mail: true })}>
              Reply
            </ReplyButton>
            <EmailBody>
              <HTMLString element="div" string={email.message} />
            </EmailBody>
          </div>
        )}
        {mail && (
          <Mailer
            close={() => this.setState({ mail: false })}
            currentUser={currentUser}
            manuscript={manuscript}
            recipients={[sender.id]}
            subject={`Re: ${email.subject}`}
          />
        )}
      </React.Fragment>
    )
  }
}

const EventDescription = ({ audit, manuscript }) => {
  const { originalData, objectType, changes } = audit
  if (objectType === 'note') {
    const content = JSON.parse(changes.content)
    if (content && content.to) {
      return (
        <EmailMessage
          email={content}
          manuscript={manuscript}
          sender={audit.user}
        />
      )
    }
    return (
      <React.Fragment>
        <NoteEvent>
          <div>
            <B>Attached a note:</B>
          </div>
          <NoPadding
            onClick={e => toggleHidden(e.currentTarget)}
            title="Message body"
          >
            <Icon color="currentColor" size={3}>
              chevron-right
            </Icon>
            <Icon className="hidden" color="currentColor" size={3}>
              chevron-down
            </Icon>
          </NoPadding>
        </NoteEvent>
        <EmailBody className="hidden">{content}</EmailBody>
      </React.Fragment>
    )
  }
  if (objectType === 'team') {
    if (
      changes.role_name === 'submitter' ||
      (originalData && originalData.role_name === 'submitter')
    ) {
      return `Became submitter`
    }
    if (
      changes.role_name === 'reviewer' ||
      (originalData && originalData.role_name === 'reviewer')
    ) {
      if (audit.user.id === changes.user_id) {
        return `Became reviewer`
      }
      return (
        <React.Fragment>
          Added reviewer: <Username id={changes.user_id} />
        </React.Fragment>
      )
    }
  }
  if (objectType === 'file') {
    if (changes.filename && changes.url) {
      const type =
        AllTypes.find(t => t.value === changes.type) &&
        AllTypes.find(t => t.value === changes.type).label
      return (
        <React.Fragment>
          Uploaded {changes.type && `${type || changes.type} `}file{' '}
          <A download={changes.filename} href={changes.url}>
            <B>{changes.filename}</B>
          </A>
        </React.Fragment>
      )
    }
    if (changes.url) {
      const type =
        AllTypes.find(t => t.value === originalData.type) &&
        AllTypes.find(t => t.value === originalData.type).label
      return (
        <React.Fragment>
          Regenerated {originalData.type && `${type || originalData.type} `}file{' '}
          <A download={originalData.filename} href={changes.url}>
            <B>{originalData.filename}</B>
          </A>
        </React.Fragment>
      )
    }
    if (changes.type) {
      const type = AllTypes.find(t => t.value === changes.type).label
      return (
        <React.Fragment>
          Set <em>{originalData.filename}</em> type to: {type}
        </React.Fragment>
      )
    }
    if (changes.label) {
      return (
        <React.Fragment>
          Set <em>{originalData.filename}</em> label to: {changes.label}
        </React.Fragment>
      )
    }
    if (changes.deleted) {
      return `Deleted ${originalData.filename}`
    }
    if (changes.updated_by) {
      return 'Regenerated previews'
    }
  }
  if (objectType === 'manuscript') {
    if (changes.claimed_by) {
      return `Claimed manuscript`
    }
  }

  if (changes.deleted) {
    return `Deleted ${objectType}`
  }

  if (changes.id) {
    return `Created ${objectType}`
  }

  const keys = Object.keys(changes)
  const list = keys.filter(k => k !== 'updated_by')

  return list.map(k => {
    switch (k) {
      case 'claimed_by':
        return 'Removed admin claim'
      case 'form_state':
        return changes[k] ? (
          <React.Fragment key={k}>
            {changes.status && changes.status === 'submission-error' ? (
              'Reported errors (see below)'
            ) : (
              <React.Fragment>
                <ParseJson col={k} val={changes[k]} />
              </React.Fragment>
            )}
          </React.Fragment>
        ) : (
          'Removed error report'
        )
      default:
        return (
          <React.Fragment key={k}>
            {`Set ${objectType} ${cleanUp(k)} to: `}
            <ParseJson col={cleanUp(k)} val={changes[k]} />
            <br />
          </React.Fragment>
        )
    }
  })
}

export default EventDescription
