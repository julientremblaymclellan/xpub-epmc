import React from 'react'
import { Field } from 'formik'
import { capitalize } from 'lodash'
import styled from 'styled-components'
import { th, override } from '@pubsweet/ui-toolkit'
import { Button, H2, TextField, CheckboxGroup } from '@pubsweet/ui'
import { Buttons, TextArea, Portal, Close, CloseButton } from '../ui'

const Recipient = styled.fieldset`
  border: 0;
  padding: 0;
  margin-bottom: calc(${th('gridUnit')} * 3);
  & > label {
    ${override('ui.Label')};
  }
`

const SubjectInput = props => <TextField label="Subject" {...props.field} />
const MessageInput = props => (
  <TextArea label="Message" rows={15} {...props.field} />
)

const MailerForm = props => {
  // Assumption: there is at most one submitter and one reviewer
  const people = props.currentUser.admin
    ? []
    : [
        {
          value: 'helpdesk',
          label: 'Europe PMC Helpdesk',
        },
      ]
  props.manuscript.teams.forEach(t => {
    if (t.teamMembers[0].user.id !== props.currentUser.id) {
      const i = people.findIndex(p => p.value === t.teamMembers[0].user.id)
      if (i >= 0) {
        people[i].label = `${people[i].label}/${capitalize(t.role)}`
      } else {
        const { title, givenNames, surname } = t.teamMembers[0].alias.name
        people.push({
          value: t.teamMembers[0].user.id,
          label: `${
            title ? `${title} ` : ''
          } ${givenNames} ${surname}, ${capitalize(t.role)}`,
        })
      }
    }
  })
  return (
    <Portal transparent>
      <Close style={{ margin: 0 }}>
        <CloseButton onClick={() => props.close()} />
      </Close>
      <H2>Send an email</H2>
      <form onSubmit={props.handleSubmit}>
        <Recipient>
          <label>Recipient(s)</label>
          <CheckboxGroup
            name="recipients"
            onChange={checked => {
              props.setFieldValue('recipients', checked)
            }}
            options={people}
            value={props.values.recipients}
          />
        </Recipient>
        <Field component={SubjectInput} name="subject" />
        <Field component={MessageInput} name="message" />
        <Buttons right>
          <Button primary type="submit">
            Send
          </Button>
          <Button onClick={() => props.close()}>Cancel</Button>
        </Buttons>
      </form>
    </Portal>
  )
}

export default MailerForm
