import { withFormik } from 'formik'
import { graphql, compose } from 'react-apollo'
import * as yup from 'yup'
import MailerForm from './MailerForm'
import { SEND_EMAIL } from './operations'
import { QUERY_ACTIVITY_INFO } from '../activity/operations'

const handleSubmit = async (
  values,
  { props, setSubmitting, setErrors, resetForm, setStatus },
) => {
  try {
    await props.sendEmail({
      variables: {
        manuscriptId: props.manuscript.id,
        to: values.recipients,
        subject: values.subject,
        message: values.message,
      },
      refetchQueries: [
        {
          query: QUERY_ACTIVITY_INFO,
          variables: {
            id: props.manuscript.id,
          },
        },
      ],
    })
    resetForm()
    props.close()
  } catch (e) {
    if (e.graphQLErrors) {
      setSubmitting(false)
      setErrors(e.graphQLErrors[0].message)
    }
  }
}

const enhancedFormik = withFormik({
  initialValues: {
    recipients: [],
    subject: '',
    message: '',
  },
  mapPropsToValues: props => ({
    recipients: props.recipients,
    subject: props.subject,
    message: props.message,
  }),
  validationSchema: yup.object().shape({
    recipients: yup.array().required('At least one recipient is required'),
    subject: yup.string().required('Subject is required'),
    message: yup.string().required('Message is required'),
  }),
  displayName: 'send-email',
  handleSubmit,
})(MailerForm)

export default compose(graphql(SEND_EMAIL, { name: 'sendEmail' }))(
  enhancedFormik,
)
