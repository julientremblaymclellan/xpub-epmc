import gql from 'graphql-tag'

export const SEND_EMAIL = gql`
  mutation SendEmail(
    $manuscriptId: ID!
    $to: [ID]!
    $subject: String!
    $message: String!
  ) {
    epmc_email(
      manuscriptId: $manuscriptId
      to: $to
      subject: $subject
      message: $message
    )
  }
`
