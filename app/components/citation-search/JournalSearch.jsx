import React from 'react'
import { debounce } from 'lodash'
import { ApolloConsumer } from 'react-apollo'
import { SEARCH_JOURNALS } from './operations'
import { HTMLString, Loading, LoadingIcon, SearchSelect } from '../ui'

class JournalSearch extends React.Component {
  state = {
    loading: false,
    journals: [],
    query: this.props.journal ? this.props.journal.journalTitle : '',
  }
  onJournalSelect = journal => {
    const { id, journalTitle, meta } = journal
    this.setState(
      {
        query: journal.journalTitle,
      },
      () => {
        this.props.setJournal({ id, journalTitle, meta })
      },
    )
  }
  render() {
    return (
      <ApolloConsumer>
        {client => {
          const searchJournals = debounce(async () => {
            const { query } = this.state
            const { data } = await client.query({
              query: SEARCH_JOURNALS,
              variables: { query },
            })
            this.setState({
              journals: data.alphaJournals,
              loading: false,
            })
          }, 500)
          const onQueryChange = e => {
            const query = e.target.value
            this.setState({
              query: e ? query : '',
              journals: [],
              loading: true,
            })
            if (query.trim().length > 0) {
              searchJournals()
            }
          }
          return (
            <SearchSelect
              invalidTest={this.props.error}
              label="Search for journal title"
              onInput={onQueryChange}
              optionsOnChange={this.onJournalSelect}
              placeholder="The journal title"
              query={this.state.query}
              singleSelect
            >
              {this.state.loading && (
                <Loading>
                  <LoadingIcon />
                </Loading>
              )}
              {this.state.journals.map(journal => (
                <SearchSelect.Option
                  data-option={journal}
                  key={journal.id}
                  propKey={journal.id}
                >
                  <HTMLString string={journal.journalTitle} />
                </SearchSelect.Option>
              ))}
              {this.state.query.trim().length > 0 && (
                <SearchSelect.Option
                  data-option={{ journalTitle: this.state.query }}
                  key="submit"
                  propKey="submit"
                >
                  <em>
                    <HTMLString
                      string={`Submit &apos;${
                        this.state.query
                      }&apos; as your journal name`}
                    />
                  </em>
                </SearchSelect.Option>
              )}
            </SearchSelect>
          )
        }}
      </ApolloConsumer>
    )
  }
}

export default JournalSearch
