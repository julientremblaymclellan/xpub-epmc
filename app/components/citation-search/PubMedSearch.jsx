import React from 'react'
import { ApolloConsumer } from 'react-apollo'
import styled from 'styled-components'
import moment from 'moment'
import { th } from '@pubsweet/ui-toolkit'
import { Action, Button, Icon, H3, H4, Link } from '@pubsweet/ui'
import {
  A,
  B,
  Buttons,
  Close,
  CloseButton,
  HTMLString,
  Loading,
  LoadingIcon,
  Notification,
  Portal,
  SearchForm,
  ZebraList,
} from '../ui'
import { JOURNAL_INFO } from './operations'
import PubMedSearchResult from './PubMedSearchResult'
import UnmatchedCitation from './UnmatchedCitation'

const LoadMore = styled(Button)`
  margin: 15px auto;
`
const SearchArea = styled.div`
  margin: 0 auto;
`
const Notice = styled(H4)`
  margin: 0 auto;
  & + div {
    margin-bottom: calc(${th('gridUnit')} * 3);
  }
`
const Links = styled.p`
  a {
    display: inline-flex;
    align-items: center;
  }
`
const isDate = d => {
  let date = moment.utc(d, 'YYYY MMM DD')
  if (!date.isValid()) {
    date = moment.utc(d, 'YYYY MMM')
    if (!date.isValid()) {
      return false
    }
  }
  return date
}
class PubMedSearch extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      results: [],
      enabled: false,
      loading: false,
      hitcount: null,
      inPMC: null,
      pmcid: null,
      inEPMC: false,
      unmatched:
        this.props.manuscript &&
        (!this.props.manuscript.meta.articleIds ||
          this.props.manuscript.meta.articleIds.length === 0),
      pubProvided: false,
    }
    this.onSearch = this.onSearch.bind(this)
    this.onLoad = this.onLoad.bind(this)
    this.onQueryChange = this.onQueryChange.bind(this)
    this.onSelected = this.onSelected.bind(this)
    this.selectResult = this.selectResult.bind(this)
    this.handleUnmatched = this.handleUnmatched.bind(this)
  }
  handleUnmatched(unm) {
    if (unm.journal && unm.journal.meta.pmcStatus) {
      this.setState({
        pubProvided: unm.journal.journalTitle,
        unmatched: false,
      })
    } else {
      const citationData = unm
      if (this.props.newNote && unm.note) {
        const { notes } = this.props.manuscript.meta
        const note = notes
          ? notes.find(n => n.notesType === 'userCitation')
          : null
        if (note) {
          this.props.changeNote({ id: note.id, ...unm.note })
        } else {
          this.props.newNote(unm.note)
        }
        delete citationData.note
      }
      this.props.citationData(citationData)
    }
  }
  selectResult(result, journal) {
    const publicationDates = []
    if (result.pubdate) {
      const ppub = { type: 'ppub' }
      if (isDate(result.pubdate)) {
        ppub.date = isDate(result.pubdate)
      } else {
        ppub.jatsDate = {
          year: result.pubdate.substring(0, 4),
          season: result.pubdate.substring(5),
        }
      }
      publicationDates.push(ppub)
    }
    if (result.epubdate) {
      const epub = { type: 'epub' }
      if (isDate(result.epubdate)) {
        epub.date = isDate(result.epubdate)
      } else {
        epub.jatsDate = {
          year: result.pubdate.substring(0, 4),
          season: result.epubdate.substring(5),
        }
      }
      publicationDates.push(epub)
    }
    const citationData = {
      journalId: journal.id,
      meta: {
        title: result.title,
        volume: result.volume,
        issue: result.issue,
        location: {
          fpage: result.pages.split('-')[0] || null,
          lpage: result.pages.split('-').pop() || null,
          elocationId: result.elocationid,
        },
        publicationDates,
        articleIds: [
          {
            pubIdType: 'pmid',
            id: result.uid,
          },
        ],
      },
    }
    if (result.articleids && result.articleids.find(i => i.idtype === 'doi')) {
      citationData.meta.articleIds.push({
        pubIdType: 'doi',
        id: result.articleids.find(i => i.idtype === 'doi').value,
      })
    }
    this.props.citationData(citationData)
  }
  onQueryChange(event) {
    this.setState({
      enabled: event ? !!event.target.value : false,
      retstart: 0,
      results: [],
      query: event ? event.target.value : '',
    })
  }
  onLoad(event) {
    this.onSearch(event)
  }
  async onSearch(event) {
    event.preventDefault()
    this.setState({
      enabled: false,
      loading: true,
    })
    const { query, retstart, results } = this.state

    if (retstart >= results.length && query) {
      const SearchUrl = `/eutils/esearch?term=${query}&retstart=${retstart}&db=pubmed`

      const response = await fetch(SearchUrl, {
        headers: new Headers({
          Authorization: `Bearer ${window.localStorage.getItem('token')}`,
        }),
      })
      const json = await response.json()
      const ids = json.esearchresult.idlist
      const hitcount = parseInt(json.esearchresult.count, 10)
      const SummaryUrl = `/eutils/esummary?db=pubmed&id=${ids.join()}`
      const summary = await fetch(SummaryUrl, {
        headers: new Headers({
          Authorization: `Bearer ${window.localStorage.getItem('token')}`,
        }),
      })
      const summaryResponse = await summary.json()
      const { result } = summaryResponse
      const newResults = this.state.results.slice()
      ids.forEach(id => {
        newResults.push(result[id])
      })
      this.setState({
        hitcount,
        results: newResults,
        retstart: newResults.length > 0 ? newResults.length : 0,
      })
    }
    this.setState({ loading: false })
  }
  async onSelected(result, journal) {
    const inPMC = result.articleids.find(id => id.idtype === 'pmc')
    if (inPMC) {
      const obj = {
        inPMC: result.title,
        pmcid: inPMC.value,
      }
      const epmcURL = `https://www.ebi.ac.uk/europepmc/webservices/rest/search?query=SRC:MED%20EXT_ID:${
        result.uid
      }&resulttype=lite&format=json`
      const response = await fetch(epmcURL)
      const json = await response.json()
      const epmc = json.resultList.result[0].inEPMC
      if (epmc === 'Y') {
        obj.inEPMC = true
      }
      this.setState(obj)
    } else if (journal.meta.pmcStatus) {
      this.setState({
        pubProvided: { title: journal.journalTitle, result, journal },
      })
    } else {
      this.selectResult(result, journal)
    }
  }
  render() {
    const {
      results,
      hitcount,
      loading,
      query,
      inPMC,
      inEPMC,
      pubProvided,
      pmcid,
    } = this.state
    const { manuscript } = this.props
    const { meta, journal } = manuscript || {}
    const { notes } = meta || {}
    const note = notes ? notes.find(n => n.notesType === 'userCitation') : null
    let journalInfo = journal
    if (!journalInfo && meta && meta.unmatchedJournal) {
      journalInfo = {
        journalTitle: meta.unmatchedJournal,
      }
    }
    return (
      <SearchArea>
        {this.state.unmatched && (
          <Portal transparent>
            <Close>
              <CloseButton
                onClick={() => this.setState({ unmatched: false })}
              />
            </Close>
            <UnmatchedCitation
              citation={this.handleUnmatched}
              close={() => this.setState({ unmatched: false })}
              journal={journalInfo}
              note={note ? note.content : ''}
              title={meta && meta.title}
            />
          </Portal>
        )}
        {inPMC || pubProvided ? (
          <React.Fragment>
            {inPMC ? (
              <React.Fragment>
                <H3>No further action is required for this manuscript</H3>
                <p>
                  The full text of the article &apos;
                  <HTMLString string={inPMC} />
                  &apos;
                  {` has already been sent to Europe PMC. Please use `}
                  <B>{pmcid}</B>
                  {` for grant reporting purposes.`}
                </p>
                {inEPMC && (
                  <Links>
                    <A href={`https://europepmc.org/articles/${pmcid}`}>
                      <Icon color="currentColor">arrow-right-circle</Icon>
                      View this article on Europe PMC
                    </A>
                    {/* TODO: ORCID claiming
                    <br />
                    <A
                      href={`https://europepmc.org/orcid/import?3-1.ILinkListener-startwizard?query=${pmcid}`}
                    >
                      <Icon color="currentColor">arrow-right-circle</Icon>
                      Claim this article to your ORCID
                    </A> */}
                  </Links>
                )}
              </React.Fragment>
            ) : (
              <React.Fragment>
                <H3>Your article will be provided by the publisher</H3>
                <p>
                  {`Your article is published in `}
                  <B>{pubProvided.title}</B>
                  {` This journal participates in PMC.  Your article will be provided to PMC and made available in Europe PMC by the publisher. Please contact the publisher regarding any delays in this process.`}
                </p>
                <p>
                  {`Would you like to `}
                  <Action
                    onClick={() =>
                      this.selectResult(pubProvided.result, pubProvided.journal)
                    }
                  >
                    continue your submission with this citation
                  </Action>
                  {` regardless?`}
                </p>
              </React.Fragment>
            )}
            <Buttons>
              <Link to="/">
                <Button primary>End Submission</Button>
              </Link>
              <Button
                onClick={() =>
                  this.setState({
                    inPMC: null,
                    pmcid: null,
                    inEPMC: false,
                    pubProvided: null,
                  })
                }
              >
                Back to Search
              </Button>
            </Buttons>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <H3>Populate from PubMed</H3>
            <SearchForm
              buttonLabel="Search"
              disabled={!this.state.enabled}
              label="Search by article title, PMID, DOI, etc."
              name="Search"
              onChange={this.onQueryChange}
              onSubmit={this.onSearch}
              placeholder="Search for your article"
              value={query}
            />
            {hitcount !== null && (
              <React.Fragment>
                <Notice>Select your citation from results</Notice>
                {(hitcount === 0 || results.length > 0) && (
                  <React.Fragment>
                    {this.props.changeNote ? (
                      <Notification type="info">
                        {hitcount === 0 && 'No results found. '}
                        {results.length > 0 && 'Manuscript not in results? '}
                        <Action
                          onClick={() => this.setState({ unmatched: true })}
                        >
                          Click to enter citation manually.
                        </Action>
                      </Notification>
                    ) : (
                      <React.Fragment>
                        {hitcount === 0 && (
                          <Notification type="info">
                            No results found.
                          </Notification>
                        )}
                      </React.Fragment>
                    )}
                  </React.Fragment>
                )}
              </React.Fragment>
            )}
            {results.length > 0 && (
              <ApolloConsumer>
                {client => (
                  <ZebraList style={{ textAlign: 'center' }}>
                    {results.map(
                      result =>
                        result.fulljournalname && (
                          <PubMedSearchResult
                            key={result.uid}
                            onClick={async () => {
                              const { data } = await client.query({
                                query: JOURNAL_INFO,
                                variables: { nlmId: result.nlmuniqueid },
                              })
                              this.onSelected(result, data.selectWithNLM)
                            }}
                            result={result}
                          />
                        ),
                    )}
                    {loading && (
                      <Loading>
                        <LoadingIcon />
                      </Loading>
                    )}
                    {results.length < hitcount && !loading && (
                      <LoadMore onClick={this.onLoad} secondary>
                        Load More Results
                      </LoadMore>
                    )}
                  </ZebraList>
                )}
              </ApolloConsumer>
            )}
            {results.length === 0 && loading && (
              <Loading>
                <LoadingIcon />
              </Loading>
            )}
          </React.Fragment>
        )}
      </SearchArea>
    )
  }
}

export default PubMedSearch
