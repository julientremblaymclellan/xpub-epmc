import React from 'react'
import * as Joi from 'joi'
import { Button, ErrorText, H2, TextField } from '@pubsweet/ui'
import { Buttons, TextArea } from '../ui'
import JournalSearch from './JournalSearch'

class UnmatchedCitation extends React.Component {
  state = {
    title: this.props.title,
    journal: this.props.journal,
    note: this.props.note,
    enabled: this.props.title && this.props.journal,
    titleMessage: '',
  }
  enable = () => {
    const validTitle = Joi.validate(this.state.title, Joi.string().required())
    const validJournal = Joi.validate(
      this.state.journal,
      Joi.object({
        id: Joi.string(),
        journalTitle: Joi.string().required(),
        meta: Joi.object({
          pmcStatus: Joi.boolean(),
        }),
      }),
      { allowUnknown: true },
    )
    if (!validTitle.error && !validJournal.error) {
      this.setState({ enabled: true })
    } else {
      this.setState({
        titleMessage: validTitle.error ? 'Title is required.' : '',
        journalMessage: validJournal.error ? 'Journal is required.' : '',
      })
    }
  }
  submitData = () => {
    if (this.state.enabled) {
      const citationData = {
        meta: {
          title: this.state.title,
        },
      }
      if (this.state.journal.id) {
        citationData.meta.unmatchedJournal = null
        citationData.journalId = this.state.journal.id
      } else {
        citationData.meta.unmatchedJournal = this.state.journal.journalTitle
        citationData.journalId = null
      }
      if (this.state.note) {
        citationData.note = {
          notesType: 'userCitation',
          content: this.state.note,
        }
      }
      this.props.citation(citationData)
    }
  }
  onTitleChange = event => {
    this.setState(
      {
        titleMessage: '',
        title: event.target.value,
      },
      () => {
        this.enable()
      },
    )
  }
  onJournalSelect = journal => {
    const { id, journalTitle, meta } = journal
    this.setState(
      {
        journalMessage: '',
        journal: { id, journalTitle, meta },
      },
      () => {
        this.enable()
      },
    )
  }
  render() {
    return (
      <div>
        <H2>Enter citation information</H2>
        <TextField
          invalidTest={this.state.titleMessage}
          label="Enter manuscript title"
          name="Title"
          onChange={this.onTitleChange}
          placeholder="The title of your manuscript"
          value={this.state.title}
        />
        {this.state.titleMessage && (
          <ErrorText>{this.state.titleMessage}</ErrorText>
        )}
        <JournalSearch
          error={this.state.journalMessage}
          journal={this.props.journal}
          setJournal={this.onJournalSelect}
        />
        {this.state.journalMessage && (
          <ErrorText>{this.state.journalMessage}</ErrorText>
        )}
        <TextArea
          label="Other journal information (optional)"
          onChange={e => this.setState({ note: e.target.value })}
          placeholder="List URL, DOI, Volume/Issue, etc."
          rows={3}
          value={this.state.note}
        />
        <Buttons right>
          <Button
            disabled={!this.state.enabled}
            onClick={() => this.submitData()}
            primary
          >
            Submit
          </Button>
          {this.props.close && (
            <Button onClick={this.props.close}>Exit to citation search</Button>
          )}
        </Buttons>
      </div>
    )
  }
}

export default UnmatchedCitation
