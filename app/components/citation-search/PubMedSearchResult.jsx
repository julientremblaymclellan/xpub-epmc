import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { HTMLString, ZebraListItem } from '../ui'

const Result = styled(ZebraListItem)`
  text-align: left;
  &:hover {
    cursor: pointer;
    color: ${th('colorPrimary')};
  }
`
const Citation = styled.span`
  font-size: ${th('fontSizeBaseSmall')};
  margin-right: ${th('gridUnit')};
`
const FullTextLabel = styled.span`
  font-size: ${th('fontSizeBaseSmall')};
  display: inline-block;
  background-color: ${th('colorFurniture')};
  color: ${th('colorBackground')};
  padding: 0 ${th('gridUnit')};
`
function Label(props) {
  if (!props.ids.some(id => id.idtype === 'pmc')) {
    return null
  }
  return <FullTextLabel>Already in Europe PMC</FullTextLabel>
}

const PubMedSearchResult = ({ result, onClick }) => (
  <Result onClick={onClick}>
    <HTMLString string={result.title} />
    <br />
    <Citation>
      <HTMLString
        string={`${result.sortfirstauthor}${
          result.authors.length > 1 ? `, ... ${result.lastauthor}` : ''
        }. ${result.fulljournalname}. ${result.pubdate}${
          result.volume ? `;${result.volume}` : ''
        }${result.issue ? `(${result.issue})` : ''}${
          result.pages ? `:${result.pages}` : ''
        }`}
      />
    </Citation>
    <Label ids={result.articleids} />
  </Result>
)

export default PubMedSearchResult
