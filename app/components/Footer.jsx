import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { A, FlexBar, LeftSide, RightSide } from './ui'

const Foot = styled(FlexBar)`
  border-top: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  * {
    font-size: ${th('fontSizeBaseSmall')};
  }
  @media screen and (max-width: 870px) {
    height: auto;
  }
  @media screen and (max-width: 450px) {
    flex-wrap: wrap;
    & > div:first-child {
      & > div {
        padding-bottom: 0;
      }
      & + div div {
        text-align: left;
        margin: 0 auto;
        padding-left: 0;
      }
    }
  }
`
const Footer = () => (
  <Foot>
    <LeftSide>
      <div>
        For assistance, see the <A href="/user-guide">User guide</A>,{' '}
        <A href="mailto:helpdesk@europepmc.org">
          email the Europe PMC Helpdesk
        </A>
        , or call +44 1223 494118.
      </div>
    </LeftSide>
    <RightSide>
      <div>
        <A href="https://europepmc.org/">Europe PMC</A>
        <A href="https://europepmc.org/PrivacyNotice">Privacy Notice</A>
        <A href="https://europepmc.org/Accessibility">Accessibility</A>
      </div>
    </RightSide>
  </Foot>
)

export default Footer
