import { compose } from 'recompose'
import { withFormik } from 'formik'
import { graphql } from 'react-apollo'
import createHistory from 'history/createBrowserHistory'
import Login from './Login'
import mutations from './mutations'
import redirectPath from './redirect'
import userHelper from '../helpers/userHelper'

const handleSubmit = (values, { props, setSubmitting, setErrors }) =>
  props
    .epmc_signinUser({ variables: { input: values } })
    .then(({ data, errors }) => {
      if (!errors) {
        userHelper.setToken(data.epmc_signinUser.token)
        const { search } = props.location
        let path = ''
        if (search && search.includes('?next=')) {
          path = search.split(/=(.+)/)[1]
            ? search.split(/=(.+)/)[1]
            : redirectPath({ location: props.location })
        }
        createHistory({ forceRefresh: true }).push(path)
        setSubmitting(true)
      }
    })
    .catch(e => {
      if (e.graphQLErrors) {
        setSubmitting(false)
        setErrors(e.graphQLErrors[0].message)
      }
    })

const enhancedFormik = withFormik({
  initialValues: {
    email: '',
    password: '',
  },
  mapPropsToValues: props => ({
    email: props.email,
    password: props.password,
  }),
  displayName: 'login',
  handleSubmit,
  passwordReset: true,
})(Login)

export default compose(
  graphql(mutations.LOGIN_USER, { name: 'epmc_signinUser' }),
)(enhancedFormik)
