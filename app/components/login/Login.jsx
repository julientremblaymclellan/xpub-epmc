import React from 'react'
import { Field } from 'formik'
import { isEmpty } from 'lodash'
import { ErrorText, H1, Link, Button, TextField, Checkbox } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import { Page, Notification } from '../ui'
import SignInFooter from '../SignInFooter'

const Signup = styled.p`
  margin-bottom: 0;
`
const ResetPassword = styled.p`
  margin-top: 0;
`
const ShowPassword = styled(Checkbox)`
  position: absolute;
  right: 0;
  top: 0;
  font-size: ${th('fontSizeBaseSmall')};
`
const PasswordField = styled.div`
  position: relative;
  max-width: 500px;
`

const Container = styled.form`
  margin: 0 0;
  max-width: 350px;
`
const EmailInput = props => <TextField label="Email address" {...props.field} />
const PasswordInput = props => (
  <TextField label="Password" {...props.field} type="password" />
)

const PlainPasswordInput = props => (
  <TextField
    invalidTest={props.invalidTest}
    label="Password"
    {...props.field}
  />
)

class Login extends React.Component {
  state = {
    showPassword: false,
  }
  toggleShowPassword = () => {
    const { showPassword } = this.state
    this.setState({
      showPassword: !showPassword,
    })
  }
  render() {
    const {
      errors,
      isSubmitting,
      handleSubmit,
      signup = true,
      passwordReset = true,
      location,
    } = this.props
    return (
      <Page>
        <H1>Sign in with your Europe PMC plus account</H1>
        {!isEmpty(errors) && <Notification type="error">{errors}</Notification>}
        <Container onSubmit={handleSubmit}>
          <Field component={EmailInput} name="email" />
          <PasswordField>
            {!this.state.showPassword && (
              <Field component={PasswordInput} name="password" />
            )}

            {this.state.showPassword && (
              <Field component={PlainPasswordInput} name="password" />
            )}
            <ShowPassword
              checked={this.state.showPassword}
              label="Show password"
              onChange={() => this.toggleShowPassword()}
            />
          </PasswordField>
          <Button disabled={isSubmitting} primary type="submit">
            Login
          </Button>
        </Container>
        {signup && (
          <Signup>
            {`Don’t have a Europe PMC plus account? `}
            <Link to={`/signup${location.search}`}>Create an account.</Link>
          </Signup>
        )}
        {passwordReset && (
          <ResetPassword>
            {`Forgot your login details? `}
            <Link to="/password-reset">Reset your password.</Link>
          </ResetPassword>
        )}
        <SignInFooter />
      </Page>
    )
  }
}

// used by tests
export { Login, ErrorText, Signup, ResetPassword }

// used by consumers
export default Login
