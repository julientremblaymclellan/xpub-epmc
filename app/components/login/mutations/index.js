import gql from 'graphql-tag'

const LOGIN_USER = gql`
  mutation($input: SigninUserInput) {
    epmc_signinUser(input: $input) {
      user {
        id
        admin
        email
      }
      token
    }
  }
`

module.exports = {
  LOGIN_USER,
}
