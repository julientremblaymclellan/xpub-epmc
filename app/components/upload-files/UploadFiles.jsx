import React from 'react'
import { Mutation } from 'react-apollo'
import styled from 'styled-components'
import Dropzone from 'react-dropzone'
import { th, lighten } from '@pubsweet/ui-toolkit'
import { H3, Icon } from '@pubsweet/ui'
import config from 'config'
import { LoadingIcon, Notification } from '../ui'
import { FileTypes, ManuscriptTypes, XMLTypes } from './uploadtypes'
import UploadFileListItem from './UploadFileListItem'
import { UPLOAD_MUTATION } from './operations'
import { GET_MANUSCRIPT } from '../operations'
import { fileSort } from '../preview-files'

const UploadDiv = styled.div`
  padding: calc(${th('gridUnit')} * 3) 0;
  &:after {
    content: '';
    display: block;
    clear: both;
  }
`
const DropArea = styled(Dropzone)`
  width: 100%;
  text-align: center;
  padding: calc(${th('gridUnit')} * 6);
  border: 2px dashed ${th('colorBorder')};
  cursor: default;
  span {
    color: ${th('colorPrimary')};
  }
  &:hover {
    border-color: ${lighten('colorPrimary', 20)};
    background-color: ${th('colorTextReverse')};
    span {
      color: ${lighten('colorPrimary', 20)};
    }
  }
`
const DropMore = styled(DropArea)`
  padding: calc(${th('gridUnit')} * 3);
  align-items: center;
  justify-content: center;
`
const FileList = styled.ul`
  list-style-type: none;
  padding: 0;
  margin: 0 0 calc(${th('gridUnit')} * 6);
  max-width: 100%;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;

  @media screen and (max-width: 1190px) and (min-width: 870px),
    screen and (max-width: 750px) {
    flex-flow: row wrap;
    justify-content: flex-start;
    align-items: flex-start;
  }
`

class UploadFiles extends React.Component {
  state = { errors: [], loading: false }

  reportError = message => {
    const errors = []
    if (message) {
      errors.push({ message })
    }
    this.setState({ errors })
  }

  render() {
    const { manuscript, types, checked = false, files } = this.props
    const { errors, loading } = this.state
    return (
      <Mutation
        awaitRefetchQueries
        mutation={UPLOAD_MUTATION}
        refetchQueries={result => {
          if (result.data.uploadFiles) {
            return [
              {
                query: GET_MANUSCRIPT,
                variables: { id: manuscript },
              },
            ]
          }
          return []
        }}
      >
        {(uploadFiles, { data }) => {
          const fileUploadHandler = async (id, files, type) => {
            this.setState({ loading: true })
            const { data } = await uploadFiles({
              variables: {
                id,
                files,
                type,
              },
            })
            if (!data.uploadFiles) {
              this.reportError('File is not an allowed file type.')
              this.setState({ loading: false })
            } else {
              this.reportError('')
              this.setState({ loading: false })
            }
          }
          return (
            <React.Fragment>
              {errors &&
                errors.map(e => (
                  <Notification key={e.message} type="error">
                    {e.message}
                  </Notification>
                ))}
              <UploadDiv>
                {files.length > 0 && (
                  <div>
                    <FileList>
                      {fileSort(files).map(file =>
                        file.type === 'manuscript' ? (
                          <UploadFileListItem
                            file={file}
                            key="manuscript"
                            loading={loading}
                            manuscript={manuscript}
                            reportError={this.reportError}
                          />
                        ) : (
                          <UploadFileListItem
                            checked={checked}
                            file={file}
                            key={file.url}
                            manuscript={manuscript}
                            types={types}
                          />
                        ),
                      )}
                    </FileList>
                  </div>
                )}
                {files && files.length > 0 ? (
                  <DropMore
                    accept={
                      types.some(t => t.value === 'PMC')
                        ? FileTypes.concat(XMLTypes).join(', ')
                        : FileTypes.join(', ')
                    }
                    onDrop={files => {
                      fileUploadHandler(manuscript, files)
                    }}
                  >
                    {loading ? (
                      <LoadingIcon size={6} />
                    ) : (
                      <Icon color="currentColor" size={6}>
                        upload
                      </Icon>
                    )}
                    <p>
                      {`Drop additional files here, or click to select from your computer.`}
                    </p>
                  </DropMore>
                ) : (
                  <DropArea
                    accept={ManuscriptTypes.join(', ')}
                    multiple={false}
                    onDrop={files => {
                      fileUploadHandler(
                        manuscript,
                        files,
                        config.file.type.manuscript,
                      )
                    }}
                  >
                    {loading ? (
                      <LoadingIcon size={8} />
                    ) : (
                      <Icon color="currentColor" size={8}>
                        upload
                      </Icon>
                    )}
                    <H3>
                      {`Drop manuscript file here, or click to select from your computer.`}
                    </H3>
                    <p>
                      {`Only Microsoft Word or PDF file formats are accepted for the manuscript file.`}
                    </p>
                  </DropArea>
                )}
              </UploadDiv>
            </React.Fragment>
          )
        }}
      </Mutation>
    )
  }
}

export default UploadFiles
