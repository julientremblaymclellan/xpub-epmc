import React from 'react'
import { Mutation } from 'react-apollo'
import Dropzone from 'react-dropzone'
import styled, { withTheme } from 'styled-components'
import { debounce } from 'lodash'
import { th, override } from '@pubsweet/ui-toolkit'
import { TextField, Icon } from '@pubsweet/ui'
import { LoadingIcon, Select } from '../ui'
import { FileLightbox } from '../preview-files'
import { SubmissionTypes, ManuscriptTypes } from './uploadtypes'
import { GET_MANUSCRIPT } from '../operations'
import {
  DELETE_FILE_MUTATION,
  UPDATE_FILE_MUTATION,
  REPLACE_MANUSCRIPT_FILE_MUTATION,
} from './operations'

const FileListItem = styled.li`
  display: flex;
  flex-wrap: no-wrap;
  align-items: flex-start;
  justify-content: space-between;
  max-width: 100%;
  & > * {
    flex: 1 1 30%;
    padding: 0 calc(${th('gridUnit')} * 3);
    &:last-child {
      flex: 1 1 6%;
      padding: 0;
      overflow: hidden;
    }
  }
  @media screen and (max-width: 1190px) and (min-width: 870px),
    screen and (max-width: 750px) {
    flex: 1 1 30%;
    padding: calc(${th('gridUnit')} * 2);
    margin: ${th('gridUnit')};
    background-color: ${th('colorBackgroundHue')};
    border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
    border-radius: ${th('borderRadius')};
    flex-direction: column;
    align-items: flex-start;
    justify-content: stretch;

    & > * {
      width: 100%;
      padding: 0;
      margin-bottom: calc(${th('gridUnit')} * 3);
      min-height: calc(${th('gridUnit')} * 9);
    }
  }
`
const LabeledDiv = styled.div`
  margin-bottom: calc(${th('gridUnit')} * 3);
  div {
    display: flex;
    align-items: center;
    min-height: calc(${th('gridUnit')} * 6);
  }
`
const Label = styled.span`
  display: block;
  font-size: ${th('fontSizeBaseSmall')};
  ${override('ui.Label')};
`
const Drop = styled(Dropzone)`
  width: auto;
  height: auto;
  border: 0;
  input {
    width: 100%;
    left: -100% !important;
  }
`
const Delete = styled.div`
  cursor: pointer;
`
const DeleteIcon = withTheme(({ children, theme }) => (
  <Icon color={theme.colorPrimary} size={3}>
    {children}
  </Icon>
))

class UploadFileListItem extends React.Component {
  state = {
    label: this.props.file.label ? this.props.file.label : '',
    type: this.props.file.type ? this.props.file.type : '',
  }
  render() {
    const { label, type } = this.state
    const { types, loading, file, manuscript, reportError } = this.props
    if (file.type === 'manuscript') {
      return (
        <FileListItem>
          <LabeledDiv>
            <Label>File</Label>
            <div>
              <FileLightbox file={file} />
            </div>
          </LabeledDiv>
          <LabeledDiv>
            <Label>File type</Label>
            <div>
              <span>manuscript</span>
            </div>
          </LabeledDiv>
          <div />
          <Mutation
            mutation={REPLACE_MANUSCRIPT_FILE_MUTATION}
            refetchQueries={() => [
              {
                query: GET_MANUSCRIPT,
                variables: { id: manuscript },
              },
            ]}
          >
            {(replaceManuscriptFile, { data }) => {
              const replaceFileHandler = async (id, fileId, files) => {
                if (files && files.length > 0) {
                  await replaceManuscriptFile({
                    variables: {
                      id,
                      fileId,
                      files,
                    },
                  })
                  reportError('')
                } else {
                  reportError('File is not of an allowed file type.')
                }
              }
              return (
                <LabeledDiv>
                  <Label>Replace</Label>
                  <Drop
                    accept={ManuscriptTypes.join(', ')}
                    multiple={false}
                    onDrop={files => {
                      replaceFileHandler(manuscript, file.id, files)
                    }}
                  >
                    {loading ? (
                      <div>
                        <LoadingIcon />
                      </div>
                    ) : (
                      <Delete>
                        <DeleteIcon>upload</DeleteIcon>
                      </Delete>
                    )}
                  </Drop>
                </LabeledDiv>
              )
            }}
          </Mutation>
        </FileListItem>
      )
    }
    return (
      <FileListItem>
        <LabeledDiv>
          <Label>File</Label>
          <div>
            <FileLightbox file={file} />
          </div>
        </LabeledDiv>
        <Mutation
          mutation={UPDATE_FILE_MUTATION}
          refetchQueries={() => [
            {
              query: GET_MANUSCRIPT,
              variables: { id: manuscript },
            },
          ]}
        >
          {(updateFile, { data }) => {
            const updateFileHandler = debounce(
              async ({ type, label }) =>
                updateFile({
                  variables: {
                    id: file.id,
                    type,
                    label,
                  },
                }),
              100,
            )
            const onPropChange = e => {
              const key = e.target.name
              const val = e.target.value
              const obj = {}
              obj[key] = val
              this.setState(obj)
              updateFileHandler(obj)
            }
            return (
              <React.Fragment>
                <Select
                  icon="chevron_down"
                  invalidTest={!file.type}
                  label="Select file type"
                  name="type"
                  onChange={onPropChange}
                  options={types}
                  value={type}
                />

                <TextField
                  invalidTest={
                    !file.label &&
                    SubmissionTypes.some(t => t.value === file.type)
                  }
                  label="Label to appear in text"
                  name="label"
                  onBlur={onPropChange}
                  onChange={e => this.setState({ label: e.target.value })}
                  placeholder="F1, Fig. 1, Figure 1"
                  value={label}
                />
              </React.Fragment>
            )
          }}
        </Mutation>
        <Mutation
          mutation={DELETE_FILE_MUTATION}
          refetchQueries={() => [
            {
              query: GET_MANUSCRIPT,
              variables: { id: manuscript },
            },
          ]}
        >
          {(deleteFile, { data }) => {
            const deleteFileHandler = () => {
              deleteFile({
                variables: {
                  id: file.id,
                },
              })
            }
            return (
              <LabeledDiv>
                <Label>Remove</Label>
                <Delete onClick={deleteFileHandler}>
                  <DeleteIcon>x</DeleteIcon>
                </Delete>
              </LabeledDiv>
            )
          }}
        </Mutation>
      </FileListItem>
    )
  }
}

export default UploadFileListItem
