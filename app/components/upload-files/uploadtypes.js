export const ManuscriptTypes = [
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  'application/msword',
  'application/pdf',
]

export const FileTypes = [
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document', // docx
  'application/rtf',
  'application/msword',
  'application/pdf',
  'application/xml',
  'application/zip',
  'application/x-zip-compressed',
  'application/x-compressed',
  'multipart/x-zip',
  'application/rtf',
  'application/ogg',
  'application/postscript', // ai, eps
  'application/vnd.oasis.opendocument.text',
  'application/vnd.oasis.opendocument.presentation',
  'application/vnd.oasis.opendocument.spreadsheet',
  'application/vnd.ms-powerpoint',
  'application/vnd.openxmlformats-officedocument.presentationml.presentation',
  'application/vnd.ms-excel',
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'application/xml',
  'application/gzip',
  'text/xml',

  'audio/midi',
  'audio/wav',
  'audio/ogg',

  'chemical/x-cif', // cif

  'image/png',
  'image/jpeg',
  'image/bmp',
  'image/gif',
  'image/svg+xml',
  'image/tiff',

  'text/csv',
  'text/rtf',
  'text/plain',
  'text/rtf',
  'text/html',

  'video/mpeg',
  'video/ogg',
  'video/quicktime', // mov
  'video/mp4',
  'video/x-flv', // flv
  'video/x-matroska', // mkv
  'video/x-ms-asf', // wmv
  'application/x-troff-msvideo',
  'video/avi',
  'video/msvideo',
  'video/x-msvideo', // avi
]

export const XMLTypes = [
  'application/xml',
  'text/html',
  'text/xml',
  'application/xhtml+xml',
  'text/xsl',
]

export const SubmissionTypes = [
  {
    label: '',
    value: '',
  },
  {
    label: 'Figure',
    value: 'figure',
  },
  {
    label: 'Table',
    value: 'table',
  },
  {
    label: 'Supplementary file',
    value: 'supplement',
  },
]

export const ReviewTypes = [
  {
    label: '',
    value: '',
  },
  {
    label: 'XML from tagger',
    value: 'PMC',
  },
  {
    label: 'GIF image',
    value: 'IMGsmall',
  },
  {
    label: 'JPG image',
    value: 'IMGview',
  },
  {
    label: 'TIF image',
    value: 'IMGprint',
  },
  {
    label: 'Supplementary file from tagger',
    value: 'supplement_tag',
  },
  {
    label: 'Converted NXML',
    value: 'PMCfinal',
  },
  {
    label: 'PDF for load to PMC',
    value: 'pdf4load',
  },
  {
    label: 'PDF preview',
    value: 'pdf4print',
  },
  /* {
    label: 'Movie in text',
    value: 'movie_in_text',
  },
  {
    label: 'Movie figure',
    value: 'movie_figure',
  }, */
  {
    label: 'Web preview',
    value: 'tempHTML',
  },
]

export const AllTypes = SubmissionTypes.concat(
  ReviewTypes.reduce((all, t) => {
    if (!SubmissionTypes.some(existing => existing.value === t.value)) {
      all.push(t)
    }
    return all
  }, []),
)
