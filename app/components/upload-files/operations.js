import gql from 'graphql-tag'

export const UPLOAD_MUTATION = gql`
  mutation UploadFiles($id: ID!, $files: Upload!, $type: String) {
    uploadFiles(id: $id, files: $files, type: $type)
  }
`

export const REPLACE_MANUSCRIPT_FILE_MUTATION = gql`
  mutation ReplaceManuscriptFile($id: ID!, $fileId: ID!, $files: Upload!) {
    replaceManuscriptFile(id: $id, fileId: $fileId, files: $files)
  }
`

export const DELETE_FILE_MUTATION = gql`
  mutation DeleleFile($id: ID!) {
    deleteFile(id: $id)
  }
`

export const UPDATE_FILE_MUTATION = gql`
  mutation UpdateFile($id: ID!, $type: String, $label: String) {
    updateFile(id: $id, type: $type, label: $label)
  }
`
