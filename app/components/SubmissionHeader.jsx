import React from 'react'
import styled from 'styled-components'
import { Action, Link, Icon } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { HTMLString, FlexBar, LeftSide, RightSide } from './ui'
import Mailer from './mailer'
import { States } from './dashboard'

const ManuscriptHead = styled(FlexBar)`
  border-bottom: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  * {
    font-weight: 600;
  }
`
const Left = styled.div`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`
class MailLink extends React.Component {
  state = { show: false }
  render() {
    return (
      <React.Fragment>
        <Action onClick={() => this.setState({ show: true })}>
          <Icon color="currentColor" size={2.5}>
            mail
          </Icon>
          Helpdesk
        </Action>
        {this.state.show && (
          <Mailer
            close={() => this.setState({ show: false })}
            currentUser={this.props.currentUser}
            manuscript={this.props.manuscript}
            recipients={['helpdesk']}
          />
        )}
      </React.Fragment>
    )
  }
}

const SubmissionHeader = BaseComponent => ({
  currentUser,
  manuscript,
  children,
  ...props
}) => (
  <React.Fragment>
    <ManuscriptHead>
      <LeftSide>
        <Left>
          {manuscript.id}: <HTMLString string={manuscript.meta.title} />
        </Left>
      </LeftSide>
      <RightSide>
        <div>
          {currentUser.admin ? (
            <React.Fragment>
              {window.location.pathname ===
              `/submission/${manuscript.id}/activity` ? (
                <React.Fragment>
                  {States.admin[manuscript.status].url !== 'activity' && (
                    <Link
                      to={`/submission/${manuscript.id}/${
                        States.admin[manuscript.status].url
                      }`}
                    >
                      <Icon color="currentColor" size={2.5}>
                        x-square
                      </Icon>
                      Exit activity
                    </Link>
                  )}
                </React.Fragment>
              ) : (
                <Link to={`/submission/${manuscript.id}/activity`}>
                  <Icon color="currentColor" size={2.5}>
                    check-square
                  </Icon>
                  Activity
                </Link>
              )}
            </React.Fragment>
          ) : (
            <MailLink currentUser={currentUser} manuscript={manuscript} />
          )}
        </div>
      </RightSide>
    </ManuscriptHead>
    <BaseComponent currentUser={currentUser} manuscript={manuscript} {...props}>
      {children}
    </BaseComponent>
  </React.Fragment>
)

export default SubmissionHeader
