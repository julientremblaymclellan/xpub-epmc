import React from 'react'
import SimpleEditor from 'wax-editor-react'
import PDFViewer from './pdf-viewer'
import { A } from './ui'

class ManuscriptPreview extends React.Component {
  constructor(props) {
    super(props)
    this.state = { hasError: false, html: '' }
  }
  async componentDidMount() {
    if (this.props.file.type === 'source') {
      const response = await fetch(this.props.file.url)
      const html = await response.text()
      this.setState({ html })
    }
  }
  componentDidCatch(error, info) {
    if (error) {
      this.setState({ hasError: true })
    }
  }
  render() {
    const { file } = this.props
    if (this.state.hasError) {
      return (
        <p style={{ margin: '1em' }}>
          {`Error: Unable to generate manuscript preview. `}
          <A download={file.name} href={file.url} title="Download this file">
            Download your file.
          </A>
        </p>
      )
    }
    if (file.type === 'source') {
      const { html } = this.state
      return (
        <SimpleEditor
          content={html}
          editing="selection"
          layout="bare"
          mode={{ styling: true }}
          readOnly
        />
      )
    } else if (file.mimeType === 'application/pdf') {
      return <PDFViewer textContent={this.props.textContent} url={file.url} />
    }
    return (
      <p style={{ margin: '1em' }}>
        {`Unable to generate manuscript preview from ${
          file.filename
        }. Previews can only be generated from files with the extensions `}
        <em>.docx</em>
        {` and `}
        <em>.pdf</em>
        {`. If your file is one of these types, please ensure it is uncorrupted. `}
        <A download={file.name} href={file.url} title="Download this file">
          Download your file.
        </A>
      </p>
    )
  }
}

export default ManuscriptPreview
