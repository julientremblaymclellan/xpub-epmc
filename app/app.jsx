import 'regenerator-runtime/runtime'

import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
// import createHistory from 'history/createBrowserHistory'

import { Root } from 'pubsweet-client'
import { JournalProvider } from 'xpub-journal'

import theme from './epmc-theme'

import Routes from './routes'

const render = () => {
  ReactDOM.render(
    <AppContainer>
      <JournalProvider
        journal={{ name: 'Europe PMC plus' }}
        style={{ height: '100%' }}
      >
        <Root connectToWebSocket="false" routes={<Routes />} theme={theme} />
      </JournalProvider>
    </AppContainer>,
    document.getElementById('root'),
  )
}

render()

if (module.hot) {
  module.hot.accept('./routes', () => {
    render()
  })
}
