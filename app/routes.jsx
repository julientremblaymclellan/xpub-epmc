import React from 'react'
import { withProps } from 'recompose'
import { Route, Switch, Redirect } from 'react-router-dom'
import { Page } from './components/ui'
import AuthenticatedComponent from './components/AuthenticatedComponent'
import Login from './components/login/LoginContainer'
import Signup from './components/signup/SignupContainer'
import MyAccount from './components/my-account/MyAccountContainer'
import UserSearch from './components/manage-users/UserSearch'
import ManageAccount from './components/manage-account/ManageAccount'
import PasswordResetEmail from './components/password-reset/PasswordResetEmailContainer'
import PasswordReset from './components/password-reset/PasswordResetContainer'
import {
  DashboardPage,
  AdminDashboard,
  MetricsPage,
  SearchResultsPage,
} from './components/dashboard'
import {
  CreatePage,
  CreateSetupPage,
  SubmitPage,
} from './components/submission-wizard'
import ReviewPage from './components/review-wizard'
import UserGuidePage from './components/user-guide/UserGuidePage'
import { ActivityPage } from './components/activity'
import App from './components/App'

const LoginPage = withProps({ passwordReset: true })(Login)

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      <AuthenticatedComponent>
        <Component key={props.match.params.version} {...props} />
      </AuthenticatedComponent>
    )}
  />
)

const NoMatch = () => <Page>404: no page found</Page>

const Routes = () => (
  <App>
    <Switch>
      <PrivateRoute component={AdminDashboard} exact path="/admin-dashboard" />
      <PrivateRoute component={CreateSetupPage} exact path="/create" />
      <PrivateRoute component={DashboardPage} exact path="/dashboard" />
      <PrivateRoute component={MetricsPage} exact path="/admin-metrics" />
      <PrivateRoute component={SearchResultsPage} exact path="/search" />
      <PrivateRoute component={MyAccount} exact path="/my-account" />
      <PrivateRoute component={UserSearch} exact path="/manage-users" />
      <PrivateRoute
        component={ManageAccount}
        exact
        path="/manage-account/:id"
      />
      <PrivateRoute
        component={CreatePage}
        exact
        path="/submission/:id/create"
      />
      <PrivateRoute
        component={SubmitPage}
        exact
        path="/submission/:id/submit"
      />
      <PrivateRoute
        component={ReviewPage}
        exact
        path="/submission/:id/review"
      />
      <PrivateRoute
        component={ActivityPage}
        exact
        path="/submission/:id/activity"
      />
      <Route component={Signup} exact path="/signup" />
      <Route component={LoginPage} exact path="/login" />
      <Route component={UserGuidePage} path="/user-guide" />
      <Route component={PasswordResetEmail} exact path="/password-reset" />
      <Route component={PasswordReset} exact path="/password-reset/:id" />
      <Route exact path="/" render={() => <Redirect to="/dashboard" />} />
      <Route component={NoMatch} path="*" />
    </Switch>
  </App>
)

export default Routes
