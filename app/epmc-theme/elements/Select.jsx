import { css } from 'styled-components'

const th = name => props => props.theme[name]

const invalid = css`
  border-color: ${th('colorError')};
`
export default {
  DropdownText: css`
    background-color: ${th('colorTextReverse')};
    border-radius: ${th('borderRadius')};
    &:focus {
      border-color: ${th('colorPrimary')};
      outline: none;
      box-shadow: ${th('dropShadow')};
    }
    ${props => props.invalidTest && invalid};
  `,
  DropdownIcon: css`
    background-color: ${th('colorTextReverse')};
    padding: ${th('gridUnit')};
    border-left: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  `,
}
