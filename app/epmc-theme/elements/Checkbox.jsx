import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export default {
  Root: css`
    cursor: default;
    align-items: flex-start;
    margin-bottom: ${th('gridUnit')};

    &:hover span {
      color: ${th('colorPrimary')};
    }
  `,
  Input: css`
    margin-top: calc(${th('gridUnit')} / 2);
  `,
}
