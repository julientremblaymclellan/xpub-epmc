import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export default css`
  color: ${th('colorText')};
  margin: calc(${th('gridUnit')} * 3) 0 calc(${th('gridUnit')} * 2);
  font-weight: 600;
`
