import { css } from 'styled-components'
import { lighten, darken, th } from '@pubsweet/ui-toolkit'

const primary = css`
  background-color: ${th('colorPrimary')};
  color: ${th('colorTextReverse')};
  font-weight: normal;
  &:hover {
    background-color: ${lighten('colorPrimary', 15)};
    color: ${th('colorTextReverse')};
  }
  &:focus {
    border-color: ${darken('colorPrimary', 50)};
    background-color: ${lighten('colorPrimary', 15)};
    color: ${th('colorTextReverse')};
  }
`

export default css`
  background-color: #ffffff;
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorPrimary')};
  color: ${th('colorPrimary')};
  font-weight: 600;
  padding: ${th('gridUnit')} calc(${th('gridUnit')} * 3);
  &:hover {
    background-color: ${th('colorTextReverse')};
    border-color: ${lighten('colorPrimary', 20)};
    color: ${lighten('colorPrimary', 20)};
  }
  &:focus {
    outline: none;
    background-color: ${th('colorTextReverse')};
    color: ${th('colorPrimary')};
    box-shadow: ${th('dropShadow')};
  }
  ${props => props.primary && primary};
`
