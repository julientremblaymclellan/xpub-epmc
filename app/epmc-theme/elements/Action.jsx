import { css } from 'styled-components'
import { th, darken } from '@pubsweet/ui-toolkit'

export default css`
  text-align: inherit;
  border-radius: 0;
  &:hover {
    color: ${darken('colorPrimary', 20)};
  }
  &:focus {
    outline: none;
    box-shadow: none;
  }
  &.current {
    color: ${th('colorText')};
    border-bottom: ${th('borderWidth')} ${th('borderStyle')}
      ${th('colorSecondary')};
    &:link,
    &:hover,
    &:focus,
    &:visited {
      color: ${th('colorText')};
      text-decoration: none;
    }
    cursor: default;
  }
`
