import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export default {
  Step: css`
    height: calc(${th('gridUnit')} * 2);
    width: calc(${th('gridUnit')} * 2);
    background-color: ${({ isCurrent, isPast }) =>
      (isPast && th('colorFurniture')) ||
      (isCurrent && th('colorTextReverse')) ||
      th('colorBorder')};
    border-color: ${({ isCurrent, isPast }) =>
      isCurrent || isPast ? th('colorFurniture') : th('colorBorder')};
    border-width: calc(${th('gridUnit')} / 2);
    box-sizing: border-box;
  `,
  Separator: css`
    background-color: ${({ isCurrent, isPast }) =>
      isPast ? th('colorFurniture') : th('colorBorder')};
    height: calc(${th('gridUnit')} / 2);
    border: 0;
  `,
  StepTitle: css`
    font-weight: ${({ isCurrent, isPast }) =>
      isCurrent || isPast ? '600' : 'normal'};
  `,
  Bullet: css`
    display: none;
  `,
  Success: css`
    display: none;
  `,
}
