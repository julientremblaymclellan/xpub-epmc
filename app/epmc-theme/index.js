/* eslint-disable import/extensions */
import 'typeface-open-sans'
import 'typeface-lora'
import {
  Action,
  AppBar,
  Button,
  Checkbox,
  ErrorText,
  Heading,
  Label,
  Link,
  Radio,
  SearchSelect,
  Select,
  Steps,
  TextField,
} from './elements'

export default {
  /* Colors */
  colorBackground: '#FBFBFB',
  colorPrimary: '#20699C',
  colorSecondary: '#E96012',
  colorFurniture: '#E96012',
  colorBorder: '#CCCCCC',
  colorBackgroundHue: '#EEF6FC',
  colorText: '#494949',
  colorTextReverse: '#FFFFFF',
  colorTextPlaceholder: '#9b9b9b',
  colorSuccess: '#5f9e42',
  colorError: '#d63500',
  colorWarning: '#ffc107',

  /* fonts */
  fontInterface: "'Open Sans'",
  fontHeading: "'Open Sans'",
  fontReading: "'Lora'",
  fontWriting: "'Open Sans'",

  // font sizes
  fontSizeBase: '1rem',
  fontSizeBaseSmall: '.875rem',
  fontSizeHeading1: '2rem',
  fontSizeHeading2: '1.5rem',
  fontSizeHeading3: '1.25rem',
  fontSizeHeading4: '1.125rem',
  fontSizeHeading5: '1rem',
  fontSizeHeading6: '.875rem',

  // line heights
  lineHeightBase: '1.5rem',
  lineHeightBaseSmall: '1.5rem',
  lineHeightHeading1: '2.5rem',
  lineHeightHeading2: '2rem',
  lineHeightHeading3: '1.75rem',
  lineHeightHeading4: '1.5rem',
  lineHeightHeading5: '1.5rem',
  lineHeightHeading6: '1.5rem',

  /* Spacing */
  gridUnit: '.5rem',

  /* Border */
  borderRadius: '2px',
  borderWidth: '1px',
  borderStyle: 'solid',
  dropShadow: '0 0 8px #036eb9',

  /* Transition */
  transitionDuration: '250ms',
  transitionTimingFunction: 'easein',
  transitionDelay: '0',

  cssOverrides: {
    ui: {
      Action,
      AppBar,
      Button,
      Checkbox,
      ErrorText,
      Heading,
      Label,
      Link,
      Radio,
      SearchSelect,
      Select,
      Steps,
      TextField,
    },
  },
}
