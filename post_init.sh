#!/bin/bash

echo "Running cron service"
service cron start

echo "Running ftp bulk upload monitor"
./runShellScript.sh "node server/ftp-integration/api" &

echo "Running ftp taggers upload monitor"
./runShellScript.sh "node server/ftp-integration/taggedXML" &

echo "Running Seed"
yarn seed






