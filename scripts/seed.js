#!/usr/bin/env node

const logger = require('@pubsweet/logger')
const Identity = require('../server/xpub-model/entities/identity/data-access')
const User = require('../server/xpub-model/entities/user/data-access')
const Journal = require('../server/xpub-model/entities/journal/data-access')
const config = require('config')
const journals = require('../config/journals.json')

async function seed() {
  try {
    // const journals = config.get('journals')
    await Journal.upsertMulti(journals)

    const users = config.get('users')
    /* eslint-disable no-await-in-loop */
    /* eslint-disable no-restricted-syntax */
    for (const user of users) {
      let i = 0
      for (const identity of user.identities) {
        const exists = await Identity.findByField('email', identity.email)
        // if this email exists, remove the identity
        if (exists.length > 0) {
          user.identities.splice(i, 1)
        } else {
          identity.passwordHash = await Identity.hashPassword(identity.password)
          delete identity.password
        }
        i += 1
      }
      // if there are not null identities
      if (user.identities.find(identity => identity !== null)) {
        await new User(user).save()
      }
    }
  } catch (e) {
    logger.warn('Could not load any seeds', e)
  }

  logger.info('Seeding complete.')
}

;(async () => {
  const beforeUpdate = Date.now()
  await seed()
  // User.knex().destroy()
  logger.info(`Seeding was finished in ${Date.now() - beforeUpdate} ms`)
})()
