-- An audit history is important on most tables. Provide an audit trigger that logs to
-- a dedicated audit table for the major relations.
--
-- This file should be generic and not depend on application roles or structures,
-- as it's being listed here:
--
--    https://wiki.postgresql.org/wiki/Audit_trigger_91plus
--
-- This trigger was originally based on
--   http://wiki.postgresql.org/wiki/Audit_trigger
-- but has been completely rewritten.
--
-- Should really be converted into a relocatable EXTENSION, with control and upgrade files.

CREATE SCHEMA audit;
REVOKE ALL ON SCHEMA audit FROM public;

CREATE EXTENSION IF NOT EXISTS hstore;

COMMENT ON SCHEMA audit IS 'Out-of-table audit/history logging tables and trigger functions';

--
-- Audited data. Lots of information is available, it's just a matter of how much
-- you really want to record. See:
--
--   http://www.postgresql.org/docs/9.1/static/functions-info.html
--
-- Remember, every column you add takes up more audit table space and slows audit
-- inserts.
--
-- Every index you add has a big impact too, so avoid adding indexes to the
-- audit table unless you REALLY need them. The hstore GIST indexes are
-- particularly expensive.
--
-- It is sometimes worth copying the audit table, or a coarse subset of it that
-- you're interested in, into a temporary table where you CREATE any useful
-- indexes and do your analysis.
--

create table audit.audit_log
(
  id            uuid default uuid_generate_v4()                    not null
    constraint audit_log_pkey
      primary key,
  created       timestamp with time zone default CURRENT_TIMESTAMP not null,
  user_id       uuid
    constraint audit_log_user_id_fkey
      references users,
  action        text                                               not null,
  original_data jsonb,
  changes       jsonb,
  object_id     uuid,
  object_type   text,
  manuscript_id text
);

create table audit.manuscript_process_dates
(
  manuscript_id            text                                               not null
    constraint manuscript_process_dates_pkey primary key
    constraint manuscript_process_dates_fkey
      references public.manuscript,
  submitted_date                  timestamp with time zone,
  xml_review_date                  timestamp with time zone,
  first_published_date                  timestamp with time zone,
  ncbi_ready_date                  timestamp with time zone
);

CREATE OR REPLACE FUNCTION audit.populate_manuscript_process_dates(pv_manuscript_id TEXT, new_status TEXT) RETURNS void AS $body$
DECLARE
  c1 CURSOR FOR
    SELECT *
    FROM audit.manuscript_process_dates
    WHERE manuscript_id = pv_manuscript_id;
  curr_process_dates audit.manuscript_process_dates;
  new_process_dates audit.manuscript_process_dates;
BEGIN
  --Does the record already exist?
  OPEN c1;
  FETCH c1 INTO curr_process_dates;
  CLOSE c1;

  IF curr_process_dates.manuscript_id IS NULL THEN
    RAISE NOTICE 'Manuscript is not found in the table';
    new_process_dates.manuscript_id=pv_manuscript_id;
  ELSE
    RAISE NOTICE 'Manuscript is found in the table, submit date is:%', curr_process_dates.submitted_date;
    new_process_dates = curr_process_dates;
  END IF;

  IF new_status='submitted' THEN
    IF new_process_dates.submitted_date IS NULL THEN
      new_process_dates.submitted_date = CURRENT_TIMESTAMP;
    END IF;
  ELSEIF new_status='xml-review' THEN
    IF new_process_dates.xml_review_date IS NULL THEN
      new_process_dates.xml_review_date = CURRENT_TIMESTAMP;
    END IF;
  ELSEIF new_status='published' THEN
    IF new_process_dates.first_published_date IS NULL THEN
      new_process_dates.first_published_date = CURRENT_TIMESTAMP;
    END IF;
  ELSEIF new_status='ncbi-ready' THEN
    IF new_process_dates.ncbi_ready_date IS NULL THEN
      new_process_dates.ncbi_ready_date = CURRENT_TIMESTAMP;
    END IF;
  END IF;

  IF curr_process_dates.manuscript_id IS NULL THEN
    raise notice 'new manuscript id:%', new_process_dates.manuscript_id;
    INSERT INTO audit.manuscript_process_dates VALUES (new_process_dates.*);
  ELSE
    UPDATE audit.manuscript_process_dates
    SET submitted_date=new_process_dates.submitted_date,
        xml_review_date=new_process_dates.xml_review_date,
        first_published_date=new_process_dates.first_published_date,
        ncbi_ready_date=new_process_dates.ncbi_ready_date
    WHERE manuscript_id=pv_manuscript_id;
  END IF;
END;
$body$
  language 'plpgsql';

COMMENT ON FUNCTION audit.populate_manuscript_process_dates(TEXT, TEXT) IS $body$
Populate the relevant date in manuscript_process_dates table with current system timestamp.

Arguments:
   pv_manuscript_id : Manuscript ID (EMS ID) which is being updated
   new_status : New Status which decides which date gets populated.
$body$;

CREATE OR REPLACE FUNCTION audit.if_modified_func() RETURNS TRIGGER AS $body$
DECLARE
  audit_row audit.audit_log;
  include_values boolean;
  log_diffs boolean;
  h_old_data hstore;
  h_changes hstore;
  excluded_cols text[] = ARRAY[]::text[];
BEGIN
  IF TG_WHEN <> 'AFTER' THEN
    RAISE EXCEPTION 'audit.if_modified_func() may only run as an AFTER trigger';
  END IF;

  audit_row = ROW(
    uuid_generate_v4(), -- id
        current_timestamp,                            -- created
    NULL,                                         -- user_id
    substring(TG_OP,1,1),                         -- action
    NULL,                                         -- original_data
    NULL,                                         -- changes
    NULL,                                         -- object_id
    --        TG_RELID,                                     -- object_id
    TG_TABLE_NAME::text,                          -- object_type
    NULL                                          -- manuscript_id
    );

  IF TG_ARGV[1] IS NOT NULL THEN
    excluded_cols = TG_ARGV[1]::text[];
  ELSE
    excluded_cols = ARRAY['created', 'updated']::text[];
  END IF;

  IF (TG_OP = 'UPDATE' AND TG_LEVEL = 'ROW') THEN
    h_old_data = hstore(OLD.*) - excluded_cols;
    audit_row.original_data = to_json(h_old_data);
    h_changes =  (hstore(NEW.*) - h_old_data) - excluded_cols;
    audit_row.user_id = NEW.updated_by;
    IF (TG_TABLE_NAME::text = 'manuscript') THEN
      audit_row.manuscript_id = NEW.id;
      IF NEW.status IN ('submitted', 'xml-review', 'published', 'ncbi-ready') THEN
        perform audit.populate_manuscript_process_dates(NEW.id, NEW.status);
      END IF;
    ELSE
      audit_row.object_id = NEW.id;
      audit_row.manuscript_id = NEW.manuscript_id;
    END IF;
    IF h_changes = hstore('') THEN
      -- All changed fields are ignored. Skip this update.
      RETURN NULL;
    ELSE
      audit_row.changes = hstore_to_jsonb(h_changes);
    END IF;
  ELSIF (TG_OP = 'DELETE' AND TG_LEVEL = 'ROW') THEN
    h_old_data = hstore(OLD.*) - excluded_cols;
    audit_row.user_id = OLD.updated_by;
    IF (TG_TABLE_NAME::text = 'manuscript') THEN
      audit_row.manuscript_id = OLD.id;
    ELSE
      audit_row.object_id = OLD.id;
      audit_row.manuscript_id = OLD.manuscript_id;
    END IF;
    audit_row.original_data = hstore_to_jsonb(h_old_data);
  ELSIF (TG_OP = 'INSERT' AND TG_LEVEL = 'ROW') THEN
    h_changes =  hstore(NEW.*) - excluded_cols;
    audit_row.changes = hstore_to_jsonb(h_changes);
    IF (TG_TABLE_NAME::text = 'manuscript') THEN
      audit_row.manuscript_id = NEW.id;
    ELSE
      audit_row.object_id = NEW.id;
      audit_row.manuscript_id = NEW.manuscript_id;
    END IF;
    audit_row.user_id = NEW.updated_by;
  ELSE
    RAISE EXCEPTION '[audit.if_modified_func] - Trigger func added as trigger for unhandled case: %, %',TG_OP, TG_LEVEL;
    RETURN NULL;
  END IF;
  INSERT INTO audit.audit_log VALUES (audit_row.*);
  RETURN NULL;
END;
$body$
  LANGUAGE plpgsql
  SECURITY DEFINER
  SET search_path = pg_catalog, public;


COMMENT ON FUNCTION audit.if_modified_func() IS $body$
Track changes to a table at the statement and/or row level.

Optional parameters to trigger in CREATE TRIGGER call:

param 0: boolean, whether to log the query text. Default 't'.

param 1: text[], columns to ignore in updates. Default [].

         Updates to ignored cols are omitted from changed_fields.

         Updates with only ignored cols changed are not inserted
         into the audit log.

         Almost all the processing work is still done for updates
         that ignored. If you need to save the load, you need to use
         WHEN clause on the trigger instead.

         No warning or error is issued if ignored_cols contains columns
         that do not exist in the target table. This lets you specify
         a standard set of ignored columns.

There is no parameter to disable logging of values. Add this trigger as
a 'FOR EACH STATEMENT' rather than 'FOR EACH ROW' trigger if you do not
want to log row values.

Note that the user name logged is the login role for the session. The audit trigger
cannot obtain the active role because it is reset by the SECURITY DEFINER invocation
of the audit trigger its self.
$body$;



CREATE OR REPLACE FUNCTION audit.audit_table(target_table regclass) RETURNS void AS $body$
DECLARE
  _q_txt text;
BEGIN
  EXECUTE 'DROP TRIGGER IF EXISTS audit_trigger_row ON ' || target_table;

  EXECUTE 'CREATE TRIGGER audit_trigger_row AFTER INSERT OR UPDATE OR DELETE ON ' ||
          target_table ||
          ' FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();';

END;
$body$
  language 'plpgsql';

COMMENT ON FUNCTION audit.audit_table(regclass) IS $body$
Add auditing support to a table.

Arguments:
   target_table:     Table name, schema qualified if not on search_path
$body$;

CREATE OR REPLACE VIEW audit.tableslist AS
SELECT DISTINCT triggers.trigger_schema AS schema,
                triggers.event_object_table AS auditedtable
FROM information_schema.triggers
WHERE triggers.trigger_name::text IN ('audit_trigger_row'::text, 'audit_trigger_stm'::text)
ORDER BY schema, auditedtable;

COMMENT ON VIEW audit.tableslist IS $body$
View showing all tables with auditing set up. Ordered by schema, then table.
$body$;

do $$
  begin
    perform audit.audit_table(target_table := 'manuscript');
    perform audit.audit_table(target_table := 'file');
    perform audit.audit_table(target_table := 'note');
    perform audit.audit_table(target_table := 'review');
    perform audit.audit_table(target_table := 'team');
  end;
  $$
