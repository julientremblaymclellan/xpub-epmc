<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [xpub-epmc](#xpub-epmc)
  - [Workflow](#workflow)
    - [xpub-collabra](#xpub-collabra)
  - [Installation](#installation)
    - [Quickstart](#quickstart)
    - [Running the app manually (recommended for development)](#running-the-app-manually-recommended-for-development)
  - [Minio](#minio)
  - [Continuous Integration](#continuous-integration)
  - [Community](#community)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# xpub-epmc
xpub-epmc is a manuscript submission system for [Europe PMC](https://europepmc.org/).  
It is currently under development by Europe PMC staff and is being built with tools provided by the [Pubsweet](https://gitlab.coko.foundation/pubsweet/pubsweet) and [INK](https://gitlab.coko.foundation/INK) projects. 

## Workflow

The development underway is intended to support the following workflow

![EPMC Workflow](https://gitlab.coko.foundation/xpub/xpub-epmc/raw/master/static/epmcworkflow.png)

To support this workflow, we must build or extend four main components/functionalities:

* [User Dashboard](https://gitlab.coko.foundation/xpub/xpub-epmc/issues?label_name%5B%5D=Dashboard)
* [Submission Wizard](https://gitlab.coko.foundation/xpub/xpub-epmc/issues?label_name%5B%5D=Submission+Wizard)
* [XML Wizard](https://gitlab.coko.foundation/xpub/xpub-epmc/issues?label_name%5B%5D=XML+Wizard)
* [FTP Integration](https://gitlab.coko.foundation/xpub/xpub-epmc/issues?label_name%5B%5D=FTP+Integration)

<br/>
If you want more detailed (and potentially more technical) information about the current set of tasks that are being worked on, you can go to

* https://gitlab.coko.foundation/xpub/xpub-epmc/milestones/

### xpub-collabra

This project is based on  the xpub-collabra project, currently under development by the [Coko Foundation](https://coko.foundation/). For more information and development progress on this project, please visit

* https://gitlab.coko.foundation/xpub/xpub


## Installation

### Quickstart

**If you're doing development, you'll probably want to follow the next set of instructions instead.**

To quickly get up and running, run

```
yarn start
```

This will run the various docker containers that the app needs.

**Note**:  
`yarn` will be run automatically inside the container to install dependencies. If dependencies were already installed on the host, this may cause problems as the binaries are compiled for the wrong platform. If you encounter errors about "invalid ELF header", try removing your node_modules folder and run `yarn start` again.

If you've previously run the app manually, you may need to check your settings in `config/default.js`. For quickstart, the db port should be:

```
  'pubsweet-server': {
    db: {
      port: 5432,
    },
```

### Running the app manually (recommended for development)

Start with installing the dependencies:
```
yarn install
```

Create the file `local-development.json` inside the `config` folder.

```json
{
    "pubsweet-server": {
        "secret": "<your-secret-here>"
    },
    "pubsweet-component-ink-backend": {
        "inkEndpoint": "< your-ink-api-endpoint >",
        "email": "< your-ink-email >",
        "password": "< your-ink-password >",
        "recipes": {
            "editoria-typescript": "< editoria-typescript-recipe-id >"
        }
    },
    
    "eutils-api-key": "< your-ncbi-eutils-api-key >",

    "users" : [
        {
          "username": "ftpuser1",
          "email": "john@example.com",
          "password": "xxxx"
        },
        {
          "username": "ftpuser2",
          "email": "bloke@example.com",
          "password": "yyyy"
        }
    ]


}
```

This will give your database a secret, as well as enable manuscript docx to HTML conversion via the INK service (ask in the Coko Mattermost [chat channel](https://mattermost.coko.foundation/coko/channels/xpub) if you don't know how to set up an INK account).

It will accompany any NCBI eUtils calls with an API Key ("eutils-api-key") that will allow for privileged handling by the NCBI hosts. This is optional.  

It also will create PubSweet user accounts ("users").

Run the docker container for the database.

```
yarn start:services
```

Now (in a separate terminal) run the app.
```
yarn server
```

## Minio

Please see [README_MINIO](https://gitlab.coko.foundation/xpub/xpub-epmc/blob/dev/README_MINIO.md) for instructions on setting up and running the Minio instance for file storage.

## Continuous Integration

CI requires a Kubernetes cluster, resources for which can be found in [`pubsweet/infra`](https://gitlab.coko.foundation/pubsweet/infra).  
In order to set up a Kubernetes cluster using AWS, you need to follow the instructions there.  

Templates for deploying to this cluster with [`pubsweet/deployer`](https://gitlab.coko.foundation/pubsweet/deployer) are located in [`xpub/deployment-config`](https://gitlab.coko.foundation/xpub/deployment-config).

## Community

Join [the Coko Mattermost channel](https://mattermost.coko.foundation/coko/channels/xpub) for discussion of xpub.
