#!/bin/bash

cmd=$1

echo "runShellScript.sh $cmd"

# Start the first process
ps aux |grep $cmd |grep -q -v grep
PROCESS_1_STATUS=$?
echo "PROCESS_1_STATUS: $PROCESS_1_STATUS"
if [ $PROCESS_1_STATUS -ne 0 ]; then
    echo "Executing: $cmd"
    $cmd
    status=$?
    echo "status: $status"
    if [ $status -ne 0 ]; then
      echo "Failed to start $cmd: $status"
      exit $status
    fi
else
    echo "Process $cmd already running"
    exit 1
fi

# Start the second process
#./my_second_process -D
#status=$?
#if [ $status -ne 0 ]; then
#  echo "Failed to start my_second_process: $status"
#  exit $status
#fi

# Naive check runs checks once a minute to see if either of the processes exited.
# This illustrates part of the heavy lifting you need to do if you want to run
# more than one service in a container. The container exits with an error
# if it detects that either of the processes has exited.
# Otherwise it loops forever, waking up every 60 seconds
while sleep 60; do
  ps aux |grep "$cmd" |grep -q -v grep
  PROCESS_1_STATUS=$?
#  ps aux |grep my_second_process |grep -q -v grep
#  PROCESS_2_STATUS=$?
  # If the greps above find anything, they exit with 0 status
  # If they are not both 0, then something is wrong
  if [ $PROCESS_1_STATUS -ne 0 ]; then
#  if [ $PROCESS_1_STATUS -ne 0 -o $PROCESS_2_STATUS -ne 0 ]; then
    echo "One of the processes has already exited."
    exit 1
  fi
done