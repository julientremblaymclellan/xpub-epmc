#!/bin/bash

echo "Running server in the background"
cmd="npx pubsweet server"

$cmd &

echo "Wait up to 3 minutes for server to respond, check every 20 seconds"
COUNTER=0
while [ $COUNTER -lt 15 ]; do

   RUNNING=$(curl --silent --connect-timeout 20 "http://localhost/" | grep "/assets/")
   if [ -n "$RUNNING" ] ; then
       echo "xPub is running"
       #echo "Creating the Users"
       #node scripts/adduser.js rakeshnambiar rakeshnbr@ebi.ac.uk Password_01 false
       echo "Running post_init.sh script"
       ./post_init.sh
       exit 0
   fi
   echo "Waiting for xPub..."
   sleep 20
   let COUNTER=COUNTER+1
done

echo "ERROR: xPub is not running"

exit 1